package com.daily.collection.web.mvc.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.daily.collection.web.mvc.interceptor.SessionInterceptor;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

  @Autowired 
  SessionInterceptor sessionInterceptor;
  
  

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(sessionInterceptor).addPathPatterns("/*");
  }
}
