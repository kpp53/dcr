package com.daily.collection.web.common;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.daily.collection.web.login.service.ILoginService;

import lombok.extern.log4j.Log4j;

@Component
@Log4j
public class DataUtil {

	@Autowired
	ILoginService loginService;
	
	
	public void createDropDownMap(HttpServletRequest request){
		log.info("Start Creating Drop Down");
		List<String> dashBoardDetails=loginService.getConfigurationDetails("2");
		request.getSession().setAttribute("dashBoardConfig", dashBoardDetails);
		List<String> applicationConfig=loginService.getConfigurationDetails("3");
		request.getSession().setAttribute("applicationConfig", applicationConfig);
		String config=(String) request.getSession().getAttribute("config");
		List<String> session = new ArrayList<String>();
		session.add("2017-18");
		session.add("2016-17");
		session.add("2015-16");
		List<String> FundType=new ArrayList<String>();
		FundType.add("Admission");
		FundType.add("Re-Admission");
		List<String> course = new ArrayList<String>();
		if(config==null || config.equalsIgnoreCase(" ")){
			for (String string : dashBoardDetails) {
				   if(string.equalsIgnoreCase("+2"))
					course.add("+2");
					if(string.equalsIgnoreCase("UG"))
					course.add("UG");
					if(string.equalsIgnoreCase("PG"))
					course.add("PG");
			}	
		}else{
			    if(config.equalsIgnoreCase("+2"))
				course.add("+2");
				if(config.equalsIgnoreCase("UG"))
				course.add("UG");
				if(config.equalsIgnoreCase("PG"))
				course.add("PG");
		}
		
		List<String> stream = new ArrayList<String>();
		for (String string : applicationConfig) {
			    if(string.equalsIgnoreCase("Arts"))
				stream.add("Arts");
				if(string.equalsIgnoreCase("Science"))
				stream.add("Science");
				if(string.equalsIgnoreCase("Commerce"))
				stream.add("Commerce");
		}
		List<String> yearBacth = new ArrayList<String>();
		yearBacth.add("1st");
		yearBacth.add("2nd");
		yearBacth.add("3rd");
		List<String> category = new ArrayList<String>();
		category.add("General");
		category.add("OBC");
		category.add("SC");
		category.add("ST");
		category.add("Girls");
		
		request.getSession().setAttribute("sessionMap", session);
		request.getSession().setAttribute("FundTypeMap", FundType);
		request.getSession().setAttribute("courseMap", course);
		request.getSession().setAttribute("streamMap", stream);
		request.getSession().setAttribute("yearBacthMap", yearBacth);
		request.getSession().setAttribute("categoryMap", category);
		log.info("End Creating Drop Down");
	}
	
	
	 private static final String[] tensNames = {
			    "",
			    " Ten",
			    " Twenty",
			    " Thirty",
			    " Forty",
			    " Fifty",
			    " Sixty",
			    " Seventy",
			    " Eighty",
			    " Ninety"
			  };

			  private static final String[] numNames = {
			    "",
			    " One",
			    " Two",
			    " Three",
			    " Four",
			    " Five",
			    " Six",
			    " Seven",
			    " Eight",
			    " Nine",
			    " Ten",
			    " Eleven",
			    " Twelve",
			    " Thirteen",
			    " Fourteen",
			    " Fifteen",
			    " Sixteen",
			    " Seventeen",
			    " Eighteen",
			    " Nineteen"
			  };


			  private static String convertLessThanOneThousand(int number) {
			    String soFar;

			    if (number % 100 < 20){
			      soFar = numNames[number % 100];
			      number /= 100;
			    }
			    else {
			      soFar = numNames[number % 10];
			      number /= 10;

			      soFar = tensNames[number % 10] + soFar;
			      number /= 10;
			    }
			    if (number == 0) return soFar;
			    return numNames[number] + " Hundred" + soFar;
			  }


			  public static String convert(long number) {
			    // 0 to 999 999 999 999
			    if (number == 0) { return "Zero"; }

			    String snumber = Long.toString(number);

			    // pad with "0"
			    String mask = "000000000000";
			    DecimalFormat df = new DecimalFormat(mask);
			    snumber = df.format(number);

			    // XXXnnnnnnnnn
			    int billions = Integer.parseInt(snumber.substring(0,3));
			    // nnnXXXnnnnnn
			    int millions  = Integer.parseInt(snumber.substring(3,6));
			    // nnnnnnXXXnnn
			    int hundredThousands = Integer.parseInt(snumber.substring(6,9));
			    // nnnnnnnnnXXX
			    int thousands = Integer.parseInt(snumber.substring(9,12));

			    String tradBillions;
			    switch (billions) {
			    case 0:
			      tradBillions = "";
			      break;
			    case 1 :
			      tradBillions = convertLessThanOneThousand(billions)
			      + " Billion ";
			      break;
			    default :
			      tradBillions = convertLessThanOneThousand(billions)
			      + " Billion ";
			    }
			    String result =  tradBillions;

			    String tradMillions;
			    switch (millions) {
			    case 0:
			      tradMillions = "";
			      break;
			    case 1 :
			      tradMillions = convertLessThanOneThousand(millions)
			         + " Million ";
			      break;
			    default :
			      tradMillions = convertLessThanOneThousand(millions)
			         + " Million ";
			    }
			    result =  result + tradMillions;

			    String tradHundredThousands;
			    switch (hundredThousands) {
			    case 0:
			      tradHundredThousands = "";
			      break;
			    case 1 :
			      tradHundredThousands = "One Thousand ";
			      break;
			    default :
			      tradHundredThousands = convertLessThanOneThousand(hundredThousands)
			         + " Thousand ";
			    }
			    result =  result + tradHundredThousands;

			    String tradThousand;
			    tradThousand = convertLessThanOneThousand(thousands);
			    result =  result + tradThousand;

			    // remove extra spaces!
			    return result.replaceAll("^\\s+", "")
			    		.replaceAll("\\b\\s{2,}\\b", " ");
			  }
	
	
	
	
}
