package com.daily.collection.web.tresaury.service;

import javax.servlet.http.HttpServletRequest;

import com.daily.collection.web.tresaury.bean.CollectionBean;

public interface ITreasuryService {
	
	public CollectionBean getCollectionList(CollectionBean collectionBean,HttpServletRequest request);

}
