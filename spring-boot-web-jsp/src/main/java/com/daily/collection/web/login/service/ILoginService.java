package com.daily.collection.web.login.service;

import java.util.List;

public interface ILoginService {
	
	public List<String> getConfigurationDetails(String config_id);
	
	public String getCurrentSession(String id);

}
