package com.daily.collection.web.transaction.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.daily.collection.web.analytic.bean.HeadDeatils;
import com.daily.collection.web.tresaury.bean.CollectionList;

@Repository
public class TransactionDaoImpl implements ITransactionDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	private final static Logger logger = Logger.getLogger(TransactionDaoImpl.class);
    
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public String countById(String course, String stream, String session) {
		logger.info("Inside Student countById Method In DAO Layer");
		try {
			logger.info("Start Getting get Student Count:: Course ::"+course+" Stream ::"+stream+" Session ::"+session);
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_STUDENT_COUNT,
					new Object[] { course, stream, session }, String.class);
			logger.info("End Getting get Student Count:: "+i);
			return i;
		} catch (Exception e) {
			logger.error("Exception While Getting Student Count By id",e);
			return "0";
		}

	}

	@Override
	public String saveStudent(Object[] studentParams) {
		logger.info("Inside saveStudent Method In DAO Layer");
		try {
			logger.info("Start Saving Student");
			int i = jdbcTemplate.update(ITransactionDao.SAVE_STUDENT, studentParams);
			logger.info("End Saving Student");
			logger.info("Response ::"+i);
			if (i == 1) {
				return "1";
			} else {
				return "0";
			}
		} catch (Exception e) {
			logger.error("Error While Saving the Student ::",e);
			return "0";
		}
	}

	@Override
	public List<Map<String, Object>> getStudent(MapSqlParameterSource parameters) {
		logger.info("Inside getStudentMethod In DAO Layer");
		try {
			logger.info("Start Getting Student From DB");
			List<Map<String, Object>> listStudent = namedParameterJdbcTemplate.queryForList(ITransactionDao.GET_STUDENT_DETAILS,
					parameters);
			logger.info("End Getting Student From DB :: Size ::"+listStudent.size());
			return listStudent;
		} catch (Exception e) {
			logger.error("Exception WHile Getting Student Details",e);
			return null;
		}
	}
	
	@Override
	public List<Map<String, Object>> getStudentAdditional(MapSqlParameterSource parameters) {
		logger.info("Inside getStudentAdditional In DAO Layer");
		try {
			logger.info("Start Getting Additional/Fine Student From DB");
			List<Map<String, Object>> listStudent = namedParameterJdbcTemplate.queryForList(ITransactionDao.GET_STUDENT_DETAILS_ADDITIONAL,
					parameters);
			logger.info("End Getting Additional/Fine Student From DB :: Size ::"+listStudent.size());
			return listStudent;
		} catch (Exception e) {
			logger.error("Exception WHile Getting Additional/Fine",e);
			return null;
		}
	}
	
	@Override
	public List<Map<String, Object>> getStudentAdditionalRefund(MapSqlParameterSource parameters) {
		logger.info("Inside getStudentAdditionalRefund In DAO Layer");
		try {
			logger.info("Start Getting Refund Student From DB");
			List<Map<String, Object>> listStudent = namedParameterJdbcTemplate.queryForList(ITransactionDao.GET_STUDENT_DETAILS_REFUND,
					parameters);
			logger.info("End Getting Refund Student From DB :: Size ::"+listStudent.size());
			return listStudent;
		} catch (Exception e) {
			logger.error("Exception WHile Getting Refund",e);
			return null;
		}
	}

	@Override
	public Long getFinalAmount(String session, String courseName, String streamName, String category,
			String yearbatch) {
		logger.info("Inside getFinalAmount Method In Dao Layer");
		try {
			logger.info("Start getting Final Amount :: CourseName::"+courseName+" StreamName:: "+streamName+" Category ::"+category+" YearBatch::"+yearbatch+" Session::"+session);
			Long finalAmount = jdbcTemplate.queryForObject(ITransactionDao.GET_AMOUNT, Long.class,
					new Object[] { courseName, streamName, category, yearbatch, session });
			logger.info("End getting Final Amount ::"+finalAmount);
			return finalAmount;
		} catch (Exception e) {
			logger.error("Exception While Final Amount",e);
			return null;
		}
	}
	
	@Override
	public Long getFinalAmountUG(String session, String courseName, String streamName, String category,
			String yearbatch) {
		logger.info("Inside getFinalAmountUG In DAO Layer");
		try {
			logger.info("Start Getting Final Amount For UG :: Course ::"+courseName+ " Session ::"+session+" Stream ::"+streamName+" Category ::"+category+" YearBatch ::"+yearbatch);
			Long finalAmount = jdbcTemplate.queryForObject(ITransactionDao.GET_AMOUNT_UG, Long.class,
					new Object[] { courseName, streamName, category, yearbatch, session });
			logger.info("End Getting Final Amount For UG Final Amount::"+finalAmount);
			return finalAmount;
		} catch (Exception e) {
			logger.info("Exception While Getting FinalAmountUG",e);
			return null;
		}
	}

	@Override
	public Integer gettransactionCount(String session) {
		logger.info("Inside gettransactionCount In DAO Layer");
		try {
			logger.info("Start Getting Tranaction Count In DAO Layer:: Session::"+session);
			Integer count = jdbcTemplate.queryForObject(ITransactionDao.GET_TRANSACTION_COUNT, new Object[] { session },
					Integer.class);
			logger.info("End Getting Tranaction Count In DAO Layer:: Count ::"+count);
			return count;
		} catch (Exception e) {
			logger.error("Exception WHile Getting Transaction Count",e);
			return null;
		}
	}

	@Override
	public String saveTransaction(Object[] transactionParam) {
		logger.info("Inside saveTransaction In DAO Layer");
		try {
			logger.info("Start Saving Transaction In DAO Layer");
			int i = jdbcTemplate.update(ITransactionDao.SAVE_TRANSACTION, transactionParam);
			logger.info("End Saving Transaction In DAO Layer :: Count ::"+i);
			if (i == 1) {
				logger.info("Sending The Response SUCCESS");
				return "SUCCESS";
			} else {
				logger.error("Sending The Response ERROR");
				return "ERROR";
			}
		} catch (Exception e) {
			logger.error("Exception While Saving Transaction",e);
			logger.error("Sending The Response ERROR");
			return "ERROR";
		}
	}

	@Override
	public String getCollegeName(String id) {
		logger.info("Inside getCollegeName In DAO Layer");
		try {
			logger.info("Start Getting College Name :: ID::"+id);
			String collegeName = jdbcTemplate.queryForObject(ITransactionDao.GET_COLLEGE_NAME, new Object[] { id },
					String.class);
			logger.info("End Getting College Name :: CollegeName::"+collegeName);
			return collegeName;
		} catch (Exception e) {
			logger.error("Exception While Getting College Name",e);
			return null;
		}
	}

	@Override
	public Integer getTransactionTypeCount(String name, String course) {
		logger.info("Inside getTransactionTypeCount In DAO Layer");
		try {
			logger.info("Start Getting Transaction Type Count :: Name ::"+name+" Course ::"+course);
			Integer count = jdbcTemplate.queryForObject(ITransactionDao.GET_COUNT_TRANSACTION_TYPE,
					new Object[] { name, course }, Integer.class);
			logger.info("End Getting Transaction Type Count ::"+count);
			return count;
		} catch (Exception e) {
			logger.error("Exception While Getting Transaction Type Count",e);
			return null;
		}
	}

	@Override
	public String updateTransaction(String status, String rollNo,String scienceType) {
		logger.info("Inside updateTransaction In DAO Layer");
		try {
			logger.info("Start Getting Update Transaction Status :: Status ::"+status+" RollNo ::"+rollNo+" ScienceType ::"+scienceType);
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_TRANSACTION_STATUS, new Object[] { status, rollNo,scienceType });
			logger.info("End Getting Update Transaction Status :: Count ::"+i);
			if (i == 1) {
				logger.info("Sending Status As Success");
				return "SUCCESS";
			} else {
				logger.error("Sending Status As Error");
				return "ERROR";
			}
		} catch (Exception e) {
			logger.error("Exception While Saving  UpdateTransaction",e);
			logger.error("Sending Status As Error");
			return "ERROR";
		}
	}

	@Override
	public String updateVerifyStatus(String verifyStatus, String userName, String rollNo,String scienceType) {
		logger.info("Inside updateVerifyStatus In DAO Layer");
		try {
			logger.info("Start Updating Verify Status :: Verify Status ::"+verifyStatus+" UserName ::"+userName+" RollNo ::"+rollNo+ " ScienceType"+scienceType);
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_VERIFY_STATUS,
					new Object[] { verifyStatus, userName, rollNo,scienceType });
			logger.info("Start Updating Verify Status ::"+i);
			if (i == 1) {
				logger.info("Sending Status As Success");
				return "SUCCESS";
			} else {
				logger.error("Sending Status As Error");
				return "ERROR";
			}
		} catch (Exception e) {
			logger.error("Exception While Updating VerifyStatus",e);
			logger.error("Sending Status As Error");
			return "ERROR";
		}
	}

	@Override
	public String checkMrinExist(String collegeCode, String mrinNo, String currentSession) {
		logger.info("Inside checkMrinExist In DAO Layer");
		try {
			logger.info("Start Checking MRIN NO Exist:: CollegeCode ::"+collegeCode+ " MRIN NO "+mrinNo+" currentSesion"+currentSession);
			Integer count = jdbcTemplate.queryForObject(ITransactionDao.CHECK_MRIN_NO,
					new Object[] { collegeCode, mrinNo, currentSession }, Integer.class);
			logger.info("End Checking Getting MRIN NO Count::"+count);
			if (count >= 1) {
				logger.error("Sending Status As ERROR");
				return "ERROR";
			} else {
				logger.info("Sending Status As SUCCESS");
				return "SUCCESS";
			}
		} catch (Exception e) {
			logger.error("Exception While CheckMrIn Exist",e);
			logger.error("Sending Status As ERROR");
			return "ERROR";
		}
	}

	@Override
	public String getCategoriesId(Object[] studentParams) {
		logger.info("Inside getCategoriesId In DAO Layer");
		try {
			logger.info("Start Getting CategoriesId From DB");
			String count = jdbcTemplate.queryForObject(ITransactionDao.GET_CATEGORIES_ID, studentParams, String.class);
			logger.info("End Getting CategoriesId From DB ::CategoriesId "+count);
			if (count != null && !count.equalsIgnoreCase("")) {
				logger.info("Sending Categories ID");
				return count;
			}
			logger.error("Sending null");
			return null;
		} catch (Exception e) {
			logger.error("Exception While getCategoriesId",e);
			logger.error("Sending Error");
			return "ERROR";
		}
	}

	@Override
	public List<Map<String, Object>> getHeadNameById(String catogoriesId, String session) {
		logger.info("Inside getHeadNameById In DAO Layer");
		try {
			logger.info("Start Getting HeadNameId :: CategoriesId"+ "Session "+session);
			List<Map<String, Object>> headNameList = jdbcTemplate.queryForList(ITransactionDao.GET_HEAD_NAME_BY_ID,
					new Object[] { catogoriesId, session });
			logger.info("Start Getting HeadNameId :: Size ::"+headNameList.size());
			if (headNameList!=null && headNameList.isEmpty()) {
				logger.error("HeadName List Is NULL/Empty :: Sending Error");
				return null;
			} else {
				logger.info("Sending HeadName List");
				return headNameList;
			}
		} catch (Exception e) {
			logger.error("Exception While Getting HeadName",e);
			logger.error("Sending Error");
			return null;
		}
	}

	/*@Override
	public String saveTransactionDetailsByHead(Object[] params) {
		logger.info("Inside saveTransactionDetailsByHead In DAO Layer");
		try {
			logger.info("Start Saving Transaction Details By Head");
			int i = jdbcTemplate.update(ITransactionDao.SAVE_TRANSACTION_DETAILS_BY_HEAD, params);
			logger.info("End Saving Transaction Details By Head :: Count ::"+i);
			if (i == 1) {
				logger.info("Sending Success Response");
				return "SUCCESS";
			} else {
				logger.error("Sending Error Response");
				return "ERROR";
			}
		} catch (Exception e) {
			logger.error("Exception While Save Transaction Details By Head",e);
			logger.error("Sending Error Response");
			return "ERROR";
		}
	}*/

	@Override
	public String getAvailbleAmount(String id) {
		logger.info("Inside getFinalAmount Method In Service Layer");
		try {
			String value = jdbcTemplate.queryForObject(ITransactionDao.GET_AVAILABLE_AMOUNT, new Object[] { id },
					String.class);
			logger.info("Available Amount From Database ::"+value);
			return value;
		} catch (Exception e) {
			logger.error("Error While Getting Available Amount");
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public String updateAvailableAmount(String amount, String configId) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_AVAILABLE_AMOUNT, new Object[] { amount, configId });
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}

	@Override
	public List<Map<String, Object>> getRecepitNumber(String tranType, String rollNo, String session,String scienceType) {
		try {
			List<Map<String, Object>> receiptNo = jdbcTemplate.queryForList(ITransactionDao.GET_RECEIPT_NO,
					new Object[] { tranType, rollNo, session,scienceType });
			return receiptNo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*@Override
	public List<Map<String, Object>> getHeadDetailsForprint(Object[] params) {
		try {
			List<Map<String, Object>> headDetails = jdbcTemplate
					.queryForList(ITransactionDao.GET_HEAD_DETAILS_FOR_PRINT, params);
			return headDetails;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}*/

	@Override
	public List<HeadDeatils> getHeadDetailsForprintUG(Object[] params) {
		try {
			List<HeadDeatils> headDetails = jdbcTemplate.query(ITransactionDao.GET_HEAD_DETAILS_FOR_PRINT_UG, params,(rs, rowNum) -> {
				HeadDeatils headList = new HeadDeatils();
				headList.setHeadname(rs.getString("HEADNAME"));
				headList.setHeadAmount(rs.getInt("AMOUNT"));
			    return headList;
			});
			return headDetails;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Override
	public List<HeadDeatils> getHeadDetailsForprint(Object[] params) {
		try {
			List<HeadDeatils> headDetails = jdbcTemplate.query(ITransactionDao.GET_HEAD_DETAILS_FOR_PRINT, params,(rs, rowNum) -> {
				HeadDeatils headList = new HeadDeatils();
				headList.setHeadname(rs.getString("HEADNAME"));
				headList.setHeadAmount(rs.getInt("AMOUNT"));
			    return headList;
			});
			return headDetails;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Date getTransactionDate(String tranType, String rollNo, String session,String scienceType) {
		try {
			Date transactionDate = jdbcTemplate.queryForObject(ITransactionDao.GET_TRANSACTION_DATE,
					new Object[] { tranType, rollNo, session,scienceType }, Date.class);
			return transactionDate;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String insertStudentTrack(String rollNo, String status) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.INSERT_STUDENT_STATUS, new Object[] { rollNo, status });
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}

	@Override
	public List<Map<String, Object>> getHeadNameByIdUG(String catogoriesId, String session) {
		try {
			List<Map<String, Object>> headNameList = jdbcTemplate.queryForList(ITransactionDao.GET_HEAD_NAME_BY_ID_UG,
					new Object[] { catogoriesId, session });
			if (headNameList.isEmpty()) {
				return null;
			} else {
				return headNameList;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*@Override
	public String saveTransactionDetailsByHeadUG(Object[] params) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.SAVE_TRANSACTION_DETAILS_BY_HEAD_UG, params);
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}*/

	@Override
	public String getReceiptNBook(String configId) {
		try {
			String count = jdbcTemplate.queryForObject(ITransactionDao.GET_RECEIPT_BOOK_ID,
					new Object[] { configId},String.class);
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String updateBookId(String bookId, String id) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_BOOK_ID, new Object[] { bookId, id });
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}

	@Override
	public String getTransactionByCourse(String dashBoardConfig) {
		try {
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_TRANSACTION_BY_COURSE,
					new Object[] { dashBoardConfig }, String.class);
			return i;
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}

	@Override
	public Integer getReceiptStarting(String id) {
		try {
			Integer i = jdbcTemplate.queryForObject(ITransactionDao.GET_RECEIPT_STARTING, new Object[] { id },
					Integer.class);
			return i;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public List<Map<String, Object>> getHeadNameAndAmountUG(Object[] params) {
		try {
			List<Map<String, Object>> hadNameMap= jdbcTemplate.queryForList(ITransactionDao.GET_HEAD_NAME_AMONT_UG, params);
			return hadNameMap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String updateTransactionAD(String transactioStatus, String rollNo, String scienceType) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_STUDENT_STATUS_AD, new Object[] { transactioStatus, rollNo,scienceType });
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}
	
	@Override
	public String updateTransactionRF(String transactioStatus, String rollNo, String scienceType) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_STUDENT_STATUS_RF, new Object[] { transactioStatus, rollNo,scienceType });
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}
	
	

	@Override
	public String updateVerifyStatusAD(String verifyStatus, String userName, String rollNo, String scienceType) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_VERIFY_STATUS_AD,
					new Object[] { verifyStatus, userName, rollNo,scienceType });
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}
	
	@Override
	public String updateVerifyStatusRF(String verifyStatus, String userName, String rollNo, String scienceType) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_VERIFY_STATUS_RF,
					new Object[] { verifyStatus, userName, rollNo,scienceType });
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}

	@Override
	public String updateStudent(Object[] params) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_STUDENT,
					params);
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}

	/*@Override
	public String getStudentAmountUG(Object[] params) {
		try {
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_SUM_AMOUNT_STUDENT_UG, params,
					String.class);
			return i;
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}*/

	@Override
	public String updateTransaction(Object[] params) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_TRANSACTION,
					params);
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}

	@Override
	public String getTransactionId(String rollNo,String scienceType) {
		try {
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_TRANSACTION_ID,
					new Object[] {rollNo,scienceType},String.class);
            return i;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	
	@Override
	public Integer getTransactionIdOldStudent() {
		try {
			Integer i = jdbcTemplate.queryForObject(ITransactionDao.GET_TRANSACTION_ID_OLD_STUDENT,
					new Object[] {},Integer.class);
            return i;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	

	/*@Override
	public String updateTransactionDetailsUG(Object[] params) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_TRANSACTION_DETAILS_BY_HEAD_UG,
					params);
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}	}*/

	@Override
	public String updateStudentTrack(String rollNo, String status) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_STUDENT_STATUS, new Object[] { rollNo, status });
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}

	@Override
	public String updateStudentRefStatus(String status,String rollNo, String scienceType) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_STUDENT_REF_STATUS, new Object[] {status,rollNo,scienceType});
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}

	@Override
	public String updateTransactionTrack(Object[] params) {
		try {
			int i = jdbcTemplate.update(ITransactionDao.SAVE_REFUND_TRANSACTION_STATUS,params);
			if (i == 1) {
				return "SUCCESS";
			} else {
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}

	@Override
	public String getheadCategoryId(String rollNo, String scienceType) {
		try {
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_HEAD_CATEGORIES_ID,
					new Object[] {rollNo,scienceType},String.class);
            return i;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public String getBankName(String rollNo, String scienceType,String tranType) {
		try {
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_BANK_NAME,
					new Object[] {rollNo,scienceType,tranType},String.class);
            return i;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public String getChequeNumber(String rollNo, String scienceType,String tranType) {
		try {
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_CHEQUE_NUMBER,
					new Object[] {rollNo,scienceType,tranType},String.class);
            return i;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public Integer getRefundTransactionTypeCount(String name, String session) {
		try {
			Integer count = jdbcTemplate.queryForObject(ITransactionDao.GET_COUNT_RF_TRANSACTION_TYPE,
					new Object[] { name, session }, Integer.class);
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Long getTotalAmount(String rollNo, String scienceType, String tranType) {
		try {
			Long count = jdbcTemplate.queryForObject(ITransactionDao.GET_REFUND_AMOUNT,
					new Object[] { rollNo, scienceType,tranType }, Long.class);
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getRecepitNumberRF(String tranType, String rollNo, String session, String scienceType) {
		try {
			String receiptNo = jdbcTemplate.queryForObject(ITransactionDao.GET_RECEIPT_NO_RF,
					new Object[] { tranType, rollNo, session,scienceType },String.class);
			return receiptNo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Date getTransactionDateRF(String tranType, String rollNo, String session, String scienceType) {
		try {
			Date transactionDate = jdbcTemplate.queryForObject(ITransactionDao.GET_TRANSACTION_DATE_RF,
					new Object[] { tranType, rollNo, session,scienceType }, Date.class);
			return transactionDate;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Integer isChequeNoExist(String bankName, String chequeNo) {
		try {
			Integer receiptNo = jdbcTemplate.queryForObject(ITransactionDao.CHECK_CHEQUE_NO,
					new Object[] { bankName, chequeNo,},Integer.class);
			return receiptNo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Integer isReceiptNoExist(String receiptNo, String bookNo) {
		try {
			Integer count = jdbcTemplate.queryForObject(ITransactionDao.GET_RECEIPT_NO_COUNT,
					new Object[] { receiptNo, bookNo},Integer.class);
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String updateReceiptNumberInConfiguration(String receiptNumber, String configId) { 
		try {
			int count = jdbcTemplate.update(ITransactionDao.UPDATE_RECEIPT_NUMBER,
					new Object[] { receiptNumber, configId});
			if(count==1){
				return "SUCCESS";
			}else{
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String saveTransactionAd(Object[] transactionParam) {
		logger.info("Inside saveTransactionAd In DAO Layer");
		try {
			logger.info("Start Saving Transaction For Additional Fee/Fine In DAO Layer");
			int i = jdbcTemplate.update(ITransactionDao.SAVE_TRANSACTION_AD, transactionParam);
			logger.info("Start Saving Transaction For Additional Fee/Fine In DAO Layer :: Count ::"+i);
			if (i == 1) {
				logger.info("Sending The Response SUCCESS");
				return "SUCCESS";
			} else {
				logger.error("Sending The Response ERROR");
				return "ERROR";
			}
		} catch (Exception e) {
			logger.error("Exception While Saving Transaction For Additional Fee/Fine In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "ERROR";
		}
	}

	/*@Override
	public String updateTransactionDetailsCLCUG(String amount, String tranId) {
		logger.info("Inside updateTransactionDetailsCLCUG In DAO Layer");
		try {
			logger.info("Start updating Transaction Details UG For CLC In DAO Layer");
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_TRANSACTION_DETAILS_UG_CLC, new Object[]{amount,tranId});
			logger.info("Start updating Transaction Details UG For CLC In DAO Layer:: Count ::"+i);
			if (i == 1) {
				logger.info("Sending The Response SUCCESS");
				return "SUCCESS";
			} else {
				logger.error("Sending The Response ERROR");
				return "ERROR";
			}
		} catch (Exception e) {
			logger.error("Exception While Updating Transaction For Transaction Details UG For CLC In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "ERROR";
		}
	}*/

	/*@Override
	public String updateTransactionDetailsAddUG(String amount, String tranId) {
		logger.info("Inside updateTransactionDetailsAddUG In DAO Layer");
		try {
			logger.info("Start Updating Transaction For Transaction Details UG For Additional Fee In DAO Layer");
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_TRANSACTION_DETAILS_UG_ADD, new Object[]{amount,tranId});
			logger.info("Start Updating Transaction For Transaction Details UG For Additional Fee In DAO Layer :: Count ::"+i);
			if (i == 1) {
				logger.info("Sending The Response SUCCESS");
				return "SUCCESS";
			} else {
				logger.error("Sending The Response ERROR");
				return "ERROR";
			}
		} catch (Exception e) {
			logger.error("Exception While Updating Transaction For Transaction Details UG For Additional Fee In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "ERROR";
		}
	}*/

	/*@Override
	public String updateTransactionDetailsCLC(String amount, String tranId) {
		logger.info("Inside updateTransactionDetailsCLC In DAO Layer");
		try {
			logger.info("Start Updating Transaction Details For CLC In DAO Layer");
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_TRANSACTION_DETAILS_CLC, new Object[]{amount,tranId});
			logger.info("Start Updating Transaction Details For CLC In DAO Layer :: Count ::"+i);
			if (i == 1) {
				logger.info("Sending The Response SUCCESS");
				return "SUCCESS";
			} else {
				logger.error("Sending The Response ERROR");
				return "ERROR";
			}
		} catch (Exception e) {
			logger.error("Exception While Updating Transaction Details For CLC In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "ERROR";
		}
	}*/

	/*@Override
	public String updateTransactionDetailsAd(String amount, String tranId) {
		logger.info("Inside updateTransactionDetailsAd In DAO Layer");
		try {
			logger.info("Start Updating Transaction Details For Additional Fee In DAO Layer");
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_TRANSACTION_DETAILS_ADD, new Object[]{amount,tranId});
			logger.info("Start Updating Transaction Details For Additional Fee In DAO Layer :: Count ::"+i);
			if (i == 1) {
				logger.info("Sending The Response SUCCESS");
				return "SUCCESS";
			} else {
				logger.error("Sending The Response ERROR");
				return "ERROR";
			}
		} catch (Exception e) {
			logger.error("Exception While Updating Transaction Details For Additional Fee In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "ERROR";
		}
	}*/

	@Override
	public String getRecepitNumberAD(String tranType, String rollNo, String session, String scienceType) {
		try {
			String receiptNo = jdbcTemplate.queryForObject(ITransactionDao.GET_RECEIPT_NO_AD,
					new Object[] { tranType, rollNo, session,scienceType },String.class);
			return receiptNo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public String getBookAD(String tranType, String rollNo, String session, String scienceType) {
		try {
			String receiptNo = jdbcTemplate.queryForObject(ITransactionDao.GET_BOOK_NO_AD,
					new Object[] { tranType, rollNo, session,scienceType },String.class);
			return receiptNo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Date getTransactionDateAD(String tranType, String rollNo, String session, String scienceType) {
		try {
			Date transactionDate = jdbcTemplate.queryForObject(ITransactionDao.GET_TRANSACTION_DATE_AD,
					new Object[] { tranType, rollNo, session,scienceType }, Date.class);
			return transactionDate;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Long getTotalAmountAD(String rollNo,String tranType,String scienceType) {
		try {
			Long count = jdbcTemplate.queryForObject(ITransactionDao.GET_ADD_AMOUNT,
					new Object[] { rollNo, scienceType,tranType }, Long.class);
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getHeadName(String rollNo, String scienceType, String tranType) {
		try {
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_HEAD_NAME,
					new Object[] {rollNo,scienceType,tranType},String.class);
            return i;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public String updateCollegeNMRIN(String collegeCode, String mrinNo, String academicSession, String rollNo,
			String scienceType) { 
		try {
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_STUDENT_COLLEGE_CODE,
					new Object[] {collegeCode,mrinNo,rollNo,scienceType,academicSession});
			if(i==1){
				return "SUCCESS";
			}else{
				return "ERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	/*@Override
	public String updateTransactionDetailsCLCBUExamFee(String amount, String tranId) {
		logger.info("Inside updateTransactionDetailsCLCBUExamFee In DAO Layer");
		try {
			logger.info("Start updating Transaction Details UG For BU Exam Fee In DAO Layer");
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_TRANSACTION_DETAILS_UG_BU_EXAM, new Object[]{amount,tranId});
			logger.info("Start updating Transaction Details UG For BU Exam Fee In DAO Layer:: Count ::"+i);
			if (i == 1) {
				logger.info("Sending The Response SUCCESS");
				return "SUCCESS";
			} else {
				logger.error("Sending The Response ERROR");
				return "ERROR";
			}
		} catch (Exception e) {
			logger.error("Exception While Updating Transaction For Transaction Details UG For BU Exam Fee In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "ERROR";
		}
	}*/

	/*@Override
	public String getCLCAmountUG(String rollNo, String scienceType) {
		logger.info("Inside getCLCAmountUG In DAO Layer");
		try {
			logger.info("Start Getting CLC Amount For UG");
			String clcAmount = jdbcTemplate.queryForObject(ITransactionDao.GET_CLC_AMOUNT_UG, new Object[]{rollNo,scienceType},String.class);
			logger.info("End Getting CLC Amount For UG:: CLC AMount ::"+clcAmount);
			return clcAmount;
		} catch (Exception e) {
			logger.error("Exception While Getting CLC AMount From DB For UG",e);
			logger.error("Sending The Response ERROR");
			return "ERROR";
		}
	}*/

	@Override
	public List<Map<String, Object>> getHeadNameAndAmount(Object[] params) {
		try {
			List<Map<String, Object>> hadNameMap= jdbcTemplate.queryForList(ITransactionDao.GET_HEAD_NAME_AMONT, params);
			return hadNameMap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*@Override
	public String getStudentAmount(Object[] params) {
		try {
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_SUM_AMOUNT_STUDENT, params,
					String.class);
			return i;
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}*/

	/*@Override
	public String getCLCAmount(String rollNo, String scienceType) {
		logger.info("Inside getCLCAmount In DAO Layer");
		try {
			logger.info("Start Getting CLC Amount For +2");
			String clcAmount = jdbcTemplate.queryForObject(ITransactionDao.GET_CLC_AMOUNT, new Object[]{rollNo,scienceType},String.class);
			logger.info("End Getting CLC Amount For:: CLC AMount ::"+clcAmount);
			return clcAmount;
		} catch (Exception e) {
			logger.error("Exception While getting CLC Amount From DB FOR +2",e);
			logger.error("Sending The Response ERROR");
			return "ERROR";
		}
	}*/

	@Override
	public List<Map<String,Object>> getPrintDetails(String rollNo, String tranType) {
		logger.info("Inside getBookAD In DAO Layer");
		try {
			logger.info("Start Getting PrintDetails For");
			List<Map<String,Object>> printDetails= jdbcTemplate.queryForList(ITransactionDao.GET_ADD_FINE_DETAILS_FOR_PRINT, new Object[]{rollNo,tranType});
			logger.info("End Getting Print Details ::"+printDetails.size());
			return printDetails;
		} catch (Exception e) {
			logger.error("Exception While getting CLC Amount From DB FOR +2",e);
			logger.error("Sending The Response ERROR");
			return null;
		}
	}

	/*@Override
	public String updateTransactionDetailsFineUG(String amount, String tranId) {
		logger.info("Inside updateTransactionDetailsFineUG In DAO Layer");
		try {
			logger.info("Start updating Transaction Details UG For Fine Fee In DAO Layer");
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_TRANSACTION_DETAILS_UG_FINE, new Object[]{amount,tranId});
			logger.info("Start updating Transaction Details UG For Fine Fee In DAO Layer:: Count ::"+i);
			if (i == 1) {
				logger.info("Sending The Response SUCCESS");
				return "SUCCESS";
			} else {
				logger.error("Sending The Response ERROR");
				return "ERROR";
			}
		} catch (Exception e) {
			logger.error("Exception While Updating Transaction For Transaction Details UG For Fine Fee In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "ERROR";
		}
	}*/

	/*@Override
	public String getTransactionDetailsFineUG(String tranId) {
		logger.info("Inside getTransactionDetailsFineUG In DAO Layer");
		try {
			logger.info("Start updating Transaction Details UG For Fine Fee In DAO Layer");
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_TRANSACTION_DETAILS_UG_FINE, new Object[]{tranId},String.class);
			logger.info("Start updating Transaction Details UG For BU Exam Fee In DAO Layer:: Count ::"+i);
			return i;
		} catch (Exception e) {
			logger.error("Exception While Updating Transaction For Transaction Details UG For Fine Fee In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "0";
		}
	}*/

	/*@Override
	public String getTransactionDetailsAddUG(String tranId) {
		logger.info("Inside getTransactionDetailsAddUG In DAO Layer");
		try {
			logger.info("Start Getting Transaction Details UG For Additional Fee In DAO Layer");
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_TRANSACTION_DETAILS_UG_ADD, new Object[]{tranId},String.class);
			logger.info("Start updating Transaction Details UG For Additional Fee In DAO Layer:: Count ::"+i);
			return i;
		} catch (Exception e) {
			logger.error("Exception While Getting Transaction For Transaction Details UG For Additional Fee In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "0";
		}
	}*/

	/*@Override
	public String getTransactionDetailsFine(String tranId) {
		logger.info("Inside getTransactionDetailsFine In DAO Layer");
		try {
			logger.info("Start updating Transaction Details +2 For Fine Fee In DAO Layer");
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_TRANSACTION_DETAILS_FINE, new Object[]{tranId},String.class);
			logger.info("Start updating Transaction Details +2 For Fine Fee In DAO Layer:: Count ::"+i);
			return i;
		} catch (Exception e) {
			logger.error("Exception While Updating Transaction For Transaction Details +2 For Fine Fee In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "0";
		}
	}*/
	
	
	/*@Override
	public String getTransactionDetailsAdd(String tranId) {
		logger.info("Inside getTransactionDetailsAddUG In DAO Layer");
		try {
			logger.info("Start Getting Transaction Details UG For Additional Fee In DAO Layer");
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_TRANSACTION_DETAILS_ADD, new Object[]{tranId},String.class);
			logger.info("Start updating Transaction Details UG For Additional Fee In DAO Layer:: Count ::"+i);
			return i;
		} catch (Exception e) {
			logger.error("Exception While Getting Transaction For Transaction Details UG For Additional Fee In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "0";
		}
	}*/
	
	/*@Override
	public String getTransactionDetailsUGBUExamFee(String tranId) {
		logger.info("Inside getTransactionDetailsUGBUExamFee In DAO Layer");
		try {
			logger.info("Start Getting Transaction Details UG For BU Examination Fee In DAO Layer");
			String i = jdbcTemplate.queryForObject(ITransactionDao.GET_TRANSACTION_DETAILS_BU, new Object[]{tranId},String.class);
			logger.info("Start updating Transaction Details UG For Bu Examination Fee In DAO Layer:: Count ::"+i);
			return i;
		} catch (Exception e) {
			logger.error("Exception While Getting Transaction For Transaction Details UG For BU Examination Fee In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "0";
		}
	}*/

	/*@Override
	public String updateTransactionDetailsFine(String amount, String tranId) {
		logger.info("Inside updateTransactionDetailsFine In DAO Layer");
		try {
			logger.info("Start updating Transaction Details +2 For Fine Fee In DAO Layer");
			int i = jdbcTemplate.update(ITransactionDao.UPDATE_TRANSACTION_DETAILS_FINE, new Object[]{amount,tranId});
			logger.info("Start updating Transaction Details +2 For FIne In DAO Layer:: Count ::"+i);
			if (i == 1) {
				logger.info("Sending The Response SUCCESS");
				return "SUCCESS";
			} else {
				logger.error("Sending The Response ERROR");
				return "ERROR";
			}
		} catch (Exception e) {
			logger.error("Exception While Updating Transaction For Transaction Details +2 For Fine Fee In DAO Layer",e);
			logger.error("Sending The Response ERROR");
			return "ERROR";
		}
	}*/

	@Override
	public List<Map<String, Object>> getRecepitNumberCLC(String tranType, String rollNo, String session,
			String scienceType) {
		try {
			List<Map<String, Object>> receiptNo = jdbcTemplate.queryForList(ITransactionDao.GET_RECEIPT_NO_CLC,
					new Object[] { tranType, rollNo, session,scienceType });
			return receiptNo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	

	@Override
	public long getPracticalAmount(Object[] params) {
		logger.info("Inside getPracticalAmount In DAO Layer");
	    Long amt=0l; 
		try {
			logger.info("Start Getting Practical Amount");
			amt= jdbcTemplate.queryForObject(ITransactionDao.GET_PRACTICAL_AMOUNT, Long.class, params);
			logger.info("Amount is ::"+amt);
		} catch (Exception e) {
			logger.error("Exception While Getting Practical Amount",e);
		}
		return amt;
	}
	
	
	@Override
	public List<HeadDeatils> getAddReason(Object[] params) {
		logger.info("Inside getAddReason In DAO Layer");
		try {
			logger.info("Start Getting Practical Amount");
			List<HeadDeatils> headDetails = jdbcTemplate.query(ITransactionDao.GET_ADD_REASON, params,(rs, rowNum) -> {
				HeadDeatils headList = new HeadDeatils();
				headList.setHeadname(rs.getString("ADD_REASON"));
				headList.setHeadAmount(rs.getInt("ADD_AMOUNT"));
			    return headList;
			});
			return headDetails;
		} catch (Exception e) {
			logger.error("Exception While Getting Add Reason And Amount",e);
		}
		return null;
	}
	
	

}
