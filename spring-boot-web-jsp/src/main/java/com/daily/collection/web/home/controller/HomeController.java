package com.daily.collection.web.home.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daily.collection.web.home.bean.HomeBean;
import com.daily.collection.web.home.service.IHomeService;

@Controller
public class HomeController {

	@Autowired IHomeService homeService;
	
	@RequestMapping(value="/getDashBoardData", method=RequestMethod.GET)
	public @ResponseBody HomeBean getDashBoardData(HttpServletRequest req,Map<String, Object> model){
		try{
			String config=(String) req.getSession().getAttribute("config");
			HomeBean dashboardDetails = homeService.getDashboardDetails(null,null,config);
			return dashboardDetails;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
}
