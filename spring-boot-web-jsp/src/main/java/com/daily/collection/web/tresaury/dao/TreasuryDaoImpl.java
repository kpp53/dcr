package com.daily.collection.web.tresaury.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.daily.collection.web.tresaury.bean.CollectionList;

@Repository
public class TreasuryDaoImpl implements ITreasuryDao {
	
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	@Autowired
	NamedParameterJdbcTemplate template;
	public void setTemplate(NamedParameterJdbcTemplate template) {
		this.template = template;
	}

	@Override
	public List<CollectionList> getCollectionReportUG(Object[] params) {
		try{
			List<CollectionList> collectionList=jdbcTemplate.query(ITreasuryDao.GET_ALL_COLLECTTION_REPORT_UG, params,
					new AllCollectuonRowMapper());
			return collectionList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	@Override
	public List<CollectionList> getCollectionReport(Object[] params) {
		try{
			List<CollectionList> collectionList=jdbcTemplate.query(ITreasuryDao.GET_ALL_COLLECTTION_REPORT, params,
					new AllCollectuonRowMapper());
			return collectionList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	@Override
	public List<CollectionList> getCollectionReportForOldStudent(Object[] params) {
		try{
			List<CollectionList> collectionList=jdbcTemplate.query(ITreasuryDao.GET_ALL_COLLECTTION_REPORT_FOR_OLD_STUDENT,
					//new AllCollectuonRowMapperForOldStudent());
					(rs, rowNum) -> {
						CollectionList colList = new CollectionList();
						colList.setNoOfStudent(String.valueOf(rs.getLong("STCNT")));
			          	colList.setTotalFund(String.valueOf(rs.getLong("CLCAMT")));
			          	colList.setDate(rs.getString("TRANDATE")); 
			          	colList.setCafAmount("0");
			          	colList.setMiscellaniousAmount("0");
					    return colList;
					});
			return collectionList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	/*final class AllCollectuonRowMapperForOldStudent implements RowMapper<CollectionList>{
		@Override
		public CollectionList mapRow(ResultSet rs, int arg1) throws SQLException {
          // List<CollectionList> collectionList=new ArrayList<CollectionList>();
			CollectionList colList = new CollectionList();
			colList.setNoOfStudent(String.valueOf(rs.getLong("STCNT")));
          	colList.setTotalFund(String.valueOf(rs.getLong("CLCAMT")));
          	colList.setDate(rs.getString("TRANDATE"));
        	   
        	//   collectionList.add(colList);
         //  }
           return colList;
		}
		
		
	}*/
	
	
	final class AllCollectuonRowMapper implements RowMapper<CollectionList>{

		@Override
		public CollectionList mapRow(ResultSet rs, int arg1) throws SQLException {
          // List<CollectionList> collectionList=new ArrayList<CollectionList>();
			CollectionList colList=null;
               colList=new CollectionList();
          	   colList.setCafAmount(String.valueOf(rs.getLong("cafAmt")));
          	   colList.setMiscellaniousAmount(String.valueOf(rs.getLong("misAmt")));
          	   colList.setNoOfStudent(String.valueOf(rs.getLong("stdCnt1")+rs.getLong("stCnt2")));
          	   colList.setTotalFund(String.valueOf(rs.getLong("totalAmt")+rs.getLong("clcAmt")));
          	   colList.setDate(rs.getString("tranDate")); 
        	   
        	//   collectionList.add(colList);
         //  }
           return colList;
		}

		
		
		
		
	}

}
