package com.daily.collection.web.login.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daily.collection.web.login.dao.ILoginDao;

@Service
public class LoginServiceImpl implements ILoginService {

    @Autowired
    ILoginDao loginDao;
	
	@Override
	public List<String> getConfigurationDetails(String config_Id) {
		return loginDao.getConfigurationDetails(config_Id);
	}

	@Override
	public String getCurrentSession(String id) {
		return loginDao.getCurrentSession(id);
	}

}
