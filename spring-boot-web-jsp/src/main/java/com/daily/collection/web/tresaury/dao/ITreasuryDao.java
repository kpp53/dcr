package com.daily.collection.web.tresaury.dao;

import java.util.List;

import com.daily.collection.web.tresaury.bean.CollectionList;

public interface ITreasuryDao {
	
	/*public static final String GET_ALL_COLLECTTION_REPORT="select  TO_CHAR(TRANSACTION.TRANSACTION_DATE, 'DD-MON-YYYY') as tranDate, (sum(TUTION_FEES) + sum(ADM_READM) + sum(LIBRARY) + sum(GAMES_AND_SPORTS) + "+ 
			"sum(MEDICAL) + sum(LABORTARY) + sum(EXTRA_CARICULAR) +   "+
			"sum(POOR_BOYS_FUND) + sum(BU_FEE) + sum(MAGAZINE) + sum(COLLEGE_EXAM) + sum(PUJA) + "+ 
			"sum(COLLEGE_DAY) + sum(PROCTORIAL) + sum(DEVELOPMENT_ONE) + sum(SCINCE_ARTS_SOCIETY) + "+ 
			"sum(RED_CROSS) + sum(CALENDAR) + sum(OTHER_FEES) + sum(BUILDING) + "+
			"sum(READING_ROOM) + sum(REGISTRATION) + sum(LABROTARY_CAUTION_MONEY) +  "+
			"sum(LIBRARY_CAUTION_MONEY) + sum(IDENTITY_CARD) + sum(CULTURAL) + sum(SCOUT_AND_GUIDE) + "+ 
			"sum(TUTION_FEE_FOR_10_MONTH) + sum(DEVELOPMENT_FOR_10_MONTH) + sum(SOCIAL_SERVICE_GUIDE) + "+
			"sum(FLAG_DAY) + sum(BU_EXAMINATION)+ sum(MISCELANIOUS) + sum(HONS_FEE) + "+
            "sum(SEMINAR_FEE) + sum(FINE) + sum(CLC) + sum(ADDITIONAL_FEE) + "+
            "sum(DEVELOPMENT_TWO) + sum(ROUNDUP) ) as totalFund, sum(CAF) as cafAmount, SUM(MISCELANIOUS) as miscellaniousAmount, count(STUDENT_ROLLNO) studentCount "+
            "from TRANSACTION_DETAILS_UG "+
            "INNER JOIN TRANSACTION ON TRANSACTION.ID = TRANSACTION_DETAILS_UG.TRAN_ID INNER JOIN HEAD_CATEGORIES "+
            "ON TRANSACTION.HEAD_CATEGORY_ID=HEAD_CATEGORIES.HEAD_CATEGORIES_ID "+
            "WHERE TRUNC(TRANSACTION_DATE) BETWEEN ? AND ? AND HEAD_CATEGORIES_ID=HEAD_CATEGORY_ID AND COURSE_NAME=? AND TRANSACTION.AADEMIC_SESSION=? "+
            "GROUP BY TRANSACTION.TRANSACTION_DATE "+ 
            "order by TRANSACTION.TRANSACTION_DATE asc";*/
	
	
public static final String GET_ALL_COLLECTTION_REPORT_UG="WITH CTE_2 AS (select A.TRAN_DATE as tranDate ,SUM(A.HEAD_AMOUNT) as clcAmt,count(ROLL_NO) as studentCount "+ 
"from ADDITIONAL_FEE_TRACK A,HEAD_CATEGORIES HC where HC.HEAD_CATEGORIES_ID=A.HEAD_CATEGORIES_ID "+
"AND HC.COURSE_NAME=? AND A.ACADEMIC_SESSION=? AND A.TRANSACTION_TYPE=? "+
"AND A.HEAD_NAME=? group by A.TRAN_DATE), "+
"CTE_1 AS(select TO_CHAR(TRANSACTION.TRANSACTION_DATE, 'DD-MON-YY') as tranDate, "+
"(sum(TUTION_FEES) + sum(ADM_READM) + sum(LIBRARY) + sum(GAMES_AND_SPORTS) + "+
"sum(MEDICAL) + sum(LABORTARY) + sum(EXTRA_CARICULAR) +  "+
"sum(POOR_BOYS_FUND) + sum(BU_FEE) + sum(MAGAZINE) + sum(COLLEGE_EXAM) + sum(PUJA) + "+ 
"sum(COLLEGE_DAY) + sum(PROCTORIAL) + sum(DEVELOPMENT_ONE) + sum(SCINCE_ARTS_SOCIETY) + "+ 
"sum(RED_CROSS) + sum(CALENDAR) + sum(OTHER_FEES) + sum(BUILDING) +  "+
"sum(READING_ROOM) + sum(REGISTRATION) + sum(LABROTARY_CAUTION_MONEY) + "+  
"sum(LIBRARY_CAUTION_MONEY) + sum(IDENTITY_CARD) + sum(CULTURAL) + sum(SCOUT_AND_GUIDE) + "+
"sum(TUTION_FEE_FOR_10_MONTH) + sum(DEVELOPMENT_FOR_10_MONTH) + sum(SOCIAL_SERVICE_GUIDE) + "+
"sum(FLAG_DAY) + sum(BU_EXAMINATION)+ sum(MISCELANIOUS) + sum(HONS_FEE) + "+
"sum(SEMINAR_FEE) + sum(FINE)  + sum(ADDITIONAL_FEE) + "+
"sum(DEVELOPMENT_TWO) + sum(ROUNDUP)) + sum(CLC) as totalFund, "+
"sum(CAF) as cafAmount, SUM(MISCELANIOUS) as miscellaniousAmount, "+
"count(STUDENT_ROLLNO) studentCount "+
"from TRANSACTION_DETAILS_UG "+
"INNER JOIN TRANSACTION ON TRANSACTION.ID = TRANSACTION_DETAILS_UG.TRAN_ID "+ 
"INNER JOIN HEAD_CATEGORIES "+
"ON TRANSACTION.HEAD_CATEGORY_ID=HEAD_CATEGORIES.HEAD_CATEGORIES_ID "+ 
"WHERE HEAD_CATEGORIES_ID=HEAD_CATEGORY_ID AND COURSE_NAME=? AND TRANSACTION.AADEMIC_SESSION=? "+ 
"GROUP BY TRANSACTION.TRANSACTION_DATE "+
"), "+
"CTE_3 AS(SELECT DISTINCT TO_CHAR(A.TRANSACTION_DATE, 'DD-MON-YY') AS TRANSACTION_DATE FROM TRANSACTION A WHERE TRUNC(A.TRANSACTION_DATE) "+ 
"BETWEEN ? AND ? "+ 
"UNION "+
"SELECT DISTINCT TO_CHAR(B.TRAN_DATE, 'DD-MON-YY') FROM ADDITIONAL_FEE_TRACK B WHERE (trunc(B.TRAN_DATE) BETWEEN ? AND "+ 
"?)) "+
"SELECT q3.TRANSACTION_DATE as tranDate, "+
"q1.totalFund as totalAmt, "+
"q1.cafAmount as cafAmt, "+
"q1.miscellaniousAmount as misAmt, "+
"q1.studentCount as stdCnt1, "+
"q2.studentCount as stCnt2, "+
"q2.clcAmt as clcAmt "+               
"FROM CTE_3 q3 "+
"LEFT JOIN CTE_1 q1 ON q1.tranDate=q3.TRANSACTION_DATE "+
"LEFT JOIN CTE_2 q2 ON q3.TRANSACTION_DATE=q2.tranDate "; 



public static final String GET_ALL_COLLECTTION_REPORT="WITH CTE_2 AS (select A.TRAN_DATE as tranDate ,sum(A.HEAD_AMOUNT) as clcAmt,count(ROLL_NO) as studentCount "+ 
"from ADDITIONAL_FEE_TRACK A,HEAD_CATEGORIES HC where HC.HEAD_CATEGORIES_ID=A.HEAD_CATEGORIES_ID "+
"AND HC.COURSE_NAME=? AND A.ACADEMIC_SESSION=? AND A.TRANSACTION_TYPE=? "+
"AND A.HEAD_NAME=? group by A.TRAN_DATE), "+
"CTE_1 AS(select TO_CHAR(TRANSACTION.TRANSACTION_DATE, 'DD-MON-YY') as tranDate, "+
"(sum(TUITION_FEES) + sum(ADM_READM) + sum(LIBRARY) +sum(SPORTS_GAMES) + "+
"sum(MEDICAL) + sum(MEDICAL_AID) + sum(LABORTARY) + sum(EXTRA_CARICULAR) + "+
"sum(POOR_BOYS_FUND) + sum(COUNCIL) + sum(MAGAZINE) + sum(COLLEGE_EXAM) + sum(PUJA) + "+ 
"sum(COLLEGE_DAY) + sum(PROCTORIAL) + sum(DEVELOPMENT) + sum(SC_ARTS_SOCIETY) +  "+
"sum(RED_CROSS) + sum(CALENDER) + sum(SYLLABUS) + sum(OTHER_FEES) + sum(BUILDING) + "+ 
"sum(ACADEMIC) + sum(READING_ROOM) + sum(REGISTRATION) + sum(LAB_CAUTION_MONEY) + "+
"sum(LIB_CAUTION_MONEY) + sum(IDENTITY_CARD) + sum(RECOGNITION) + sum(SCOUT_AND_GUIDE) + "+ 
"sum(TUITIONFEES_FOR_MONTH) + sum(DEVELOPMENT_FOR_10_MONTH) + sum(SOCIAL_SCIENCE) +  "+
"sum(FLAG_DAY) + sum(EMS_FEE) + sum(CAF) + sum(MISCELLANIOUS) + sum(ADDITIONAL_FEE) "+
"+ sum(FINE) + sum(MIGRATION_ORIGINAL_CERTIFICATE) + sum(CHSE_EXAM_FEE) + sum(DEVELOPMENT_TWO) + sum(ROUNDUP) + sum(CLC)) as totalFund, "+
"sum(CAF) as cafAmount, SUM(MISCELLANIOUS) as miscellaniousAmount, "+
"count(STUDENT_ROLLNO) studentCount "+
"from TRANSACTION_DETAILS "+
"INNER JOIN TRANSACTION ON TRANSACTION.ID = TRANSACTION_DETAILS.TRAN_ID "+ 
"INNER JOIN HEAD_CATEGORIES  "+
"ON TRANSACTION.HEAD_CATEGORY_ID=HEAD_CATEGORIES.HEAD_CATEGORIES_ID "+ 
"WHERE HEAD_CATEGORIES_ID=HEAD_CATEGORY_ID AND COURSE_NAME=? AND TRANSACTION.AADEMIC_SESSION=? "+ 
"GROUP BY TRANSACTION.TRANSACTION_DATE "+
"), "+
"CTE_3 AS(SELECT DISTINCT TO_CHAR(A.TRANSACTION_DATE, 'DD-MON-YY') AS TRANSACTION_DATE FROM TRANSACTION A WHERE TRUNC(A.TRANSACTION_DATE) "+ 
"BETWEEN ? AND ? "+
"UNION "+
"SELECT DISTINCT TO_CHAR(B.TRAN_DATE, 'DD-MON-YY') FROM ADDITIONAL_FEE_TRACK B WHERE (trunc(B.TRAN_DATE) BETWEEN ? AND ? "+ 
")) "+
"SELECT q3.TRANSACTION_DATE as tranDate, "+
"q1.totalFund as totalAmt, "+
"q1.cafAmount as cafAmt, "+
"q1.miscellaniousAmount as misAmt, "+
"q1.studentCount as stdCnt1, "+
"q2.studentCount as stCnt2, "+
"q2.clcAmt as clcAmt  "+             
"FROM CTE_3 q3 "+
"LEFT JOIN CTE_1 q1 ON q1.tranDate=q3.TRANSACTION_DATE "+
"LEFT JOIN CTE_2 q2 ON q3.TRANSACTION_DATE=q2.tranDate"; 



	
	
	
	
	public static final String GET_ALL_COLLECTTION_REPORT_CLC="";
	
	
	public static final String GET_ALL_COLLECTTION_REPORT_FOR_OLD_STUDENT="SELECT TO_CHAR(TRAN_DATE,'DD-MON-YY') AS TRANDATE,SUM(HEAD_AMOUNT) AS CLCAMT,COUNT(1) AS STCNT  FROM ADDITIONAL_FEE_TRACK WHERE TRANSACTION_TYPE='OAD' GROUP BY TRAN_DATE";
	
	
	
	public List<CollectionList> getCollectionReportUG(Object[] params);
	
	public List<CollectionList> getCollectionReport(Object[] params);
	
	public List<CollectionList> getCollectionReportForOldStudent(Object[] params);
	
	

}
