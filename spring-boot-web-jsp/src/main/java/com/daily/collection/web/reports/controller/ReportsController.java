package com.daily.collection.web.reports.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daily.collection.web.creation.bean.Student;
import com.daily.collection.web.reports.bean.DCRBook;
import com.daily.collection.web.reports.bean.Reports;
import com.daily.collection.web.reports.service.IReportService;
import com.google.gson.Gson;

@Controller	
public class ReportsController {

	@Autowired
	IReportService reportService;
	
	private String searchtype="";
	
	@Autowired
	Reports reports;
	
	@Autowired
	DCRBook dcrBOok;
	
 	
	
	
	@RequestMapping("/showMasterReports")
	public String showMasterReports(HttpServletRequest req,Map<String, Object> model) {
		if(searchtype!=null && searchtype.equalsIgnoreCase("")){
			req.setAttribute("allDiv", null);
			req.setAttribute("fundtypeblock",null);
		}else if("All".equalsIgnoreCase(searchtype)){
			searchtype="";
			req.setAttribute("allDiv","allDiv");
			model.put("reports", reports);
		}
		else if("Fund".equalsIgnoreCase(searchtype)){
			searchtype="";
			req.setAttribute("fundtypeblock","fundtypeblock");
			model.put("reports", reports);
		}
		else if("student".equalsIgnoreCase(searchtype)){
			searchtype="";
			req.setAttribute("studentblock","studentblock");
			model.put("reports", reports);
		}
		else if("dcrBook".equalsIgnoreCase(searchtype)){
			searchtype="";
			req.setAttribute("dcrBookBlock","dcrBookBlock");
			model.put("reports", reports);
		}
		req.getSession().setAttribute("reports", reports);
		return "views/reports/reports";			
	}
	
	
	@RequestMapping(value="/reportAllSearch", method=RequestMethod.POST)
	public @ResponseBody String reportAllSearch(@RequestBody String reportJson,HttpServletRequest req,Map<String, Object> model) throws ParseException {
		try{
			Gson gson = new Gson();
			reports = gson.fromJson(reportJson, Reports.class);
			SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
		    SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yy");
		    Date dateFrom = format1.parse(reports.getDateFrom());
		    reports.setDateFrom(format2.format(dateFrom));
		    Date dateTo = format1.parse(reports.getDateTo());
		    reports.setDateTo(format2.format(dateTo));
			reportService.getAllreport(reports,req);
			searchtype=reports.getSearchBy();
			return "SUCCESS";		
		}catch(Exception e){
			e.printStackTrace();
			return "ERROR";
		}
			
	}
	
	@RequestMapping(value="/reportFundSearch", method=RequestMethod.POST)
	public @ResponseBody String reportFundSearch(@RequestBody String reportJson,HttpServletRequest req,Map<String, Object> model) throws ParseException {
		try{
			Gson gson = new Gson();
			reports = gson.fromJson(reportJson, Reports.class);
			SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
		    SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yy");
		    Date dateFrom = format1.parse(reports.getDateFrom());
		    reports.setDateFrom(format2.format(dateFrom));
		    Date dateTo = format1.parse(reports.getDateTo());
		    reports.setDateTo(format2.format(dateTo));
			searchtype=reports.getSearchBy();
			req.getSession().removeAttribute("headDetailsMap");
			return reportService.getheadDetails(reports, req);
		}catch(Exception e){
			e.printStackTrace();
			return "ERROR";
		}
			
	}
	
	@RequestMapping(value="/reportStudentSearch", method=RequestMethod.POST)
	public @ResponseBody String reportStudentSearch(@RequestBody String reportJson,HttpServletRequest req,Map<String, Object> model) throws ParseException {
		try{
			Gson gson = new Gson();
			reports = gson.fromJson(reportJson, Reports.class);
			SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
		    SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yy");
		    Date dateFrom = format1.parse(reports.getDateFrom());
		    reports.setDateFrom(format2.format(dateFrom));
		    Date dateTo = format1.parse(reports.getDateTo());
		    reports.setDateTo(format2.format(dateTo));
			searchtype=reports.getSearchBy();
			req.getSession().removeAttribute("headDetailsMap");
			return reportService.getstudentDetails(reports, req);
		}catch(Exception e){
			e.printStackTrace();
			return "ERROR";
		}
			
	}
	
	@RequestMapping(value="/getStudentDetails", method=RequestMethod.POST)
	public @ResponseBody Student getStudentDetails(@RequestBody String reportJson,HttpServletRequest req,Map<String, Object> model) throws ParseException {
		try{
			Gson gson = new Gson();
			String id = gson.fromJson(reportJson, String.class);
			List<Student> listStudent=(List<Student>) req.getSession().getAttribute("studentListreport");
			Student student=listStudent.get(Integer.parseInt(id));
			return student;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
			
	}
	
	
	@RequestMapping(value="/reportDCRBookSearch", method=RequestMethod.POST)
	public @ResponseBody String reportDCRBookSearch(@RequestBody String reportJson,HttpServletRequest req,Map<String, Object> model) throws ParseException {
		try{
			Gson gson = new Gson();
			reports = gson.fromJson(reportJson, Reports.class);
			SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
		    SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yy");
		    Date dateFrom = format1.parse(reports.getDateFrom());
		    reports.setDateFrom(format2.format(dateFrom));
		    Date dateTo = format1.parse(reports.getDateTo());
		    reports.setDateTo(format2.format(dateTo));
			searchtype=reports.getSearchBy();
			req.getSession().removeAttribute("dcrBookResponse");
			String config=(String) req.getSession().getAttribute("config");
			List<String> stream=(List<String>) req.getSession().getAttribute("streamMap");
			dcrBOok.setReports(reports);
			List<Map<String, Object>> dcrBookResponse = reportService.getDCRBook(dcrBOok, config, stream);
			req.getSession().setAttribute("dcrBookResponse",dcrBookResponse.isEmpty()?null:dcrBookResponse);
			return "SUCCESS";
		}catch(Exception e){
			e.printStackTrace();
			return "ERROR";
		}
			
	}
	
	
	
}
