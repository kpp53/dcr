package com.daily.collection.web.creation.bean;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class Student implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String savedid;
	private String name;
	private String dob;
	private String fundType;
	private String session;
	private String course;
	private String stream;
	private String yearBatch;
	private String category;
	private String rollNo;
	private String admStatus="";
	private String refStatus="";
	private String adfStatus="";
	private String created_Date;
	private String receiptNo;
	private String scienceSelect="";
	
	
	private String inputamount;
	private String additionalCheckBox;
	private String addReason;
	private String addAmount;
	private String paymentModeDrop;
	private String chequeddnotext;
	
	private String collegeCode="";
	private String mrNo="";
	
	private int totalAmount;
	private String status;
	private String dateOfRefund;
	
	private String isAddFeeGiven;
	
	public Student(){
    	
    }

	@Override
	public String toString() {
		return "Student [id=" + id + ", savedid=" + savedid + ", name=" + name + ", dob=" + dob + ", fundType="
				+ fundType + ", session=" + session + ", course=" + course + ", stream=" + stream + ", yearBatch="
				+ yearBatch + ", category=" + category + ", rollNo=" + rollNo + ", admStatus=" + admStatus
				+ ", refStatus=" + refStatus + ", adfStatus=" + adfStatus + ", created_Date=" + created_Date
				+ ", receiptNo=" + receiptNo + ", scienceSelect=" + scienceSelect + ", inputamount=" + inputamount
				+ ", additionalCheckBox=" + additionalCheckBox + ", addReason=" + addReason + ", addAmount=" + addAmount
				+ ", paymentModeDrop=" + paymentModeDrop + ", chequeddnotext=" + chequeddnotext + ", collegeCode="
				+ collegeCode + ", mrNo=" + mrNo + ", totalAmount=" + totalAmount + ", status=" + status
				+ ", dateOfRefund=" + dateOfRefund + ", isAddFeeGiven=" + isAddFeeGiven + "]";
	}
	
	
	
	
	
	
	
	
	
	

}
