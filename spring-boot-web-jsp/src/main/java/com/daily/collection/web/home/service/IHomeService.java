package com.daily.collection.web.home.service;

import java.text.ParseException;
import java.util.Date;

import com.daily.collection.web.home.bean.HomeBean;

public interface IHomeService {
	
	public HomeBean getDashboardDetails(Date from,Date to,String configId) throws ParseException;

}
