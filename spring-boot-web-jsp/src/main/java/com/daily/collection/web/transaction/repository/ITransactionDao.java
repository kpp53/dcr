package com.daily.collection.web.transaction.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.daily.collection.web.analytic.bean.HeadDeatils;
import com.daily.collection.web.transaction.bean.AdditionalTransaction;

public interface ITransactionDao{
	
	public final String GET_STUDENT_COUNT="select count(*) from Student where course=? and stream=? and AADEMIC_SESSION=?";
	
	public static final String SAVE_STUDENT="Insert into STUDENT (NAME,ROLLNO,DOB,FUND_TYPE,AADEMIC_SESSION,COURSE,STREAM,CATEGORY,YEAR_BATCH,ADM_STATUS,REFUND_STATUS,CREATED_DATE,CREATED_BY,ADF_STATUS,VERIFIED_BY,COLLEGE_CODE,MRN_NO,SCIENCE_OPTION)"+ 
			"values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public static final String GET_AMOUNT="SELECT SUM(AMOUNT) FROM HEAD_CATEGORIES_DETAILS WHERE HEAD_CATEGORIES_ID=(SELECT HEAD_CATEGORIES_ID "+
"FROM HEAD_CATEGORIES WHERE COURSE_NAME=? AND STREAM_NAME=? AND  CATEGORIES_DETAILS=? AND YEAR_BATCH=? ) and YEAR_SESSION=? "+
"GROUP BY HEAD_CATEGORIES_ID";
	
	public static final String GET_AMOUNT_UG="SELECT SUM(AMOUNT) FROM HEAD_CATEGORIES_DETAILS_UG WHERE HEAD_CATEGORIES_ID=(SELECT HEAD_CATEGORIES_ID "+
			"FROM HEAD_CATEGORIES WHERE COURSE_NAME=? AND STREAM_NAME=? AND  CATEGORIES_DETAILS=? AND YEAR_BATCH=? ) and YEAR_SESSION=? "+
			"GROUP BY HEAD_CATEGORIES_ID";
	
	public static final String GET_STUDENT_DETAILS="SELECT * FROM STUDENT WHERE COURSE IN (:course) AND FUND_TYPE IN (:fundType) AND ADM_STATUS NOT IN ('40')";
	
	public static final String GET_STUDENT_DETAILS_ADDITIONAL="SELECT DISTINCT ID, Name,ROLLNO,DOB,FUND_TYPE,AADEMIC_SESSION,COURSE,STREAM,CATEGORY,YEAR_BATCH,ADM_STATUS,REFUND_STATUS,CREATED_DATE,CREATED_BY, "+ 
                        "ADF_STATUS,VERIFIED_BY,COLLEGE_CODE,MRN_NO,STUDENT.SCIENCE_OPTION,(NVL(ADDITIONAL_FEE_TRACK.ROLL_NO,0)) AS ISFeeGiven FROM STUDENT LEFT JOIN ADDITIONAL_FEE_TRACK ON STUDENT.RollNo = ADDITIONAL_FEE_TRACK.Roll_no "+
			            "Where STUDENT.COURSE IN (:course) AND STUDENT.FUND_TYPE IN (:fundType) AND STUDENT.YEAR_BATCH IN (:yearBatch) AND STUDENT.ADM_STATUS IN ('30','40')";
	
	public static final String GET_STUDENT_DETAILS_REFUND="SELECT * FROM STUDENT WHERE COURSE IN (:course) AND YEAR_BATCH IN (:yearBatch) AND ROLLNO IN (SELECT STUDENT_ROLLNO FROM STUDENT_TRACK WHERE STATUS IN ('TC Taken','Rejection'))";
	
	public static final String GET_TRANSACTION_COUNT="SELECT MAX(TO_NUMBER(REGEXP_SUBSTR(ID, '\\d+'))) COUNT FROM TRANSACTION WHERE AADEMIC_SESSION=?";
	
	public static final String SAVE_TRANSACTION="INSERT INTO TRANSACTION (ID,TRANSACTION_TYPE,RECEIPT_NO,STUDENT_ROLLNO,TRANSACTION_AMOUNT,TRANSACTION_DATE,"+
	"ADDITIONAL_REASON,TRANSACTION_BY,TRANSACTION_MODE,CHEQUE_DD_NO,ADD_REASON,ADD_AMOUNT,TOTAL_AMOUNT,AADEMIC_SESSION,HEAD_CATEGORY_ID,BOOK_NO,SCIENCE_OPTION)"+
			"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public static final String SAVE_TRANSACTION_AD="INSERT INTO ADDITIONAL_FEE_TRACK (TRAN_ID,TRAN_DATE,TRAN_BY,RECEIPT_NUMBER,BOOK_NO,TRANSACTION_MODE,ACADEMIC_SESSION,HEAD_CATEGORIES_ID,"+
			"SCIENCE_OPTION,TRANSACTION_TYPE,HEAD_NAME,HEAD_AMOUNT,ROLL_NO)"+
					"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public static final String GET_RECEIPT_NO_COUNT="SELECT COUNT(*) FROM TRANSACTION WHERE RECEIPT_NO=? AND BOOK_NO=? AND HEAD_CATEGORY_ID in (select HEAD_CATEGORIES_ID FROM HEAD_CATEGORIES WHERE COURSE_NAME='UG')";
	
	public static final String GET_COLLEGE_NAME="SELECT VALUE FROM CONFIGURATION WHERE ID=?"; 
	
	public static final String GET_COUNT_TRANSACTION_TYPE="SELECT COUNT(*) FROM TRANSACTION WHERE TRANSACTION_TYPE=? AND AADEMIC_SESSION=?";
	
	public static final String GET_COUNT_RF_TRANSACTION_TYPE="SELECT COUNT(*) FROM REFUND_TRACK WHERE TRANSACTION_TYPE=? AND ACADEMIC_SESSION=?";
	
	public static final String GET_COUNT_REFUND_TRANSACTION_TYPE="SELECT COUNT(*) FROM REFUND_TRACK WHERE TRANSACTION_TYPE=? AND AADEMIC_SESSION=?";
	
	public static final String UPDATE_TRANSACTION_STATUS="UPDATE STUDENT SET ADM_STATUS=? WHERE ROLLNO=? AND SCIENCE_OPTION=?";
	
	public static final String UPDATE_VERIFY_STATUS="UPDATE STUDENT SET ADM_STATUS=?,VERIFIED_BY=? WHERE ROLLNO=? AND SCIENCE_OPTION=?";
	
	
    public static final String UPDATE_STUDENT_STATUS_AD="UPDATE STUDENT SET ADF_STATUS=? WHERE ROLLNO=? AND SCIENCE_OPTION=?";
    
    public static final String UPDATE_STUDENT_STATUS_RF="UPDATE STUDENT SET REFUND_STATUS=? WHERE ROLLNO=? AND SCIENCE_OPTION=?";
 	
	public static final String UPDATE_VERIFY_STATUS_AD="UPDATE STUDENT SET ADF_STATUS=?,VERIFIED_BY=? WHERE ROLLNO=? AND SCIENCE_OPTION=?";
	
	public static final String UPDATE_VERIFY_STATUS_RF="UPDATE STUDENT SET REFUND_STATUS=?,VERIFIED_BY=? WHERE ROLLNO=? AND SCIENCE_OPTION=?";
	
	public static final String CHECK_MRIN_NO="SELECT COUNT(*) FROM STUDENT WHERE COLLEGE_CODE=? AND MRN_NO=? AND AADEMIC_SESSION=?";
	
	public static final String CHECK_CHEQUE_NO="SELECT COUNT(*) FROM REFUND_TRACK WHERE BANK_NAME=? AND CHEQUE_DD_NO=?";
	
	public static final String GET_CATEGORIES_ID="SELECT HEAD_CATEGORIES_ID FROM HEAD_CATEGORIES WHERE COURSE_NAME=? AND STREAM_NAME=? AND CATEGORIES_DETAILS=? AND YEAR_BATCH=?";
	
	public static final String GET_HEAD_NAME_BY_ID="SELECT HD.HEADNAME,HCD.AMOUNT FROM HEAD_CATEGORIES_DETAILS HCD, HEAD_DETAILS HD WHERE HEAD_CATEGORIES_ID=? "+
                                                   "AND HCD.YEAR_SESSION=? and HCD.HEAD_DETAILS_ID=HD.ID";
	
	
	public static final String GET_HEAD_NAME_BY_ID_UG="SELECT HD.HEADNAME,HCD.AMOUNT FROM HEAD_CATEGORIES_DETAILS_UG HCD, HEAD_DETAILS_UG HD WHERE HEAD_CATEGORIES_ID=? "+
            "AND HCD.YEAR_SESSION=? and HCD.HEAD_DETAILS_ID=HD.ID";
	
	/*public static final String SAVE_TRANSACTION_DETAILS_BY_HEAD="INSERT INTO TRANSACTION_DETAILS "
			+ "(TRAN_ID,TUITION_FEES,ADM_READM,LIBRARY,SPORTS_GAMES,MEDICAL,MEDICAL_AID,LABORTARY,EXTRA_CARICULAR,POOR_BOYS_FUND,COUNCIL,MAGAZINE,COLLEGE_EXAM,PUJA,COLLEGE_DAY,PROCTORIAL,DEVELOPMENT,SC_ARTS_SOCIETY,RED_CROSS,CALENDER,SYLLABUS,OTHER_FEES,"
			+ "BUILDING,ACADEMIC,READING_ROOM,REGISTRATION,LAB_CAUTION_MONEY,LIB_CAUTION_MONEY,IDENTITY_CARD,RECOGNITION,SCOUT_AND_GUIDE,TUITIONFEES_FOR_MONTH,DEVELOPMENT_FOR_10_MONTH,SOCIAL_SCIENCE,FLAG_DAY,EMS_FEE,"
			+ "CAF,MISCELLANIOUS,ADDITIONAL_FEE,FINE,CLC,MIGRATION_ORIGINAL_CERTIFICATE,CHSE_EXAM_FEE,DEVELOPMENT_TWO,ROUNDUP) "
			+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";*/
	
	/*public static final String SAVE_TRANSACTION_DETAILS_BY_HEAD_UG="INSERT INTO TRANSACTION_DETAILS_UG "
			+ "(TRAN_ID,TUTION_FEES,ADM_READM,LIBRARY,GAMES_AND_SPORTS,MEDICAL,LABORTARY,EXTRA_CARICULAR,POOR_BOYS_FUND,BU_FEE,MAGAZINE,COLLEGE_EXAM,PUJA,COLLEGE_DAY,PROCTORIAL,DEVELOPMENT_ONE,SCINCE_ARTS_SOCIETY,RED_CROSS,CALENDAR,OTHER_FEES, "
			+ "BUILDING,READING_ROOM,REGISTRATION,LABROTARY_CAUTION_MONEY,LIBRARY_CAUTION_MONEY,IDENTITY_CARD,CULTURAL,SCOUT_AND_GUIDE,DEVELOPMENT_FOR_10_MONTH,TUTION_FEE_FOR_10_MONTH,SOCIAL_SERVICE_GUIDE,FLAG_DAY,BU_EXAMINATION,HONS_FEE,SEMINAR_FEE,CLC,DEVELOPMENT_TWO,CAF,ADDITIONAL_FEE,FINE,MISCELANIOUS,ROUNDUP) "
			+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	*/
	/*public static final String UPDATE_TRANSACTION_DETAILS_BY_HEAD_UG="UPDATE TRANSACTION_DETAILS_UG "
			+ "SET TUTION_FEES=?,ADM_READM=?,LIBRARY=?,GAMES_AND_SPORTS=?,MEDICAL=?,LABORTARY=?,EXTRA_CARICULAR=?,POOR_BOYS_FUND=?,BU_FEE=?,MAGAZINE=?,COLLEGE_EXAM=?,PUJA=?,COLLEGE_DAY=?,PROCTORIAL=?,DEVELOPMENT_ONE=?,SCINCE_ARTS_SOCIETY=?,RED_CROSS=?,CALENDAR=?,OTHER_FEES=?, "
			+ "BUILDING=?,READING_ROOM=?,REGISTRATION=?,LABROTARY_CAUTION_MONEY=?,LIBRARY_CAUTION_MONEY=?,IDENTITY_CARD=?,CULTURAL=?,SCOUT_AND_GUIDE=?,DEVELOPMENT_FOR_10_MONTH=?,TUTION_FEE_FOR_10_MONTH=?,SOCIAL_SERVICE_GUIDE=?,FLAG_DAY=?,BU_EXAMINATION=?,HONS_FEE=?,SEMINAR_FEE=?,CLC=?,DEVELOPMENT_TWO=?,CAF=?,ADDITIONAL_FEE=?,FINE=?,MISCELANIOUS=?,ROUNDUP=? WHERE TRAN_ID=?";
	*/
	
	public static final String SAVE_REFUND_TRANSACTION_STATUS="INSERT INTO REFUND_TRACK "
			+ "(TRAN_ID,ROLL_NO,ACTUAL_AMOUNT,REFUND_AMOUNT,TRAN_DATE,TRAN_BY,RECEIPT_NUMBER,TRANSACTION_MODE,CHEQUE_DD_NO,BANK_NAME,ACADEMIC_SESSION,HEAD_CATEGORIES_ID,SCIENCE_OPTION,TRANSACTION_TYPE)"
			+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	
	public static final String GET_AVAILABLE_AMOUNT="SELECT STATUS FROM CONFIGURATION WHERE CONFIG_ID=?";
	
	public static final String UPDATE_AVAILABLE_AMOUNT="UPDATE CONFIGURATION SET STATUS=? WHERE CONFIG_ID=?";
	
	public static final String GET_RECEIPT_NO="SELECT RECEIPT_NO,BOOK_NO FROM TRANSACTION WHERE TRANSACTION_TYPE=? AND STUDENT_ROLLNO=? AND AADEMIC_SESSION=? AND SCIENCE_OPTION=?";
	
	public static final String GET_RECEIPT_NO_CLC="SELECT RECEIPT_NUMBER,BOOK_NO,HEAD_AMOUNT,TRAN_DATE FROM ADDITIONAL_FEE_TRACK WHERE TRANSACTION_TYPE=? AND ROLL_NO=? AND ACADEMIC_SESSION=? AND SCIENCE_OPTION=? AND HEAD_NAME='CLC' ";
	
	public static final String GET_RECEIPT_NO_RF="SELECT RECEIPT_NUMBER FROM REFUND_TRACK WHERE TRANSACTION_TYPE=? AND ROLL_NO=? AND ACADEMIC_SESSION=? AND SCIENCE_OPTION=?";
	
	public static final String GET_RECEIPT_NO_AD="SELECT RECEIPT_NUMBER FROM ADDITIONAL_FEE_TRACK WHERE TRANSACTION_TYPE=? AND ROLL_NO=? AND ACADEMIC_SESSION=? AND SCIENCE_OPTION=?";
	
	public static final String GET_BOOK_NO_AD="SELECT BOOK_NO FROM ADDITIONAL_FEE_TRACK WHERE TRANSACTION_TYPE=? AND ROLL_NO=? AND ACADEMIC_SESSION=? AND SCIENCE_OPTION=?";
	
	public static final String GET_TRANSACTION_DATE="SELECT TRANSACTION_DATE FROM TRANSACTION WHERE TRANSACTION_TYPE=? AND STUDENT_ROLLNO=? AND AADEMIC_SESSION=? AND SCIENCE_OPTION=?";
	
	public static final String GET_TRANSACTION_DATE_RF="SELECT TRAN_DATE FROM REFUND_TRACK WHERE TRANSACTION_TYPE=? AND ROLL_NO=? AND ACADEMIC_SESSION=? AND SCIENCE_OPTION=?";
	
	public static final String GET_TRANSACTION_DATE_AD="SELECT TRAN_DATE FROM ADDITIONAL_FEE_TRACK WHERE TRANSACTION_TYPE=? AND ROLL_NO=? AND ACADEMIC_SESSION=? AND SCIENCE_OPTION=? AND HEAD_NAME='CLC' ";

	/*public static final String GET_HEAD_DETAILS_FOR_PRINT="select TUITION_FEES as TUITION_FEES, ADM_READM as Adm_ReAdm ,LIBRARY as LIBRARY,SPORTS_GAMES SPORTS_GAMES, "+
"MEDICAL as MEDICAL, MEDICAL_AID  as MEDICAL_AID, LABORTARY as LABORTARY, EXTRA_CARICULAR as EXTRA_CARICULAR , "+
"POOR_BOYS_FUND as POOR_BOYS_FUND, COUNCIL as COUNCIL,MAGAZINE as MAGAZINE, COLLEGE_EXAM as COLLEGE_EXAM,PUJA as PUJA, "+
"COLLEGE_DAY as COLLEGE_DAY, PROCTORIAL as PROCTORIAL, DEVELOPMENT as DEVELOPMENT, SC_ARTS_SOCIETY  as SC_ARTS_SOCIETY, " +
"RED_CROSS as RED_CROSS,CALENDER as CALENDER, SYLLABUS SYLLABUS, OTHER_FEES as OTHER_FEES, BUILDING as BUILDING, "+
"ACADEMIC as ACADEMIC, READING_ROOM as READING_ROOM,REGISTRATION as REGISTRATION, LAB_CAUTION_MONEY as LAB_CAUTION_MONEY, "+
"LIB_CAUTION_MONEY as LIB_CAUTION_MONEY, IDENTITY_CARD as IDENTITY_CARD, RECOGNITION as RECOGNITION, SCOUT_AND_GUIDE as SCOUT_AND_GUIDE, "+
"TUITIONFEES_FOR_MONTH as TUITIONFEES_FOR_MONTH, DEVELOPMENT_FOR_10_MONTH as DEVELOPMENT_FOR_10_MONTH, SOCIAL_SCIENCE as SOCIAL_SCIENCE, "+
"FLAG_DAY as FLAG_DAY, EMS_FEE as EMS_FEE,CAF as CAF,MISCELLANIOUS as MISCELLANIOUS,ADDITIONAL_FEE as ADDITIONAL_FEE,FINE as FINE,CLC as CLC,MIGRATION_ORIGINAL_CERTIFICATE as MIGRATION_ORIGINAL_CERTIFICATE, CHSE_EXAM_FEE as CHSE_EXAM_FEE, DEVELOPMENT_TWO as DEVELOPMENT_TWO , TOTAL_AMOUNT as TOTAL_AMOUNT, ROUNDUP as ROUNDUP "+
"from TRANSACTION_DETAILS TD,TRANSACTION T "+
"where TD.TRAN_ID=T.ID AND T.STUDENT_ROLLNO=? AND T.TRANSACTION_TYPE=? AND T.SCIENCE_OPTION=?"; */
	
	/*public static final String GET_HEAD_DETAILS_FOR_PRINT_UG="select TUTION_FEES as TUTION_FEES,ADM_READM as ADM_READM,LIBRARY as LIBRARY,GAMES_AND_SPORTS as GAMES_AND_SPORTS,MEDICAL as MEDICAL,LABORTARY as LABORTARY,EXTRA_CARICULAR as EXTRA_CARICULAR,POOR_BOYS_FUND as POOR_BOYS_FUND,BU_FEE as BU_FEE,MAGAZINE as MAGAZINE,COLLEGE_EXAM as COLLEGE_EXAM,PUJA as PUJA,COLLEGE_DAY as COLLEGE_DAY,PROCTORIAL as PROCTORIAL,DEVELOPMENT_ONE as DEVELOPMENT_ONE,SCINCE_ARTS_SOCIETY as SCINCE_ARTS_SOCIETY, "+
	        "RED_CROSS as RED_CROSS,CALENDAR as CALENDAR,OTHER_FEES as OTHER_FEES,BUILDING as BUILDING,READING_ROOM as READING_ROOM,REGISTRATION as REGISTRATION,LABROTARY_CAUTION_MONEY as LABROTARY_CAUTION_MONEY,LIBRARY_CAUTION_MONEY as LIBRARY_CAUTION_MONEY,IDENTITY_CARD as IDENTITY_CARD,CULTURAL as CULTURAL,SCOUT_AND_GUIDE as SCOUT_AND_GUIDE,DEVELOPMENT_FOR_10_MONTH as DEVELOPMENT_FOR_10_MONTH,TUTION_FEE_FOR_10_MONTH as TUTION_FEE_FOR_10_MONTH,SOCIAL_SERVICE_GUIDE as SOCIAL_SERVICE_GUIDE,FLAG_DAY as FLAG_DAY,BU_EXAMINATION as BU_EXAMINATION,HONS_FEE as HONS_FEE,SEMINAR_FEE as SEMINAR_FEE,CLC as CLC,DEVELOPMENT_TWO as DEVELOPMENT_TWO,CAF as CAF,ADDITIONAL_FEE as ADDITIONAL_FEE,FINE as FINE,MISCELANIOUS as MISCELANIOUS, TOTAL_AMOUNT as TOTAL_AMOUNT,ROUNDUP as ROUNDUP  "+
			"from TRANSACTION_DETAILS_UG TD,TRANSACTION T "+
			"where TD.TRAN_ID=T.ID AND T.STUDENT_ROLLNO=? AND T.TRANSACTION_TYPE=? AND T.SCIENCE_OPTION=?"; 
	*/
	
	public static final String GET_HEAD_DETAILS_FOR_PRINT="SELECT HDG.HEADNAME AS HEADNAME,HUG.AMOUNT AS AMOUNT FROM HEAD_CATEGORIES_DETAILS HUG,HEAD_DETAILS HDG,TRANSACTION T " + 
			"WHERE  T.STUDENT_ROLLNO=? AND T.TRANSACTION_TYPE=? AND T.SCIENCE_OPTION=? "+ 
			"AND T.HEAD_CATEGORY_ID=HUG.HEAD_CATEGORIES_ID AND HUG.HEAD_DETAILS_ID=HDG.ID";
	
	
	public static final String GET_HEAD_DETAILS_FOR_PRINT_UG="SELECT HDG.HEADNAME AS HEADNAME,HUG.AMOUNT AS AMOUNT FROM HEAD_CATEGORIES_DETAILS_UG HUG,HEAD_DETAILS_UG HDG,TRANSACTION T " + 
			"WHERE  T.STUDENT_ROLLNO=? AND T.TRANSACTION_TYPE=? AND T.SCIENCE_OPTION=? "+ 
			"AND T.HEAD_CATEGORY_ID=HUG.HEAD_CATEGORIES_ID AND HUG.HEAD_DETAILS_ID=HDG.ID";
	
	public static final String INSERT_STUDENT_STATUS="INSERT INTO STUDENT_TRACK (STUDENT_ROLLNO,STATUS) VALUES(?,?)";
	
	public static final String UPDATE_STUDENT_STATUS="UPDATE STUDENT_TRACK SET STATUS=? WHERE STUDENT_ROLLNO=? ";
	
	/*public static final String GET_RECEIPT_BOOK_ID="SELECT max(RECEIPT_NO) as RECEIPT_NO ,max(BOOK_NO) as BOOK_NO  FROM TRANSACTION WHERE HEAD_CATEGORY_ID in(select HEAD_CATEGORIES_ID from HEAD_CATEGORIES where COURSE_NAME=?) AND BOOK_NO=(select max(BOOK_NO) as BOOK_NO from TRANSACTION where HEAD_CATEGORY_ID in(select HEAD_CATEGORIES_ID from HEAD_CATEGORIES where COURSE_NAME=?)) AND BOOK_NO IS NOT NULL";*/
	
	public static final String GET_RECEIPT_BOOK_ID="SELECT STATUS FROM CONFIGURATION WHERE CONFIG_ID=?";
	
	public static final String GET_TRANSACTION_BY_COURSE="SELECT count(ID) as ID FROM TRANSACTION WHERE HEAD_CATEGORY_ID in(select HEAD_CATEGORIES_ID from HEAD_CATEGORIES where COURSE_NAME=?) AND BOOK_NO IS NOT NULL";
	
	public static final String UPDATE_BOOK_ID="UPDATE TRANSACTION SET BOOK_NO=? WHERE ID=?";
	
	public static final String GET_RECEIPT_STARTING="SELECT STATUS FROM CONFIGURATION WHERE CONFIG_ID=?";
	
	public static final String UPDATE_RECEIPT_NUMBER="UPDATE CONFIGURATION SET STATUS=? WHERE CONFIG_ID=?";
	
	public static final String GET_TRANSACTION_ID="SELECT ID FROM TRANSACTION WHERE STUDENT_ROLLNO=? AND SCIENCE_OPTION=?";
	
	public static final String GET_TRANSACTION_ID_OLD_STUDENT="SELECT COUNT(*) FROM ADDITIONAL_FEE_TRACK WHERE TRAN_ID LIKE '%O%'";
	
	public static final String GET_BANK_NAME="SELECT BANK_NAME FROM REFUND_TRACK WHERE ROLL_NO=? AND SCIENCE_OPTION=? AND TRANSACTION_TYPE=?";
	
	public static final String GET_HEAD_NAME="SELECT HEAD_NAME FROM ADDITIONAL_FEE_TRACK WHERE ROLL_NO=? AND SCIENCE_OPTION=? AND TRANSACTION_TYPE=?";
	
	public static final String GET_REFUND_AMOUNT="SELECT REFUND_AMOUNT FROM REFUND_TRACK WHERE ROLL_NO=? AND SCIENCE_OPTION=? AND TRANSACTION_TYPE=?";
	
	public static final String GET_ADD_AMOUNT="SELECT HEAD_AMOUNT FROM ADDITIONAL_FEE_TRACK WHERE ROLL_NO=? AND SCIENCE_OPTION=? AND TRANSACTION_TYPE=?";
	
	public static final String GET_CHEQUE_NUMBER="SELECT CHEQUE_DD_NO FROM REFUND_TRACK WHERE ROLL_NO=? AND SCIENCE_OPTION=? AND TRANSACTION_TYPE=?";
	
	public static final String GET_HEAD_CATEGORIES_ID="SELECT HEAD_CATEGORY_ID FROM TRANSACTION WHERE STUDENT_ROLLNO=? AND SCIENCE_OPTION=?";
	
	public static final String GET_HEAD_NAME_AMONT_UG="SELECT HD.HEADNAME as HEADNAME ,HCD.AMOUNT as AMOUNT FROM HEAD_DETAILS_UG HD, HEAD_CATEGORIES_DETAILS_UG HCD ,HEAD_CATEGORIES HC "+
			"WHERE HCD.HEAD_DETAILS_ID=HD.ID AND "+
			"HC.COURSE_NAME=? AND HC.STREAM_NAME=? AND HC.CATEGORIES_DETAILS=? AND HC.YEAR_BATCH=? "+ 
            "AND HCD.YEAR_SESSION=? AND  HC.HEAD_CATEGORIES_ID=HCD.HEAD_CATEGORIES_ID";
	
	public static final String GET_HEAD_NAME_AMONT="SELECT HD.HEADNAME as HEADNAME ,HCD.AMOUNT as AMOUNT FROM HEAD_DETAILS HD, HEAD_CATEGORIES_DETAILS HCD ,HEAD_CATEGORIES HC "+
			"WHERE HCD.HEAD_DETAILS_ID=HD.ID AND "+
			"HC.COURSE_NAME=? AND HC.STREAM_NAME=? AND HC.CATEGORIES_DETAILS=? AND HC.YEAR_BATCH=? "+ 
            "AND HCD.YEAR_SESSION=? AND  HC.HEAD_CATEGORIES_ID=HCD.HEAD_CATEGORIES_ID";
	
	public static final String UPDATE_STUDENT="UPDATE STUDENT SET CREATED_DATE=?,CREATED_BY=?,FUND_TYPE=?,REFUND_STATUS=? WHERE ROLLNO=? AND SCIENCE_OPTION=?";
	
	public static final String UPDATE_TRANSACTION="UPDATE TRANSACTION SET TRANSACTION_TYPE=?,TRANSACTION_AMOUNT=?,TRANSACTION_DATE=?,TRANSACTION_BY=?,TRANSACTION_MODE=?,CHEQUE_DD_NO=?,ADD_AMOUNT=?,TOTAL_AMOUNT=? WHERE STUDENT_ROLLNO=? AND SCIENCE_OPTION=?";
	
	/*public static final String UPDATE_TRANSACTION_DETAILS_UG_CLC="UPDATE TRANSACTION_DETAILS_UG SET CLC=? WHERE TRAN_ID=?";*/
	
	/*public static final String UPDATE_TRANSACTION_DETAILS_UG_BU_EXAM="UPDATE TRANSACTION_DETAILS_UG SET BU_EXAMINATION=? WHERE TRAN_ID=?";*/
	
	/*public static final String UPDATE_TRANSACTION_DETAILS_UG_FINE="UPDATE TRANSACTION_DETAILS_UG SET FINE=? WHERE TRAN_ID=?";*/
	
	//public static final String UPDATE_TRANSACTION_DETAILS_FINE="UPDATE TRANSACTION_DETAILS SET FINE=? WHERE TRAN_ID=?";
	
	
	
	
	/*public static final String GET_TRANSACTION_DETAILS_UG_ADD="SELECT ADDITIONAL_FEE FROM TRANSACTION_DETAILS_UG WHERE TRAN_ID=?";
	
	public static final String GET_TRANSACTION_DETAILS_UG_FINE="SELECT FINE FROM TRANSACTION_DETAILS_UG WHERE TRAN_ID=?";*/
	
	//public static final String GET_TRANSACTION_DETAILS_FINE="SELECT FINE FROM TRANSACTION_DETAILS WHERE TRAN_ID=?";
	
	//public static final String GET_TRANSACTION_DETAILS_ADD="SELECT ADDITIONAL_FEE FROM TRANSACTION_DETAILS WHERE TRAN_ID=?";
	
	/*public static final String GET_TRANSACTION_DETAILS_BU="SELECT BU_EXAMINATION FROM TRANSACTION_DETAILS_UG WHERE TRAN_ID=?";*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*public static final String UPDATE_TRANSACTION_DETAILS_UG_ADD="UPDATE TRANSACTION_DETAILS_UG SET ADDITIONAL_FEE=? WHERE TRAN_ID=?";*/
	
    //public static final String UPDATE_TRANSACTION_DETAILS_CLC="UPDATE TRANSACTION_DETAILS SET CLC=? WHERE TRAN_ID=?";
	
	//public static final String UPDATE_TRANSACTION_DETAILS_ADD="UPDATE TRANSACTION_DETAILS SET ADDITIONAL_FEE=? WHERE TRAN_ID=?";
	
	public static final String UPDATE_STUDENT_REF_STATUS="UPDATE STUDENT SET REFUND_STATUS=? WHERE ROLLNO=? AND SCIENCE_OPTION=?";
	
	/*public static final String GET_SUM_AMOUNT_STUDENT_UG="select (sum(TUTION_FEES) + sum(ADM_READM) + sum(LIBRARY) + sum(GAMES_AND_SPORTS) + "+
			"sum(MEDICAL) + sum(LABORTARY) + sum(EXTRA_CARICULAR) +  "+
			"sum(POOR_BOYS_FUND) + sum(BU_FEE) + sum(MAGAZINE) + sum(COLLEGE_EXAM) + sum(PUJA) + "+ 
			"sum(COLLEGE_DAY) + sum(PROCTORIAL) + sum(DEVELOPMENT_ONE) + sum(SCINCE_ARTS_SOCIETY) + "+ 
			"sum(RED_CROSS) + sum(CALENDAR) + sum(OTHER_FEES) + sum(BUILDING) + "+
			"sum(READING_ROOM) + sum(REGISTRATION) + sum(LABROTARY_CAUTION_MONEY) + "+ 
			"sum(LIBRARY_CAUTION_MONEY) + sum(IDENTITY_CARD) + sum(CULTURAL) + sum(SCOUT_AND_GUIDE) + "+ 
			"sum(TUTION_FEE_FOR_10_MONTH) + sum(DEVELOPMENT_FOR_10_MONTH) + sum(SOCIAL_SERVICE_GUIDE) + "+
			"sum(FLAG_DAY) + sum(BU_EXAMINATION)+ sum(CAF) + sum(MISCELANIOUS) + sum(HONS_FEE) + "+
            "sum(SEMINAR_FEE) + sum(FINE) + sum(CLC) + sum(ADDITIONAL_FEE) + "+
            "sum(DEVELOPMENT_TWO) + sum(ROUNDUP) ) as TOTAL "+
			"from TRANSACTION_DETAILS_UG TD,TRANSACTION T  "+
			"where T.STUDENT_ROLLNO=? AND T.SCIENCE_OPTION=? AND TD.TRAN_ID=T.ID";*/
	
	/*public static final String GET_SUM_AMOUNT_STUDENT="select sum(TUITION_FEES) + sum(ADM_READM) + sum(LIBRARY) +sum(SPORTS_GAMES) +"+
"sum(MEDICAL) + sum(MEDICAL_AID) + sum(LABORTARY) + sum(EXTRA_CARICULAR) + "+
"sum(POOR_BOYS_FUND) + sum(COUNCIL) + sum(MAGAZINE) + sum(COLLEGE_EXAM) + sum(PUJA) + "+
"sum(COLLEGE_DAY) + sum(PROCTORIAL) + sum(DEVELOPMENT) + sum(SC_ARTS_SOCIETY) + " +
"sum(RED_CROSS) + sum(CALENDER) + sum(SYLLABUS) + sum(OTHER_FEES) + sum(BUILDING) + "+
"sum(ACADEMIC) + sum(READING_ROOM) + sum(REGISTRATION) + sum(LAB_CAUTION_MONEY) + "+
"sum(LIB_CAUTION_MONEY) + sum(IDENTITY_CARD) + sum(RECOGNITION) + sum(SCOUT_AND_GUIDE) + "+
"sum(TUITIONFEES_FOR_MONTH) + sum(DEVELOPMENT_FOR_10_MONTH) + sum(SOCIAL_SCIENCE) + "+
"sum(FLAG_DAY) + sum(EMS_FEE) + sum(CAF) + sum(MISCELLANIOUS) + sum(ADDITIONAL_FEE) + sum(FINE) + sum(CLC) + sum(MIGRATION_ORIGINAL_CERTIFICATE) + sum(CHSE_EXAM_FEE) + sum(DEVELOPMENT_TWO) + sum(ROUNDUP) as TOTAL "+
"from TRANSACTION_DETAILS TD,TRANSACTION T  "+
"where T.STUDENT_ROLLNO=? AND T.SCIENCE_OPTION=? AND TD.TRAN_ID=T.ID";*/
	
	/*public static final String GET_HEAD_DETAILS="select sum(TUTION_FEES) as TUITION_FEES, sum(ADM_READM) as Adm_ReAdm ,sum(LIBRARY) as LIBRARY,sum(GAMES_AND_SPORTS) SPORTS_GAMES, "+
			"sum(MEDICAL) MEDICAL,sum(LABORTARY) LABORTARY, sum(EXTRA_CARICULAR) EXTRA_CARICULAR , "+
			"sum(POOR_BOYS_FUND) POOR_BOYS_FUND, sum(BU_FEE) BU_FEE,sum(MAGAZINE) MAGAZINE, sum(COLLEGE_EXAM) COLLEGE_EXAM,sum(PUJA) PUJA, "+
			"sum(COLLEGE_DAY) COLLEGE_DAY, sum(PROCTORIAL) PROCTORIAL, sum(DEVELOPMENT_ONE) DEVELOPMENT,sum(SCINCE_ARTS_SOCIETY) as SC_ARTS_SOCIETY, " +
			"sum(RED_CROSS) as RED_CROSS,sum(CALENDAR) as CALENDER, sum(OTHER_FEES) as OTHER_FEES, sum(BUILDING) as BUILDING, "+
			"sum(READING_ROOM) as READING_ROOM,sum(REGISTRATION) as REGISTRATION, sum(LABROTARY_CAUTION_MONEY) as LAB_CAUTION_MONEY, "+
			"sum(LIBRARY_CAUTION_MONEY) as LIB_CAUTION_MONEY, sum(IDENTITY_CARD) as IDENTITY_CARD,sum(CULTURAL) CULTURAL,sum(SCOUT_AND_GUIDE) as SCOUT_AND_GUIDE, "+
			"sum(TUTION_FEE_FOR_10_MONTH) as TUITIONFEES_FOR_MONTH, sum(DEVELOPMENT_FOR_10_MONTH) as DEVELOPMENT_FOR_10_MONTH, sum(SOCIAL_SERVICE_GUIDE) SOCIAL_SCIENCE, "+
			"sum(FLAG_DAY) as FLAG_DAY, sum(BU_EXAMINATION) as BU_EXAMINATION,sum(CAF) as CAF,sum(MISCELANIOUS) as MISCELLANIOUS,sum(HONS_FEE) as HONS_FEE,sum(SEMINAR_FEE) as SEMINAR_FEE, sum(FINE) as FINE,sum(CLC) as CLC, sum(ADDITIONAL_FEE) as ADDITIONAL_FEE, sum(DEVELOPMENT_TWO) as DEVELOPMENT_TWO,sum(ROUNDUP) as ROUNDUP "+
			"from TRANSACTION_DETAILS_UG TD,TRANSACTION T  "+
			"where T.STUDENT_ROLLNO=? AND TD.TRAN_ID=T.ID"; */
	
	/*public static final String GET_CLC_AMOUNT_UG="select TDG.CLC AS CLC from TRANSACTION T,TRANSACTION_DETAILS_UG TDG Where T.ID=TDG.TRAN_ID "+
            "AND T.STUDENT_ROLLNO=? AND T.SCIENCE_OPTION=?";*/
	
	/*public static final String GET_CLC_AMOUNT="select TDG.CLC AS CLC from TRANSACTION T,TRANSACTION_DETAILS TDG Where T.ID=TDG.TRAN_ID "+
            "AND T.STUDENT_ROLLNO=? AND T.SCIENCE_OPTION=?";*/
	
	public static final String UPDATE_STUDENT_COLLEGE_CODE="UPDATE STUDENT SET COLLEGE_CODE=?, MRN_NO=? WHERE ROLLNO=? AND SCIENCE_OPTION=? AND AADEMIC_SESSION=?";
	
	public static final String GET_ADD_FINE_DETAILS_FOR_PRINT="select * from ADDITIONAL_FEE_TRACK where ROLL_NO=? AND  TRANSACTION_TYPE=?";
	
	
	
	public static final String GET_PRACTICAL_AMOUNT="SELECT ADD_AMOUNT FROM TRANSACTION WHERE STUDENT_ROLLNO=? AND TRANSACTION_TYPE=? AND SCIENCE_OPTION=? AND ADD_REASON='Practical' ";
	
	public static final String GET_ADD_REASON="SELECT ADD_REASON, ADD_AMOUNT FROM TRANSACTION WHERE STUDENT_ROLLNO=? AND TRANSACTION_TYPE=? AND SCIENCE_OPTION=?";
	
	public String getAvailbleAmount(String id);
	
	public Integer getReceiptStarting(String id);
	
	public String getReceiptNBook(String dashBoardConfig);
	
	public String getTransactionByCourse(String dashBoardConfig);
	
	public String updateBookId(String bookId,String id);
	
	public List<HeadDeatils> getHeadDetailsForprint(Object[] params);
	
	public List<HeadDeatils> getHeadDetailsForprintUG(Object[] params);
	
	public long getPracticalAmount(Object[] params);
	
	public List<Map<String, Object>> getRecepitNumber(String tranType,String rollNo,String session,String scienceType);
	
	public List<Map<String, Object>> getRecepitNumberCLC(String tranType,String rollNo,String session,String scienceType);
	
	public String getRecepitNumberRF(String tranType,String rollNo,String session,String scienceType);
	
	public String getRecepitNumberAD(String tranType,String rollNo,String session,String scienceType);
	
	public Date getTransactionDate(String tranType,String rollNo,String session,String scienceType);
	
	public Date getTransactionDateRF(String tranType,String rollNo,String session,String scienceType);
	
	public Date getTransactionDateAD(String tranType,String rollNo,String session,String scienceType);
	
	public String updateAvailableAmount(String amount,String configId);
	
	public String countById(String course,String stream,String session);
	
	public List<Map<String,Object>> getHeadNameById(String catogoriesId,String session);
	
	public List<Map<String,Object>> getHeadNameByIdUG(String catogoriesId,String session);
	
	//public String saveTransactionDetailsByHead(Object[] params);
	
	//public String saveTransactionDetailsByHeadUG(Object[] params);// We Can Remove
	
	public String getCategoriesId(Object[] studentParams);
	
	public String saveStudent(Object[] studentParams);
	
	public List<Map<String, Object>> getStudent(MapSqlParameterSource parameters);
	
	public List<Map<String, Object>> getStudentAdditional(MapSqlParameterSource parameters);
	
	public Long getFinalAmount(String session,String courseName,String streamName,String category,String yearbatch);
	
	public Long getFinalAmountUG(String session,String courseName,String streamName,String category,String yearbatch);
	
	public Integer gettransactionCount(String session);
	
	public String saveTransaction(Object[] transactionParam);
	
	public String saveTransactionAd(Object[] transactionParam);
	
	public String getCollegeName(String id);
	
	public Integer getTransactionTypeCount(String name,String session);
	
	public Integer getRefundTransactionTypeCount(String name,String session);
	
	public String updateTransaction(String transactioStatus,String rollNo,String scienceType);
	
	public String updateTransactionAD(String transactioStatus,String rollNo,String scienceType);
	
	//public String updateTransactionDetailsCLCUG(String amount,String tranId);//We Can Remove
	
	//public String updateTransactionDetailsCLCBUExamFee(String amount,String tranId);//We Can Remove
	
	//public String updateTransactionDetailsAddUG(String amount,String tranId);// We Can Remove
	
	//public String getTransactionDetailsAddUG(String tranId);//We Can Remove
	
	//public String updateTransactionDetailsFineUG(String amount,String tranId); // We Can Remove
	
	//public String updateTransactionDetailsFine(String amount,String tranId);
	
	//public String getTransactionDetailsFineUG(String tranId); // We Can Remove
	
	//public String getTransactionDetailsFine(String tranId);
	
   // public String updateTransactionDetailsCLC(String amount,String tranId);
	
	//public String updateTransactionDetailsAd(String amount,String tranId);
	
	/*public String updateTransactionDetails(String transactioStatus,String rollNo,String scienceType);*/ // We Can Remove
	
	
	public String updateVerifyStatus(String verifyStatus,String userName,String rollNo,String scienceType);
	
	public String updateVerifyStatusAD(String verifyStatus,String userName,String rollNo,String scienceType);
	
	public String updateVerifyStatusRF(String verifyStatus,String userName,String rollNo,String scienceType);
	
	public String checkMrinExist(String collegeCode,String mrinNo,String currentSession);
	
	public String insertStudentTrack(String rollNo,String status);
	
	public String updateReceiptNumberInConfiguration(String receiptNumber,String configId);
	
	public String updateStudentTrack(String rollNo,String status);
	
	public String updateCollegeNMRIN(String collegeCode,String mrinNo,String academicSession,String rollNo,String scienceType);
	
	public String updateStudentRefStatus(String rollNo,String scienceType,String status);
	
	public List<Map<String, Object>> getHeadNameAndAmountUG(Object[] params);
	
	public List<Map<String, Object>> getHeadNameAndAmount(Object[] params);
	
	public String updateTransactionRF(String transactioStatus, String rollNo, String scienceType) ;
	
	public String updateStudent(Object[] params) ;
	
	//public String getStudentAmountUG(Object[] params) ;
	
	//public String getStudentAmount(Object[] params) ;
	
	public String updateTransaction(Object[] params) ;
	
	//public String updateTransactionDetailsUG(Object[] params) ;
	
	public String getTransactionId(String rollNo,String scienceType);
	
	public String getheadCategoryId(String rollNo,String scienceType);
	
	public String updateTransactionTrack(Object[] params);
	
	public String getBankName(String rollNo,String scienceType,String tranType);
	
	public String getHeadName(String rollNo,String scienceType,String tranType);
	
	//public String getCLCAmountUG(String rollNo,String scienceType);
	
	//public String getCLCAmount(String rollNo,String scienceType);
	
	//public String getCLCAmountForPrint(String rollNo,String scienceType);
	
	public String getChequeNumber(String rollNo,String scienceType,String tranType);
	
	public Long getTotalAmount(String rollNo,String scienceType,String tranType);
	
	public Long getTotalAmountAD(String rollNo,String scienceType,String tranType);
	
	public Integer isChequeNoExist(String bankName,String chequeNo);
	
	public Integer isReceiptNoExist(String receiptNo,String bookNo);
    
	public List<Map<String, Object>> getStudentAdditionalRefund(MapSqlParameterSource parameters);
	
	public String getBookAD(String tranType, String rollNo, String session, String scienceType);
	
	public List<Map<String,Object>> getPrintDetails(String rollNo,String tranType);

	//public String getTransactionDetailsAdd(String tranId);
	
	//public String getTransactionDetailsUGBUExamFee(String tranId);
	
	public Integer getTransactionIdOldStudent();
	
	
	public List<HeadDeatils> getAddReason(Object[] params);
	
	
	
}
