package com.daily.collection.web.transaction.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daily.collection.web.analytic.bean.HeadDeatils;
import com.daily.collection.web.common.DataUtil;
import com.daily.collection.web.creation.bean.Student;
import com.daily.collection.web.transaction.bean.AdditionalTransaction;
import com.daily.collection.web.transaction.bean.Transaction;
import com.daily.collection.web.transaction.service.ITransactionService;
import com.google.gson.Gson;

@Controller
public class TransactionController {
	
	private final static Logger log = Logger.getLogger(TransactionController.class);
	
	@Autowired
	ITransactionService transactionService;
	
	@Autowired
	Student student;
	
	List<Student> studentList=new ArrayList<Student>();
	
	@RequestMapping("/showAdmission")
	public String showAdmission(HttpServletRequest req,Map<String, Object> model) {
		log.info("Inside showAdmission Method");
		if(student==null){
			log.info("Student object is null");
			model.put("student", student);	
		}else{
			log.info("Student object is not null");
			model.put("student", student);	
		}
		    List<Student> listStudent = null;
			try {
				String config=(String) req.getSession().getAttribute("config");
				log.info("Start Getting Student Details");
				log.info("Inputs ::"+config);
				listStudent = transactionService.getStudentDetails(config,"A");
				log.info("End Getting Student Details");
				log.info("Student List Size ::"+listStudent.size());
			} catch (Exception e) {
				e.printStackTrace();
			}
			req.getSession().setAttribute("listStudent",listStudent);
			if(req.getSession().getAttribute("availableAmt")!=null){
	    		req.getSession().removeAttribute("availableAmt");
	    	}
			//req.getSession().setAttribute("print_url", transactionService.getAvailableAmount("5"));	
			String config=(String) req.getSession().getAttribute("config");
			if(config.equalsIgnoreCase("+2")){
				req.getSession().setAttribute("availableAmt", transactionService.getAvailableAmount("4"));	
			}else if(config.equalsIgnoreCase("UG")){
				req.getSession().setAttribute("availableAmt", transactionService.getAvailableAmount("7"));
				req.getSession().setAttribute("lastReciptNumber", transactionService.getAvailableAmount("14"));
			}else{
				
			}
	    	
		//createDropDownMap(req);
		return "views/transaction/transaction_admission";			
	}
	
	
	@RequestMapping("/showAdditionalFee")
	public String showAdditionalFee(HttpServletRequest req,Map<String, Object> model) {
		log.info("Inside showAdditionalFee Method");
		if(student==null){
			log.info("Student object is null");
			model.put("student", student);	
		}else{
			log.info("Student object is not null");
			model.put("student", student);	
		}
		    List<Student> listStudent = null;
			try {
				String config=(String) req.getSession().getAttribute("config");
				log.info("Start Getting Student Details");
				log.info("Inputs ::"+config);
				listStudent = transactionService.getStudentDetails(config,"B");
				log.info("End Getting Student Details");
				log.info("Student List Size ::"+listStudent.size());
			} catch (Exception e) {
				e.printStackTrace();
			}
			req.getSession().setAttribute("listStudent",listStudent);
			if(req.getSession().getAttribute("availableAmt")!=null){
	    		req.getSession().removeAttribute("availableAmt");
	    	}
			//req.getSession().setAttribute("print_url", transactionService.getAvailableAmount("5"));	
			String config=(String) req.getSession().getAttribute("config");
			if(config.equalsIgnoreCase("+2")){
				req.getSession().setAttribute("availableAmt", transactionService.getAvailableAmount("4"));	
			}else if(config.equalsIgnoreCase("UG")){
				req.getSession().setAttribute("availableAmt", transactionService.getAvailableAmount("7"));
			}else{
				
			}
	    	
		//createDropDownMap(req);
		return "views/transaction/transaction_additionalFee";			
	}
	
	
	@RequestMapping("/showRefund")
	public String showRefund(HttpServletRequest req,Map<String, Object> model) {
		log.info("Inside showRefund Method");
		if(student==null){
			log.info("Student object is null");
			model.put("student", student);	
		}else{
			log.info("Student object is not null");
			model.put("student", student);	
		}
		    List<Student> listStudent = null;
			try {
				String config=(String) req.getSession().getAttribute("config");
				log.info("Start Getting Student Details");
				log.info("Inputs ::"+config);
				listStudent = transactionService.getStudentDetails(config,"C");
				log.info("End Getting Student Details");
				log.info("Student List Size ::"+listStudent.size());
			} catch (Exception e) {
				e.printStackTrace();
			}
			req.getSession().setAttribute("listStudent",listStudent);
			/*if(req.getSession().getAttribute("print_url")==null){
				req.getSession().setAttribute("print_url", transactionService.getAvailableAmount("5"));	
			}*/
			if(req.getSession().getAttribute("availablerefAmt")!=null){
	    		req.getSession().removeAttribute("availablerefAmt");
	    	}
			String config=(String) req.getSession().getAttribute("config");
			if(config.equalsIgnoreCase("+2")){
					req.getSession().setAttribute("availablerefAmt", transactionService.getAvailableAmount("11"));	
			}else if(config.equalsIgnoreCase("UG")){
					req.getSession().setAttribute("availablerefAmt", transactionService.getAvailableAmount("10")); 					
			}else{
		}
		return "views/transaction/transaction_refund";			
	}
	
	
	
	
	
	
	@RequestMapping("/admisssionShowFromPopup")
	public String admisssionShowFromPopup(HttpServletRequest req,Map<String, Object> model) {
		log.info("Inside admisssionShowFromPopup Method");
		if(student==null){
			log.info("Student Object Is Null");
			model.put("student", student);	
		}else{
			log.info("Student Object Is Not Null");
			model.put("student", student);	
		}
			req.getSession().setAttribute("listStudent",studentList);
			log.info("Redirected To transaction_admission.jsp Page");
		return "views/transaction/transaction_admission";			
	}
	
	
	@RequestMapping("/additionalFeeShowFromPopup")
	public String additionalFeeShowFromPopup(HttpServletRequest req,Map<String, Object> model) {
		log.info("Inside admisssionShowFromPopup Method");
		if(student==null){
			log.info("Student Object Is Null");
			model.put("student", student);	
		}else{
			List<Student> listStudent = null;
			try {
				String config=(String) req.getSession().getAttribute("config");
				log.info("Start Getting Student Details");
				log.info("Inputs ::"+config);
				listStudent = transactionService.getStudentDetails(config,"B");
				log.info("End Getting Student Details");
				log.info("Student List Size ::"+listStudent.size());
			} catch (Exception e) {
				e.printStackTrace();
			}
			req.getSession().setAttribute("listStudent",listStudent);
		}
			log.info("Redirected To transaction_additionalFee.jsp Page");
		return "views/transaction/transaction_additionalFee";			
	}
	
	@RequestMapping(value="/showAdmissionFromPopup", method=RequestMethod.POST)
	public @ResponseBody String showAdmissionFromPopup(@RequestBody String studentJson,HttpServletRequest req,Map<String, Object> model) throws ParseException {
		log.info("Inside showAdmissionFromPopup Method");
		try{
			Gson gson = new Gson();
		    Student student = gson.fromJson(studentJson, Student.class);
		    log.info("Student Details ::"+student.toString());
			String currentSession=(String)req.getSession().getAttribute("currentSession");
			log.info("Start Checking MRIN Exist Or Not");
			log.info("Inputs :"+student.getCollegeCode()+","+student.getMrNo()+","+currentSession);
			String mrnStaatus=transactionService.checkMrnExist(student.getCollegeCode(), student.getMrNo(),currentSession);
			log.info("End Checking MRIN Exist Or Not");
			log.info("MRIN Exist Status ::"+mrnStaatus);
			if(mrnStaatus.equalsIgnoreCase("MRN_ERROR")){
				return mrnStaatus;
			}
			String config=(String) req.getSession().getAttribute("config");
			log.info("Start Getting Student Details");
			log.info("Inputs ::"+config);
			List<Student> listStudent = transactionService.getStudentDetails(config,"A");
			log.info("End Getting Student Details");
			log.info("List Student Size::"+listStudent.size());
			listStudent.add(student);
			studentList=new ArrayList<Student>();
			studentList.addAll(listStudent);
			log.info("Sending Success response");
			return "SUCCESS";		
		}catch(Exception e){
			log.error("Sending Error response",e);
			e.printStackTrace();
			return "ERROR";
		}
			
	}
	
	@RequestMapping(value="/updateTransaction", method=RequestMethod.POST)
	public @ResponseBody String updateTransaction(@RequestBody String transactionJson,HttpServletRequest req,Map<String, Object> model) throws ParseException {
		log.info("Inside showAdmissionFromPopup Method");
		try{
			Gson gson = new Gson();
			Transaction transaction = gson.fromJson(transactionJson, Transaction.class);
			log.info("Input Transaction Details ::"+transaction.toString());
			List<Student> listStudent=(List<Student>)req.getSession().getAttribute("listStudent");
			Student student =listStudent.get(Integer.parseInt(transaction.getSavedId()));
			log.info("Student Details ::"+student.toString());
			String username=(String) req.getSession().getAttribute("userName");
			if(student.getScienceSelect().equalsIgnoreCase("Select") || student.getScienceSelect().equalsIgnoreCase("")){
		    	student.setScienceSelect("NA");
		    }
			String currentSession= (String) req.getSession().getAttribute("currentSession");
			String updateTransacton = transactionService.updateTransacton(transaction, student, username, currentSession);
			String availableAmount=(String)req.getSession().getAttribute("availablerefAmt");
			if(availableAmount!= null &&  !availableAmount.equalsIgnoreCase("") && updateTransacton.startsWith("Payment")){
				transactionService.updateAvailableAmount(Integer.parseInt(availableAmount), transaction.getTotalAmount(),"10");
			}
		    return updateTransacton;		
		}catch(Exception e){
			log.error("Sending Error response",e);
			e.printStackTrace();
			return "ERROR";
		}
			
	}
	
	
	@RequestMapping(value="/saveTransaction", method=RequestMethod.POST)
	public @ResponseBody String saveTransaction(@RequestBody String transactionJson, HttpServletRequest req,Map<String, Object> model) throws ParseException {
		log.info("Inside saveTransaction Method");
		try{
			log.info("----------------Start Savng Transaction-----------------");
			Gson gson = new Gson();
		    Transaction transaction = gson.fromJson(transactionJson, Transaction.class);
		    log.info("Input Transaction Details ::"+transaction.toString());
		    List<Student> listStudent=(List<Student>)req.getSession().getAttribute("listStudent");
		    Student student =listStudent.get(Integer.parseInt(transaction.getSavedId()));
		    log.info("Student Details ::"+student.toString());
		    transaction.setRollNo(student.getRollNo());
		    
		    transaction.setSession(student.getSession());
		    if(transaction.getHeadName()!=null && !transaction.getHeadName().equalsIgnoreCase("")){
		    	transaction.setPaymentModeDrop("Cash");
		    	//transaction.setTransactiontype("Additional Fee");
		    }else if(transaction.getHeadName()==null || transaction.getHeadName().equalsIgnoreCase("")){
		    	transaction.setPaymentModeDrop("Cash");
		    	transaction.setTransactiontype(student.getFundType());
		    }
		    
		    else  if(transaction.getHeadName()!=null && !transaction.getHeadName().equalsIgnoreCase("") && student.getStream().equalsIgnoreCase("Arts")){
		    	student.setScienceSelect("NA");
		    }/*else  if(transaction.getHeadName()==null && transaction.getHeadName().equalsIgnoreCase("") && student.getStream().equalsIgnoreCase("Science")){
		    	student.setScienceSelect("Bio Science");
		    }*/
		    if(student.getCourse().equalsIgnoreCase("+2") && (student.getStream().equalsIgnoreCase("Science") || student.getStream().equalsIgnoreCase("Arts"))){
		    	student.setScienceSelect("NA");
		    }
		    String username=(String) req.getSession().getAttribute("userName");
		    String currentSession= (String) req.getSession().getAttribute("currentSession");
		    String dashBoardConfig=(String) req.getSession().getAttribute("config");
		    log.info("After Adding Value Transaction Details ::"+transaction.toString());
		    log.info("Start Saving Transaction Details For"+student.getName()+","+transaction.getRollNo());
		    log.info("Input trnsaction Object and student Object and::"+currentSession+","+username+","+dashBoardConfig);
		    String saveTxnStatus=transactionService.savetransaction(transaction, username,student,currentSession,dashBoardConfig);
		    log.info("End Saving Transaction Details");
		    log.info("Transaction Status ::"+saveTxnStatus);
		    if(saveTxnStatus!=null && !saveTxnStatus.equalsIgnoreCase("") && saveTxnStatus.startsWith("Payment")){
		    	String availableAmount=(String) req.getSession().getAttribute("availableAmt");
		    	log.info("Availlable Amount ::"+availableAmount);
		    	String totalAMount=transaction.getTotalAmount();
		    	log.info("Total Amount ::"+totalAMount);
		    	if(dashBoardConfig.equalsIgnoreCase("+2")){
		    		log.info("Start Updating Available Amount For +2");
		    		transactionService.updateAvailableAmount(Integer.parseInt(availableAmount), totalAMount,"4");
		    		log.info("End Updating Available Amount For +2");
		    	}else if(dashBoardConfig.equalsIgnoreCase("UG")){
		    		log.info("Start Updating Available Amount For UG");
		    		transactionService.updateAvailableAmount(Integer.parseInt(availableAmount), totalAMount,"7");
		    		log.info("End Updating Available Amount For UG");
		    	}
		    	log.info("Return SaveTxnStatus ::"+saveTxnStatus);
		    	log.info("Transaction Success");
		    	log.info("----------------End Saving Transaction-----------------");
				return saveTxnStatus;
		    }else if(saveTxnStatus!=null && !saveTxnStatus.equalsIgnoreCase("") && saveTxnStatus.startsWith("Receipt")){
		    	log.info("Return Error");
		    	log.error("----------------End Saving Transaction-----------------");
		    	return saveTxnStatus;
		    }else{
		    	log.info("Return Error");
		    	log.error("----------------End Saving Transaction-----------------");
		    	return "ERROR";
		    }
		}catch(Exception e){
			log.error("Exception While Transaction",e);
			log.error("----------------End Saving Transaction-----------------");
			e.printStackTrace();
			return "ERROR";
		}
	}
	
	@RequestMapping(value="/saveStudentFromAdd",method=RequestMethod.POST)
	public String saveStudentFromAdd(@RequestBody String transactionJson,HttpServletRequest req,Map<String, Object> model) throws ParseException {
		log.info("Inside saveVerifyStatus Method");
		try{
			Gson gson = new Gson();
		    String id = gson.fromJson(transactionJson, String.class);
		    log.info("ID ::"+id);
		    List<Student> listStudent=(List<Student>)req.getSession().getAttribute("listStudent");
		    Student student=listStudent.get(Integer.parseInt(id));
		    log.info("Student Details ::"+student.toString());
			 String username=(String) req.getSession().getAttribute("userName");
			String updatestatus=transactionService.updateverifyStatusAD("10", student.getRollNo(),username,student.getScienceSelect());
			log.info("Update Status ::"+updatestatus);
			return "views/transaction/transaction_additionalFee";
		}catch(Exception e){
			log.error("Error While Updating Verify Status",e);
			e.printStackTrace();
			return "ERROR";
		}
	}	
	
	@RequestMapping(value="/reloadSaveStudent")
	public String reloadSaveStudent(HttpServletRequest req,Map<String, Object> model) throws ParseException {
		List<Student> listStudentDetails = null;
		try {
			String config=(String) req.getSession().getAttribute("config");
			log.info("Start Getting Student Details");
			log.info("Inputs ::"+config);
			listStudentDetails = transactionService.getStudentDetails(config,"B");
			log.info("End Getting Student Details");
			log.info("Student List Size ::"+listStudentDetails.size());
			req.getSession().setAttribute("listStudent",listStudentDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}return "views/transaction/transaction_additionalFee";
	}
	
		
	
	
	
	@RequestMapping(value="/saveVerifyStatus", method=RequestMethod.POST)
	public @ResponseBody String saveVerifyStatus(@RequestBody String transactionJson, HttpServletRequest req,Map<String, Object> model) throws ParseException {
		log.info("Inside saveVerifyStatus Method");
		try{
			Gson gson = new Gson();
		    String transactionId = gson.fromJson(transactionJson, String.class);
		    String transactionType=transactionId.substring(transactionId.length()-2,transactionId.length());
		    transactionId=transactionId.replace(transactionType, "");
		    log.info("Transaction ID ::"+transactionId);
		    List<Student> listStudent=(List<Student>)req.getSession().getAttribute("listStudent");
		    Student student=listStudent.get(Integer.parseInt(transactionId));
		    log.info("Student Details ::"+student.toString());
		    String username=(String) req.getSession().getAttribute("userName");
		    log.info("Start Updating verify Status");
		    log.info("Inputs ::"+"20"+","+student.getRollNo()+","+username);
		    log.info("Transaction Type ::"+transactionType);
		    String status=null;
		    if(transactionType.equalsIgnoreCase("AD")){
		    	status=transactionService.updateverifyStatus("20", student.getRollNo(),username,student.getScienceSelect());	
		    }else if(transactionType.equalsIgnoreCase("AF")){
		         status=transactionService.updateverifyStatusAD("20", student.getRollNo(),username,student.getScienceSelect());
		    }else if(transactionType.equalsIgnoreCase("RF")){
		    	status=transactionService.updateverifyStatusRF("20", student.getRollNo(),username,student.getScienceSelect());
		    }
		    log.info("End Updating Verify Status");
		    log.info("Status ::"+status);
		    return status;
		}catch(Exception e){
			log.error("Error While Updating Verify Status",e);
			e.printStackTrace();
			return "ERROR";
		}
	}	
	
	
	
	
	@RequestMapping(value="/getStudentByStudentId",produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Student getStudentByStudentId(@RequestBody String studentJosn,HttpServletRequest req,Map<String, Object> model) throws ParseException {
		log.info("Inside getStudentByStudentId Method");
		try{
			Gson gson = new Gson();
		    String savedId = gson.fromJson(studentJosn, String.class);
			//String savedId=req.getParameter("callback");
		    List<Student> listStudent=(List<Student>)req.getSession().getAttribute("listStudent");
		    Student student=listStudent.get(Integer.parseInt(savedId));
		    log.info("Student Details ::"+student.toString());
		    return student;
		    //return "";
		}catch(Exception e){
			log.error("Exception WHile Getting Student Details",e);
			e.printStackTrace();
		}
		log.error("Return null");
		return null;	
	}
	
	
	@RequestMapping(value ="/saveStudent" , method = RequestMethod.POST )
	public @ResponseBody String saveStudent(@RequestBody String studentJosn,HttpServletRequest req){
		log.info("Inside saveStudent Method");
		try{
			Gson gson = new Gson();
		    Student student = gson.fromJson(studentJosn, Student.class);
		    log.info("Student Details ::"+student.toString());
		    String username=(String) req.getSession().getAttribute("userName");
		    Student stu=studentList.get(Integer.parseInt(student.getSavedid()));
		    student.setCollegeCode(stu.getCollegeCode());
		    student.setMrNo(stu.getMrNo());
		    student.setAdmStatus("10");
	    	student.setRefStatus("0");
		    if(student.getScienceSelect().equalsIgnoreCase("Select") || student.getScienceSelect().equalsIgnoreCase("")){
		    	student.setScienceSelect("NA");
		    }
		    log.info("Start Saving Student Details");
		    log.info("Inputs ::"+student.toString()+","+username);
		    String status=transactionService.saveStudent(student, username,"A");
		    log.info("End Saving Student Details");
		    log.info("Status ::"+status);
		    if(status==null){
		    	return null;
		    }else{
		    	student.setAdmStatus("10");
		    	student.setAdfStatus("10");
		    	student.setRefStatus("10");
		    	student.setRollNo(status);
		    	List<Student> listStudent=(List<Student>)req.getSession().getAttribute("listStudent");
		    	listStudent.remove(Integer.parseInt(student.getSavedid()));
		    	listStudent.add(Integer.parseInt(student.getSavedid()), student);
		    	return status;
		    }
		}catch(Exception e){
			log.error("Error While Saving Student",e);
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value ="/updateStudent" , method = RequestMethod.POST )
	public @ResponseBody String updateStudent(@RequestBody String studentJosn,HttpServletRequest req){
		log.info("Inside updateStudent Method");
		try{
			Gson gson = new Gson();
		    Student student = gson.fromJson(studentJosn, Student.class);
		    log.info("Student Details ::"+student.toString());
		    String username=(String) req.getSession().getAttribute("userName");
		    List<Student> listStudentde=(List<Student>) req.getSession().getAttribute("listStudent");
		    Student stu=listStudentde.get(Integer.parseInt(student.getSavedid()));
		    student.setScienceSelect(stu.getScienceSelect());
		    student.setRollNo(stu.getRollNo());
		    log.info("Start Saving Student Details");
		    log.info("Inputs ::"+student.toString()+","+username);
		    String status=transactionService.updateStudent(student, username);
		    log.info("End Saving Student Details");
		    log.info("Status ::"+status);
		    if(status==null){
		    	return null;
		    }else{
		    	 List<Student> listStudentDetails = null;
					try {
						String config=(String) req.getSession().getAttribute("config");
						log.info("Start Getting Student Details");
						log.info("Inputs ::"+config);
						listStudentDetails = transactionService.getStudentDetails(config,"B");
						log.info("End Getting Student Details");
						log.info("Student List Size ::"+listStudentDetails.size());
					} catch (Exception e) {
						e.printStackTrace();
					}
					req.getSession().setAttribute("listStudent",listStudentDetails);
		    	return status;
		    }
		}catch(Exception e){
			log.error("Error While Saving Student",e);
			e.printStackTrace();
		}
		return null;
	}
	
	
	@RequestMapping(value ="/saveAddtionalFeeStudent" , method = RequestMethod.POST )
	public @ResponseBody String saveAddtionalFeeStudent(@RequestBody String studentJosn,HttpServletRequest req){
		log.info("Inside saveAddtionalFeeStudent Method");
		try{
			Gson gson = new Gson();
		    Student student = gson.fromJson(studentJosn, Student.class);
		    log.info("Student Details ::"+student.toString());
		    String username=(String) req.getSession().getAttribute("userName");
		    String dashBoardConfig=(String) req.getSession().getAttribute("config");
		    student.setAdmStatus("30");
	    	student.setRefStatus("0");
	    	student.setCourse(dashBoardConfig);
		    String status=transactionService.saveStudent(student, username,"B");
		    log.info("End Saving Student Details");
		    log.info("Status ::"+status);
		    if(status==null){
		    	return null;
		    }else{
		    	return status;
		    }
		}catch(Exception e){
			log.error("Error While Saving Student",e);
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@RequestMapping(value ="/getFinalAmount" , method = RequestMethod.POST )
	public @ResponseBody Long getFinalAmount(@RequestBody String id,HttpServletRequest req){
		log.info("Inside getFinalAmount Method");
		try{
			Gson gson = new Gson();
		    String positionIndex = gson.fromJson(id, String.class);
		    List<Student> listStudent=(List<Student>) req.getSession().getAttribute("listStudent");
		    Student student=listStudent.get(Integer.parseInt(positionIndex));
		    log.info("Student RollNo :"+student.getRollNo());
		    log.info("Start Getting FInal Amount");
		    Long finalAmount=null;
		    if(student.getCourse().equalsIgnoreCase("UG")){
		    	finalAmount=transactionService.getFinalAmountUG(student.getCourse(), student.getStream(),student.getCategory(), student.getYearBatch());
		    }else{
		    	finalAmount=transactionService.getFinalAmount(student.getCourse(), student.getStream(),student.getCategory(), student.getYearBatch());
		    }
		    
		    log.info("End Getting Final Amount");
		    log.info("Final AMount ::"+finalAmount);
		    return finalAmount;
		}catch(Exception e){
			log.error("Exception WHile FInal AMount",e);
			e.printStackTrace();
		}
		log.error("Return Null");
		return null;
	}
	
	/*@RequestMapping(value ="/getStudentAmount" , method = RequestMethod.POST )
	public @ResponseBody String getStudentAmount(@RequestBody String id,HttpServletRequest req){
		log.info("Inside getStudentAmount Method");
		try{
			Gson gson = new Gson();
		    String positionIndex = gson.fromJson(id, String.class);
		    List<Student> listStudent=(List<Student>) req.getSession().getAttribute("listStudent");
		    Student student=listStudent.get(Integer.parseInt(positionIndex));
		    log.info("Student RollNo :"+student.getRollNo());
		    log.info("Start Getting FInal Amount");
		    Long finalAmount=null;
		    String CLCAmount="";
		    if(student.getCourse().equalsIgnoreCase("UG")){
		    	finalAmount=transactionService.getStudentAmountUG(student.getRollNo(), student.getScienceSelect());
		    	CLCAmount=transactionService.getCLCAmountUG(student.getRollNo(), student.getScienceSelect());
		    }else{
		    	finalAmount=transactionService.getStudentAmount(student.getRollNo(), student.getScienceSelect());
		    	CLCAmount=transactionService.getCLCAmount(student.getRollNo(), student.getScienceSelect());
		    	//finalAmount=transactionService.getFinalAmount(student.getCourse(), student.getStream(),student.getCategory(), student.getYearBatch());
		    }
		    log.info("End Getting Final Amount");
		    log.info("Final AMount ::"+finalAmount);
		    log.info("CLC AMount ::"+CLCAmount);
		    String returnAmount=String.valueOf(finalAmount)+"/"+CLCAmount;
		    log.info("Return AMount ::"+CLCAmount);
		    return returnAmount;
		}catch(Exception e){
			log.error("Exception WHile FInal AMount",e);
			e.printStackTrace();
		}
		log.error("Return Null");
		return null;
	}*/
	
	@RequestMapping(value ="/proceed" , method = RequestMethod.POST )
	public @ResponseBody Long proceed(@RequestBody String id,HttpServletRequest req){
		log.info("Inside proceed Method");
		try{
			Gson gson = new Gson();
		    String positionIndex = gson.fromJson(id, String.class);
		    List<Student> listStudent=(List<Student>) req.getSession().getAttribute("listStudent");
		    Student student=listStudent.get(Integer.parseInt(positionIndex));
		    log.info("Student RollNo :"+student.getRollNo());
		    Long finalAmount=null;
		    List<HeadDeatils> headNameAndAmount=null;
		    String currentSession=(String)req.getSession().getAttribute("currentSession");
		    if(student.getCourse().equalsIgnoreCase("UG")){
		    	log.info("Start Getting FInal Amount For UG");
		    	log.info("Inputs In Controller Layer ::"+student.getCourse()+" "+student.getStream()+" "+student.getCategory()+ " "+student.getYearBatch());
		    	finalAmount=transactionService.getFinalAmountUG(student.getCourse(), student.getStream(),student.getCategory(), student.getYearBatch());
		    	log.info("End Getting FInal Amount For UG");
		    	log.info("Start Getting Head Details And Amount For UG");
		    	headNameAndAmount = transactionService.getHeadNameAndAmountUG(student,currentSession);
		    	log.info("End Getting Head Details And Amount For UG:: Size ::"+headNameAndAmount);
		    }else{
		    	log.info("Start Getting FInal Amount For +2");
		    	log.info("Inputs In Controller Layer ::"+student.getCourse()+" "+student.getStream()+" "+student.getCategory()+ " "+student.getYearBatch());
		    	finalAmount=transactionService.getFinalAmount(student.getCourse(), student.getStream(),student.getCategory(), student.getYearBatch());
		    	log.info("End Getting FInal Amount For +2");
		    	log.info("Start Getting Head Details And Amount For +2");
		    	headNameAndAmount = transactionService.getHeadNameAndAmount(student,currentSession);
		    	log.info("End Getting Head Details And Amount For +2:: Size ::"+headNameAndAmount);
		    }
		    
		    log.info("End Getting Final Amount");
		    log.info("Final AMount ::"+finalAmount);
		    req.getSession().setAttribute("headNameNAmount", headNameAndAmount);
		    return finalAmount;
		}catch(Exception e){
			log.error("Exception WHile FInal AMount",e);
			e.printStackTrace();
		}
		log.error("Return Null");
		return null;
	}
	

	@RequestMapping(value ="/getHeadAmount" , method = RequestMethod.POST )
	public @ResponseBody Integer getHeadAmount(@RequestBody String id,HttpServletRequest req){
		log.info("Inside getHeadAmount Method");
		try{
			Gson gson = new Gson();
		    String positionIndex = gson.fromJson(id, String.class);
		    List<HeadDeatils> headNameAndAmount=(List<HeadDeatils>) req.getSession().getAttribute("headNameNAmount");
		    HeadDeatils headDetails=headNameAndAmount.get(Integer.parseInt(positionIndex));
		    return headDetails.getHeadAmount();
		}catch(Exception e){
			log.error("Exception WHile FInal AMount",e);
			e.printStackTrace();
		}
		log.error("Return Null");
		return 0;
	}
	
	@RequestMapping(value ="/getPrintDetails" , method = RequestMethod.POST )
	public @ResponseBody String getPrintDetails(@RequestBody String id,HttpServletRequest req){
		log.info("Inside getPrintDetails Method");
		try{
			Gson gson = new Gson();
			Transaction positionIndex = gson.fromJson(id, Transaction.class);
		    List<Student> listStudent=(List<Student>) req.getSession().getAttribute("listStudent");
		    Student student=listStudent.get(Integer.parseInt(positionIndex.getSavedId()));
		    List<AdditionalTransaction> printDetails = transactionService.getPrintDetails(student.getRollNo(), student.getFundType().equalsIgnoreCase("Additional Fee")?"OAD":positionIndex.getTransactiontype());
		    if(printDetails!=null && !printDetails.isEmpty()){
		    	req.getSession().setAttribute("printDetails", printDetails);
		    	return "SUCCESS";
		    }
		    return "ERROR";
		}catch(Exception e){
			log.error("Exception WHile FInal AMount",e);
			e.printStackTrace();
		}
		log.error("Return Null");
		return "ERROR";
	}
	
	
	
	/*@RequestMapping("/printRefund")
	public String printRefund(HttpServletRequest req,Map<String, Object> model) {
		log.info("Inside print Method");
		 String printId=req.getParameter("printid");
		 log.info("PrintId ::"+printId);
		 String tran_Type=req.getParameter("tran_Type");
		 log.info("Transaction Type ::"+tran_Type);
		 List<Student> listStudent=(List<Student>)req.getSession().getAttribute("listStudent");
		 Student student =listStudent.get(Integer.parseInt(printId));
		 log.info("Student RollNo ::"+student.getRollNo());
	}*/
	
	
	
	@RequestMapping("/print")
	public String print(HttpServletRequest req,Map<String, Object> model) throws ParseException {
		 log.info("Inside print Method");
		 String printId=req.getParameter("printid");
		 log.info("PrintId ::"+printId);
		 String tran_Type=req.getParameter("tran_Type");
		 log.info("Transaction Type ::"+tran_Type);
		 List<Student> listStudent=(List<Student>)req.getSession().getAttribute("listStudent");
		 Student student =listStudent.get(Integer.parseInt(printId));
		 log.info("Student RollNo ::"+student.getRollNo());
		 String transaction_type="";
		 if(tran_Type.trim().equalsIgnoreCase("Admission")){
			 transaction_type="A";
		 }else if(tran_Type.trim().equalsIgnoreCase("Re-Admission")){
			 transaction_type="R";
		 }else if(tran_Type.trim().equalsIgnoreCase("Additional Fee")){
			 transaction_type="AD";
		 }else if(tran_Type.trim().equalsIgnoreCase("Fine")){
			 transaction_type="F";
		 }else if(tran_Type.trim().equalsIgnoreCase("Refund")){
			 transaction_type="RF";
		 }else if(tran_Type.trim().equalsIgnoreCase("Rejection")){
			 transaction_type="RE";
		 }
		 if (student.getFundType().equalsIgnoreCase("Additional Fee")){
			 transaction_type="OAD";
		 }
		 long amount=0;
		 String bankName="";
		 String chequeDDNo="";
		 String receiptNo="";
		 String tran_Date="";
		 String dashBoardConfig=(String) req.getSession().getAttribute("config");
		 if(transaction_type.equalsIgnoreCase("A") || transaction_type.equalsIgnoreCase("R")){
			 log.info("Strat Print Type Is Admission/Re-Admission");
			 log.info("Start Getting Receipt Number");
			 receiptNo =transactionService.getRecepitNumber(transaction_type, student.getRollNo(),student.getScienceSelect());
			 log.info("End Getting Receipt Number");
			 log.info("Receipt Number ::"+receiptNo);
			 student.setReceiptNo(receiptNo);
			 log.info("Start Transaction Date");
			 tran_Date=transactionService.getTransactionDate(transaction_type, student.getRollNo(),student.getScienceSelect());
			 log.info("End Transaction Date");
			 student.setCreated_Date(tran_Date);
			 if(dashBoardConfig.equalsIgnoreCase("+2")){
				 log.info("Start Get Head Details For +2 Print ");
				 amount= transactionService.getHeadDetailsForprint(student.getRollNo(),transaction_type,req,student.getScienceSelect());
				 log.info("End Get Head Details For +2 Print");
			 }else if(dashBoardConfig.equalsIgnoreCase("UG")){
				 log.info("Start Get Head Details For UG Print");
				  amount= transactionService.getHeadDetailsForprintUG(student.getRollNo(),transaction_type,req,student.getYearBatch(),student.getScienceSelect());
				 log.info("End Get Head Details For UG Print ");
			 }else{
				 
			 }
		   	
		 }else if (transaction_type.equalsIgnoreCase("RF") || transaction_type.equalsIgnoreCase("RE")){
			 log.info("Start Print Type Is Refund/Rjection");
			 log.info("Start Getting Receipt Number");
			 receiptNo =transactionService.getRecepitNumberRF(transaction_type, student.getRollNo(),student.getScienceSelect());
			 log.info("End Getting Receipt Number");
			 log.info("Receipt Number ::"+receiptNo);
			 student.setReceiptNo(receiptNo);
			 log.info("Start Transaction Date");
			 tran_Date=transactionService.getTransactionDateRF(transaction_type, student.getRollNo(),student.getScienceSelect());
			 log.info("End Transaction Date");
			 student.setCreated_Date(tran_Date);
			 if(transaction_type.equalsIgnoreCase("RF")){
				 bankName= transactionService.getBankName(student.getRollNo(), student.getScienceSelect());
				 chequeDDNo= transactionService.getChequeDDNo(student.getRollNo(), student.getScienceSelect());
				 amount= transactionService.getTotalAmount(student.getRollNo(),transaction_type,student.getScienceSelect());
				 req.setAttribute("bankName", bankName);
			   	 req.setAttribute("chequeNo", chequeDDNo);	 
			   	 req.setAttribute("tranType", "Refund");
			   	req.setAttribute("Status", "TC Taken");
			 }else{
				 req.setAttribute("tranType", "Rejection");
				 req.setAttribute("Status", "Rejection");
			 }
		 }else{
			 String clc_type=req.getParameter("clc_type");
			 if(clc_type!=null && !clc_type.equalsIgnoreCase("") && clc_type.equalsIgnoreCase("CLC")){
				 log.info("Start Print Type Is CLC");
				 log.info("Start Getting Receipt Number For CLC");
				 List<Map<String, Object>> clcMap=transactionService.getRecepitNumberCLC(transaction_type, student.getRollNo(),student.getScienceSelect());
				 receiptNo=(String) clcMap.get(0).get("RECEIPT_NUMBER");
				 String bookNo=(String)clcMap.get(0).get("BOOK_NO");	
				 String amt=(String)clcMap.get(0).get("HEAD_AMOUNT");	
				 amount=Long.parseLong(amt);
				 tran_Date=transactionService.getTransactionDateCLC(transaction_type,student.getRollNo(),student.getScienceSelect());
				 log.info("CLC Receipt Number ::"+receiptNo);
				 log.info("CLC Book Number ::"+bookNo);
				 log.info("CLC Amount ::"+amount);
				 log.info("CLC TranDate ::"+tran_Date);
				 student.setReceiptNo(receiptNo+"/"+bookNo);
				 student.setCreated_Date(tran_Date);
				 req.setAttribute("headName", "CLC");
			 }else{
				 log.info("Start Print Type Is Additional Fee/Fine");
				 String printOption=req.getParameter("print_Option");
				 List<AdditionalTransaction> printDetails =(List<AdditionalTransaction>) req.getSession().getAttribute("printDetails");
				 AdditionalTransaction addTransaction=printDetails.get(Integer.parseInt(printOption));
				 log.info("Start Getting Receipt Number For Additional Fee");
				 receiptNo=addTransaction.getRECEIPT_NUMBER();
				 log.info("End Getting Receipt Number Additional Fee");
				 log.info("Receipt Number Additional Fee::"+receiptNo);
				 String bookNo=addTransaction.getBOOK_NO();
				 student.setReceiptNo(receiptNo+"/"+bookNo);
				 log.info("Start getting Transaction Date For Additional Fee");
				 Date tranDate=addTransaction.getTRAN_DATE();
				 SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
				 tran_Date=format.format(tranDate);
				 log.info("End Transaction Date For ");
				 student.setCreated_Date(tran_Date);
				 amount=Long.parseLong(addTransaction.getHEAD_AMOUNT());
				 bankName=addTransaction.getHEAD_NAME();
				 req.setAttribute("headName", bankName);	 
			 }
		 }
	   		String inWords=DataUtil.convert(amount);
	   		log.info("After Converting Amount In Words ::"+inWords);
	   		req.setAttribute("amount", amount);
	   		req.setAttribute("amntinwrds", inWords);
		 req.setAttribute("printstudent", student);
		 if(transaction_type.equalsIgnoreCase("A") || transaction_type.equalsIgnoreCase("R")){
			 log.info("Forwarding The Request For Admission/Re-Admission");
			 return "views/print/print";	 
		 }else if (transaction_type.equalsIgnoreCase("RF") || transaction_type.equalsIgnoreCase("RE")){
			 log.info("Forwarding The Request For Refund/Rejection");
			 return "views/print/print_refund";
		 }else{
			 log.info("Forwarding The Request For Additional Fee");
			 return "views/print/print_additional";
		 }
		 			
	}
	
	
}
