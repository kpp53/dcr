package com.daily.collection.web.reports.service;

import org.springframework.stereotype.*;
import com.daily.collection.web.reports.dao.*;
import org.springframework.beans.factory.annotation.*;
import javax.servlet.http.*;
import com.daily.collection.web.analytic.bean.*;
import java.math.*;
import com.daily.collection.web.creation.bean.*;
import java.text.*;
import java.util.*;
import com.daily.collection.web.reports.bean.*;
import org.springframework.jdbc.core.namedparam.*;

@Service
public class ReportsService implements IReportService
{
    @Autowired
    IReportsDao reportsDao;
    
    public void getAllreport(final Reports reports, final HttpServletRequest request) throws ParseException {
        final String config = (String)request.getSession().getAttribute("config");
        final Object[] reportAdmParams = { "A", reports.getSession(), reports.getDateFrom(), reports.getDateTo(), config };
        final Map<String, Object> allAdmissionreport = (Map<String, Object>)this.reportsDao.getAllreport(reportAdmParams);
        request.getSession().setAttribute("admissionData", (Object)allAdmissionreport);
        final Object[] reportReAdmParams = { "R", reports.getSession(), reports.getDateFrom(), reports.getDateTo(), config };
        final Map<String, Object> allReAdmissionreport = (Map<String, Object>)this.reportsDao.getAllreport(reportReAdmParams);
        request.getSession().setAttribute("readmissionData", (Object)allReAdmissionreport);
        final Object[] reportCafParams = { "C", reports.getSession(), reports.getDateFrom(), reports.getDateTo(), config };
        final Map<String, Object> allcafreport = (Map<String, Object>)this.reportsDao.getAllreport(reportCafParams);
        request.getSession().setAttribute("allcafData", (Object)allcafreport);
        final Object[] reportmisParams = { "M", reports.getSession(), reports.getDateFrom(), reports.getDateTo(), config };
        final Map<String, Object> allmisreport = (Map<String, Object>)this.reportsDao.getAllreport(reportmisParams);
        request.getSession().setAttribute("misData", (Object)allmisreport);
        final Object[] reportAdditionalFee = { "AD", reports.getSession(), reports.getDateFrom(), reports.getDateTo(), config };
        final Map<String, Object> allAdditionalFeeReport = (Map<String, Object>)this.reportsDao.getAllAddreport(reportAdditionalFee);
        request.getSession().setAttribute("allAdditionalFeeReport", (Object)allAdditionalFeeReport);
        final Object[] reportAdditionalFeeForStudent = { "OAD", reports.getSession(), reports.getDateFrom(), reports.getDateTo(), config };
        final Map<String, Object> allAdditionalFeeReportForOldStudent = (Map<String, Object>)this.reportsDao.getAllAddreport(reportAdditionalFeeForStudent);
        request.getSession().setAttribute("allAdditionalFeeReportForOldStudent", (Object)allAdditionalFeeReportForOldStudent);
        final Object[] reportFineParams = { "F", reports.getSession(), reports.getDateFrom(), reports.getDateTo(), config };
        final Map<String, Object> allFinereport = (Map<String, Object>)this.reportsDao.getAllAddreport(reportFineParams);
        request.getSession().setAttribute("allFinereport", (Object)allFinereport);
        final Object[] reportRefunParams = { "RF", reports.getSession(), reports.getDateFrom(), reports.getDateTo(), config };
        final Map<String, Object> allRefundreport = (Map<String, Object>)this.reportsDao.getAllRefundReport(reportRefunParams);
        request.getSession().setAttribute("allRefundreport", (Object)allRefundreport);
    }
    
    public String getheadDetails(final Reports reports, final HttpServletRequest request) {
        final Set<String> academicSessionId = new HashSet<String>();
        academicSessionId.add(reports.getSession());
        final Set<String> tranTypeId = new HashSet<String>();
        if (reports.getFundType() == null || reports.getFundType().equalsIgnoreCase("")) {
            return "ERROR";
        }
        if (reports.getFundType().equalsIgnoreCase("all")) {
            tranTypeId.add("A");
            tranTypeId.add("R");
            tranTypeId.add("AD");
            tranTypeId.add("F");
            tranTypeId.add("OAD");
        }
        else if (reports.getFundType().equalsIgnoreCase("Admission")) {
            tranTypeId.add("A");
        }
        else if (reports.getFundType().equalsIgnoreCase("Re-Admission")) {
            tranTypeId.add("R");
        }
        else if (reports.getFundType().equalsIgnoreCase("Additional Fee")) {
            tranTypeId.add("AD");
        }
        else if (reports.getFundType().equalsIgnoreCase("Fine")) {
            tranTypeId.add("F");
        }
        else if (reports.getFundType().equalsIgnoreCase("Old Student Additional Fee")) {
            tranTypeId.add("OAD");
        }
        final String config = (String)request.getSession().getAttribute("config");
        final Set<String> courseId = new HashSet<String>();
        courseId.add(config);
        final Set<String> streamId = new HashSet<String>();
        if (reports.getStream() == null || reports.getStream().equalsIgnoreCase("")) {
            return "ERROR";
        }
        if (reports.getStream().equalsIgnoreCase("all")) {
            final List<String> stream = (List<String>)request.getSession().getAttribute("streamMap");
            streamId.addAll(stream);
        }
        else {
            streamId.add(reports.getStream());
        }
        final Set<String> categoriesId = new HashSet<String>();
        final Set<String> categories = new HashSet<String>();
        if (reports.getCategory() == null || reports.getCategory().equalsIgnoreCase("")) {
            return "ERROR";
        }
        if (reports.getCategory().equalsIgnoreCase("all")) {
            categories.add("General");
            categories.add("OBC");
            categories.add("SC");
            categories.add("ST");
            categories.add("Girls");
            categoriesId.add("Gen/OBC");
            categoriesId.add("SC/ST/Girls");
        }
        else if (reports.getCategory() != null && (reports.getCategory().equalsIgnoreCase("General") || reports.getCategory().equalsIgnoreCase("OBC"))) {
            categoriesId.add("Gen/OBC");
        }
        else {
            categoriesId.add("SC/ST/Girls");
        }
        final Set<String> yearBatchId = new HashSet<String>();
        if (reports.getClassType() != null && !reports.getClassType().equalsIgnoreCase("")) {
            if (reports.getClassType().equalsIgnoreCase("all")) {
                yearBatchId.add("1st");
                yearBatchId.add("2nd");
                if (!config.equalsIgnoreCase("+2")) {
                    yearBatchId.add("3rd");
                }
            }
            else {
                yearBatchId.add(reports.getClassType());
            }
        }
        final MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("academicSessionId", (Object)academicSessionId);
        parameters.addValue("tranTypeId", (Object)tranTypeId);
        if (reports.getCategory().equalsIgnoreCase("all")) {
            parameters.addValue("category", (Object)categories);
        }
        else {
            parameters.addValue("category", (Object)reports.getCategory());
        }
        parameters.addValue("courseId", (Object)courseId);
        parameters.addValue("streamId", (Object)streamId);
        parameters.addValue("categoriesId", (Object)categoriesId);
        parameters.addValue("yearBatchId", (Object)yearBatchId);
        parameters.addValue("fromDateId", (Object)reports.getDateFrom());
        parameters.addValue("toDateId", (Object)reports.getDateTo());
        if (config.equalsIgnoreCase("+2")) {
            final List<Map<String, Object>> headDeTails = (List<Map<String, Object>>)this.reportsDao.getHeadDeTails(parameters);
            final List<Map<String, Object>> studentCount = (List<Map<String, Object>>)this.reportsDao.getStudentCount(parameters);
            final List<Map<String, Object>> headDeTailsUGCLC = (List<Map<String, Object>>)this.reportsDao.getHeadDeTailsUGCLC(parameters);
            BigDecimal bd = null;
            if (headDeTails != null && !headDeTails.isEmpty() && studentCount != null && !studentCount.isEmpty()) {
                HeadDeatils headDetails = null;
                final Map<String, Object> map = headDeTails.get(0);
                final Map<String, Object> studentCountMap = studentCount.get(0);
                final Map<String, Object> headDetailsUGCLCMap = headDeTailsUGCLC.get(0);
                final List<HeadDeatils> headDetailsList = new ArrayList<HeadDeatils>();
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Adm/Re-Adm");
                bd = (BigDecimal) map.get("Adm_ReAdm");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("ADM_ReadM"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Tuition Fee");
                bd = (BigDecimal) map.get("TUITION_FEES");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("Tution"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("CLC");
                bd = (BigDecimal) map.get("CLC");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("CLC"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Migration/Original Certificate");
                bd = (BigDecimal) map.get("MIGRATION_ORIGINAL_CERTIFICATE");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("MIGRATION_ORIGINAL_CERTIFICATE"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Library");
                bd = (BigDecimal) map.get("LIBRARY");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("LIBRARY"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Sports & Games");
                bd = (BigDecimal) map.get("SPORTS_GAMES");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("SPORTS_GAMES"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Medical");
                bd = (BigDecimal) map.get("MEDICAL");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("MEDICAL"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Meical Aid(Student & Fund)");
                bd = (BigDecimal) map.get("MEDICAL_AID");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("MEDICAL_AID"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Labrotary");
                bd = (BigDecimal) map.get("LABORTARY");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("LABORTARY"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Extra Caricular");
                bd = (BigDecimal) map.get("EXTRA_CARICULAR");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("EXTRA_CARICULAR"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Poor Boys Fund");
                bd = (BigDecimal) map.get("POOR_BOYS_FUND");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("POOR_BOYS_FUND"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Council");
                bd = (BigDecimal) map.get("COUNCIL");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("COUNCIL"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Magazine");
                bd = (BigDecimal) map.get("MAGAZINE");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("MAGAZINE"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("College Exam");
                bd = (BigDecimal) map.get("COLLEGE_EXAM");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("COLLEGE_EXAM"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Puja");
                bd = (BigDecimal) map.get("PUJA");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("PUJA"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("College Day");
                bd = (BigDecimal) map.get("COLLEGE_DAY");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("COLLEGE_DAY"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Proctorial");
                bd = (BigDecimal) map.get("PROCTORIAL");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("PROCTORIAL"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Development One");
                bd = (BigDecimal) map.get("DEVELOPMENT");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("DEVELOPMENT"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Science/Arts Society");
                bd = (BigDecimal) map.get("SC_ARTS_SOCIETY");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("SC_ARTS_SOCIETY"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Red Cross");
                bd = (BigDecimal) map.get("RED_CROSS");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("RED_CROSS"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Calendar");
                bd = (BigDecimal) map.get("CALENDER");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("CALENDER"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Syllabus");
                bd = (BigDecimal) map.get("SYLLABUS");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("SYLLABUS"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Other Fees");
                bd = (BigDecimal) map.get("OTHER_FEES");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("OTHER_FEES"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Building");
                bd = (BigDecimal) map.get("BUILDING");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("BUILDING"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Academic");
                bd = (BigDecimal) map.get("ACADEMIC");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("ACADEMIC"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Reading Room");
                bd = (BigDecimal) map.get("READING_ROOM");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("READING_ROOM"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Registration");
                bd = (BigDecimal) map.get("REGISTRATION");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("REGISTRATION"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Labrotary Caution Money");
                bd = (BigDecimal) map.get("LAB_CAUTION_MONEY");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("LAB_CAUTION_MONEY"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Library Caution Money");
                bd = (BigDecimal) map.get("LIB_CAUTION_MONEY");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("LIB_CAUTION_MONEY"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Identity Card");
                bd = (BigDecimal) map.get("IDENTITY_CARD");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("IDENTITY_CARD"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Recognition");
                bd = (BigDecimal) map.get("RECOGNITION");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("RECOGNITION"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Scout And Guide");
                bd = (BigDecimal) map.get("SCOUT_AND_GUIDE");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("SCOUT_AND_GUIDE"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Tuition Fees For 10 Month");
                bd = (BigDecimal) map.get("TUITIONFEES_FOR_MONTH");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("TUITIONFEES_FOR_MONTH"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Development For 10 Month");
                bd = (BigDecimal) map.get("DEVELOPMENT_FOR_10_MONTH");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("DEVELOPMENT_FOR_10_MONTH"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Social Service Guide");
                bd = (BigDecimal) map.get("SOCIAL_SCIENCE");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("SOCIAL_SCIENCE"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Flag Day");
                bd = (BigDecimal) map.get("FLAG_DAY");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("FLAG_DAY"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("EMS Fee");
                bd = (BigDecimal) map.get("EMS_FEE");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("EMS_FEE"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Development Two");
                bd = (BigDecimal) map.get("DEVELOPMENT_TWO");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("DEVELOPMENT_TWO"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("CAF");
                bd = (BigDecimal) map.get("CAF");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("CAF"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("CHSE Exam Fee");
                bd = (BigDecimal) map.get("CHSE_EXAM_FEE");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("CHSE_EXAM_FEE"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("RoundUp");
                bd = (BigDecimal) map.get("ROUNDUP");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("ROUNDUP"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Additional Fee");
                bd = (BigDecimal) map.get("ADDITIONAL_FEE");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("ADDITIONAL_FEE"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Fine Fee");
                bd = (BigDecimal) map.get("FINE");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("FINE"));
                headDetailsList.add(headDetails);
                headDetails = new HeadDeatils();
                headDetails.setHeadname("Miscellanious");
                bd = (BigDecimal) map.get("MISCELLANIOUS");
                headDetails.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails.setStudentCount((BigDecimal)studentCountMap.get("MISCELLANIOUS"));
                headDetailsList.add(headDetails);
                request.getSession().setAttribute("headDetailsMap", (Object)headDetailsList);
                return "SUCCESS";
            }
            return "ERROR";
        }
        else {
            if (!config.equalsIgnoreCase("UG")) {
                return "ERROR";
            }
            final List<Map<String, Object>> headDeTails = (List<Map<String, Object>>)this.reportsDao.getHeadDeTailsUG(parameters);
            final List<Map<String, Object>> studentCount = (List<Map<String, Object>>)this.reportsDao.getStudentCountUG(parameters);
            final List<Map<String, Object>> headDeTailsUGCLC = (List<Map<String, Object>>)this.reportsDao.getHeadDeTailsUGCLC(parameters);
            BigDecimal bd = null;
            final List<HeadDeatils> headDetailsList2 = new ArrayList<HeadDeatils>();
            if (headDeTails != null && !headDeTails.isEmpty() && studentCount != null && !studentCount.isEmpty()) {
                HeadDeatils headDetails2 = null;
                final Map<String, Object> map2 = headDeTails.get(0);
                final Map<String, Object> studentCountMap2 = studentCount.get(0);
                final Map<String, Object> headDetailsUGCLCMap2 = headDeTailsUGCLC.get(0);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Adm/Re-Adm");
                bd = (BigDecimal) map2.get("Adm_ReAdm");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("ADM_ReadM"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Tuition Fee");
                bd = (BigDecimal) map2.get("TUITION_FEES");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("Tution"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("CLC");
                bd = (BigDecimal) headDetailsUGCLCMap2.get("HeadAmount");
                int clcAdd = (bd == null) ? 0 : bd.intValue();
                bd = (BigDecimal) map2.get("CLC");
                clcAdd += ((bd == null) ? 0 : bd.intValue());
                headDetails2.setHeadAmount(clcAdd);
                BigDecimal studentCnt = (headDetailsUGCLCMap2.get("RollNO") == BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) headDetailsUGCLCMap2.get("RollNO");
                studentCnt = studentCnt.add((studentCountMap2.get("CLC") == BigDecimal.ZERO) ? BigDecimal.ZERO : (BigDecimal) studentCountMap2.get("CLC"));
                headDetails2.setStudentCount(studentCnt);
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Library");
                bd = (BigDecimal) map2.get("LIBRARY");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("LIBRARY"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Sports & Games");
                bd = (BigDecimal) map2.get("SPORTS_GAMES");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("SPORTS_GAMES"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Medical");
                bd = (BigDecimal) map2.get("MEDICAL");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("MEDICAL"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Labrotary");
                bd = (BigDecimal) map2.get("LABORTARY");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("LABORTARY"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Extra Caricular");
                bd = (BigDecimal) map2.get("EXTRA_CARICULAR");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("EXTRA_CARICULAR"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Poor Boys Fund");
                bd = (BigDecimal) map2.get("POOR_BOYS_FUND");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("POOR_BOYS_FUND"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Magazine");
                bd = (BigDecimal) map2.get("MAGAZINE");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("MAGAZINE"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("College Exam");
                bd = (BigDecimal) map2.get("COLLEGE_EXAM");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("COLLEGE_EXAM"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Puja");
                bd = (BigDecimal) map2.get("PUJA");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("PUJA"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("College Day");
                bd = (BigDecimal) map2.get("COLLEGE_DAY");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("COLLEGE_DAY"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("B.U. Fee");
                bd = (BigDecimal) map2.get("BU_FEE");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("BU_FEE"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Proctorial");
                bd = (BigDecimal) map2.get("PROCTORIAL");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("PROCTORIAL"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Development One");
                bd = (BigDecimal) map2.get("DEVELOPMENT");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("DEVELOPMENT"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Science/Arts Society");
                bd = (BigDecimal) map2.get("SC_ARTS_SOCIETY");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("SC_ARTS_SOCIETY"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Red Cross");
                bd = (BigDecimal) map2.get("RED_CROSS");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("RED_CROSS"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Calendar");
                bd = (BigDecimal) map2.get("CALENDER");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("CALENDER"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Other Fees");
                bd = (BigDecimal) map2.get("OTHER_FEES");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("OTHER_FEES"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Building");
                bd = (BigDecimal) map2.get("BUILDING");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("BUILDING"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Reading Room");
                bd = (BigDecimal) map2.get("READING_ROOM");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("READING_ROOM"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Registration");
                bd = (BigDecimal) map2.get("REGISTRATION");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("REGISTRATION"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Labrotary Caution Money");
                bd = (BigDecimal) map2.get("LAB_CAUTION_MONEY");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("LAB_CAUTION_MONEY"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Library Caution Money");
                bd = (BigDecimal) map2.get("LIB_CAUTION_MONEY");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("LIB_CAUTION_MONEY"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Identity Card");
                bd = (BigDecimal) map2.get("IDENTITY_CARD");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("IDENTITY_CARD"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Cultural");
                bd = (BigDecimal) map2.get("CULTURAL");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("CULTURAL"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Scout And Guide");
                bd = (BigDecimal) map2.get("SCOUT_AND_GUIDE");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("SCOUT_AND_GUIDE"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Tuition Fees For 10 Month");
                bd = (BigDecimal) map2.get("TUITIONFEES_FOR_MONTH");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("TUITIONFEES_FOR_MONTH"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Development For 10 Month");
                bd = (BigDecimal) map2.get("DEVELOPMENT_FOR_10_MONTH");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("DEVELOPMENT_FOR_10_MONTH"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Social Service Guide");
                bd = (BigDecimal) map2.get("SOCIAL_SCIENCE");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("SOCIAL_SCIENCE"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Flag Day");
                bd = (BigDecimal) map2.get("FLAG_DAY");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("FLAG_DAY"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Hons Fee");
                bd = (BigDecimal) map2.get("HONS_FEE");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("HONS_FEE"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Seminar Fee");
                bd = (BigDecimal) map2.get("SEMINAR_FEE");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("SEMINAR_FEE"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Additional Fee");
                bd = (BigDecimal) map2.get("ADDITIONAL_FEE");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("ADDITIONAL_FEE"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Development Fee Two");
                bd = (BigDecimal) map2.get("DEVELOPMENT_TWO");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("DEVELOPMENT_TWO"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("BU Examination Fee");
                bd = (BigDecimal) map2.get("BU_EXAMINATION");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("BU_EXAMINATION"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("CAF");
                bd = (BigDecimal) map2.get("CAF");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("CAF"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Fine");
                bd = (BigDecimal) map2.get("FINE");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("FINE"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("Miscellanious");
                bd = (BigDecimal) map2.get("MISCELLANIOUS");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("MISCELLANIOUS"));
                headDetailsList2.add(headDetails2);
                headDetails2 = new HeadDeatils();
                headDetails2.setHeadname("RoundUp");
                bd = (BigDecimal) map2.get("ROUNDUP");
                headDetails2.setHeadAmount((bd == null) ? 0 : bd.intValue());
                headDetails2.setStudentCount((BigDecimal)studentCountMap2.get("ROUNDUP"));
                headDetailsList2.add(headDetails2);
                request.getSession().setAttribute("headDetailsMap", (Object)headDetailsList2);
                return "SUCCESS";
            }
            return "ERROR";
        }
    }
    
    public String getstudentDetails(final Reports reports, final HttpServletRequest request) {
        final Set<String> academicSessionId = new HashSet<String>();
        academicSessionId.add(reports.getSession());
        final Set<String> tranTypeId = new HashSet<String>();
        if (reports.getFundType() == null || reports.getFundType().equalsIgnoreCase("")) {
            return "ERROR";
        }
        if (reports.getFundType().equalsIgnoreCase("all")) {
            tranTypeId.add("A");
            tranTypeId.add("R");
            tranTypeId.add("AD");
            tranTypeId.add("F");
            tranTypeId.add("RF");
            tranTypeId.add("RE");
            tranTypeId.add("OAD");
        }
        else if (reports.getFundType().equalsIgnoreCase("Admission")) {
            tranTypeId.add("A");
        }
        else if (reports.getFundType().equalsIgnoreCase("Re-Admission")) {
            tranTypeId.add("R");
        }
        else if (reports.getFundType().equalsIgnoreCase("Additional Fee")) {
            tranTypeId.add("AD");
        }
        else if (reports.getFundType().equalsIgnoreCase("Fine")) {
            tranTypeId.add("F");
        }
        else if (reports.getFundType().equalsIgnoreCase("Old Student Additional Fee")) {
            tranTypeId.add("OAD");
        }
        final String config = (String)request.getSession().getAttribute("config");
        final Set<String> courseId = new HashSet<String>();
        courseId.add(config);
        final Set<String> streamId = new HashSet<String>();
        if (reports.getStream() == null || reports.getStream().equalsIgnoreCase("")) {
            return "ERROR";
        }
        if (reports.getStream().equalsIgnoreCase("all")) {
            final List<String> stream = (List<String>)request.getSession().getAttribute("streamMap");
            streamId.addAll(stream);
        }
        else {
            streamId.add(reports.getStream());
        }
        final Set<String> categoriesId = new HashSet<String>();
        final Set<String> categories = new HashSet<String>();
        if (reports.getCategory() != null && !reports.getCategory().equalsIgnoreCase("")) {
            if (reports.getCategory().equalsIgnoreCase("all")) {
                categories.add("General");
                categories.add("OBC");
                categories.add("SC");
                categories.add("ST");
                categories.add("Girls");
                categoriesId.add("Gen/OBC");
                categoriesId.add("SC/ST/Girls");
            }
            else {
                if (reports.getCategory() != null && (reports.getCategory().equalsIgnoreCase("General") || reports.getCategory().equalsIgnoreCase("OBC"))) {
                    categoriesId.add("Gen/OBC");
                }
                else {
                    categoriesId.add("SC/ST/Girls");
                }
                categories.add(reports.getCategory());
            }
            final Set<String> yearBatchId = new HashSet<String>();
            if (reports.getClassType() != null && !reports.getClassType().equalsIgnoreCase("")) {
                if (reports.getClassType().equalsIgnoreCase("all")) {
                    yearBatchId.add("1st");
                    yearBatchId.add("2nd");
                    if (!config.equalsIgnoreCase("+2")) {
                        yearBatchId.add("3rd");
                    }
                }
                else {
                    yearBatchId.add(reports.getClassType());
                }
            }
            final MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("tranTypeId", (Object)tranTypeId);
            parameters.addValue("fromDateId", (Object)reports.getDateFrom());
            parameters.addValue("toDateId", (Object)reports.getDateTo());
            parameters.addValue("academicSessionId", (Object)academicSessionId);
            parameters.addValue("courseId", (Object)courseId);
            parameters.addValue("streamId", (Object)streamId);
            parameters.addValue("categoriesId", (Object)categoriesId);
            parameters.addValue("yearBatchId", (Object)yearBatchId);
            if (reports.getCategory().equalsIgnoreCase("all")) {
                parameters.addValue("category", (Object)categories);
            }
            else {
                parameters.addValue("category", (Object)reports.getCategory());
            }
            final List<Map<String, Object>> studentDetails = (List<Map<String, Object>>)this.reportsDao.getstudentDetails(parameters);
            final List<Student> studentListreport = new ArrayList<Student>();
            Student student = null;
            for (final Map<String, Object> map : studentDetails) {
                student = new Student();
                student.setName((String)map.get("NAME"));
                student.setRollNo((String)map.get("ROLLNO"));
                student.setDob((String)map.get("DOB"));
                final BigDecimal bd = (BigDecimal) map.get("TOTAL_AMOUNT");
                student.setTotalAmount(bd.intValue());
                student.setStatus("Continue");
                final Date date = (Date) map.get("TRANSACTION_DATE");
                final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                student.setCreated_Date(format.format(date));
                student.setSession((String)map.get("AADEMIC_SESSION"));
                student.setStream((String)map.get("STREAM"));
                student.setYearBatch((String)map.get("YEAR_BATCH"));
                student.setCategory((String)map.get("CATEGORY"));
                student.setCollegeCode((String)map.get("COLLEGE_CODE"));
                student.setMrNo((String)map.get("MRN_NO"));
                student.setFundType((String)map.get("FUND_TYPE"));
                student.setReceiptNo(map.get("RECEIPT_NO") + "/" + map.get("BOOK_NO"));
                student.setPaymentModeDrop((String)map.get("TRANSACTION_MODE"));
                student.setScienceSelect((String)map.get("SCIENCE_OPTION"));
                studentListreport.add(student);
            }
            System.out.println("After Getting Student Details :: " + studentListreport);
            final List<Map<String, Object>> studentRFDetails = (List<Map<String, Object>>)this.reportsDao.getstudentDetailsRF(parameters);
            for (final Map<String, Object> map2 : studentRFDetails) {
                student = new Student();
                student.setName((String)map2.get("NAME"));
                student.setRollNo((String)map2.get("ROLLNO"));
                student.setDob((String)map2.get("DOB"));
                student.setTotalAmount(Integer.parseInt((String) map2.get("REFUND_AMOUNT")));
                student.setStatus((String)map2.get("STATUS"));
                final Date date = (Date) map2.get("CREATED_DATE");
                final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                student.setCreated_Date(format.format(date));
                final Date date2 = (Date) map2.get("TRAN_DATE");
                student.setDateOfRefund(format.format(date2));
                student.setSession((String)map2.get("AADEMIC_SESSION"));
                student.setStream((String)map2.get("STREAM"));
                student.setYearBatch((String)map2.get("YEAR_BATCH"));
                student.setCategory((String)map2.get("CATEGORY"));
                student.setCollegeCode((String)map2.get("COLLEGE_CODE"));
                student.setMrNo((String)map2.get("MRN_NO"));
                student.setFundType((String)map2.get("FUND_TYPE"));
                student.setReceiptNo((String)map2.get("RECEIPT_NUMBER"));
                student.setPaymentModeDrop((String)map2.get("TRANSACTION_MODE"));
                student.setScienceSelect((String)map2.get("SCIENCE_OPTION"));
                studentListreport.add(student);
            }
            System.out.println("After Getting Student Refund  Details :: " + studentListreport);
            final List<Map<String, Object>> studentAdDetails = (List<Map<String, Object>>)this.reportsDao.getstudentDetailsAD(parameters);
            for (final Map<String, Object> map3 : studentAdDetails) {
                student = new Student();
                student.setName((String)map3.get("NAME"));
                student.setRollNo((String)map3.get("ROLLNO"));
                student.setDob((String)map3.get("DOB"));
                student.setTotalAmount(Integer.parseInt((String) map3.get("HEAD_AMOUNT")));
                student.setStatus((String)map3.get("STATUS"));
                final Date date3 = (Date) map3.get("CREATED_DATE");
                final SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
                student.setCreated_Date(format2.format(date3));
                final Date date4 = (Date) map3.get("TRAN_DATE");
                student.setDateOfRefund(format2.format(date4));
                student.setSession((String)map3.get("AADEMIC_SESSION"));
                student.setStream((String)map3.get("STREAM"));
                student.setYearBatch((String)map3.get("YEAR_BATCH"));
                student.setCategory((String)map3.get("CATEGORY"));
                student.setCollegeCode((String)map3.get("COLLEGE_CODE"));
                student.setMrNo((String)map3.get("MRN_NO"));
                student.setFundType((String)map3.get("FUND_TYPE"));
                student.setReceiptNo(map3.get("RECEIPT_NUMBER") + "/" + map3.get("BOOK_NO"));
                student.setPaymentModeDrop((String)map3.get("TRANSACTION_MODE"));
                student.setScienceSelect((String)map3.get("SCIENCE_OPTION"));
                studentListreport.add(student);
            }
            System.out.println("After Getting Student Additional Details :: " + studentListreport);
            request.getSession().setAttribute("studentListreport", (Object)studentListreport);
            return "SUCCESS";
        }
        return "ERROR";
    }
    
    public List<Map<String, Object>> getDCRBook(final DCRBook dcrBook, final String config, final List<String> stream) {
        try {
            final Set<String> academicSessionId = new HashSet<String>();
            final Reports reports = dcrBook.getReports();
            academicSessionId.add(reports.getSession());
            final Set<String> tranTypeId = new HashSet<String>();
            if (reports.getFundType() == null || reports.getFundType().equalsIgnoreCase("")) {
                return null;
            }
            if (reports.getFundType().equalsIgnoreCase("all")) {
                tranTypeId.add("A");
                tranTypeId.add("R");
                tranTypeId.add("AD");
                tranTypeId.add("F");
                tranTypeId.add("OAD");
            }
            else if (reports.getFundType().equalsIgnoreCase("Admission")) {
                tranTypeId.add("A");
            }
            else if (reports.getFundType().equalsIgnoreCase("Re-Admission")) {
                tranTypeId.add("R");
            }
            else if (reports.getFundType().equalsIgnoreCase("Additional Fee")) {
                tranTypeId.add("AD");
            }
            else if (reports.getFundType().equalsIgnoreCase("Fine")) {
                tranTypeId.add("F");
            }
            else if (reports.getFundType().equalsIgnoreCase("Old Student Additional Fee")) {
                tranTypeId.add("OAD");
            }
            final Set<String> courseId = new HashSet<String>();
            courseId.add(config);
            final Set<String> streamId = new HashSet<String>();
            if (reports.getStream() == null || reports.getStream().equalsIgnoreCase("")) {
                return null;
            }
            if (reports.getStream().equalsIgnoreCase("all")) {
                streamId.addAll(stream);
            }
            else {
                streamId.add(reports.getStream());
            }
            final Set<String> categoriesId = new HashSet<String>();
            final Set<String> categories = new HashSet<String>();
            if (reports.getCategory() != null && !reports.getCategory().equalsIgnoreCase("")) {
                if (reports.getCategory().equalsIgnoreCase("all")) {
                    categories.add("General");
                    categories.add("OBC");
                    categories.add("SC");
                    categories.add("ST");
                    categories.add("Girls");
                    categoriesId.add("Gen/OBC");
                    categoriesId.add("SC/ST/Girls");
                }
                else if (reports.getCategory() != null && (reports.getCategory().equalsIgnoreCase("General") || reports.getCategory().equalsIgnoreCase("OBC"))) {
                    categoriesId.add("Gen/OBC");
                }
                else {
                    categoriesId.add("SC/ST/Girls");
                }
                final Set<String> yearBatchId = new HashSet<String>();
                if (reports.getClassType() != null && !reports.getClassType().equalsIgnoreCase("")) {
                    if (reports.getClassType().equalsIgnoreCase("all")) {
                        yearBatchId.add("1st");
                        yearBatchId.add("2nd");
                        if (!config.equalsIgnoreCase("+2")) {
                            yearBatchId.add("3rd");
                        }
                    }
                    else {
                        yearBatchId.add(reports.getClassType());
                    }
                }
                final SqlParameterSource parameters = (SqlParameterSource)new MapSqlParameterSource().addValue("academicSessionId", (Object)academicSessionId).addValue("tranTypeId", (Object)tranTypeId).addValue("courseId", (Object)courseId).addValue("streamId", (Object)streamId).addValue("categoriesId", (Object)categoriesId).addValue("yearBatchId", (Object)yearBatchId).addValue("fromDateId", (Object)reports.getDateFrom()).addValue("toDateId", (Object)reports.getDateTo());
                List<Map<String, Object>> dcrBookMap = null;
                if (config.equalsIgnoreCase("UG")) {
                    dcrBookMap = (List<Map<String, Object>>)this.reportsDao.getDCRBookUG(parameters);
                }
                else if (config.equalsIgnoreCase("+2")) {
                    dcrBookMap = (List<Map<String, Object>>)this.reportsDao.getDCRBook(parameters);
                }
                if (dcrBookMap != null && !dcrBookMap.isEmpty()) {
                    final List<Map<String, Object>> addDCRBook = (List<Map<String, Object>>)this.reportsDao.getAddDCRBook(parameters);
                    final int mapSize = dcrBookMap.size();
                    if (addDCRBook != null && !addDCRBook.isEmpty()) {
                        for (int i = 0; i <= mapSize - 1; ++i) {
                            final Map<String, Object> outerMap = dcrBookMap.get(i);
                            if (i > 0) {
                                for (int j = 0; j <= addDCRBook.size() - 1; ++j) {
                                    final String outerRollNo = (String) outerMap.get("ROLLNO");
                                    final Map<String, Object> innermap = addDCRBook.get(j);
                                    final String innerRollNO = (String) innermap.get("ROLLNO");
                                    final String tranType = (String) innermap.get("TRANSACTION_TYPE");
                                    final String headAmount = (String) innermap.get("HEAD_AMOUNT");
                                    if (tranType.equalsIgnoreCase("OAD")) {
                                        innermap.put("CLC", headAmount);
                                        dcrBookMap.add(dcrBookMap.size(), innermap);
                                        addDCRBook.remove(j);
                                    }
                                    else if (outerRollNo.equalsIgnoreCase(innerRollNO)) {
                                        System.out.println(innerRollNO + "---" + headAmount);
                                        outerMap.put("CLC", headAmount);
                                        dcrBookMap.remove(i);
                                        dcrBookMap.add(i, outerMap);
                                    }
                                }
                            }
                        }
                    }
                }
                System.out.println(dcrBookMap);
                return dcrBookMap;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
