package com.daily.collection.web.analytic.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daily.collection.web.analytic.bean.HeadDeatils;
import com.daily.collection.web.analytic.dao.IAnalyticDAO;

@Service
public class AnalyticServiceImpl implements IAnalyticService {
	
	@Autowired
	IAnalyticDAO analyticDao; 
    
	@Override
	public void getHeadDropDown(HttpServletRequest req){
		try{
			String dashBoardConfig=(String) req.getSession().getAttribute("config");
			List<Map<String, Object>> headDropDown=null;
			if(dashBoardConfig.equalsIgnoreCase("+2")){
				 headDropDown = analyticDao.getHeadDropDown();
			}
			else if(dashBoardConfig.equalsIgnoreCase("UG")){
				headDropDown = analyticDao.getHeadDropDownUG();
			}else{
				
			}
		  HeadDeatils head=null;
		  List<HeadDeatils> headDetailsList=null;
	   		if(headDropDown!=null){
	   			headDetailsList=new ArrayList<>();
	   			for (Map<String, Object> map : headDropDown) {
	   				head=new HeadDeatils();
	   				head.setHeadname((String)map.get("HEADNAME"));
	   				/*BigDecimal bd=(BigDecimal)map.get("HEADAMOUNT");
	   				head.setHeadAmount(bd.intValue());*/
	   				headDetailsList.add(head);
	   			}
	   			req.getSession().setAttribute("dropdownList", headDetailsList);
	   		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public List<HeadDeatils> addHead(HeadDeatils head, String userName,List<String> applicationConfiguration) throws Exception {
		try{
			Long startTime=System.currentTimeMillis();	
			 String headCount = null;
	            headCount = this.analyticDao.getheadId(head.getHeadname());
	            if (headCount.equalsIgnoreCase("ERROR")) {
	                final Object[] headNameParams = { head.getHeadname(), userName };
	                final String insertHeadNameStatus = this.analyticDao.insertHeadName(headNameParams);
	                if (insertHeadNameStatus.equalsIgnoreCase("ERROR")) {
	                    return null;
	                }
	            }
	            headCount = this.analyticDao.getheadId(head.getHeadname());
			String headCatogoriesId;
			Object[] haedParamsCategory=null;
			Object[] getheadparams=null;
			Object[] getheadparamsWithAmount=null;
			Object[] getHeadCategoriesId=null;
			if(headCount!=null){
				if(head.getCheboxSelect()!=null && !head.getCheboxSelect().equals("") && head.getCheboxSelect().equalsIgnoreCase("selectAllCategory")){
					for(int i=1;i<=12;i++){
						switch (i) {
						case 1:  
							   if(applicationConfiguration.contains("Science")){
								     getHeadCategoriesId=new Object[]{"+2","Science","1st","Gen/OBC"};
								     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
								     getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
								     haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
								     getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
				                     break;   
							    }
			            case 2:  
			            	     if(applicationConfiguration.contains("Science")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Science","1st","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
				                     break;
			            	     }
			            case 3:  
			            	     if(applicationConfiguration.contains("Science")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Science","2nd","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
						         } 
			            case 4:  
			            	     if(applicationConfiguration.contains("Science")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Science","2nd","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
				                     break; 
			            	     }
			            case 5:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Arts","1st","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 6:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Arts","1st","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 7:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Arts","2nd","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 8:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Arts","2nd","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 9:  
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Commerce","1st","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 10: 
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Commerce","1st","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 11: 
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Commerce","2nd","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 12: 
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Commerce","2nd","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			        }
						
						
						int headCategoriesCount=analyticDao.getHeadCategoriesExist(getheadparams);
						if(headCategoriesCount==0){
							String categoryAddStatus=analyticDao.addHeadForClassAndStream(haedParamsCategory);
							if(categoryAddStatus.equalsIgnoreCase("SUCCESS")){
								continue;
							}else{
								throw new Exception("Erro WHile Adding HeadDetails For Category");
							}
						}else{
							int i1=analyticDao.updateHeadForClassAndStream(getheadparamsWithAmount);
							if(i1>=1){
								continue;
							}else{
								throw new Exception("Error WHile Updating HeadDetails For Category");
							}
						}
					}
				}else{
					for (String str : head.categories) {
						switch (Integer.parseInt(str)) {
						case 1:  
							   if(applicationConfiguration.contains("Science")){
								     getHeadCategoriesId=new Object[]{"+2","Science","1st","Gen/OBC"};
								     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
								    getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
								     haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
								     getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
				                     break;   
							    }
			            case 2:  
			            	     if(applicationConfiguration.contains("Science")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Science","1st","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
				                     break;
			            	     }
			            case 3:  
			            	     if(applicationConfiguration.contains("Science")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Science","2nd","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
						         } 
			            case 4:  
			            	     if(applicationConfiguration.contains("Science")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Science","2nd","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
				                     break; 
			            	     }
			            case 5:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Arts","1st","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 6:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Arts","1st","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 7:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Arts","2nd","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 8:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Arts","2nd","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 9:  
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Commerce","1st","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 10: 
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Commerce","1st","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 11: 
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Commerce","2nd","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 12: 
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"+2","Commerce","2nd","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
					}
						
						int headCategoriesCount=analyticDao.getHeadCategoriesExist(getheadparams);
						if(headCategoriesCount==0){
							String categoryAddStatus=analyticDao.addHeadForClassAndStream(haedParamsCategory);
							if(categoryAddStatus.equalsIgnoreCase("SUCCESS")){
								continue;
							}else{
								throw new Exception("Error WHile Adding HeadDetails For Category");
							}
						}else{
							
							int i=analyticDao.updateHeadForClassAndStream(getheadparamsWithAmount);
							if(i>=1){
								continue;
							}else{
								throw new Exception("Error WHile Updating HeadDetails For Category");
							}
						}
				}
			}
				/*int updateHeadAmount = analyticDao.updateHeadAmount(head.getHeadAmount(), userName, headCount);
				if(updateHeadAmount!=1){
					throw new Exception("Error WHile Updating HeadDetails For Head Details Table");
				}*/
		    Long endTime=System.currentTimeMillis();	
		    System.out.println("Time Taken ::"+(endTime-startTime));
			}else{
				throw new Exception("Error While Adding Head Details");
			}
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	
	
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public List<HeadDeatils> addHeadUG(HeadDeatils head, String userName,List<String> applicationConfiguration) throws Exception {
		try{
			Long startTime=System.currentTimeMillis();	
			 String headCount = null;
	            headCount = this.analyticDao.getheadIdUG(head.getHeadname());
	            if (headCount.equalsIgnoreCase("ERROR")) {
	                final Object[] headNameParams = { head.getHeadname(), userName };
	                final String insertHeadNameUGStatus = this.analyticDao.insertHeadNameUG(headNameParams);
	                if (insertHeadNameUGStatus.equalsIgnoreCase("ERROR")) {
	                    return null;
	                }
	            }
	            headCount = this.analyticDao.getheadIdUG(head.getHeadname());
			String headCatogoriesId;
			Object[] haedParamsCategory=null;
			Object[] getheadparams=null;
			Object[] getheadparamsWithAmount=null;
			Object[] getHeadCategoriesId=null;
			if(headCount!=null){
				if(head.getCheboxSelect()!=null && !head.getCheboxSelect().equals("") && head.getCheboxSelect().equalsIgnoreCase("selectAllCategory")){
					for(int i=1;i<=18;i++){
						switch (i) {
						case 1:  
							   if(applicationConfiguration.contains("Science")){
								     getHeadCategoriesId=new Object[]{"UG","Science","1st","Gen/OBC"};
								     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
								     getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
								     haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
								     getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
				                     break;   
							    }
			            case 2:  
			            	     if(applicationConfiguration.contains("Science")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Science","1st","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
				                     break;
			            	     }
			            case 3:  
			            	     if(applicationConfiguration.contains("Science")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Science","2nd","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
						         } 
			            case 4:  
			            	     if(applicationConfiguration.contains("Science")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Science","2nd","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
				                     break; 
			            	     }
			            	     
			            case 5:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Arts","1st","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 6:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Arts","1st","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 7:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Arts","2nd","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 8:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Arts","2nd","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 9:  
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Commerce","1st","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 10: 
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Commerce","1st","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 11: 
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Commerce","2nd","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 12: 
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Commerce","2nd","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            	     
			            case 13:  
		            	     if(applicationConfiguration.contains("Science")){
		            	    	 getHeadCategoriesId=new Object[]{"UG","Science","3rd","Gen/OBC"};
		            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
		            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
		            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
		            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break; 
		            	     }	
			            case 14:  
		            	     if(applicationConfiguration.contains("Science")){
		            	    	 getHeadCategoriesId=new Object[]{"UG","Science","3rd","SC/ST/Girls"};
		            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
		            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
		            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
		            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break; 
		            	     }    
			            case 15:  
		            	     if(applicationConfiguration.contains("Arts")){
		            	    	 getHeadCategoriesId=new Object[]{"UG","Arts","3rd","Gen/OBC"};
		            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
		            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
		            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
		            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break; 
		            	     }	
			            case 16:  
		            	     if(applicationConfiguration.contains("Arts")){
		            	    	 getHeadCategoriesId=new Object[]{"UG","Arts","3rd","SC/ST/Girls"};
		            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
		            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
		            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
		            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break; 
		            	     }    
			            case 17:  
		            	     if(applicationConfiguration.contains("Commerce")){
		            	    	 getHeadCategoriesId=new Object[]{"UG","Commerce","3rd","Gen/OBC"};
		            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
		            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
		            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
		            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break; 
		            	     }	
			            case 18:  
		            	     if(applicationConfiguration.contains("Commerce")){
		            	    	 getHeadCategoriesId=new Object[]{"UG","Commerce","3rd","SC/ST/Girls"};
		            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
		            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
		            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
		            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break; 
		            	     }      		            	     
		            	     
		            	     
			        }
						
						
						int headCategoriesCount=analyticDao.getHeadCategoriesExistUG(getheadparams);
						if(headCategoriesCount==0){
							String categoryAddStatus=analyticDao.addHeadForClassAndStream_UG(haedParamsCategory);
							if(categoryAddStatus.equalsIgnoreCase("SUCCESS")){
								continue;
							}else{
								throw new Exception("Erro WHile Adding HeadDetails For Category");
							}
						}else{
							int i1=analyticDao.updateHeadForClassAndStream_UG(getheadparamsWithAmount);
							if(i1>=1){
								continue;
							}else{
								throw new Exception("Error WHile Updating HeadDetails For Category");
							}
						}
					}
				}else{
					for (String str : head.categories) {
						switch (Integer.parseInt(str)) {
						case 1:  
							   if(applicationConfiguration.contains("Science")){
								     getHeadCategoriesId=new Object[]{"UG","Science","1st","Gen/OBC"};
								     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
								    getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
								     haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
								     getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
				                     break;   
							    }
			            case 2:  
			            	     if(applicationConfiguration.contains("Science")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Science","1st","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
				                     break;
			            	     }
			            case 3:  
			            	     if(applicationConfiguration.contains("Science")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Science","2nd","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
						         } 
			            case 4:  
			            	     if(applicationConfiguration.contains("Science")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Science","2nd","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
				                     break; 
			            	     }
			            case 5:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Arts","1st","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 6:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Arts","1st","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 7:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Arts","2nd","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 8:  
			            	     if(applicationConfiguration.contains("Arts")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Arts","2nd","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 9:  
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Commerce","1st","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 10: 
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Commerce","1st","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 11: 
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Commerce","2nd","Gen/OBC"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 12: 
			            	     if(applicationConfiguration.contains("Commerce")){
			            	    	 getHeadCategoriesId=new Object[]{"UG","Commerce","2nd","SC/ST/Girls"};
			            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
			            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
			            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
			                     break;
			            	     }
			            case 13: 
		            	     if(applicationConfiguration.contains("Science")){
		            	    	 getHeadCategoriesId=new Object[]{"UG","Science","3rd","Gen/OBC"};
		            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
		            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
		            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
		            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
		                     break;
		            	     }	
			            case 14: 
		            	     if(applicationConfiguration.contains("Science")){
		            	    	 getHeadCategoriesId=new Object[]{"UG","Science","3rd","SC/ST/Girls"};
		            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
		            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
		            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
		            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
		                     break;
		            	     }
			            case 15: 
		            	     if(applicationConfiguration.contains("Arts")){
		            	    	 getHeadCategoriesId=new Object[]{"UG","Arts","3rd","Gen/OBC"};
		            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
		            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
		            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
		            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
		                     break;
		            	     }	
			            case 16: 
		            	     if(applicationConfiguration.contains("Arts")){
		            	    	 getHeadCategoriesId=new Object[]{"UG","Arts","3rd","SC/ST/Girls"};
		            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
		            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
		            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
		            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
		                     break;
		            	     }   
			            case 17: 
		            	     if(applicationConfiguration.contains("Commerce")){
		            	    	 getHeadCategoriesId=new Object[]{"UG","Commerce","3rd","Gen/OBC"};
		            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
		            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
		            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
		            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
		                     break;
		            	     }	
			            case 18: 
		            	     if(applicationConfiguration.contains("Commerce")){
		            	    	 getHeadCategoriesId=new Object[]{"UG","Commerce","3rd","SC/ST/Girls"};
		            	    	 headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
		            	    	getheadparams= new Object[]{headCount,headCatogoriesId,head.getSession()};
		            	    	 haedParamsCategory=new Object[]{headCount,headCatogoriesId,head.getHeadAmount(),userName,head.getSession()};
		            	    	 getheadparamsWithAmount=new Object[]{head.getHeadAmount(),headCatogoriesId,headCount};
		                     break;
		            	     }     
		            	     
					}
						
						int headCategoriesCount=analyticDao.getHeadCategoriesExistUG(getheadparams);
						if(headCategoriesCount==0){
							String categoryAddStatus=analyticDao.addHeadForClassAndStream_UG(haedParamsCategory);
							if(categoryAddStatus.equalsIgnoreCase("SUCCESS")){
								continue;
							}else{
								throw new Exception("Error WHile Adding HeadDetails For Category");
							}
						}else{
							
							int i=analyticDao.updateHeadForClassAndStream_UG(getheadparamsWithAmount);
							if(i>=1){
								continue;
							}else{
								throw new Exception("Error WHile Updating HeadDetails For Category");
							}
						}
				}
			}
				/*int updateHeadAmount = analyticDao.updateHeadAmount(head.getHeadAmount(), userName, headCount);
				if(updateHeadAmount!=1){
					throw new Exception("Error WHile Updating HeadDetails For Head Details Table");
				}*/
		    Long endTime=System.currentTimeMillis();	
		    System.out.println("Time Taken ::"+(endTime-startTime));
			}else{
				throw new Exception("Error While Adding Head Details");
			}
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return null;
	}
	
	
	
	
	

	@Override
	public void getHeadAmount(HttpServletRequest req,String session,List<String> applicationConfiguration) throws Exception {
       try{
    	   Object[] haedParamsCategory=null;
    	   List<HeadDeatils> headDetailsList=null;
			req.getSession().setAttribute("searchSession", session);
			String headCatogoriesId;
			Object[] getHeadCategoriesId=null;
   		for(int i=1;i<=12;i++){
   			headDetailsList=new ArrayList<>();
   			switch (i) {
               case 1:  
            	        if(applicationConfiguration.contains("Science")){
            	         getHeadCategoriesId=new Object[]{"+2","Science","1st","Gen/OBC"};
	            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
            	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
   			            } else{
   			            	continue;
   			            }
               case 2:  
            	        if(applicationConfiguration.contains("Science")){
            	        getHeadCategoriesId=new Object[]{"+2","Science","1st","SC/ST/Girls"};
   	            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
               	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
            	        }
            	        else{
            	        	continue;
   			            }
               case 3:  
            	        if(applicationConfiguration.contains("Science")){
            	        	getHeadCategoriesId=new Object[]{"+2","Science","2nd","Gen/OBC"};
      	            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
                  	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
            	        }
            	        else{
            	        	continue;
   			            }
               case 4:  
            	        if(applicationConfiguration.contains("Science")){
            	        	getHeadCategoriesId=new Object[]{"+2","Science","2nd","SC/ST/Girls"};
     	            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
                 	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
            	        }
            	        else{
            	        	continue;
   			            }
               case 5:  
            	        if(applicationConfiguration.contains("Arts")){
            	        	getHeadCategoriesId=new Object[]{"+2","Arts","1st","Gen/OBC"};
     	            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
                 	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
            	        }
            	        else{
            	        	continue;
   			            }
               case 6:  
            	   
            	        if(applicationConfiguration.contains("Arts")){
            	        	getHeadCategoriesId=new Object[]{ "+2","Arts","1st","SC/ST/Girls"};
     	            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
                 	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
            	        }
            	        else{
            	        	continue;
   			            }
               case 7:  if(applicationConfiguration.contains("Arts")){
            	   getHeadCategoriesId=new Object[]{"+2","Arts","2nd","Gen/OBC"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 8:  if(applicationConfiguration.contains("Arts")){
            	   getHeadCategoriesId=new Object[]{ "+2","Arts","2nd","SC/ST/Girls"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 9:  if(applicationConfiguration.contains("Commerce")){
            	   getHeadCategoriesId=new Object[]{"+2","Commerce","1st","Gen/OBC"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 10: if(applicationConfiguration.contains("Commerce")){
            	   getHeadCategoriesId=new Object[]{"+2","Commerce","1st","SC/ST/Girls"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 11: if(applicationConfiguration.contains("Commerce")){
            	   getHeadCategoriesId=new Object[]{ "+2","Commerce","2nd","Gen/OBC"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 12: if(applicationConfiguration.contains("Commerce")){
		            	   getHeadCategoriesId=new Object[]{ "+2","Commerce","2nd","SC/ST/Girls"};
		            	   headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
		        	       haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
           }
   		List<Map<String, Object>> headAmount = analyticDao.getHeadAmount(haedParamsCategory);
   		HeadDeatils head=null;
   		if(headAmount!=null){
   			for (Map<String, Object> map : headAmount) {
   				head=new HeadDeatils();
   				head.setHeadname((String)map.get("HEADNAME"));
   				BigDecimal bd=(BigDecimal)map.get("AMOUNT");
   				head.setHeadAmount(bd.intValue());
   				headDetailsList.add(head);
   			}
   		}
   		
   		
   		req.getSession().setAttribute("head"+i,headDetailsList);
   		
    	}   
   		
       }catch(Exception e){
    	   e.printStackTrace();
    	   throw new Exception(e.getMessage());
       }
		
  }

	
	@Override
	public void getHeadAmountUG(HttpServletRequest req,String session,List<String> applicationConfiguration) throws Exception {
       try{
    	   Object[] haedParamsCategory=null;
    	   List<HeadDeatils> headDetailsList=null;
			req.getSession().setAttribute("searchSession", session);
			String headCatogoriesId;
			Object[] getHeadCategoriesId=null;
   		for(int i=1;i<=18;i++){
   			headDetailsList=new ArrayList<>();
   			switch (i) {
               case 1:  
            	        if(applicationConfiguration.contains("Science")){
            	         getHeadCategoriesId=new Object[]{"UG","Science","1st","Gen/OBC"};
	            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
            	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
   			            } else{
   			            	continue;
   			            }
               case 2:  
            	        if(applicationConfiguration.contains("Science")){
            	        getHeadCategoriesId=new Object[]{"UG","Science","1st","SC/ST/Girls"};
   	            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
               	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
            	        }
            	        else{
            	        	continue;
   			            }
               case 3:  
            	        if(applicationConfiguration.contains("Science")){
            	        	getHeadCategoriesId=new Object[]{"UG","Science","2nd","Gen/OBC"};
      	            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
                  	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
            	        }
            	        else{
            	        	continue;
   			            }
               case 4:  
            	        if(applicationConfiguration.contains("Science")){
            	        	getHeadCategoriesId=new Object[]{"UG","Science","2nd","SC/ST/Girls"};
     	            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
                 	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
            	        }
            	        else{
            	        	continue;
   			            }
               case 5:  
            	        if(applicationConfiguration.contains("Arts")){
            	        	getHeadCategoriesId=new Object[]{"UG","Arts","1st","Gen/OBC"};
     	            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
                 	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
            	        }
            	        else{
            	        	continue;
   			            }
               case 6:  
            	   
            	        if(applicationConfiguration.contains("Arts")){
            	        	getHeadCategoriesId=new Object[]{ "UG","Arts","1st","SC/ST/Girls"};
     	            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
                 	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
            	        }
            	        else{
            	        	continue;
   			            }
               case 7:  if(applicationConfiguration.contains("Arts")){
            	   getHeadCategoriesId=new Object[]{"UG","Arts","2nd","Gen/OBC"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 8:  if(applicationConfiguration.contains("Arts")){
            	   getHeadCategoriesId=new Object[]{ "UG","Arts","2nd","SC/ST/Girls"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 9:  if(applicationConfiguration.contains("Commerce")){
            	   getHeadCategoriesId=new Object[]{"UG","Commerce","1st","Gen/OBC"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 10: if(applicationConfiguration.contains("Commerce")){
            	   getHeadCategoriesId=new Object[]{"UG","Commerce","1st","SC/ST/Girls"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 11: if(applicationConfiguration.contains("Commerce")){
            	   getHeadCategoriesId=new Object[]{ "UG","Commerce","2nd","Gen/OBC"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 12: if(applicationConfiguration.contains("Commerce")){
		            	   getHeadCategoriesId=new Object[]{ "UG","Commerce","2nd","SC/ST/Girls"};
		            	   headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
		        	       haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 13: if(applicationConfiguration.contains("Science")){
            	   getHeadCategoriesId=new Object[]{ "UG","Science","3rd","Gen/OBC"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 14: if(applicationConfiguration.contains("Science")){
		            	   getHeadCategoriesId=new Object[]{ "UG","Science","3rd","SC/ST/Girls"};
		            	   headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
		        	       haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 15: if(applicationConfiguration.contains("Arts")){
            	   getHeadCategoriesId=new Object[]{ "UG","Arts","3rd","Gen/OBC"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 16: if(applicationConfiguration.contains("Arts")){
		            	   getHeadCategoriesId=new Object[]{ "UG","Arts","3rd","SC/ST/Girls"};
		            	   headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
		        	       haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 17: if(applicationConfiguration.contains("Commerce")){
            	   getHeadCategoriesId=new Object[]{ "UG","Commerce","3rd","Gen/OBC"};
            	     headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
        	        haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               case 18: if(applicationConfiguration.contains("Commerce")){
		            	   getHeadCategoriesId=new Object[]{ "UG","Commerce","3rd","SC/ST/Girls"};
		            	   headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);	
		        	       haedParamsCategory=new Object[]{headCatogoriesId,session};
                        break;
                        }
                        else{
                        	continue;
		                }
               
           }
   		
   		List<Map<String, Object>> headAmount = analyticDao.getHeadAmountUG(haedParamsCategory);
   		HeadDeatils head=null;
   		if(headAmount!=null){
   			for (Map<String, Object> map : headAmount) {
   				head=new HeadDeatils();
   				head.setHeadname((String)map.get("HEADNAME"));
   				//BigDecimal bd=(BigDecimal)map.get("AMOUNT");
   				head.setHeadAmount(Integer.parseInt((String)map.get("AMOUNT")));
   				headDetailsList.add(head);
   			}
   		}
   		req.getSession().setAttribute("headUG"+i,headDetailsList);
   		
    	}   
   		
       }catch(Exception e){
    	   e.printStackTrace();
    	   throw new Exception(e.getMessage());
       }
		
  }

	
	
	
	@Override
	public String editHeadByCategory(String headName,int headAmount, String userName,String headnm,String yearSession) throws Exception {
		String headId=analyticDao.getheadId(headName);
		Object[] getheadparamsWithAmount=null;
		Calendar today = Calendar.getInstance();
		//String headCount=analyticDao.getheadId(headName);
		String headCatogoriesId;
		Object[] getHeadCategoriesId=null;
		switch (headnm) {
		case "editfirsthead": getHeadCategoriesId=new Object[]{ "+2","Science","1st","Gen/OBC"};
 	                          headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			                 getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editsecondhead":
        	getHeadCategoriesId=new Object[]{"+2","Science","1st","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editthirdhead":
        	getHeadCategoriesId=new Object[]{"+2","Science","2nd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editfourthhead":
        	getHeadCategoriesId=new Object[]{ "+2","Science","2nd","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editfifthhead":
        	getHeadCategoriesId=new Object[]{"+2","Arts","1st","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editsixthhead":
        	getHeadCategoriesId=new Object[]{"+2","Arts","1st","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editseventhhead":
        	getHeadCategoriesId=new Object[]{"+2","Arts","2nd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editeighthhead":
        	getHeadCategoriesId=new Object[]{ "+2","Arts","2nd","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editninthhead":
        	getHeadCategoriesId=new Object[]{"+2","Commerce","1st","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "edittengthhead":
        	getHeadCategoriesId=new Object[]{"+2","Commerce","1st","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editelevengthhead":
        	getHeadCategoriesId=new Object[]{"+2","Commerce","2nd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "edittwelthhead":
        	getHeadCategoriesId=new Object[]{"+2","Commerce","2nd","SC/ST/Girls",};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
	}
		
		String status=analyticDao.editHeadByCategory_By_Id(getheadparamsWithAmount);
		return status;
	}
	
	
	
	@Override
	public String editHeadByCategoryUG(String headName,int headAmount, String userName,String headnm,String yearSession) throws Exception {
		String headId=analyticDao.getheadIdUG(headName);
		Object[] getheadparamsWithAmount=null;
		Calendar today = Calendar.getInstance();
		//String headCount=analyticDao.getheadId(headName);
		String headCatogoriesId;
		Object[] getHeadCategoriesId=null;
		switch (headnm) {
		case "editfirsthead": getHeadCategoriesId=new Object[]{ "UG","Science","1st","Gen/OBC"};
 	                          headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			                 getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editsecondhead":
        	getHeadCategoriesId=new Object[]{"UG","Science","1st","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editthirdhead":
        	getHeadCategoriesId=new Object[]{"UG","Science","2nd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editfourthhead":
        	getHeadCategoriesId=new Object[]{ "UG","Science","2nd","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editfifthhead":
        	getHeadCategoriesId=new Object[]{"UG","Arts","1st","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editsixthhead":
        	getHeadCategoriesId=new Object[]{"UG","Arts","1st","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editseventhhead":
        	getHeadCategoriesId=new Object[]{"UG","Arts","2nd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editeighthhead":
        	getHeadCategoriesId=new Object[]{ "UG","Arts","2nd","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editninthhead":
        	getHeadCategoriesId=new Object[]{"UG","Commerce","1st","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "edittengthhead":
        	getHeadCategoriesId=new Object[]{"UG","Commerce","1st","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editelevengthhead":
        	getHeadCategoriesId=new Object[]{"UG","Commerce","2nd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "edittwelthhead":
        	getHeadCategoriesId=new Object[]{"UG","Commerce","2nd","SC/ST/Girls",};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editthiteenhead":
        	getHeadCategoriesId=new Object[]{"UG","Science","3rd","Gen/OBC",};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editfourteenhead":
        	getHeadCategoriesId=new Object[]{"UG","Science","3rd","SC/ST/Girls",};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editfifteenhead":
        	getHeadCategoriesId=new Object[]{"UG","Arts","3rd","Gen/OBC",};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editsixteenhead":
        	getHeadCategoriesId=new Object[]{"UG","Arts","3rd","SC/ST/Girls",};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editseventeenhead":
        	getHeadCategoriesId=new Object[]{"UG","Commerce","3rd","Gen/OBC",};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        case "editeighteenhead":
        	getHeadCategoriesId=new Object[]{"UG","Commerce","3rd","SC/ST/Girls",};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
           getheadparamsWithAmount=new Object[]{headAmount,today,userName,headId,headCatogoriesId,yearSession,};
                             break;
        
	}
		
		String status=analyticDao.editHeadByCategory_By_IdUG(getheadparamsWithAmount);
		return status;
	}

	@Override
	public String deleteHeadByCategory(HeadDeatils head, String userName,String headnm,String yearSession) throws Exception {
		Object[] deleteHeadCategory=null;
		String headCatogoriesId;
		String headId=analyticDao.getheadId(head.getHeadname());
		Object[] getHeadCategoriesId=null;
		switch (headnm) {
		case "deletefirsthead":
			 getHeadCategoriesId=new Object[]{ "+2","Science","1st","Gen/OBC"};
              headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletesecondhead":
        	 getHeadCategoriesId=new Object[]{ "+2","Science","1st","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletethirdhead":
        	 getHeadCategoriesId=new Object[]{ "+2","Science","2nd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletefourthhead":
        	 getHeadCategoriesId=new Object[]{"+2","Science","2nd","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletefifthhead":
        	 getHeadCategoriesId=new Object[]{"+2","Arts","1st","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletesixthhead":
        	 getHeadCategoriesId=new Object[]{"+2","Arts","1st","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deleteseventhhead":
        	 getHeadCategoriesId=new Object[]{"+2","Arts","2nd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deleteeighthhead":
        	 getHeadCategoriesId=new Object[]{"+2","Arts","2nd","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deleteninthhead":
        	 getHeadCategoriesId=new Object[]{"+2","Commerce","1st","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletetengthhead":
        	 getHeadCategoriesId=new Object[]{"+2","Commerce","1st","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deleteelevengthhead":
        	 getHeadCategoriesId=new Object[]{"+2","Commerce","2nd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletetwelthhead":
        	 getHeadCategoriesId=new Object[]{"+2","Commerce","2nd","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
	    
		}
		
		String status=analyticDao.deleteHeadByCategory_By_Id(deleteHeadCategory);
		return status;
	}
	
	
	@Override
	public String deleteHeadByCategoryUG(HeadDeatils head, String userName,String headnm,String yearSession) throws Exception {
		Object[] deleteHeadCategory=null;
		String headCatogoriesId;
		String headId=analyticDao.getheadIdUG(head.getHeadname());
		Object[] getHeadCategoriesId=null;
		switch (headnm) {
		case "deletefirsthead":
			 getHeadCategoriesId=new Object[]{ "UG","Science","1st","Gen/OBC"};
              headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletesecondhead":
        	 getHeadCategoriesId=new Object[]{ "UG","Science","1st","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletethirdhead":
        	 getHeadCategoriesId=new Object[]{ "UG","Science","2nd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletefourthhead":
        	 getHeadCategoriesId=new Object[]{"UG","Science","2nd","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletefifthhead":
        	 getHeadCategoriesId=new Object[]{"UG","Arts","1st","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletesixthhead":
        	 getHeadCategoriesId=new Object[]{"UG","Arts","1st","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deleteseventhhead":
        	 getHeadCategoriesId=new Object[]{"UG","Arts","2nd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deleteeighthhead":
        	 getHeadCategoriesId=new Object[]{"UG","Arts","2nd","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deleteninthhead":
        	 getHeadCategoriesId=new Object[]{"UG","Commerce","1st","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletetengthhead":
        	 getHeadCategoriesId=new Object[]{"UG","Commerce","1st","SC/ST/Girls"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deleteelevengthhead":
        	 getHeadCategoriesId=new Object[]{"UG","Commerce","2nd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletetwelthhead":
          	 getHeadCategoriesId=new Object[]{"UG","Commerce","2nd","SC/ST/Girls"};
               headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
   			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                               break;
        case "deletethirteenhead":
        	 getHeadCategoriesId=new Object[]{"UG","Science","3rd","Gen/OBC"};
             headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                             break;
        case "deletefourteenhead":
       	 getHeadCategoriesId=new Object[]{"UG","Science","3rd","SC/ST/Girls"};
            headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                            break;
        case "deletefifteenhead":
       	 getHeadCategoriesId=new Object[]{"UG","Arts","3rd","Gen/OBC"};
            headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                            break;
        case "deletesixteenhead":
       	 getHeadCategoriesId=new Object[]{"UG","Arts","3rd","SC/ST/Girls"};
            headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                            break;
        case "deletesevnteenhead":
       	 getHeadCategoriesId=new Object[]{"UG","Commerce","3rd","Gen/OBC"};
            headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                            break;
        case "deleteeighteenhead":
       	 getHeadCategoriesId=new Object[]{"UG","Commerce","3rd","SC/ST/Girls"};
            headCatogoriesId=analyticDao.getHeadCatorigesId(getHeadCategoriesId);
			deleteHeadCategory=new Object[]{headId,headCatogoriesId,yearSession};
                            break;
	    
		}
		
		String status=analyticDao.deleteHeadByCategory_By_IdUG(deleteHeadCategory);
		return status;
		
	}
}
