package com.daily.collection.web.login.bean;

import org.springframework.stereotype.Component;

@Component
public class LoginBean {
	
	public String userName;
	public String password;
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	

}
