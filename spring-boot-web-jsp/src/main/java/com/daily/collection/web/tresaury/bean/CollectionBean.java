package com.daily.collection.web.tresaury.bean;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class CollectionBean implements Serializable{

	
	
	private static final long serialVersionUID = 1L;
	private String id;
	private String searchBy;
	private String dateFrom;
	private String dateTo;
	private String fundType;
	private String session;
	private String yearBatch;
	private String stream;
	private String ctegory;
	
	private List<CollectionList> collectionList;
	
	
	
	
	
	
}
