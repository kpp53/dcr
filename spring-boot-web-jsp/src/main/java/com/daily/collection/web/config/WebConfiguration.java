package com.daily.collection.web.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.daily.collection.web.download.excel.ExcelViewResolver;
import com.daily.collection.web.json.viewresolver.JsonViewResolver;


@Configuration
@ComponentScan(basePackages = "com.*")
public class WebConfiguration extends WebMvcConfigurerAdapter {
	

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**")
                        .addResourceLocations("/resources/");
	}

	@Override
	public void configureContentNegotiation(
			ContentNegotiationConfigurer configurer) {
		// Simple strategy: only path extension is taken into account
		/*configurer.favorPathExtension(true).
			ignoreAcceptHeader(true).
			useJaf(false).
			defaultContentType(MediaType.TEXT_HTML).
			mediaType("html", MediaType.TEXT_HTML).
			mediaType("xml", MediaType.APPLICATION_XML).
			mediaType("json", MediaType.APPLICATION_JSON);*/
		
		/*configurer.ignoreAcceptHeader(true).defaultContentType(
                MediaType.TEXT_HTML);*/
		
		 configurer
		// .favorPathExtension(false)
		 //.favorParameter(true).
		 //parameterName("mediaType").
		 .ignoreAcceptHeader(true).
		 //useJaf(false).
		 defaultContentType(MediaType.ALL);
		  //mediaType("html", MediaType.TEXT_HTML).
		 //mediaType("json",MediaType.APPLICATION_JSON);
	}
	
	
	    @Bean
	    public ViewResolver contentNegotiatingViewResolver(ContentNegotiationManager manager) {
	        ContentNegotiatingViewResolver resolver = new ContentNegotiatingViewResolver();
	        resolver.setContentNegotiationManager(manager);

	        // Define all possible view resolvers
	        List<ViewResolver> resolvers = new ArrayList<>();
	          resolvers.add(jspViewResolver());
	       // resolvers.add(csvViewResolver());
	          resolvers.add(excelViewResolver());
	       // resolvers.add(pdfViewResolver());
	          resolvers.add(jsonViewResolver());
	          
	        resolver.setViewResolvers(resolvers);
	        return resolver;
	    }

	
	  @Bean
	    public ViewResolver excelViewResolver() {
	        return new ExcelViewResolver();
	    }
	  
	  @Bean
	    public ViewResolver jsonViewResolver() {
	        return new JsonViewResolver();
	    }
	  
	  
	  @Bean
		public ViewResolver jspViewResolver() {
			InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
			viewResolver.setViewClass(JstlView.class);
			viewResolver.setPrefix("/WEB-INF/jsp/");
			viewResolver.setSuffix(".jsp");
			viewResolver.setOrder(0);
			return viewResolver;
		}
	
	
	  
	  
	
     
}
