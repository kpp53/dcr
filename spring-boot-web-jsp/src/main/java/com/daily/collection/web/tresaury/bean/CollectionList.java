package com.daily.collection.web.tresaury.bean;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import lombok.Data;


@Component
@Data
public class CollectionList implements Serializable{

	
	private static final long serialVersionUID = 1L;
	private String id;
	private String date;
	private String totalFund;
	private String cafAmount;
	private String miscellaniousAmount;
    private String noOfStudent;
    
    
    
    
    
	
}
