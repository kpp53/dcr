package com.daily.collection.web.login.dao;

import java.util.List;

public interface ILoginDao {
	
    public static final String CONF_DETAILS="SELECT VALUE FROM CONFIGURATION WHERE CONFIG_ID=? AND STATUS=?";
    
    public static final String GET_STATUS="SELECT STATUS FROM CONFIGURATION WHERE CONFIG_ID=?";
	
	public List<String> getConfigurationDetails(String config_id);
	
	public String getCurrentSession(String id);

}
