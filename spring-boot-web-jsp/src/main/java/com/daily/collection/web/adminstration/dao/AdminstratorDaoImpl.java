package com.daily.collection.web.adminstration.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AdminstratorDaoImpl implements IAdminstratorDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	
	
	@Override
	public String saveConfiguration(Object[] params) {
		try{
			int i=jdbcTemplate.update(IAdminstratorDao.SAVE_CONFIGURATION,params);
			if(i==1){
				return "SUCCESS";
			}else
			return "ERROR";
		}catch(Exception e){
			e.printStackTrace();
			return "ERROR";
		}
	}



	@Override
	public List<String> getCnfigurationDetailsByConfigId(String id) {
		try{
			List<String> configurationList=jdbcTemplate.queryForList(IAdminstratorDao.GET_CONFIGURATION_BY_ID, String.class, new Object[]{id});
			return configurationList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}



	@Override
	public String updateConfiguration(String status, String value, String userName, Date createdDate) {
		try{
			int i=jdbcTemplate.update(IAdminstratorDao.UPDATE_CONFIGURATION,new Object[]{status,userName,createdDate,value});
			if(i==1){
				return "SUCCESS";
			}else
			return "ERROR";
		}catch(Exception e){
			e.printStackTrace();
			return "ERROR";
		}
	}

}
