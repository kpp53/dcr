package com.daily.collection.web.home.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.daily.collection.web.home.bean.CollectionSummary;
import com.daily.collection.web.tresaury.bean.CollectionList;
import com.daily.collection.web.tresaury.dao.ITreasuryDao;


@Repository
public class HomeRepositoryImpl implements IHomerepository {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	

	@Override
	public String getNewAdmissionCount(String configId) throws ParseException {
		String admCount=null;
		try{
			admCount=jdbcTemplate.queryForObject(IHomerepository.GETADMISSIONCOUNT.toString(),new Object[]{configId}, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return admCount;
	}

	@Override
	public String getReAdmissionCount(String configId) throws ParseException {
		String readmCount=null;
		try{
			readmCount=jdbcTemplate.queryForObject(IHomerepository.GETREADMISSIONCOUNT.toString(),new Object[]{configId}, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return readmCount;
	}

	@Override
	public String getCancelledAdmissionCount(String configId) throws ParseException {
		String canAdmCount=null;
		try{
			canAdmCount=jdbcTemplate.queryForObject(IHomerepository.GETCANCELLEDADMISSIONCOUNT.toString(),new Object[]{configId}, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return canAdmCount;
	}

	@Override
	public List<CollectionSummary> getCollectionDetails(Date from, Date To,String courseId) throws ParseException {
		List<CollectionSummary> collectionSummaryList=null;
		try{
			if(from==null && To==null){
				collectionSummaryList=jdbcTemplate.query(IHomerepository.GETCOLLECTIONSUMMARY.toString(),new Object[]{courseId,courseId},new CollectionSummaryRowMapper());	
			}else{
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return collectionSummaryList;
	}
	
	
	
	final class CollectionSummaryRowMapper implements RowMapper<CollectionSummary>{

		@Override
		public CollectionSummary mapRow(ResultSet rs, int arg1) throws SQLException {
			   CollectionSummary colList=null;
               colList=new CollectionSummary();
               colList.setStreamName(rs.getString("STREAM_NAME"));
               colList.setStudentCount(String.valueOf(rs.getLong("STUDENTCOUNT")));
               colList.setTotalAmount(String.valueOf(rs.getLong("TOTALAMOUNT")));
           return colList;
		}

		
		
		
		
	}
	
	

}
