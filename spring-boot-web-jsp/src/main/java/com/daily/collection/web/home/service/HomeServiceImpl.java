package com.daily.collection.web.home.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daily.collection.web.home.bean.CollectionSummary;
import com.daily.collection.web.home.bean.HomeBean;
import com.daily.collection.web.home.repository.IHomerepository;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service
public class HomeServiceImpl implements IHomeService {
	
	@Autowired
	IHomerepository homeRepository;
	
	@Autowired
	HomeBean homeBean;

	@Override
	public HomeBean getDashboardDetails(Date from,Date to,String configId) throws ParseException {
		try{
			ExecutorService executorService=Executors.newFixedThreadPool(2);
			
			long ms=System.currentTimeMillis();
			
			Future<String> admisionCount = executorService.submit(()->{
				try {
					return homeRepository.getNewAdmissionCount(configId);	
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
				
			});
			
			Future<String> reAdmisionCount = executorService.submit(()->{
				try {
					return homeRepository.getReAdmissionCount(configId);	
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
				
			});
			
			
			
			Future<String> canAdmisionCount = executorService.submit(()->{
				try {
					return homeRepository.getCancelledAdmissionCount(configId);	
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
				
			});
			
			
			Future<List<CollectionSummary>> collectionDetails = executorService.submit(()->{
				try {
					return homeRepository.getCollectionDetails(null, null,configId);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return null;
				
			});
			
			
			homeBean.setAdmissionCount(admisionCount.get());
			homeBean.setReAdmissionCount(reAdmisionCount.get());
			homeBean.setCancelAdmCount(canAdmisionCount.get());
			homeBean.setCollectionSummary(collectionDetails.get());
			ObjectMapper mapper = new ObjectMapper();
			String collectionList = mapper.writeValueAsString(homeBean.getCollectionSummary());
			homeBean.setCollectionList(collectionList);
			
			long out=System.currentTimeMillis();
			System.out.println(out-ms);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return homeBean;
		
		

	}

}
