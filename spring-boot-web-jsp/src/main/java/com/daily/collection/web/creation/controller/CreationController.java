package com.daily.collection.web.creation.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.daily.collection.web.creation.bean.Student;
import com.daily.collection.web.creation.service.ICreationService;

@Controller
public class CreationController {
	
	@Autowired
	ICreationService creationService;

	
	@RequestMapping("/showStudent")
	public String addStudent(HttpServletRequest req,Map<String, Object> model) {
		return "views/creation/creation_addStudent";			
	}
	
	@RequestMapping("/showUploadStudent")
	public String showUploadStudent(HttpServletRequest req,Map<String, Object> model) {
		return "views/creation/creation_uploadStudent";			
	}
	
	private final static Logger logger = Logger.getLogger(CreationController.class);
	
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public @ResponseBody String uploadStudent(MultipartHttpServletRequest request,
			HttpServletResponse response) throws Exception {
		//logger.info("Enter Into saveStudentImage Method");
		logger.info("Inside uploadStudent Method");
		long startTime=System.currentTimeMillis();
		logger.info("Start Time ::"+startTime);
		
		MultipartFile multipartFile = request.getFile("file");
		InputStream inputStream=null;
		try{
			Long length=multipartFile.getSize();
			String contentType = multipartFile.getContentType();
			inputStream= multipartFile.getInputStream();
	        logger.info("Original File Name::"+multipartFile.getOriginalFilename());
	        Workbook workbook = getWorkbook(inputStream, multipartFile.getOriginalFilename());
			Sheet firstSheet = workbook.getSheetAt(0);
			List<Student> listStudent = new ArrayList<>();
			Iterator<Row> iterator = firstSheet.iterator();
			Map<Integer,Map<String,String>> errorMap=new LinkedHashMap<>();
			int i=0;
			try{
				
				while (iterator.hasNext()) {
					Row nextRow = iterator.next();
					if(i==1){
					Iterator<Cell> cellIterator = nextRow.cellIterator();
					Student student=new Student();
					outerblock:{
					while (cellIterator.hasNext()) {
						Cell nextCell = cellIterator.next();
						int columnIndex = nextCell.getColumnIndex();
						switch (columnIndex) {
						case 0:
							try{
								String d=(String) getCellValue(nextCell);
								logger.error("Id ::"+d);
								if(d!=null && !d.equalsIgnoreCase("")){
									if(d.length()<=4){
										student.setId(Integer.parseInt(d));	
										continue;	
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("Id", "Should Be Less Than 4 Digit");
										logger.error("Id Should Be Less Than 4 Digit... Row Number::"+nextRow.getRowNum());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("Id", "Should Not Be Empty/Null");
									logger.error("Id Should Not Be Empty/Null... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
							}catch(NumberFormatException e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("Id", "No Number And Special Character");
								errorMap.put(nextRow.getRowNum(), insideMap);
								logger.error("Exception In ID Field...Row Number::"+nextRow.getRowNum(),e);
								break outerblock;
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("Id", "Error While getting the id from excel");
								logger.error("Exception In ID Field...Row Number::"+nextRow.getRowNum(),e);
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						case 1:
							try{
								String name=(String) getCellValue(nextCell);
								logger.error("Name ::"+name);
								if(name!=null && !name.equalsIgnoreCase("")){
									if(name.length()<=30){
										student.setName(name);
										continue;		
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("Student Name","Should Be Less Than or Equals To 30 Digit");
										logger.error("Name SHould Be Less Than or Equal To 30 digit... Row Number::"+nextRow.getRowNum()+" RollNo Length ::"+name.length());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("Student Name", "Should Not Be Empty/Null");
									logger.error("Name SHould not Be Empty/NULL... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
								
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("Student Name", "Error While getting the Student Name from excel");
								logger.error("Exception While getting Student Name... Row Number::"+nextRow.getRowNum(),e);
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						case 2:
							try{
								String session=(String) getCellValue(nextCell);
								logger.error("session ::"+session);
								if(session!=null && !session.equalsIgnoreCase("")){
									if(session.length()<=15){
										student.setSession(session);
										continue;
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("session","Should Be Less Than or Equals To 15 Digit");
										logger.error("session Should Be Less Than 15 digit... Row Number::"+nextRow.getRowNum()+" College Code Length ::"+session.length());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("session", "Should Not Be Empty/Null");
									logger.error("session Should not be NULL/Empty... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
								
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("session", "Error While getting the Session from excel");
								logger.error("Excption WHile Getting session ::"+nextRow.getRowNum(),e);
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						case 3:
							try{
								String rollNo=(String) getCellValue(nextCell);
								logger.error("RollNo ::"+rollNo);
								if(rollNo!=null && !rollNo.equalsIgnoreCase("")){
									if(rollNo.length()<=15){
										student.setRollNo(rollNo);
										continue;	
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("Student RollNo","Should Be Less Than or Equals To 15 Digit");
										logger.error("RollNo SHould Be Less Than 15 digit... Row Number::"+nextRow.getRowNum()+" RollNo Length ::"+rollNo.length());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("Student RollNo", "Should Not Be Empty/Null");
									logger.error("RollNo SHould not Be Empty/NULL... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
								
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("Student RollNo", "Error While getting the Student RollNo from excel");
								logger.error("Exception WHile getting Student RollNo... Row Number::"+nextRow.getRowNum(),e);
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						
						case 4:
							try{
								String collegeCode=(String) getCellValue(nextCell);
								logger.error("CollegeCode ::"+collegeCode);
								if(collegeCode!=null && !collegeCode.equalsIgnoreCase("")){
									if(collegeCode.length()<=15){
										student.setCollegeCode(collegeCode.substring(0, 8));
										student.setMrNo(collegeCode.substring(9, 13));
										continue;	
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("College Code","Should Be Less Than or Equals To 15 Digit");
										logger.error("College Code/MRIN Should Be Less Than 15 digit... Row Number::"+nextRow.getRowNum()+" College Code Length ::"+collegeCode.length());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("College Code/MRIN", "Should Not Be Empty/Null");
									logger.error("College Code/MRIN Should not be NULL/Empty... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
								
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("College Code", "Error While getting the College Code from excel");
								logger.error("Excption WHile Getting College Code ::"+nextRow.getRowNum(),e);
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						case 5:
							try{
								String dob=(String) getCellValue(nextCell);
								logger.error("dob ::"+dob);
								if(dob!=null && !dob.equalsIgnoreCase("")){
									if(dob.length()<=15){
										student.setDob(dob);
										continue;
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("DOB","Should Be Less Than or Equals To 15 Digit");
										logger.error("dob Should Be Less Than 15 digit... Row Number::"+nextRow.getRowNum()+" College Code Length ::"+dob.length());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("DOB", "Should Not Be Empty/Null");
									logger.error("dob Should not be NULL/Empty... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
								
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("DOB", "Error While getting the DOB from excel");
								logger.error("Excption WHile Getting dob ::"+nextRow.getRowNum(),e);
								
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						
						case 6:
							try{
								String course=(String) getCellValue(nextCell);
								logger.error("course ::"+course);
								if(course!=null && !course.equalsIgnoreCase("")){
									if(course.length()<=15){
										if(course.equalsIgnoreCase("2")){
											student.setCourse("+2");
											continue;
										}else{
											student.setCourse("UG");
											continue;
										}
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("Course","Should Be Less Than or Equals To 15 Digit");
										logger.error("course Should Be Less Than 15 digit... Row Number::"+nextRow.getRowNum()+" College Code Length ::"+course.length());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("Course", "Should Not Be Empty/Null");
									logger.error("course Should not be NULL/Empty... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
								
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("Course", "Error While getting the Course from excel");
								logger.error("Excption WHile Getting course::"+nextRow.getRowNum(),e);
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						case 7:
							try{
								String stream1=(String) getCellValue(nextCell);
								logger.error("stream ::"+stream1);
								if(stream1!=null && !stream1.equalsIgnoreCase("")){
									if(stream1.length()<=15){
										student.setStream(stream1);
										continue;
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("Stream","Should Be Less Than or Equals To 15 Digit");
										logger.error("stream Should Be Less Than 15 digit... Row Number::"+nextRow.getRowNum()+" College Code Length ::"+stream1.length());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("Stream", "Should Not Be Empty/Null");
									logger.error("stream Should not be NULL/Empty... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
								
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("Stream", "Error While getting the Stream from excel");
								logger.error("Excption WHile Getting stream1 ::"+nextRow.getRowNum(),e);
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						case 8:
							try{
								String yearBatch=(String) getCellValue(nextCell);
								logger.error("YearBatch ::"+yearBatch);
								if(yearBatch!=null && !yearBatch.equalsIgnoreCase("")){
									if(yearBatch.length()<=15){
										student.setYearBatch(yearBatch);
										continue;
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("Class","Should Be Less Than or Equals To 15 Digit");
										logger.error("yearBatch Should Be Less Than 15 digit... Row Number::"+nextRow.getRowNum()+" College Code Length ::"+yearBatch.length());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("Class", "Should Not Be Empty/Null");
									logger.error("yearBatch Should not be NULL/Empty... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
								
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("Class", "Error While getting the Class from excel");
								logger.error("Excption WHile Getting yearBatch ::"+nextRow.getRowNum(),e);
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						case 9:
							try{
								String category=(String) getCellValue(nextCell);
								logger.error("Category ::"+category);
								if(category!=null && !category.equalsIgnoreCase("")){
									if(category.length()<=15){
										student.setCategory(category);
										continue;
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("category","Should Be Less Than or Equals To 15 Digit");
										logger.error("category Should Be Less Than 15 digit... Row Number::"+nextRow.getRowNum()+" College Code Length ::"+category.length());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("category", "Should Not Be Empty/Null");
									logger.error("category Should not be NULL/Empty... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
								
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("category", "Error While getting the category from excel");
								logger.error("Excption WHile Getting category ::"+nextRow.getRowNum(),e);
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						case 10:
							try{
								String fundType=(String) getCellValue(nextCell);
								logger.error("FundType ::"+fundType);
								if(fundType!=null && !fundType.equalsIgnoreCase("")){
									if(fundType.length()<=1){
										if(fundType.equalsIgnoreCase("R")){
											student.setFundType("Re-Admission");
											continue;
										}
										else{
											if(fundType.equalsIgnoreCase("A")){
												student.setFundType("Admission");
												continue;
											}
										}
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("fundType","Should Be Less Than or Equals To 1 Digit");
										logger.error("Fund Type Should Be Less Than 15 digit... Row Number::"+nextRow.getRowNum()+" College Code Length ::"+fundType.length());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("fundType", "Should Not Be Empty/Null");
									logger.error("fund Type Should not be NULL/Empty... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
								
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("fundType", "Error While getting the fundType from excel");
								logger.error("Excption WHile Getting fundType ::"+nextRow.getRowNum(),e);
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						case 11:
							try{
								String createdDate=(String) getCellValue(nextCell);
								logger.error("CreatedDate ::"+createdDate);
								if(createdDate!=null && !createdDate.equalsIgnoreCase("")){
									if(createdDate.length()<=15){
										student.setCreated_Date(createdDate);
										continue;	
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("createdDate","Should Be Less Than or Equals To 15 Digit");
										logger.error("createdDate Should Be Less Than 15 digit... Row Number::"+nextRow.getRowNum()+" College Code Length ::"+createdDate.length());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("createdDate", "Should Not Be Empty/Null");
									logger.error("createdDate Should not be NULL/Empty... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
								
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("createdDate", "Error While getting the createdDate from excel");
								logger.error("Excption WHile Getting createdDate ::"+nextRow.getRowNum(),e);
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						case 12:
							try{
								String scienceType=(String) getCellValue(nextCell);
								logger.error("ScienceType ::"+scienceType);
								if(scienceType!=null && !scienceType.equalsIgnoreCase("")){
									if(scienceType.length()<=15){
										student.setScienceSelect(scienceType);
										break;	
									}else{
										Map<String,String> insideMap=new HashMap<String,String>();
										insideMap.put("Science Type","Should Be Less Than or Equals To 15 Digit");
										logger.error("Science Type Should Be Less Than 15 digit... Row Number::"+nextRow.getRowNum()+" College Code Length ::"+scienceType.length());
										errorMap.put(nextRow.getRowNum(), insideMap);
										break outerblock;
									}
								}else{
									Map<String,String> insideMap=new HashMap<String,String>();
									insideMap.put("Science Type", "Should Not Be Empty/Null");
									logger.error("Science Type Should not be NULL/Empty... Row Number::"+nextRow.getRowNum());
									errorMap.put(nextRow.getRowNum(), insideMap);
									break outerblock;
								}
								
							}catch(Exception e){
								Map<String,String> insideMap=new HashMap<String,String>();
								insideMap.put("Science Type", "Error While getting the Science Type from excel");
								logger.error("Excption WHile Getting Science Type ::"+nextRow.getRowNum(),e);
								errorMap.put(nextRow.getRowNum(), insideMap);
								break outerblock;
							}
						}
						System.out.println("Start Adding Student Details Into the List ::"+student.toString());
						System.out.println("Before Student List Size::"+listStudent.size());
						listStudent.add(student);
						System.out.println("After Student List Size::"+listStudent.size());
				  }
				}
			}
				else{
					 i=1;
					 continue;
				 }	
				}	
			}catch(Exception e){
				logger.error("Excption WHile Getting Reading The Excel Sheet",e);
				e.printStackTrace();
			}
			if(inputStream!=null)
			inputStream.close();
			String userName=(String) request.getSession().getAttribute("userName");
			logger.info("Start Uploading The Data");
			logger.info("Student List Size ::"+listStudent.size());
			logger.info("Student List ::"+listStudent.toString());
			String status=creationService.uploadStudent(listStudent, userName);
			logger.info("Status ::"+status);
			long endTime=System.currentTimeMillis();
			logger.info("End Time ::"+endTime);
			logger.info("Actual Time Taken ::"+(endTime-startTime));
			System.out.println("Actual Time Taken :: "+(endTime-startTime));
			if(status=="SUCCESS"){
				return status;
			}else{
				return "ERROR";
			}
			
		}catch(Exception e){
			e.printStackTrace();
			return "ERROR";
		}
	}
	
	
	private Object getCellValue(Cell cell) {
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();
			
		case Cell.CELL_TYPE_BOOLEAN:
			return cell.getBooleanCellValue();

		case Cell.CELL_TYPE_NUMERIC:
			cell.setCellType(Cell.CELL_TYPE_STRING);
			return cell.getStringCellValue();
		}
		return null;
	}
	
	
	private Workbook getWorkbook(InputStream inputStream, String excelFilePath) 
			throws IOException {
		Workbook workbook = null;
		
		if (excelFilePath.endsWith("xlsx")) {
			workbook = new XSSFWorkbook(inputStream);
		} else if (excelFilePath.endsWith("xls")) {
			workbook = new HSSFWorkbook(inputStream);
		} else {
			throw new IllegalArgumentException("The specified file is not Excel file");
		}
		
		return workbook;
	}
	
}
