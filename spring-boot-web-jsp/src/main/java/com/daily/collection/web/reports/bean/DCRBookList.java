package com.daily.collection.web.reports.bean;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.daily.collection.web.analytic.bean.HeadDeatils;

import lombok.Data;

@Component
@Data
public class DCRBookList {

    private String id;
    private String studentName;
    private String rollNo;
    private String dob;
    private String receiptNo;
    private String doj;
    private String stream;
    private String scienceType;
    private String classType;
    private String category;
    private String academicAmount;
    
    /*private String headName;
    private String tranAmount;*/
    
    private List<HeadDeatils> headDetailsList=new ArrayList<HeadDeatils>();
    
    
    
    
}
