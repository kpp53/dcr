package com.daily.collection.web.adminstration.service;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daily.collection.web.adminstration.bean.Configuration;
import com.daily.collection.web.adminstration.dao.AdminstratorDaoImpl;


@Service
public class AdminstratorServiceImpl implements IAdminstratorService {


	@Autowired
	AdminstratorDaoImpl adminstratorDAOimpl;
	
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public String saveConfiguration(Configuration configuration, String userName) throws Exception {
		String  courseStatus="SUCCESS";
		try{
			Calendar today=Calendar.getInstance();
				List<String> courseConfiguration= adminstratorDAOimpl.getCnfigurationDetailsByConfigId("2");
				 for(String str:configuration.getConfigCourse()){
						if(courseConfiguration.contains(str)){
							if(courseStatus.equalsIgnoreCase("SUCCESS")){
								courseStatus=adminstratorDAOimpl.updateConfiguration("Y",str,userName,today.getTime());
								courseConfiguration.remove(str);
							}else{
								throw new Exception ("Exception while Configuration Details For Course In Database");
							}
						}else{
							if(courseStatus.equalsIgnoreCase("SUCCESS")){
								courseStatus=adminstratorDAOimpl.updateConfiguration("N",str,userName,today.getTime());
							}else{
								throw new Exception ("Exception while Configuration Details For Course In Database");
							}
					  }
					}
				 if(!courseConfiguration.isEmpty()){
					 for (String string : courseConfiguration) {
							if(courseStatus.equalsIgnoreCase("SUCCESS")){
								courseStatus=adminstratorDAOimpl.updateConfiguration("N",string,userName,today.getTime());
							}else{
								throw new Exception ("Exception while Configuration Details For Course In Database");
							}
						} 
				 }
			 if(courseStatus.equalsIgnoreCase("SUCCESS")){
				 List<String> streamConfiguration= adminstratorDAOimpl.getCnfigurationDetailsByConfigId("3");
				 for(String str:configuration.getConfigStream()){
						if(streamConfiguration.contains(str)){
							if(courseStatus.equalsIgnoreCase("SUCCESS")){
								courseStatus=adminstratorDAOimpl.updateConfiguration("Y",str,userName,today.getTime());
								streamConfiguration.remove(str);
							}else{
								throw new Exception ("Exception while Configuration Details For Stream In Database");
							}
						}else{
							if(courseStatus.equalsIgnoreCase("SUCCESS")){
								courseStatus=adminstratorDAOimpl.updateConfiguration("N",str,userName,today.getTime());
							}else{
								throw new Exception ("Exception while Configuration Details For Stream In Database");
							}
						}
					}
				 if(streamConfiguration.isEmpty()){
					 return courseStatus;	 
				 }else{
					for (String string : streamConfiguration) {
						if(courseStatus.equalsIgnoreCase("SUCCESS")){
							courseStatus=adminstratorDAOimpl.updateConfiguration("N",string,userName,today.getTime());
						}else{
							throw new Exception ("Exception while Configuration Details For Stream In Database");
						}
					}
				 }
				 return courseStatus;
				 
			 }else{
				 throw new Exception ("Exception while Configuration Details For Stream In Database");
			 }
			 	
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception ("Exception while Configuration Details  In Database");
		}
	   
		
		
		
	}

}
