package com.daily.collection.web.transaction.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.daily.collection.web.creation.bean.Student;


@Repository
public interface ITransactionRepository {
	
	List<Student> findByCourseAndStream(String course, String sream);
	
	

	
	
}
