package com.daily.collection.web.adminstration.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daily.collection.web.adminstration.bean.Configuration;
import com.daily.collection.web.adminstration.service.IAdminstratorService;
import com.daily.collection.web.common.DataUtil;
import com.daily.collection.web.login.service.ILoginService;
import com.google.gson.Gson;

@Controller
public class AdminstrationController {

	@Autowired
	IAdminstratorService adminstratorService;
	
	@Autowired
    DataUtil dataUtil;	
	
	
	@RequestMapping(value="/saveConfigurationDetails", method=RequestMethod.POST)
	public @ResponseBody String saveConfigurationDetails(@RequestBody String cnfigJson,HttpServletRequest request){
		try{
			Gson gson = new Gson();
		    Configuration cofig = gson.fromJson(cnfigJson, Configuration.class);
		    String username=(String) request.getSession().getAttribute("userName");
		    if(cofig.getConfigCourse()==null || Arrays.equals(cofig.getConfigCourse(), new String[]{})){
		    	cofig.setConfigCourse(new String[]{"+2","UG","PG"});
		    }
		    if(cofig.getConfigStream()==null || Arrays.equals(cofig.getConfigCourse(), new String[]{})){
		    	cofig.setConfigStream(new String[]{"Science","Arts","Commerce"});
		    }
		    String returStatus=adminstratorService.saveConfiguration(cofig, username);
		    if(returStatus.equalsIgnoreCase("SUCCESS")){
		    	dataUtil.createDropDownMap(request);
		    	request.getSession().getAttribute("config");
		    }
		    return returStatus;
		}catch(Exception e){
			e.printStackTrace();
			return "ERROR";
		}
	}
	
	
}
