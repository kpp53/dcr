package com.daily.collection.web.analytic.dao;

import java.util.List;
import java.util.Map;

import com.daily.collection.web.analytic.bean.HeadDeatils;

public interface IAnalyticDAO {
	//HEAD_CATEGORIES_DETAILS_UG
	/*public final static String GET_HEAD_AMOUNT="SELECT HD.HEADNAME AS HEADNAME,HC.HEAD_AMOUNT AS HEADAMOUNT FROM HEAD_DETAILS HD, HEAD_CATEGORIES HC "+
             "WHERE HD.ID=HC.ID AND HC.COURSE_NAME=? AND HC.STREAM_NAME=? AND HC.YEAR_BATCH=? AND  HC.CATEGORIES_DETAILS=? AND HC.YEAR_SESSION=?";*/
	public final static String GET_HEAD_AMOUNT="SELECT HD.HEADNAME,HCD.AMOUNT FROM HEAD_DETAILS HD, HEAD_CATEGORIES_DETAILS HCD "+ 
												"WHERE HCD.HEAD_DETAILS_ID=HD.ID AND "+
												"HCD.HEAD_CATEGORIES_ID=? AND HCD.YEAR_SESSION=?";
	public List<Map<String, Object>> getHeadAmount(Object[] objectParams);
	
	public final static String GET_HEAD_AMOUNT_UG="SELECT HD.HEADNAME,HCD.AMOUNT FROM HEAD_DETAILS_UG HD, HEAD_CATEGORIES_DETAILS_UG HCD "+ 
			"WHERE HCD.HEAD_DETAILS_ID=HD.ID AND "+
			"HCD.HEAD_CATEGORIES_ID=? AND HCD.YEAR_SESSION=?";
    public List<Map<String, Object>> getHeadAmountUG(Object[] objectParams);
	
	public final static String HEAD_DROP_DOWN="SELECT HEADNAME FROM HEAD_DETAILS";
	public List<Map<String, Object>> getHeadDropDown();
	
	public final static String HEAD_DROP_DOWN_UG="SELECT HEADNAME FROM HEAD_DETAILS_UG";
	public List<Map<String, Object>> getHeadDropDownUG();
	
	
	/*public  final static String HEAD_COUNT="SELECT COUNT(*) FROM HEAD_DETAILS";
	public int getHeadCount() throws Exception;*/
	
	public  final static String GET_GEAD_ID_BY_NAME="SELECT ID FROM HEAD_DETAILS WHERE HEADNAME=?";
	public String getheadId(String headname);
	
	public  final static String GET_GEAD_ID_BY_NAME_UG="SELECT ID FROM HEAD_DETAILS_UG WHERE HEADNAME=?";
	public String getheadIdUG(String headname);
	
	
	public  final static String UPDATE_HEAD_AMOUNT="UPDATE HEAD_DETAILS SET HEADAMOUNT=?,MODIFIED_DATE=sysdate,MODIFIED_BY=? WHERE ID=?";
	public int updateHeadAmount(int headAmount,String modifiedBy,String ID);
	
	
	public  final static String ADD_HEAD_DETAILS_CATEGORY="INSERT INTO HEAD_CATEGORIES_DETAILS"
			+ " (HEAD_DETAILS_ID,HEAD_CATEGORIES_ID,AMOUNT,CREATED_DATE,CREATED_BY,YEAR_SESSION) VALUES(?,?,?,sysdate,?,?)";
	public String addHeadForClassAndStream(Object[] headParams);
	
	public  final static String ADD_HEAD_DETAILS_CATEGORY_UG="INSERT INTO HEAD_CATEGORIES_DETAILS_UG"
			+ " (HEAD_DETAILS_ID,HEAD_CATEGORIES_ID,AMOUNT,CREATED_DATE,CREATED_BY,YEAR_SESSION) VALUES(?,?,?,sysdate,?,?)";
	public String addHeadForClassAndStream_UG(Object[] headParams);
	
	
	public final static String IS_HEAD_EXIST_IN_CATEGORIES="SELECT COUNT(*) FROM HEAD_CATEGORIES_DETAILS WHERE HEAD_DETAILS_ID=? AND HEAD_CATEGORIES_ID=? AND YEAR_SESSION=?";
	public int getHeadCategoriesExist(Object[] headParams) throws Exception;
	
	public final static String IS_HEAD_EXIST_IN_CATEGORIES_UG="SELECT COUNT(*) FROM HEAD_CATEGORIES_DETAILS_UG WHERE HEAD_DETAILS_ID=? AND HEAD_CATEGORIES_ID=? AND YEAR_SESSION=?";
	public int getHeadCategoriesExistUG(Object[] headParams) throws Exception;
	
	
	public final static String UPDATE_HEAD_DETAILS_CATEGORY="UPDATE HEAD_CATEGORIES_DETAILS SET AMOUNT=? WHERE HEAD_CATEGORIES_ID=? AND HEAD_DETAILS_ID=?";
	public int updateHeadForClassAndStream(Object[] headParams);
	
	public final static String UPDATE_HEAD_DETAILS_CATEGORY_UG="UPDATE HEAD_CATEGORIES_DETAILS_UG SET AMOUNT=? WHERE HEAD_CATEGORIES_ID=? AND HEAD_DETAILS_ID=?";
	public int updateHeadForClassAndStream_UG(Object[] headParams);
	
	
	public final static String UPDATE_HEAD_DETAILS_CATEGORY_BY_ID="UPDATE HEAD_CATEGORIES_DETAILS SET AMOUNT=?, MODIFIED_DATE=?,MODIFIED_BY=? WHERE HEAD_DETAILS_ID=? AND HEAD_CATEGORIES_ID=? AND YEAR_SESSION=?";
	public String editHeadByCategory_By_Id(Object[] params) throws Exception;
	
	public final static String UPDATE_HEAD_DETAILS_CATEGORY_BY_ID_UG="UPDATE HEAD_CATEGORIES_DETAILS_UG SET AMOUNT=?, MODIFIED_DATE=?,MODIFIED_BY=? WHERE HEAD_DETAILS_ID=? AND HEAD_CATEGORIES_ID=? AND YEAR_SESSION=?";
	public String editHeadByCategory_By_IdUG(Object[] params) throws Exception;
	
	public final static String DELETE_HEAD_DETAILS_CATEGORY_BY_ID="DELETE FROM HEAD_CATEGORIES_DETAILS WHERE HEAD_DETAILS_ID=? AND HEAD_CATEGORIES_ID=? AND YEAR_SESSION=?";
	public String deleteHeadByCategory_By_Id(Object[] params) throws Exception;
	
	public final static String DELETE_HEAD_DETAILS_CATEGORY_BY_ID_UG="DELETE FROM HEAD_CATEGORIES_DETAILS_UG WHERE HEAD_DETAILS_ID=? AND HEAD_CATEGORIES_ID=? AND YEAR_SESSION=?";
	public String deleteHeadByCategory_By_IdUG(Object[] params) throws Exception;
	
	public final static String GET_HEAD_CATEGORIES_ID="SELECT HEAD_CATEGORIES_ID FROM HEAD_CATEGORIES WHERE COURSE_NAME=? AND STREAM_NAME=? AND YEAR_BATCH=? AND CATEGORIES_DETAILS=?";
	public String getHeadCatorigesId(Object[] params) throws Exception;
	
	
	public static final String INSERT_HEAD_NAME = "INSERT INTO HEAD_DETAILS (HEADNAME,CREATED_DATE,CREATED_BY,MODIFIED_DATE,MODIFIED_BY) values (?,sysdate,?,null,null)";
	String insertHeadName(final Object[] p0);
	
	public static final String INSERT_HEAD_NAME_UG = "INSERT INTO HEAD_DETAILS_UG (HEADNAME,CREATED_DATE,CREATED_BY,MODIFIED_DATE,MODIFIED_BY) values (?,sysdate,?,null,null)";
	String insertHeadNameUG(final Object[] p0);
	
	
	
	
	
	
	
	
	
	
	
	
	
}
