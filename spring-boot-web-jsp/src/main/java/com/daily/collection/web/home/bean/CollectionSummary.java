package com.daily.collection.web.home.bean;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class CollectionSummary implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String streamName;
	private String studentCount;
	private String totalAmount;
	

	
	
	
}
