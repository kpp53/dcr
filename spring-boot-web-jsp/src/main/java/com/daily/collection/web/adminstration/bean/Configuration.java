package com.daily.collection.web.adminstration.bean;

import org.springframework.stereotype.Component;

@Component
public class Configuration {
	
	public String configurationId;
	
	public String[] configCourse;
	
	public String[] configStream;
	
	

	public String getConfigurationId() {
		return configurationId;
	}

	public void setConfigurationId(String configurationId) {
		this.configurationId = configurationId;
	}

	public String[] getConfigCourse() {
		return configCourse;
	}

	public void setConfigCourse(String[] configCourse) {
		this.configCourse = configCourse;
	}

	public String[] getConfigStream() {
		return configStream;
	}

	public void setConfigStream(String[] configStream) {
		this.configStream = configStream;
	}
	
	
	
	

}
