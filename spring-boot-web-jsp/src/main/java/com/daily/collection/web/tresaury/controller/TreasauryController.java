package com.daily.collection.web.tresaury.controller;

import java.text.ParseException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daily.collection.web.common.DataUtil;
import com.daily.collection.web.transaction.service.ITransactionService;
import com.daily.collection.web.tresaury.bean.CollectionBean;
import com.daily.collection.web.tresaury.bean.CollectionList;
import com.daily.collection.web.tresaury.service.ITreasuryService;
import com.google.gson.Gson;

@Controller
public class TreasauryController {

	
	private final static Logger log = Logger.getLogger(TreasauryController.class);
	
	@Autowired
	CollectionBean collectionBean;
	
	@Autowired
	ITreasuryService treasuryService;
	
	@Autowired
	ITransactionService transactionService;
	
	private String allReportStatus="";
	
	@RequestMapping("/showCollection")
	public String showCollection(HttpServletRequest req,HttpServletResponse res,Map<String, Object> model){
		if(allReportStatus.equalsIgnoreCase("all")){
			allReportStatus="";
			req.setAttribute("reqcollectionBean", req.getSession().getAttribute("collectionBean"));
			model.put("collectionBeanObj", collectionBean);
		}else{
			req.getSession().setAttribute("print_url", transactionService.getAvailableAmount("5"));	
		}
		
		return "views/tresaury/collection";
	}
	
	
	@RequestMapping(value="/getCollectionReport", method=RequestMethod.POST)
	public @ResponseBody String getCollectionReport(@RequestBody String collectionJson,HttpServletRequest req,Map<String, Object> model) throws ParseException {
		log.info("Inside getCollectionReport Method");
		try{
			Gson gson = new Gson();
			allReportStatus="";
		    collectionBean = gson.fromJson(collectionJson, CollectionBean.class);
		    CollectionBean collectionResultBean=treasuryService.getCollectionList(collectionBean, req);
		    if(collectionResultBean!=null){
		    	allReportStatus="all";
		    	req.getSession().setAttribute("collectionBean", collectionResultBean);
				return "SUCCESS";	
		    }
		    return "ERROR";	
		}catch(Exception e){
			log.error("Sending Error response",e);
			e.printStackTrace();
			return "ERROR";
		}
			
	}
	
	@RequestMapping("/collectionPrint")
	public String collectionPrint(HttpServletRequest req,HttpServletResponse res,Map<String, Object> model){
		req.setAttribute("reqcollectionBean",req.getSession().getAttribute("collectionBean"));
		model.put("collectionBeanObj", req.getSession().getAttribute("collectionBean"));
		collectionBean=(CollectionBean) req.getSession().getAttribute("collectionBean");
		int amount=0;
		for (Iterator iterator = collectionBean.getCollectionList().iterator(); iterator.hasNext();) {
			CollectionList  type = (CollectionList ) iterator.next();
			amount=amount+Integer.parseInt(type.getCafAmount())+Integer.parseInt(type.getMiscellaniousAmount())+Integer.parseInt(type.getTotalFund());
		}
		
		
		String inWords=DataUtil.convert(amount);
		req.setAttribute("amt", inWords);
		return "views/print/collectionPrint";
	}
	
	
}
