package com.daily.collection.web.analytic.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AnalyticDAOImpl implements IAnalyticDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	
	@Override
	public String getheadId(String headname){
	   try{
		   String id=jdbcTemplate.queryForObject(IAnalyticDAO.GET_GEAD_ID_BY_NAME, String.class, new Object[]{headname});
		   if(id!=null && !id.equalsIgnoreCase("")){
			   return id;
		   }
		   return null;
	   }catch(Exception e){
		   e.printStackTrace();
		   return "ERROR";
	   }
	}


	/*@Override
	public int getHeadCount() throws Exception {
		try{
			int i=jdbcTemplate.queryForObject(IAnalyticDAO.HEAD_COUNT, Integer.class);
			return i;
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
	}*/


	@Override
	public String addHeadForClassAndStream(Object[] headParams) {
		  try{
			   int i=jdbcTemplate.update(IAnalyticDAO.ADD_HEAD_DETAILS_CATEGORY, headParams);
			   if(i==1){
				   return "SUCCESS";
			   }
				return "ERROR";
		   }catch(Exception e){
			   e.printStackTrace();
			   return "ERROR";
		   }
	}


	@Override
	public List<Map<String, Object>> getHeadDropDown() {
		 try{
			List<Map<String, Object>> listDropDown=jdbcTemplate.queryForList(IAnalyticDAO.HEAD_DROP_DOWN);
			   if(listDropDown.isEmpty()){
				   return null;
			   }else{
				   return listDropDown;
			   }
		   }catch(Exception e){
			   e.printStackTrace();
			   return null;
		   }
	}
	
	
	@Override
	public List<Map<String, Object>> getHeadDropDownUG() {
		 try{
				List<Map<String, Object>> listDropDown=jdbcTemplate.queryForList(IAnalyticDAO.HEAD_DROP_DOWN_UG);
				   if(listDropDown.isEmpty()){
					   return null;
				   }else{
					   return listDropDown;
				   }
			   }catch(Exception e){
				   e.printStackTrace();
				   return null;
			   }
	}
	


	@Override
	public List<Map<String, Object>> getHeadAmount(Object[] headParams) {
		 try{
			 List<Map<String, Object>> queryForMap = jdbcTemplate.queryForList(IAnalyticDAO.GET_HEAD_AMOUNT, headParams);
			   if(queryForMap.isEmpty()){
				   return null;
			   }else{
				   return queryForMap;
			   }
		   }catch(Exception e){
			   e.printStackTrace();
			   return null;
		   }
	}


	@Override
	public int getHeadCategoriesExist(Object[] headParams) throws Exception {
		try{
			int i=jdbcTemplate.queryForObject(IAnalyticDAO.IS_HEAD_EXIST_IN_CATEGORIES,headParams,Integer.class);
			return i;
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
	}


	@Override
	public int updateHeadForClassAndStream(Object[] headParams) {
		 try{
			   int i=jdbcTemplate.update(IAnalyticDAO.UPDATE_HEAD_DETAILS_CATEGORY, headParams);
			   return i; 
		   }catch(Exception e){
			   e.printStackTrace();
			   return 0;
		   }
	}


	@Override
	public int updateHeadAmount(int headAmount, String modifiedBy, String ID) {
		 try{
			   int i=jdbcTemplate.update(IAnalyticDAO.UPDATE_HEAD_AMOUNT, new Object[]{headAmount,modifiedBy,ID});
			   return i; 
		   }catch(Exception e){
			   e.printStackTrace();
			   return 0;
		   }
	}


	@Override
	public String editHeadByCategory_By_Id(Object[] params) throws Exception {
		try{
			   int i=jdbcTemplate.update(IAnalyticDAO.UPDATE_HEAD_DETAILS_CATEGORY_BY_ID,params);
			   if(i==1){
				   return "SUCCESS";
			   }
			   return "ERROR"; 
		   }catch(Exception e){
			   e.printStackTrace();
			   return "ERROR"; 
		   }
	}


	@Override
	public String deleteHeadByCategory_By_Id(Object[] params) throws Exception {
		try{
			   int i=jdbcTemplate.update(IAnalyticDAO.DELETE_HEAD_DETAILS_CATEGORY_BY_ID,params);
			   if(i==1){
				   return "SUCCESS";
			   }
			   return "ERROR"; 
		   }catch(Exception e){
			   e.printStackTrace();
			   return "ERROR"; 
		   }
	}


	@Override
	public String getHeadCatorigesId(Object[] params) throws Exception {
		 try{
			   String id=jdbcTemplate.queryForObject(IAnalyticDAO.GET_HEAD_CATEGORIES_ID, String.class, params);
			   if(id!=null && !id.equalsIgnoreCase("")){
				   return id;
			   }
			   return null;
		   }catch(Exception e){
			   e.printStackTrace();
			   return "ERROR";
		   }
	}


	@Override
	public String getheadIdUG(String headname) {
		 try{
			   String id=jdbcTemplate.queryForObject(IAnalyticDAO.GET_GEAD_ID_BY_NAME_UG, String.class, new Object[]{headname});
			   if(id!=null && !id.equalsIgnoreCase("")){
				   return id;
			   }
			   return null;
		   }catch(Exception e){
			   e.printStackTrace();
			   return "ERROR";
		   }
	}


	@Override
	public List<Map<String, Object>> getHeadAmountUG(Object[] objectParams) {
		try{
			 List<Map<String, Object>> queryForMap = jdbcTemplate.queryForList(IAnalyticDAO.GET_HEAD_AMOUNT_UG, objectParams);
			   if(queryForMap.isEmpty()){
				   return null;
			   }else{
				   return queryForMap;
			   }
		   }catch(Exception e){
			   e.printStackTrace();
			   return null;
		   }
	}


	@Override
	public String addHeadForClassAndStream_UG(Object[] headParams) {
		try{
			   int i=jdbcTemplate.update(IAnalyticDAO.ADD_HEAD_DETAILS_CATEGORY_UG, headParams);
			   if(i==1){
				   return "SUCCESS";
			   }
				return "ERROR";
		   }catch(Exception e){
			   e.printStackTrace();
			   return "ERROR";
		   }
	}


	@Override
	public int getHeadCategoriesExistUG(Object[] headParams) throws Exception {
		try{
			int i=jdbcTemplate.queryForObject(IAnalyticDAO.IS_HEAD_EXIST_IN_CATEGORIES_UG,headParams,Integer.class);
			return i;
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
	}


	@Override
	public int updateHeadForClassAndStream_UG(Object[] headParams) {
		 try{
			   int i=jdbcTemplate.update(IAnalyticDAO.UPDATE_HEAD_DETAILS_CATEGORY_UG, headParams);
			   return i; 
		   }catch(Exception e){
			   e.printStackTrace();
			   return 0;
		   }
	}


	@Override
	public String editHeadByCategory_By_IdUG(Object[] params) throws Exception {
		try{
			   int i=jdbcTemplate.update(IAnalyticDAO.UPDATE_HEAD_DETAILS_CATEGORY_BY_ID_UG,params);
			   if(i==1){
				   return "SUCCESS";
			   }
			   return "ERROR"; 
		   }catch(Exception e){
			   e.printStackTrace();
			   return "ERROR"; 
		   }
	}


	@Override
	public String deleteHeadByCategory_By_IdUG(Object[] params) throws Exception {
		try{
			   int i=jdbcTemplate.update(IAnalyticDAO.DELETE_HEAD_DETAILS_CATEGORY_BY_ID_UG,params);
			   if(i==1){
				   return "SUCCESS";
			   }
			   return "ERROR"; 
		   }catch(Exception e){
			   e.printStackTrace();
			   return "ERROR"; 
		   }
	}
	
	
	 @Override
	    public String insertHeadNameUG(final Object[] params) {
	        try {
	            final int i = this.jdbcTemplate.update("INSERT INTO HEAD_DETAILS_UG (HEADNAME,CREATED_DATE,CREATED_BY,MODIFIED_DATE,MODIFIED_BY) values (?,sysdate,?,null,null)", params);
	            if (i == 1) {
	                return "SUCCESS";
	            }
	            return "ERROR";
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	            return "ERROR";
	        }
	    }
	    
	    @Override
	    public String insertHeadName(final Object[] params) {
	        try {
	            final int i = this.jdbcTemplate.update("INSERT INTO HEAD_DETAILS (HEADNAME,CREATED_DATE,CREATED_BY,MODIFIED_DATE,MODIFIED_BY) values (?,sysdate,?,null,null)", params);
	            if (i == 1) {
	                return "SUCCESS";
	            }
	            return "ERROR";
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	            return "ERROR";
	        }
	    }


	

}
