package com.daily.collection.web.download.excel;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DownloadExcel {

	
	
	@RequestMapping(value="/downloadReport", method = RequestMethod.GET)
	public String downloadAllReport(HttpServletRequest req,Model model){
		String comingFrom=req.getParameter("from");
		model.addAttribute("from", comingFrom);
		return "";
	}
	
	
}
