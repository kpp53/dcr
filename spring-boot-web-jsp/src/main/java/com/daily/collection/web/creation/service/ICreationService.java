package com.daily.collection.web.creation.service;

import java.text.ParseException;
import java.util.List;

import com.daily.collection.web.creation.bean.Student;

public interface ICreationService {
	
	public String uploadStudent(List<Student> student,String userName) throws ParseException, Exception;

}
