package com.daily.collection.web.analytic.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daily.collection.web.analytic.bean.HeadDeatils;
import com.daily.collection.web.analytic.service.IAnalyticService;
import com.google.gson.Gson;

@Controller
public class AnalyticController {
	
	
	@Autowired
	HeadDeatils headD;
	
	@Autowired
	IAnalyticService analyticSerivice;
	
	
	@RequestMapping("/showManageHead")
	public String showManageHead(HttpServletRequest req,Map<String, Object> model) {
		model.put("head", headD);
		try{
			analyticSerivice.getHeadDropDown(req);
			return "views/Analytic/analytic_headmanage";		
		}catch(Exception e){
			return "views/Analytic/analytic_headmanage";
		}
				
	}
	
	@RequestMapping("/showManageDetails")
	public String showManageDetails(HttpServletRequest req,Map<String, Object> model) {
		try{
			return "views/Analytic/analytic_headdetail";		
		}catch(Exception e){
			return "views/Analytic/analytic_headdetail";
		}
				
	}
	
	@RequestMapping("/searchBySession")
	public @ResponseBody String searchBySession(@RequestBody String session,HttpServletRequest req,Map<String, Object> model) {
		try{
			Gson gson = new Gson();
			String yearSession = gson.fromJson(session, String.class);
			List<String> applicationConfig=(List<String> )req.getSession().getAttribute("applicationConfig");
			String dashboardConfig=(String)req.getSession().getAttribute("config");
			if(dashboardConfig.equalsIgnoreCase("+2")){
				analyticSerivice.getHeadAmount(req,yearSession,applicationConfig);
			}
			else if(dashboardConfig.equalsIgnoreCase("UG")){
				analyticSerivice.getHeadAmountUG(req,yearSession,applicationConfig);
			}else{
				
			}
			
			return "SUCCESS";		
		}catch(Exception e){
			return "ERROR";
		}
				
	}
	
	@RequestMapping("/searchBySessionForDetails")
	public @ResponseBody String searchBySessionForDetails(@RequestBody String session,HttpServletRequest req,Map<String, Object> model) {
		try{
			Gson gson = new Gson();
			String yearSession = gson.fromJson(session, String.class);
			List<String> applicationConfig=(List<String> )req.getSession().getAttribute("applicationConfig");
			String dashboardConfig=(String)req.getSession().getAttribute("config");
			if(dashboardConfig.equalsIgnoreCase("+2")){
				analyticSerivice.getHeadAmount(req,yearSession,applicationConfig);
			}
			else if(dashboardConfig.equalsIgnoreCase("UG")){
				analyticSerivice.getHeadAmountUG(req,yearSession,applicationConfig);
			}else{
				
			}
			return "SUCCESS";		
		}catch(Exception e){
			return "ERROR";
		}
				
	}
	
	
	
	@RequestMapping("/saveHead")
	public String saveHead(@ModelAttribute("head") HeadDeatils headDetails,HttpServletRequest req,Map<String, Object> model) {
		String userName=(String) req.getSession().getAttribute("userName");
		List<String> applicationConfig=(List<String> )req.getSession().getAttribute("applicationConfig");
		String dashboardConfig=(String)req.getSession().getAttribute("config");
		try {
			if(dashboardConfig.equalsIgnoreCase("+2")){
				analyticSerivice.addHead(headDetails, userName,applicationConfig);
			}
			else if(dashboardConfig.equalsIgnoreCase("UG")){
				analyticSerivice.addHeadUG(headDetails, userName,applicationConfig);
			}else{
				
			}
			
			analyticSerivice.getHeadDropDown(req);
			req.getSession().removeAttribute("searchSession");
			return "views/Analytic/analytic_headmanage";
		} catch (Exception e) {
			e.printStackTrace();
			return "views/Analytic/analytic_headmanage";
		}
	}
	
	
	/*@RequestMapping(value ="/getHeadAmount" , method = RequestMethod.POST )
	public @ResponseBody int getHeadAmount(@RequestBody String headname,HttpServletRequest req){
		try{
			Gson gson = new Gson();
		    String headName = gson.fromJson(headname, String.class);
		    List<HeadDeatils> headDetails=(List<HeadDeatils>) req.getSession().getAttribute("dropdownList");
		    for (HeadDeatils headDeatils : headDetails) {
		    	if(headName.equalsIgnoreCase(headDeatils.getHeadname())){
		    		return headDeatils.getHeadAmount();
		    	}
			}
		    return 0;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
	}*/
	
	
	@RequestMapping(value ="/firsteditHead" , method = RequestMethod.POST )
	public @ResponseBody Integer firsteditHead(@RequestBody String head,HttpServletRequest req){
		try{
			Gson gson = new Gson();
			HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
			String headId=headDeatils.getId();
			String headName=headId.substring(0, 13);
		    List<HeadDeatils> headDetails=null;
		    String userName=(String) req.getSession().getAttribute("userName");
		    String yearSession=(String) req.getSession().getAttribute("searchSession");
		    String dashboardConfig=(String) req.getSession().getAttribute("config");
		    String status=null;
		    if(dashboardConfig.equalsIgnoreCase("+2")){
		    	 headDetails=(List<HeadDeatils>) req.getSession().getAttribute("head1");
		         status=analyticSerivice.editHeadByCategory(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);	
		    }else if(dashboardConfig.equalsIgnoreCase("UG")){
		    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG1");
		    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
		    }else{
		    	
		    }
		    
		    if("SUCCESS".equalsIgnoreCase(status)){
		    	int index=Integer.parseInt(headId.substring(13, headId.length()));
		    	headDetails.remove(index);
		    	headDetails.add(index, headDeatils);
		    	int total=0;
		    	for (HeadDeatils headDeatils2 : headDetails) {
		    		total=total+headDeatils2.getHeadAmount();
				}
		    	return total;
		    }
		    return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	@RequestMapping(value ="/secondeditHead" , method = RequestMethod.POST )
	public @ResponseBody Integer secondeditHead(@RequestBody String head,HttpServletRequest req){
		try{
			Gson gson = new Gson();
			HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
			String headId=headDeatils.getId();
			String headName=headId.substring(0, 14);
		    List<HeadDeatils> headDetails=null;
		    String userName=(String) req.getSession().getAttribute("userName");
		    String yearSession=(String) req.getSession().getAttribute("searchSession");
		    String dashboardConfig=(String) req.getSession().getAttribute("config");
		    String status=null;
		    if(dashboardConfig.equalsIgnoreCase("+2")){
		    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("head2"); 
		    	status=analyticSerivice.editHeadByCategory(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);	
		    }else if(dashboardConfig.equalsIgnoreCase("UG")){
		    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG2");
		    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
		    }else{
		    	
		    }
		    if("SUCCESS".equalsIgnoreCase(status)){
		    	int index=Integer.parseInt(headId.substring(14, headId.length()));
		    	headDetails.remove(index);
		    	headDetails.add(index, headDeatils);
		    	int total=0;
		    	for (HeadDeatils headDeatils2 : headDetails) {
		    		total=total+headDeatils2.getHeadAmount();
				}
		    	return total;
		    }
		    return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value ="/thirdeditHead" , method = RequestMethod.POST )
	public @ResponseBody Integer thirdeditHead(@RequestBody String head,HttpServletRequest req){
		try{
			Gson gson = new Gson();
			HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
			String headId=headDeatils.getId();
			String headName=headId.substring(0, 13);
		    List<HeadDeatils> headDetails=null;
		    String userName=(String) req.getSession().getAttribute("userName");
		    String yearSession=(String) req.getSession().getAttribute("searchSession");
		    String dashboardConfig=(String) req.getSession().getAttribute("config");
		    String status=null;
		    if(dashboardConfig.equalsIgnoreCase("+2")){
		    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("head3");
		         status=analyticSerivice.editHeadByCategory(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);	
		    }else if(dashboardConfig.equalsIgnoreCase("UG")){
		    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG3");
		    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
		    }else{
		    	
		    }
		    if("SUCCESS".equalsIgnoreCase(status)){
		    	int index=Integer.parseInt(headId.substring(13, headId.length()));
		    	headDetails.remove(index);
		    	headDetails.add(index, headDeatils);
		    	int total=0;
		    	for (HeadDeatils headDeatils2 : headDetails) {
		    		total=total+headDeatils2.getHeadAmount();
				}
		    	return total;
		    }
		    return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value ="/fourtheditHead" , method = RequestMethod.POST )
	public @ResponseBody Integer fourtheditHead(@RequestBody String head,HttpServletRequest req){
		try{
			Gson gson = new Gson();
			HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
			String headId=headDeatils.getId();
			String headName=headId.substring(0, 14);
		    List<HeadDeatils> headDetails=null;
		    String userName=(String) req.getSession().getAttribute("userName");
		    String yearSession=(String) req.getSession().getAttribute("searchSession");
		    String dashboardConfig=(String) req.getSession().getAttribute("config");
		    String status=null;
		    if(dashboardConfig.equalsIgnoreCase("+2")){
		    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("head4");
		         status=analyticSerivice.editHeadByCategory(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);	
		    }else if(dashboardConfig.equalsIgnoreCase("UG")){
		    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG4");
		    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
		    }else{
		    	
		    }
		    if("SUCCESS".equalsIgnoreCase(status)){
		    	int index=Integer.parseInt(headId.substring(14, headId.length()));
		    	headDetails.remove(index);
		    	headDetails.add(index, headDeatils);
		    	int total=0;
		    	for (HeadDeatils headDeatils2 : headDetails) {
		    		total=total+headDeatils2.getHeadAmount();
				}
		    	return total;
		    }
		    return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

@RequestMapping(value ="/fiftheditHead" , method = RequestMethod.POST )
public @ResponseBody Integer fiftheditHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 13);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("head5");
	         status=analyticSerivice.editHeadByCategory(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG5");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    }else{
	    	
	    }
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(13, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}

@RequestMapping(value ="/sixtheditHead" , method = RequestMethod.POST )
public @ResponseBody Integer sixtheditHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 13);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("head6");
	         status=analyticSerivice.editHeadByCategory(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG6");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    }else{
	    	
	    }
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(13, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}




@RequestMapping(value ="/seventheditHead" , method = RequestMethod.POST )
public @ResponseBody Integer seventheditHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 15);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("head7");
	         status=analyticSerivice.editHeadByCategory(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG7");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    }else{
	    	
	    }
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(15, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/eightheditHead" , method = RequestMethod.POST )
public @ResponseBody Integer eightheditHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 14);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("head8");
	         status=analyticSerivice.editHeadByCategory(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG8");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    }else{
	    	
	    }
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(14, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}

@RequestMapping(value ="/nintheditHead" , method = RequestMethod.POST )
public @ResponseBody Integer nintheditHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 13);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("head9");
	         status=analyticSerivice.editHeadByCategory(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG9");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    }else{
	    	
	    }
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(13, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/tengtheditHead" , method = RequestMethod.POST )
public @ResponseBody Integer tengtheditHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 14);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("head10");
	         status=analyticSerivice.editHeadByCategory(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG10");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    }else{
	    	
	    }
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(14, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}

@RequestMapping(value ="/eleventheditHead" , method = RequestMethod.POST )
public @ResponseBody Integer eleventheditHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 17);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("head11");
	         status=analyticSerivice.editHeadByCategory(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG11");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    }else{
	    	
	    }
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(17, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/tweltheditHead" , method = RequestMethod.POST )
public @ResponseBody Integer tweltheditHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 14);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("head12");
	         status=analyticSerivice.editHeadByCategory(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG12");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    }else{
	    	
	    }
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(14, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/editthiteenhead" , method = RequestMethod.POST )
public @ResponseBody Integer editthiteenhead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 15);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG13");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(15, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/editfourteenhead" , method = RequestMethod.POST )
public @ResponseBody Integer editfourteenhead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 16);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG14");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(16, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/editfifteenhead" , method = RequestMethod.POST )
public @ResponseBody Integer editfifteenhead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 15);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG15");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(15, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/editsixteenhead" , method = RequestMethod.POST )
public @ResponseBody Integer editsixteenhead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 15);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG16");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(15, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/editseventeenhead" , method = RequestMethod.POST )
public @ResponseBody Integer editseventeenhead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 17);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG17");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(17, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/editeighteenhead" , method = RequestMethod.POST )
public @ResponseBody Integer editeighteenhead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		HeadDeatils headDeatils = gson.fromJson(head, HeadDeatils.class);
		String headId=headDeatils.getId();
		String headName=headId.substring(0, 16);
	    List<HeadDeatils> headDetails=null;
	    String userName=(String) req.getSession().getAttribute("userName");
	    String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String dashboardConfig=(String) req.getSession().getAttribute("config");
	    String status=null;
	    	headDetails=(List<HeadDeatils>) req.getSession().getAttribute("headUG18");
	    	status=analyticSerivice.editHeadByCategoryUG(headDeatils.getHeadname(), headDeatils.getHeadAmount(), userName,headName,yearSession);
	    if("SUCCESS".equalsIgnoreCase(status)){
	    	int index=Integer.parseInt(headId.substring(16, headId.length()));
	    	headDetails.remove(index);
	    	headDetails.add(index, headDeatils);
	    	int total=0;
	    	for (HeadDeatils headDeatils2 : headDetails) {
	    		total=total+headDeatils2.getHeadAmount();
			}
	    	return total;
	    }
	    return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}







@RequestMapping(value ="/deleteFirstHead" , method = RequestMethod.POST )
public @ResponseBody String deleteFirstHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		if(dashboardConfig.equalsIgnoreCase("+2")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("head1");
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG1");
	    }else{
	    }

		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
		
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	status=analyticSerivice.deleteHeadByCategory(headDetails, userName,"deletefirsthead",yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletefirsthead",yearSession);
	    }else{
	    }
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
		
		
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deleteSecondHead" , method = RequestMethod.POST )
public @ResponseBody String deleteSecondHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		if(dashboardConfig.equalsIgnoreCase("+2")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("head2");
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG2");
	    }else{
	    }
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	status=analyticSerivice.deleteHeadByCategory(headDetails, userName,"deletesecondhead",yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletesecondhead",yearSession);;
	    }else{
	    }
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/DeletethirdHead" , method = RequestMethod.POST )
public @ResponseBody String deletethirdHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		if(dashboardConfig.equalsIgnoreCase("+2")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("head3");
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG3");
	    }else{
	    }
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	status=analyticSerivice.deleteHeadByCategory(headDetails, userName,"deletethirdhead",yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletethirdhead",yearSession);
	    }else{
	    }
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
		
		
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deleteFourthHead" , method = RequestMethod.POST )
public @ResponseBody String deleteFourthHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		if(dashboardConfig.equalsIgnoreCase("+2")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("head4");
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG4");
	    }else{
	    }
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	status=analyticSerivice.deleteHeadByCategory(headDetails, userName,"deletefourthhead",yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletefourthhead",yearSession);
	    }else{
	    }
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
		
		
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deleteFifthHead" , method = RequestMethod.POST )
public @ResponseBody String deleteFifthHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		if(dashboardConfig.equalsIgnoreCase("+2")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("head5");
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG5");
	    }else{
	    }
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	status=analyticSerivice.deleteHeadByCategory(headDetails, userName,"deletefifthhead",yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletefifthhead",yearSession);
	    }else{
	    }
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
		
		
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deleteSixthHead" , method = RequestMethod.POST )
public @ResponseBody String deleteSixthHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		if(dashboardConfig.equalsIgnoreCase("+2")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("head6");
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG6");
	    }else{
	    }
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	status=analyticSerivice.deleteHeadByCategory(headDetails, userName,"deletesixthhead",yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletesixthhead",yearSession);
	    }else{
	    }
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deleteSeventhHead" , method = RequestMethod.POST )
public @ResponseBody String deleteSeventhHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		if(dashboardConfig.equalsIgnoreCase("+2")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("head7");
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG7");
	    }else{
	    }
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	status=analyticSerivice.deleteHeadByCategory(headDetails, userName,"deleteseventhhead",yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deleteseventhhead",yearSession);
	    }else{
	    }
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deleteEighthHead" , method = RequestMethod.POST )
public @ResponseBody String deleteEighthHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		if(dashboardConfig.equalsIgnoreCase("+2")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("head8");
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG8");
	    }else{
	    }
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	status=analyticSerivice.deleteHeadByCategory(headDetails, userName,"deleteeighthhead",yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deleteeighthhead",yearSession);
	    }else{
	    }
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}

@RequestMapping(value ="/deleteNinthHead" , method = RequestMethod.POST )
public @ResponseBody String deleteNinthHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		if(dashboardConfig.equalsIgnoreCase("+2")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("head9");
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG9");
	    }else{
	    }
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	status=analyticSerivice.deleteHeadByCategory(headDetails, userName,"deleteninthhead",yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deleteninthhead",yearSession);
	    }else{
	    }
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
		
		
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deleteTenthHead" , method = RequestMethod.POST )
public @ResponseBody String deleteTenthHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		if(dashboardConfig.equalsIgnoreCase("+2")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("head10");
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG10");
	    }else{
	    }
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	status=analyticSerivice.deleteHeadByCategory(headDetails, userName,"deletetengthhead",yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletetengthhead",yearSession);
	    }else{
	    }
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deleteEleventhHead" , method = RequestMethod.POST )
public @ResponseBody String deleteEleventhHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		if(dashboardConfig.equalsIgnoreCase("+2")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("head11");
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG11");
	    }else{
	    }
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	status=analyticSerivice.deleteHeadByCategory(headDetails, userName,"deleteelevengthhead",yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deleteelevengthhead",yearSession);
	    }else{
	    }
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
		
		
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deleteTwelthHead" , method = RequestMethod.POST )
public @ResponseBody String deleteTwelthHead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		if(dashboardConfig.equalsIgnoreCase("+2")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("head12");
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG12");
	    }else{
	    }
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    if(dashboardConfig.equalsIgnoreCase("+2")){
	    	status=analyticSerivice.deleteHeadByCategory(headDetails, userName,"deletetwelthhead",yearSession);	
	    }else if(dashboardConfig.equalsIgnoreCase("UG")){
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletetwelthhead",yearSession);
	    }else{
	    }
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}

@RequestMapping(value ="/deletethirteenhead" , method = RequestMethod.POST )
public @ResponseBody String deletethirteenhead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG13");
	    HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletethirteenhead",yearSession);
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deletefourteenhead" , method = RequestMethod.POST )
public @ResponseBody String deletefourteenhead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG14");
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletefourteenhead",yearSession);
	    		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deletefifteenhead" , method = RequestMethod.POST )
public @ResponseBody String deletefifteenhead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG15");
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletefifteenhead",yearSession);
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deletesixteenhead" , method = RequestMethod.POST )
public @ResponseBody String deletesixteenhead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG16");
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletesixteenhead",yearSession);
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deleteseventeenhead" , method = RequestMethod.POST )
public @ResponseBody String deleteseventeenhead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
		headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG17");
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deletesevnteenhead",yearSession);
	   
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}


@RequestMapping(value ="/deleteeighteenhead" , method = RequestMethod.POST )
public @ResponseBody String deleteeighteenhead(@RequestBody String head,HttpServletRequest req){
	try{
		Gson gson = new Gson();
		String headId = gson.fromJson(head, String.class);
		String dashboardConfig=(String) req.getSession().getAttribute("config");
		List<HeadDeatils> headDetailsList=null;
			headDetailsList=(List<HeadDeatils>) req.getSession().getAttribute("headUG18");
		HeadDeatils headDetails=headDetailsList.get(Integer.parseInt(headId));
		String userName=(String) req.getSession().getAttribute("userName");
		String yearSession=(String) req.getSession().getAttribute("searchSession");
	    String status=null;
	    	status=analyticSerivice.deleteHeadByCategoryUG(headDetails, userName,"deleteeighteenhead",yearSession);
		if("SUCCESS".equalsIgnoreCase(status)){
			//analyticSerivice.getHeadAmount(req,headDetails.getSession());
			return "SUCCESS";
		}else{
			return "ERROR";
		}
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
}
















}
