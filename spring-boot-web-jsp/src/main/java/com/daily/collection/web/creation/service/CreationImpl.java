package com.daily.collection.web.creation.service;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daily.collection.web.creation.bean.Student;
import com.daily.collection.web.transaction.repository.ITransactionDao;

@Service
public class CreationImpl implements ICreationService {

	@Autowired
	ITransactionDao transactionDao;
	
	private final static Logger logger = Logger.getLogger(CreationImpl.class);
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public String uploadStudent(List<Student> listStudent,String userName) throws Exception {
		logger.info("Inside uploadStudent Method In Service Layer");
		String status="";
		if(listStudent!=null && !listStudent.isEmpty()){
			logger.info("Student Lis Size ::"+listStudent.size());
		for (Student student : listStudent) {
			logger.info("Student "+student.toString());
			if(student.getCategory()!=null && (student.getCategory().equalsIgnoreCase("General"))){
				student.setCategory("General");
			}else if(student.getCategory()!=null && (student.getCategory().equalsIgnoreCase("OBC"))){
				student.setCategory("OBC");
			}else if(student.getCategory()!=null && (student.getCategory().equalsIgnoreCase("SC"))){
				student.setCategory("SC");
			}else if(student.getCategory()!=null && (student.getCategory().equalsIgnoreCase("ST"))){
				student.setCategory("ST");
			}else if(student.getCategory()!=null && (student.getCategory().equalsIgnoreCase("Girls"))){
				student.setCategory("Girls");
			}
			SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
		    SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yy");
		    Date dateFrom = format1.parse(student.getCreated_Date());
		   // student.setCreated_Date(format2.format(dateFrom));
			Object[] studentObjectParam={student.getName(),student.getRollNo(),student.getDob(),student.getFundType(),student.getSession(),student.getCourse(),student.getStream(),
					student.getCategory(),student.getYearBatch(),"10","10",format2.format(dateFrom),userName,"10",userName,student.getCollegeCode(),student.getMrNo(),student.getScienceSelect()};
			
			logger.info("Stating Saving Student Details");
			logger.info("Inputs ::"+Arrays.toString(studentObjectParam));
			status=transactionDao.saveStudent(studentObjectParam);
			logger.info("Status ::"+status);
			if(status.equalsIgnoreCase("1") ){
				logger.info("Continue");
				continue;
			}else{
				logger.error("Error While Uploading The Student");
				throw new Exception("Error While Uploading Fle");
			}
		  }
		}else{
			logger.error("Student Lis IS Empty");
			return "ERROR";
		}
		if(!status.equalsIgnoreCase("")){
			logger.error("Sending Response As SUCCESS");
			return "SUCCESS";
		}else{
			logger.error("Sending Response As ERROR");
			return "ERROR";
		}
	}

}
