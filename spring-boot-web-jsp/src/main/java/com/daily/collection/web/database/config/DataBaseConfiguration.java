package com.daily.collection.web.database.config;


import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

@Configuration
@ComponentScan(basePackages = "com.*")
@EnableTransactionManagement
public class DataBaseConfiguration implements TransactionManagementConfigurer {
	
	//private static final String PROPERTY_NAME_DATABASE_DRIVER = "oracle.jdbc.driver.OracleDriver";
   // private static final String PROPERTY_NAME_DATABASE_PASSWORD = "DCR2017";
    //private static final String PROPERTY_NAME_DATABASE_URL = "jdbc:oracle:thin:@localhost:1521:orcl";
  //  private static final String PROPERTY_NAME_DATABASE_USERNAME = "DCR";
	
	private static final String PROPERTY_NAME_DATABASE_DRIVER ="oracle.jdbc.OracleDriver";
	
	private static final String PROPERTY_NAME_DATABASE_PASSWORD = "dcr20171";
    //private static final String PROPERTY_NAME_DATABASE_URL = "testaws.cwwjrux0bsxs.ap-south-1.rds.amazonaws.com:1521/TESTAWS";
	private static final String PROPERTY_NAME_DATABASE_URL = "jdbc:oracle:thin:@//testaws.cwwjrux0bsxs.ap-south-1.rds.amazonaws.com:1521/TESTAWS";
    private static final String PROPERTY_NAME_DATABASE_USERNAME = "dcrtest";
	
	
	 @Bean
     public DataSource dataSource() {
             DriverManagerDataSource dataSource = new DriverManagerDataSource();
              
             dataSource.setDriverClassName(PROPERTY_NAME_DATABASE_DRIVER);
             dataSource.setUrl(PROPERTY_NAME_DATABASE_URL);
             dataSource.setUsername(PROPERTY_NAME_DATABASE_USERNAME);
             dataSource.setPassword(PROPERTY_NAME_DATABASE_PASSWORD);
             return dataSource;
     }
	 
	 @Bean
		public JdbcTemplate getJdbcTemplate() {
			return new JdbcTemplate(dataSource());
		}
	 
	 
	 @Bean
	 public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate(){
		 return new NamedParameterJdbcTemplate(dataSource());
	 }

	 @Bean
     public PlatformTransactionManager txManager() {
         return new DataSourceTransactionManager(dataSource());
     }
	 
	@Override
	public PlatformTransactionManager annotationDrivenTransactionManager() {
		return txManager();
	}

}
