package com.daily.collection.web.login.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daily.collection.web.common.DataUtil;
import com.daily.collection.web.login.bean.LoginBean;
import com.daily.collection.web.login.service.ILoginService;
import com.daily.collection.web.main.SpringBootWebApplication;
import com.google.gson.Gson;

import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;

@Controller
//@Log4j
public class LoginController implements Serializable {
	
	private static final long serialVersionUID = 7585967570897248449L;
	
	
	private final static Logger log = Logger.getLogger(LoginController.class);
	
	@Autowired
	LoginBean loginBean;
	
	
	@Autowired
	ILoginService loginService;
	
	@Autowired
	DataUtil dataUtil;
	
	
	@RequestMapping("/login")
	public String login(@ModelAttribute("admin") LoginBean login,HttpServletRequest req,Map<String, Object> model) {
		log.info("Inside login Method");
		try{
			String currentSession=loginService.getCurrentSession("6");
			log.info("Current Session Is :"+currentSession);
			req.getSession(true).setAttribute("currentSession", currentSession);
			req.getSession().setAttribute("print_url", loginService.getCurrentSession("5"));
			if(login.getUserName().equalsIgnoreCase("dcr2017admin@gmail.com") && login.getPassword().equalsIgnoreCase("dcr@2017")){
				req.getSession().setAttribute("userName", "dcr2017admin@gmail.com");
				dataUtil.createDropDownMap(req);
				return "views/login/home";	
			}else if(login.getUserName().equalsIgnoreCase("dcr2017user@gmail.com") && login.getPassword().equalsIgnoreCase("dcr@2017")){
				req.getSession().setAttribute("userName", "dcr2017user@gmail.com");
				dataUtil.createDropDownMap(req);
				return "views/login/home";	
			}else if(login.getUserName().equalsIgnoreCase("admin1") && login.getPassword().equalsIgnoreCase("admin1_kms")){
				req.getSession().setAttribute("userName", "admin1");
				log.info("UserName Logged In Is "+login.getUserName());
				dataUtil.createDropDownMap(req);
				return "views/login/home";
			}else if(login.getUserName().equalsIgnoreCase("user") && login.getPassword().equalsIgnoreCase("user@kms")){
				req.getSession().setAttribute("userName", "user");
				log.info("UserName Logged In Is "+login.getUserName());
				dataUtil.createDropDownMap(req);
				return "views/login/home";
			}else if(login.getUserName().equalsIgnoreCase("admin2") && login.getPassword().equalsIgnoreCase("123")){
				req.getSession(true).setAttribute("userName", "admin2");
				log.info("UserName Logged In Is "+login.getUserName());
				dataUtil.createDropDownMap(req);
				return "views/login/home";
			}
			else{
				req.setAttribute("invalid","please enter a valid user name and password");
				model.put("loginBean", loginBean);
				return "views/login/login";
			}	
		}catch(Exception e){
			log.error("Exception While Login...Inside Login Method");
			e.printStackTrace();
			req.setAttribute("invalid","Error While Login...");
			model.put("loginBean", loginBean);
			log.error("Forward To Login Page");
			return "views/login/login";
		}
		
	}
	
	
	
	
	@RequestMapping("/registration")
	public String registration(HttpServletRequest req,Map<String, Object> model) {
		return "views/login/registeration";			
	}
	
	@RequestMapping("/showHome")
	public String showHome(HttpServletRequest req,Map<String, Object> model) {
		log.info("Inside ShowHome Method..");
		log.info("Forwarded TO Home Controller");
		return "views/login/home";			
	}
	
	
	@RequestMapping("/logout")
	public String logout(HttpServletRequest req,Map<String, Object> model) {
		log.info("Inside logout Method..");
		HttpSession session = req.getSession(false);
		if(session!=null)
		session.invalidate();
		model.put("loginBean", loginBean);
		log.info("Forwarded TO login page");
		log.info("...............End Using Application............");
		return "views/login/login";		
	}
	
	@RequestMapping("/showLogin")
	public String showLogin(HttpServletRequest req,Map<String, Object> model) {
		HttpSession session = req.getSession(false);
		/*if(session!=null){
			model.put("sessionTimeout", session.getAttribute("sessiontimeout"));	
			session.invalidate();
		}*/
		model.put("loginBean", loginBean);
		return "views/login/login";		
	}
	
	@RequestMapping("/welcome")
	public String welcome(Map<String, Object> model,HttpServletRequest req) {
		/*HttpSession session = req.getSession(false);*/
		/*if(session!=null){
			session.invalidate();	
		}*/
		log.info("...............Start Using Application............");
		model.put("loginBean", loginBean);
		log.info("Redirecting to Login.jsp Page");
		return "views/login/login";
		
	}
	@RequestMapping(value="/dashBoardConfig", method=RequestMethod.POST)
	public @ResponseBody String dashBoardConfig(@RequestBody String configJson,HttpServletRequest req,Map<String, Object> model) throws ParseException {
		log.info("Inside DashBoard Config method");
		try{
			
			Gson gson = new Gson();
		    String config = gson.fromJson(configJson, String.class);
		    log.info("Config Value ::"+config);
		    req.getSession().setAttribute("config", config);
		    dataUtil.createDropDownMap(req);
			return "SUCCESS";		
		}catch(Exception e){
			log.error("Exception WHile getting DashBoard Config",e);
			e.printStackTrace();
			return "ERROR";
		}
			
	}
	
	
	
	

	
	
}
