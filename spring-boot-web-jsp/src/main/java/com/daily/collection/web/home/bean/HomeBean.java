package com.daily.collection.web.home.bean;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class HomeBean implements Serializable {

	
	private static final long serialVersionUID = 1L;

	private String admissionCount;
	private String reAdmissionCount;
	private String cancelAdmCount;
	private List<CollectionSummary> collectionSummary;
	private String collectionList;
	
}
