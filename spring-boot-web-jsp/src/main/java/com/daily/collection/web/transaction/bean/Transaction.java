package com.daily.collection.web.transaction.bean;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class Transaction {
	
	private String savedId;
	
	private String inputamount;
	
	private String paymentModeDrop;
	
	private String chequeddnotext;
	
	private String additionalCheckBox;
	
	private String addReason;
	
	private String addAmount;
	
	private String totalAmount;
	
	private String transactiontype;
	
	private String rollNo;
	
	private String session;
	
	private String transactionDate;
	
	private String rounupAmount;
	
	private String actualAmount;
	
	private String chequeNumber;
	
	private String bankName;
	
	private String headName="";
	
	private String receiptNumber;

}
