package com.daily.collection.web.transaction.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daily.collection.web.analytic.bean.HeadDeatils;
import com.daily.collection.web.creation.bean.Student;
import com.daily.collection.web.transaction.bean.AdditionalTransaction;
import com.daily.collection.web.transaction.bean.Transaction;
import com.daily.collection.web.transaction.repository.ITransactionDao;

@Service
@Transactional
public class TransactionServiceImpl implements ITransactionService
{
    private static final Logger log;
    @Autowired
    ITransactionDao transactionDao;
    
    @Override
    public String saveStudent(final Student student, final String userName, final String from) throws Exception {
        TransactionServiceImpl.log.info((Object)"Inside saveStudent Method In Service Layer");
        final StringBuffer sb = new StringBuffer();
        try {
            String addStatus = "";
            if (from.equalsIgnoreCase("A")) {
                TransactionServiceImpl.log.info((Object)"Start getting count of student");
                TransactionServiceImpl.log.info((Object)("Inputs:: " + student.getCourse() + "," + student.getStream() + "," + student.getSession()));
                final String count = this.transactionDao.countById(student.getCourse(), student.getStream(), student.getSession());
                TransactionServiceImpl.log.info((Object)"End Getting Student Count");
                TransactionServiceImpl.log.info((Object)("Count::" + count));
                final int cnt = Integer.parseInt(count);
                if (student.getCourse().equalsIgnoreCase("+2")) {
                    sb.append("I");
                }
                else if (student.getCourse().equalsIgnoreCase("UG")) {
                    sb.append("B");
                }
                else if (student.getCourse().equalsIgnoreCase("PG")) {
                    sb.append("P");
                }
                if (student.getStream().equalsIgnoreCase("Science")) {
                    sb.append("S");
                }
                else if (student.getStream().equalsIgnoreCase("Arts")) {
                    sb.append("A");
                }
                else if (student.getStream().equalsIgnoreCase("Commerce")) {
                    sb.append("C");
                }
                sb.append(student.getSession().substring(2, 4));
                sb.append("-");
                if (cnt == 0) {
                    sb.append("001");
                }
                else {
                    final String tempstudentId = String.format("%03d", cnt + 1);
                    sb.append(tempstudentId);
                }
                TransactionServiceImpl.log.info((Object)("Roll No Created::" + sb.toString()));
                addStatus = "0";
            }
            else {
                TransactionServiceImpl.log.info((Object)("Roll No Already Created::" + student.getRollNo()));
                addStatus = "30";
                sb.append(student.getRollNo());
            }
            final Calendar cal = Calendar.getInstance();
            final Date today = cal.getTime();
            final Object[] studentObjectParam = { student.getName(), sb.toString(), student.getDob(), student.getFundType(), student.getSession(), student.getCourse(), student.getStream(), student.getCategory(), student.getYearBatch(), student.getAdmStatus(), student.getRefStatus(), today, userName, addStatus, userName, student.getCollegeCode(), student.getMrNo(), student.getScienceSelect() };
            TransactionServiceImpl.log.info((Object)"Start Saving Student Information");
            TransactionServiceImpl.log.info((Object)("Inputs ::" + Arrays.toString(studentObjectParam)));
            final String status = this.transactionDao.saveStudent(studentObjectParam);
            TransactionServiceImpl.log.info((Object)"End Saving Student Information");
            TransactionServiceImpl.log.info((Object)("Count ::" + status));
            if (status.equalsIgnoreCase("1")) {
                return sb.toString();
            }
            return null;
        }
        catch (Exception e) {
            TransactionServiceImpl.log.error((Object)"Exception While Saving Student", (Throwable)e);
            throw new Exception();
        }
    }
    
    @SuppressWarnings("null")
	@Override
    public List<Student> getStudentDetails(final String courseName, final String type) throws ParseException {
        TransactionServiceImpl.log.info((Object)"Inside getStudentDetails Method In Service Layer");
        TransactionServiceImpl.log.info((Object)"Start Getting Student Details");
        TransactionServiceImpl.log.info((Object)("Input ::" + courseName));
        List<Map<String, Object>> studentMapList = null;
        final Set<String> tranType = new HashSet<String>();
        final Set<String> yearBatch = new HashSet<String>();
        final Set<String> course = new HashSet<String>();
        final MapSqlParameterSource parameters = new MapSqlParameterSource();
        if (type.equalsIgnoreCase("A")) {
            TransactionServiceImpl.log.info((Object)"Get Student Details Type Admission");
            tranType.add("Admission");
            tranType.add("Re-Admission");
            course.add(courseName);
            parameters.addValue("course", (Object)course);
            parameters.addValue("fundType", (Object)tranType);
            TransactionServiceImpl.log.info((Object)"Start Getting Student Detils For Admission");
            studentMapList = (List<Map<String, Object>>)this.transactionDao.getStudent(parameters);
            TransactionServiceImpl.log.info((Object)"End Getting Student Detils For Admission ");
        }
        else if (type.equalsIgnoreCase("B")) {
            TransactionServiceImpl.log.info((Object)"Get Student Details Type Additional Fee");
            tranType.add("Additional Fee");
            tranType.add("Refund");
            tranType.add("Admission");
            tranType.add("Re-Admission");
            yearBatch.add("1st");
            yearBatch.add("2nd");
            yearBatch.add("3rd");
            course.add(courseName);
            parameters.addValue("course", (Object)course);
            parameters.addValue("fundType", (Object)tranType);
            parameters.addValue("yearBatch", (Object)yearBatch);
            TransactionServiceImpl.log.info((Object)"Start Getting Student Detils For Addditional Fee");
            studentMapList = (List<Map<String, Object>>)this.transactionDao.getStudentAdditional(parameters);
            TransactionServiceImpl.log.info((Object)"End Getting Student Detils For Addditional Fee");
        }
        else {
            TransactionServiceImpl.log.info((Object)"Get Student Details Type Refund");
            course.add(courseName);
            yearBatch.add("1st");
            parameters.addValue("course", (Object)course);
            parameters.addValue("yearBatch", (Object)yearBatch);
            TransactionServiceImpl.log.info((Object)"Start Getting Student Detils For Refund");
            studentMapList = (List<Map<String, Object>>)this.transactionDao.getStudentAdditionalRefund(parameters);
            TransactionServiceImpl.log.info((Object)"End Getting Student Detils For Refund");
        }
        if (studentMapList != null || !studentMapList.isEmpty()) {
            TransactionServiceImpl.log.info((Object)"Student Map Is Not Empty/Null");
            TransactionServiceImpl.log.info((Object)("Size is ::" + studentMapList.size()));
            Student student = null;
            final List<Student> listStudent = new ArrayList<Student>();
            for (final Map<String, Object> map : studentMapList) {
                student = new Student();
                BigDecimal bd = (BigDecimal) map.get("ID");
                student.setId(bd.intValue());
                student.setName((String)map.get("NAME"));
                student.setRollNo((String)map.get("ROLLNO"));
                student.setDob((String)map.get("DOB"));
                student.setFundType((String)map.get("FUND_TYPE"));
                student.setSession((String)map.get("AADEMIC_SESSION"));
                student.setCourse((String)map.get("COURSE"));
                student.setStream((String)map.get("STREAM"));
                student.setCategory((String)map.get("CATEGORY"));
                student.setYearBatch((String)map.get("YEAR_BATCH"));
                student.setAdmStatus((String)map.get("ADM_STATUS"));
                student.setRefStatus((String)map.get("REFUND_STATUS"));
                student.setAdfStatus((String)map.get("ADF_STATUS"));
                final Date date = (Date) map.get("CREATED_DATE");
                final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                student.setCreated_Date(format.format(date));
                student.setScienceSelect((String)map.get("SCIENCE_OPTION"));
                student.setIsAddFeeGiven((String)map.get("ISFeeGiven"));
                listStudent.add(student);
            }
            TransactionServiceImpl.log.info((Object)("Student Information Set into List :: Size Is ::" + listStudent.size()));
            return listStudent;
        }
        TransactionServiceImpl.log.info((Object)"Student Map Is Empty/Null");
        return null;
    }
    
    @Override
    public Long getFinalAmount(final String courseName, final String streamName, final String category, final String yearbatch) {
        TransactionServiceImpl.log.info((Object)"Inside getFinalAmount Method In Service Layer");
        final String currsentSession = this.transactionDao.getAvailbleAmount("6");
        TransactionServiceImpl.log.info((Object)("Current Session ::" + currsentSession));
        Long finalAmount = null;
        if (category != null && (category.equalsIgnoreCase("General") || category.equalsIgnoreCase("OBC"))) {
            TransactionServiceImpl.log.info((Object)"Start Getting Final Amount For General/OBC");
            finalAmount = this.transactionDao.getFinalAmount(currsentSession, courseName, streamName, "Gen/OBC", yearbatch);
            TransactionServiceImpl.log.info((Object)"End Getting Final Amount For General/OBC");
        }
        else {
            TransactionServiceImpl.log.info((Object)"Start Getting Final Amount For SC/ST/Girls");
            finalAmount = this.transactionDao.getFinalAmount(currsentSession, courseName, streamName, "SC/ST/Girls", yearbatch);
            TransactionServiceImpl.log.info((Object)"End Getting Final Amount For SC/ST/Girls");
        }
        TransactionServiceImpl.log.info((Object)("Final AMount::" + finalAmount));
        return finalAmount;
    }
    
    @Override
    public Long getFinalAmountUG(final String courseName, final String streamName, final String category, final String yearbatch) {
        TransactionServiceImpl.log.info((Object)"Inside getFinalAmountUG Method In Service Layer");
        final String currsentSession = this.transactionDao.getAvailbleAmount("6");
        TransactionServiceImpl.log.info((Object)("Current Session ::" + currsentSession));
        Long finalAmount = null;
        if (category != null && (category.equalsIgnoreCase("General") || category.equalsIgnoreCase("OBC"))) {
            TransactionServiceImpl.log.info((Object)"Start Getting Final Amount For General/OBC");
            finalAmount = this.transactionDao.getFinalAmountUG(currsentSession, courseName, streamName, "Gen/OBC", yearbatch);
            TransactionServiceImpl.log.info((Object)"End Getting Final Amount For General/OBC");
        }
        else {
            TransactionServiceImpl.log.info((Object)"Start Getting Final Amount For SC/ST/Girls");
            finalAmount = this.transactionDao.getFinalAmountUG(currsentSession, courseName, streamName, "SC/ST/Girls", yearbatch);
            TransactionServiceImpl.log.info((Object)"End Getting Final Amount For General/OBC");
        }
        TransactionServiceImpl.log.info((Object)("Final AMount::" + finalAmount));
        return finalAmount;
    }
    
    @Transactional(rollbackFor = { Exception.class })
    @Override
    public String savetransaction(final Transaction transaction, final String userName, final Student student, final String currsentSession, final String dashBoardConfig) throws Exception {
        try {
            TransactionServiceImpl.log.info((Object)"Inside getFinalAmountUG Method In Service Layer");
            TransactionServiceImpl.log.info((Object)"Start getting Transaction Count");
            Integer trasactionCount = this.transactionDao.gettransactionCount(currsentSession);
            TransactionServiceImpl.log.info((Object)"End getting Transaction Count");
            TransactionServiceImpl.log.info((Object)("Transaction Count ::" + trasactionCount));
            if (trasactionCount == null) {
                trasactionCount = 0;
            }
            ++trasactionCount;
            final String id = "T" + String.valueOf(trasactionCount);
            TransactionServiceImpl.log.info((Object)("Transaction ID ::" + id));
            final StringBuffer sb = new StringBuffer();
            String transactionType = "";
            Integer count = 0;
            final DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
            final SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
            final Date FormatDate = inputFormat.parse(transaction.getTransactionDate());
            final String outputText = outputFormat.format(FormatDate);
            final Date today = outputFormat.parse(outputText);
            today.getTime();
            String addtionalCheckBox = "";
            TransactionServiceImpl.log.info((Object)"Start getting College Name");
            final String collegename = this.transactionDao.getCollegeName("1");
            TransactionServiceImpl.log.info((Object)"End getting College Name");
            TransactionServiceImpl.log.info((Object)("CollegeName ::" + collegename));
            if (collegename == null || collegename.equalsIgnoreCase("")) {
                TransactionServiceImpl.log.error((Object)"College name empty/null");
                throw new Exception("College name empty/null");
            }
            if (transaction.getTransactiontype() == null || transaction.getTransactiontype().equalsIgnoreCase("")) {
                TransactionServiceImpl.log.error((Object)"Transaction type empty/null");
                throw new Exception("Transaction type empty/null");
            }
            if (transaction.getTransactiontype().equalsIgnoreCase("Admission")) {
                TransactionServiceImpl.log.info((Object)"Transaction Type Is Admission");
                transactionType = "A";
            }
            else if (transaction.getTransactiontype().equalsIgnoreCase("Re-Admission")) {
                TransactionServiceImpl.log.info((Object)"Transaction Type Is Re-Admission");
                transactionType = "R";
            }
            else if (transaction.getTransactiontype().equalsIgnoreCase("Additional Fee")) {
                TransactionServiceImpl.log.info((Object)"Transaction Type Is Additional Fee");
                transactionType = "AD";
            }
            else if (transaction.getTransactiontype().equalsIgnoreCase("Fine")) {
                TransactionServiceImpl.log.info((Object)"Transaction Type Is Fine");
                transactionType = "F";
            }
            String receiptNo = "";
            String bookid = "01";
            String tempId = "001";
            Integer receiptStarting = 0;
            String receiptNBook = "";
            if (dashBoardConfig.equalsIgnoreCase("+2")) {
                TransactionServiceImpl.log.info((Object)"Start Getting Receipt Starting Count For +2");
                receiptStarting = this.transactionDao.getReceiptStarting("8");
                TransactionServiceImpl.log.info((Object)"Start Getting Receipt And Book From Configuration For +2");
                receiptNBook = this.transactionDao.getReceiptNBook("13");
                TransactionServiceImpl.log.info((Object)("Receipt Starting ::" + receiptStarting));
                TransactionServiceImpl.log.info((Object)("Receipt N Book ::" + receiptNBook));
            }
            else if (dashBoardConfig.equalsIgnoreCase("UG")) {
                TransactionServiceImpl.log.info((Object)"Start Getting Receipt Starting Count For UG");
                receiptStarting = this.transactionDao.getReceiptStarting("9");
                TransactionServiceImpl.log.info((Object)"Start Getting Receip And Bpok From Configuration For UG");
                receiptNBook = this.transactionDao.getReceiptNBook("12");
                TransactionServiceImpl.log.info((Object)("Receipt Starting ::" + receiptStarting));
                TransactionServiceImpl.log.info((Object)("Receipt N Book ::" + receiptNBook));
            }
            if (("1st".equalsIgnoreCase(student.getYearBatch()) && !transactionType.equalsIgnoreCase("A")) || "2nd".equalsIgnoreCase(student.getYearBatch()) || "3rd".equalsIgnoreCase(student.getYearBatch())) {
                if (transaction.getReceiptNumber() == null || transaction.getReceiptNumber().equalsIgnoreCase("")) {
                    TransactionServiceImpl.log.info((Object)"Receipt Number Is Not Avaialable In Transaction Object");
                    final String tranCout = this.transactionDao.getTransactionByCourse(dashBoardConfig);
                    TransactionServiceImpl.log.info((Object)("transaction Count ::" + tranCout));
                    if (Integer.parseInt(tranCout) != 0) {
                        TransactionServiceImpl.log.info((Object)"Transaction Count Not Equal To 0");
                        if (receiptNBook == null || receiptNBook.isEmpty()) {
                            TransactionServiceImpl.log.error((Object)"Exception WHile Getting Receipt N Book");
                            throw new Exception("Exception While getting receipt n book id");
                        }
                        TransactionServiceImpl.log.info((Object)"ReceiptNBook IS NOt Empty/NULL");
                        final int index = receiptNBook.indexOf("/");
                        receiptNo = receiptNBook.substring(0, index);
                        bookid = receiptNBook.substring(index + 1, receiptNBook.length());
                        count = Integer.parseInt(receiptNo);
                        if (count == 0 && receiptStarting > count) {
                            TransactionServiceImpl.log.info((Object)"Receipt Starting Greater Than Receipt Number Exist In DB");
                            tempId = String.format("%03d", receiptStarting + 1);
                            TransactionServiceImpl.log.info((Object)("Temporray Id Has Been Created With ::" + tempId));
                        }
                        else {
                            TransactionServiceImpl.log.info((Object)"Receipt Starting Less Than Receipt Number Exist In DB");
                            if (count % 500 == 0) {
                                TransactionServiceImpl.log.info((Object)"Now Count Is Greater Than 500");
                                bookid = String.format("%02d", Integer.parseInt(bookid) + 1);
                                TransactionServiceImpl.log.info((Object)("Book Number Has Been Created With ::" + bookid));
                            }
                            TransactionServiceImpl.log.info((Object)("Current Recipt Number Is ::" + count));
                            tempId = String.format("%03d", count + 1);
                            TransactionServiceImpl.log.info((Object)("Temporray Id Has Been Created With ::" + tempId));
                        }
                    }
                    else {
                        TransactionServiceImpl.log.info((Object)"Transaction Count Equal To 0");
                        TransactionServiceImpl.log.info((Object)"Receipt Starting Is 500");
                        tempId = String.format("%03d", receiptStarting + 1);
                        TransactionServiceImpl.log.info((Object)("Temporray Id Has Been Created With ::" + tempId));
                    }
                }
                else {
                    TransactionServiceImpl.log.info((Object)"Receipt Number Is Avaialable On Transaction Object");
                    tempId = String.format("%03d", Integer.parseInt(transaction.getReceiptNumber()));
                    TransactionServiceImpl.log.info((Object)("Temporray Id Has Been Created With ::" + tempId));
                    bookid = "01";
                    TransactionServiceImpl.log.info((Object)("Book Number Has Been Created With ::" + bookid));
                    final Integer countOFReceipTNumber = this.transactionDao.isReceiptNoExist(tempId, bookid);
                    TransactionServiceImpl.log.info((Object)("Count Of Receipt Number ::" + countOFReceipTNumber));
                    if (countOFReceipTNumber >= 1) {
                        return "Receipt Number Already Exist.. Try Another";
                    }
                }
            }
            else {
                tempId = "";
                bookid = "";
                TransactionServiceImpl.log.info((Object)("TempId and BookId is Empty::"));
            }
            if (transaction.getAdditionalCheckBox() != null && !transaction.getAdditionalCheckBox().equalsIgnoreCase("")) {
                TransactionServiceImpl.log.info((Object)("Additional CheeckBox Is Selected ::" + transaction.getAdditionalCheckBox()));
                addtionalCheckBox = transaction.getAdditionalCheckBox();
                TransactionServiceImpl.log.info((Object)("TempId and BookId is Empty::"));

            }
            Object[] categoriesParam = null;
            if (student.getCategory() != null && (student.getCategory().equalsIgnoreCase("General") || student.getCategory().equalsIgnoreCase("OBC"))) {
                categoriesParam = new Object[] { student.getCourse(), student.getStream(), "Gen/OBC", student.getYearBatch() };
                TransactionServiceImpl.log.info((Object)("Categories Param For Gen/OBC::" + Arrays.toString(categoriesParam)));
            }
            else {
                categoriesParam = new Object[] { student.getCourse(), student.getStream(), "SC/ST/Girls", student.getYearBatch() };
                TransactionServiceImpl.log.info((Object)("Categories Param For SC/ST/Girls::" + Arrays.toString(categoriesParam)));
            }
            TransactionServiceImpl.log.info((Object)"Start getting Categories Id");
            final String categoriesId = this.transactionDao.getCategoriesId(categoriesParam);
            TransactionServiceImpl.log.info((Object)"End getting Categories Id");
            TransactionServiceImpl.log.info((Object)("Categories Id ::" + categoriesId));
            if (categoriesId == null || categoriesId.equalsIgnoreCase("") || categoriesId.equalsIgnoreCase("ERROR")) {
                TransactionServiceImpl.log.info((Object)"Categories Id Is Returning Null/Empty");
                throw new Exception("Exception While getting Categories Details");
            }
            String totalAmount = "";
            Object[] transactionParam = null;
            String transactionStatus = "";
            String tran_Id = "";
            if (transactionType.equalsIgnoreCase("A") || transactionType.equalsIgnoreCase("R")) {
                TransactionServiceImpl.log.info((Object)"Start Creting Object Param For Admission ANd Re-Admission");
                totalAmount = transaction.getTotalAmount();
                transactionParam = new Object[] { id, transactionType, tempId, transaction.getRollNo(), totalAmount, today, addtionalCheckBox, userName, transaction.getPaymentModeDrop(), transaction.getChequeddnotext(), transaction.getAddReason(), transaction.getAddAmount(), transaction.getTotalAmount(), currsentSession, categoriesId, bookid, student.getScienceSelect() };
                TransactionServiceImpl.log.info((Object)("Transaction Param ::" + Arrays.toString(transactionParam)));
                TransactionServiceImpl.log.info((Object)"Start Saving The Transaction");
                transactionStatus = this.transactionDao.saveTransaction(transactionParam);
                TransactionServiceImpl.log.info((Object)"End Saving The Transaction");
                TransactionServiceImpl.log.info((Object)("Transaction Save Status ::" + transactionStatus));
            }
            else {
                TransactionServiceImpl.log.info((Object)"Start Creting Object Param For Additional Fee/Fine");
                TransactionServiceImpl.log.info((Object)"Start getting Transaction Id");
                if (student.getFundType().equalsIgnoreCase("Additional Fee")) {
                    Integer countoldStudentId = this.transactionDao.getTransactionIdOldStudent();
                    ++countoldStudentId;
                    tran_Id = "O" + String.valueOf(countoldStudentId);
                    transactionType = "OAD";
                }
                else {
                    tran_Id = this.transactionDao.getTransactionId(student.getRollNo(), student.getScienceSelect());
                }
                TransactionServiceImpl.log.info((Object)"End getting Transaction Id");
                TransactionServiceImpl.log.info((Object)("Tran Id ::" + tran_Id));
                if (tran_Id == null || tran_Id.equalsIgnoreCase("")) {
                    TransactionServiceImpl.log.error((Object)"Transaction Id Is Returning Null/Empty From Database");
                    throw new Exception("Exception While getting Transaction Id From DB");
                }
                transactionParam = new Object[] { tran_Id, today, userName, tempId, bookid, "Cash", currsentSession, categoriesId, student.getScienceSelect(), transactionType, transaction.getHeadName(), transaction.getTotalAmount(), transaction.getRollNo() };
                TransactionServiceImpl.log.info((Object)("Transaction Param ::" + Arrays.toString(transactionParam)));
                transactionStatus = this.transactionDao.saveTransactionAd(transactionParam);
                TransactionServiceImpl.log.info((Object)("Transaction Save Status ::" + transactionStatus));
            }
            if (transactionStatus == null || transactionStatus.equalsIgnoreCase("")) {
                TransactionServiceImpl.log.error((Object)"Exception While Saving Transaction Details");
                throw new Exception("Exception While Saving Transaction Details");
            }
            if (!transactionStatus.equalsIgnoreCase("SUCCESS")) {
                TransactionServiceImpl.log.error((Object)"Exception While Inserting Transaction");
                throw new Exception("Exception While Inserting Transaction");
            }
            TransactionServiceImpl.log.info((Object)"Transaction Status IS SUCCESS");
            String updateStatus = null;
            if (transactionType.equalsIgnoreCase("A") || transactionType.equalsIgnoreCase("R")) {
                TransactionServiceImpl.log.info((Object)"Start Updating Admission StatusIn Student Table For Admission/Re-admission :: 30");
                updateStatus = this.transactionDao.updateTransaction("30", transaction.getRollNo(), student.getScienceSelect());
                TransactionServiceImpl.log.info((Object)"Start Updating Admission StatusIn Student Table");
                TransactionServiceImpl.log.info((Object)("Update Admission Status ::" + updateStatus));
                if (updateStatus == null || updateStatus.equalsIgnoreCase("")) {
                    TransactionServiceImpl.log.error((Object)"Exception while updating admission status For Admission/Re-admission");
                    throw new Exception("Exception WHile Updating Admission Status For Admission/Re-admission");
                }
                TransactionServiceImpl.log.info((Object)"Updating Transaction For For Additional Fee For Admission/Re-admission:: 30");
                TransactionServiceImpl.log.info((Object)"Start Updating Additional Fee Status In Student Table");
                updateStatus = this.transactionDao.updateTransactionAD("30", transaction.getRollNo(), student.getScienceSelect());
                TransactionServiceImpl.log.info((Object)"Start Updating Admission StatusIn Student Table");
                TransactionServiceImpl.log.info((Object)("Update Additional Fee/Fine Status ::" + updateStatus));
            }
            else if (transaction.getHeadName().equalsIgnoreCase("CLC")) {
                TransactionServiceImpl.log.info((Object)"Start Updating Transaction Status After CLC Taken For Additiona Fee-CLC::40");
                updateStatus = this.transactionDao.updateTransaction("40", student.getRollNo(), student.getScienceSelect());
                TransactionServiceImpl.log.info((Object)("End Updating Transaction Status After CLC Taken :: Status ::" + updateStatus));
                if (updateStatus == null || updateStatus.equalsIgnoreCase("")) {
                    TransactionServiceImpl.log.error((Object)"Exception while updating admission status For Additiona Fee CLC");
                    throw new Exception("Exception WHile Updating Admission Status For Additiona Fee -CLC");
                }
                TransactionServiceImpl.log.info((Object)"Updating Transaction For For Additional Fee For Additiona Fee-CLC:: 40");
                TransactionServiceImpl.log.info((Object)"Start Updating Additional Fee Status In Student Table");
                updateStatus = this.transactionDao.updateTransactionAD("40", student.getRollNo(), student.getScienceSelect());
                TransactionServiceImpl.log.info((Object)"End Updating Admission StatusIn Student Table");
                TransactionServiceImpl.log.info((Object)("Update Additional Fee/Fine Status ::" + updateStatus));
            }
            else {
                TransactionServiceImpl.log.info((Object)"Update Status Set As SUCCESS For Other Additional Fee ");
                updateStatus = "SUCCESS";
            }
            if (updateStatus == null || updateStatus.equalsIgnoreCase("")) {
                TransactionServiceImpl.log.error((Object)"Error In Update Transaction Status");
                throw new Exception("Error In Update Transaction Status");
            }
            if (!updateStatus.equalsIgnoreCase("SUCCESS")) {
                TransactionServiceImpl.log.error((Object)"Exception While Updating Transaction Status");
                throw new Exception("Exception While Updating Transaction Status");
            }
            TransactionServiceImpl.log.info((Object)"Update Student Status IS SUCCESS");
            String caluclationStatus = null;
            if (transactionType.equalsIgnoreCase("A") || transactionType.equalsIgnoreCase("R")) {
                if (dashBoardConfig.equalsIgnoreCase("+2")) {
                    TransactionServiceImpl.log.info((Object)"Start Calculating For +2 Admission/Re-Admission");
                   // caluclationStatus = this.insertCalulationDetails(id, categoriesId, currsentSession, transaction.getRollNo(), transaction.getTotalAmount(), transaction.getAddAmount(), "0", transaction.getHeadName());
                    caluclationStatus="SUCCESS";
                    TransactionServiceImpl.log.info((Object)"End Calculating For +2 Admission/Re-Admission");
                    TransactionServiceImpl.log.info((Object)("Update Calculation Status ::" + caluclationStatus));
                }
                else if (dashBoardConfig.equalsIgnoreCase("UG")) {
                    TransactionServiceImpl.log.info((Object)"Start Calculating For +2 Admission/Re-Admission");
                    String roundup = "0";
                    if (student.getYearBatch().equalsIgnoreCase("1st") && transaction.getHeadName().equalsIgnoreCase("")) {
                        roundup = transaction.getRounupAmount();
                        TransactionServiceImpl.log.info((Object)("RoundUp Amount IS ::" + roundup));
                    }
                    TransactionServiceImpl.log.info((Object)"Start Calculating For UG Admission/Re-Admission");
                    //caluclationStatus = this.insertCalulationDetailsUG(id, categoriesId, currsentSession, transaction.getRollNo(), transaction.getTotalAmount(), transaction.getAddAmount(), roundup, transaction.getHeadName());
                    caluclationStatus="SUCCESS";
                    TransactionServiceImpl.log.info((Object)"End Calculating For UG Admission/Re-Admission");
                    TransactionServiceImpl.log.info((Object)("Update Calculation Status ::" + caluclationStatus));
                }
            }
            /*else if (transactionType.equalsIgnoreCase("AD") || transactionType.equalsIgnoreCase("F")) {
                if (dashBoardConfig.equalsIgnoreCase("+2")) {
                    if (transactionType.equalsIgnoreCase("F")) {
                       // TransactionServiceImpl.log.info((Object)"Start Getting Transaction Details For Fine For +2");
                      //  String tran_amt = this.transactionDao.getTransactionDetailsFine(tran_Id);
                       // TransactionServiceImpl.log.info((Object)("Start Getting Transaction Details For Fine For +2 ::" + tran_amt));
                       // tran_amt = String.valueOf(Integer.parseInt(tran_amt) + Integer.parseInt(transaction.getTotalAmount()));
                        //TransactionServiceImpl.log.info((Object)("Updated Transaction Amount ::" + tran_amt));
                        //TransactionServiceImpl.log.info((Object)"Start Updating Transaction Details For Fine For UG");
                       // caluclationStatus = this.transactionDao.updateTransactionDetailsFine(tran_amt, tran_Id);
                       // TransactionServiceImpl.log.info((Object)("Update Calculation Status Fine For UG::" + caluclationStatus));
                    }
                    else if (transactionType.equalsIgnoreCase("AD")) {
                      //  TransactionServiceImpl.log.info((Object)"Start Getting Transaction Details For Addtional Fee For +2");
                      //  String tran_amt = this.transactionDao.getTransactionDetailsAdd(tran_Id);
                      //  TransactionServiceImpl.log.info((Object)("Start Getting Transaction Details For Addtional Fee For +2 ::" + tran_amt));
                      //  tran_amt = String.valueOf(Integer.parseInt(tran_amt) + Integer.parseInt(transaction.getTotalAmount()));
                      //  TransactionServiceImpl.log.info((Object)("Updated Transaction Amount ::" + tran_amt));
                      //  TransactionServiceImpl.log.info((Object)"Start Updating Transaction Details For Additional Fee For +2");
                      //  caluclationStatus = this.transactionDao.updateTransactionDetailsAd(tran_amt, tran_Id);
                       // TransactionServiceImpl.log.info((Object)("Update Calculation Status Additional Fee For +2:" + caluclationStatus));
                    }
                }
                else if (dashBoardConfig.equalsIgnoreCase("UG")) {
                    if (transaction.getHeadName().equalsIgnoreCase("BU Examination")) {
                        //TransactionServiceImpl.log.info((Object)"Start Getting Transaction Details For BU Examination Fee For UG");
                       // String tran_amt = this.transactionDao.getTransactionDetailsUGBUExamFee(tran_Id);
                       // TransactionServiceImpl.log.info((Object)"End Getting Transaction Details For BU Examination Fee For UG");
                        //tran_amt = String.valueOf(Integer.parseInt(tran_amt) + Integer.parseInt(transaction.getTotalAmount()));
                        //TransactionServiceImpl.log.info((Object)("Updated Transaction Amount ::" + tran_amt));
                        //TransactionServiceImpl.log.info((Object)"Start Updating Transaction Details For BU Exam Fee For UG");
                        //caluclationStatus = this.transactionDao.updateTransactionDetailsCLCBUExamFee(tran_amt, tran_Id);
                        //TransactionServiceImpl.log.info((Object)("Update Calculation Status BU Exam Fee For UG::" + caluclationStatus));
                    }
                    else if (transactionType.equalsIgnoreCase("F")) {
                        //TransactionServiceImpl.log.info((Object)"Start Getting Transaction Details For Fine For UG");
                        //String tran_amt = this.transactionDao.getTransactionDetailsFineUG(tran_Id);
                        //TransactionServiceImpl.log.info((Object)("Start Getting Transaction Details For Fine For UG ::" + tran_amt));
                        //tran_amt = String.valueOf(Integer.parseInt(tran_amt) + Integer.parseInt(transaction.getTotalAmount()));
                        //TransactionServiceImpl.log.info((Object)("Updated Transaction Amount ::" + tran_amt));
                        //TransactionServiceImpl.log.info((Object)"Start Updating Transaction Details For Fine For UG");
                        //caluclationStatus = this.transactionDao.updateTransactionDetailsFineUG(tran_amt, tran_Id);
                        //TransactionServiceImpl.log.info((Object)("Update Calculation Status Fine For UG::" + caluclationStatus));
                    }
                    else if (transactionType.equalsIgnoreCase("AD")) {
                        //TransactionServiceImpl.log.info((Object)"Start Getting Transaction Details For Addtional Fee For UG");
                        //String tran_amt = this.transactionDao.getTransactionDetailsAddUG(tran_Id);
                        //TransactionServiceImpl.log.info((Object)("Start Getting Transaction Details For Addtional Fee For UG ::" + tran_amt));
                        //tran_amt = String.valueOf(Integer.parseInt(tran_amt) + Integer.parseInt(transaction.getTotalAmount()));
                        //TransactionServiceImpl.log.info((Object)("Updated Transaction Amount ::" + tran_amt));
                        //TransactionServiceImpl.log.info((Object)"Start Updating Transaction Details For Additional Fee For UG");
                        //caluclationStatus = this.transactionDao.updateTransactionDetailsAddUG(tran_amt, tran_Id);
                        //TransactionServiceImpl.log.info((Object)("Update Calculation Status Additional Fee For UG::" + caluclationStatus));
                    }
                }
            }*/
            else if (student.getFundType().equalsIgnoreCase("Additional Fee")) {
                caluclationStatus = "SUCCESS";
            }
            if (caluclationStatus == null || caluclationStatus.equalsIgnoreCase("")) {
                TransactionServiceImpl.log.error((Object)"Exception While Updating Calulation Details");
                throw new Exception("Exception While Updating Calulation Details");
            }
            if (!caluclationStatus.equalsIgnoreCase("SUCCESS")) {
                TransactionServiceImpl.log.error((Object)"Exception While Updating Calulation Details");
                throw new Exception("Exception While Updating Calulation Details");
            }
            TransactionServiceImpl.log.info((Object)"Update Calculation Status SUCCESS");
            String studentTrackStatus = null;
            if (transactionType.equalsIgnoreCase("AD") || transactionType.equalsIgnoreCase("OAD")) {
                if (transaction.getHeadName().equalsIgnoreCase("CLC")) {
                    TransactionServiceImpl.log.info((Object)"Start Updating Student Track :: TC Taken");
                    if (transactionType.equalsIgnoreCase("OAD")) {
                        studentTrackStatus = this.transactionDao.insertStudentTrack(student.getRollNo(), "TC Taken");
                    }
                    else {
                        studentTrackStatus = this.transactionDao.updateStudentTrack("TC Taken", student.getRollNo());
                    }
                    TransactionServiceImpl.log.info((Object)"End Updating Student Track");
                    TransactionServiceImpl.log.info((Object)"Start Updating College Code/MRIN In DB");
                    final String updateCollegeNMRINStatus = this.transactionDao.updateCollegeNMRIN("", "", student.getSession(), student.getRollNo(), student.getScienceSelect());
                    TransactionServiceImpl.log.info((Object)"End Updating College Code/MRIN In DB");
                    TransactionServiceImpl.log.info((Object)("Status ::" + updateCollegeNMRINStatus));
                    if (updateCollegeNMRINStatus == null || updateCollegeNMRINStatus.equalsIgnoreCase("")) {
                        throw new Exception("Update While CollegeCode/MRIN Number In Database");
                    }
                }
                else {
                    TransactionServiceImpl.log.info((Object)"Set StudentTrack Status As SUCCESS");
                    studentTrackStatus = "SUCCESS";
                }
            }
            else if (transactionType.equalsIgnoreCase("A") || transactionType.equalsIgnoreCase("R")) {
                TransactionServiceImpl.log.info((Object)("Start Inserting Status Into Student Track Table ::" + transaction.getRollNo()));
                studentTrackStatus = this.transactionDao.insertStudentTrack(transaction.getRollNo(), "Continue");
                TransactionServiceImpl.log.info((Object)("End Inserting Status Into Student Track Table  Status::" + studentTrackStatus));
            }
            else if (transactionType.equalsIgnoreCase("F")) {
                TransactionServiceImpl.log.info((Object)"Set StudentTrack Status As SUCCESS For Fine");
                studentTrackStatus = "SUCCESS";
            }
            if (studentTrackStatus == null || studentTrackStatus.equalsIgnoreCase("")) {
                TransactionServiceImpl.log.error((Object)"Exception While Inserting Student Track Details");
                throw new Exception("Exception While Inserting Student Track Details");
            }
            if (!studentTrackStatus.equalsIgnoreCase("SUCCESS")) {
                TransactionServiceImpl.log.error((Object)"Exception While Inserting Student Track Details");
                throw new Exception("Exception While Inserting Student Track Details");
            }
            TransactionServiceImpl.log.info((Object)"Student Track Status SUCCESS");
            String updateReciptNumberStatus = "";
            if (("1st".equalsIgnoreCase(student.getYearBatch()) && !transactionType.equalsIgnoreCase("A")) || "2nd".equalsIgnoreCase(student.getYearBatch()) || "3rd".equalsIgnoreCase(student.getYearBatch())) {
                final String receiptNBookNo = tempId + "/" + bookid;
                TransactionServiceImpl.log.info((Object)("Final ReceiptNBook No ::" + receiptNBookNo));
                if (dashBoardConfig.equalsIgnoreCase("+2")) {
                    TransactionServiceImpl.log.info((Object)("Start Updating Receipt Number Into Configuration Table For +2::" + receiptNBookNo));
                    updateReciptNumberStatus = this.transactionDao.updateReceiptNumberInConfiguration(receiptNBookNo, "13");
                    TransactionServiceImpl.log.info((Object)("Start Updating Receipt Number Into Configuration Table For +2 :: Status::" + updateReciptNumberStatus));
                }
                else if (dashBoardConfig.equalsIgnoreCase("UG")) {
                    TransactionServiceImpl.log.info((Object)("Start Updating Receipt Number Into Configuration Table For UG::" + receiptNBookNo));
                    if (transaction.getReceiptNumber() != null && !transaction.getReceiptNumber().equalsIgnoreCase("")) {
                        TransactionServiceImpl.log.info((Object)("Update Receipt Number For Manula Entry::" + receiptNBookNo));
                        updateReciptNumberStatus = this.transactionDao.updateReceiptNumberInConfiguration(receiptNBookNo, "14");
                    }
                    else {
                        TransactionServiceImpl.log.info((Object)("Update Receipt Number For Generated::" + receiptNBookNo));
                        updateReciptNumberStatus = this.transactionDao.updateReceiptNumberInConfiguration(receiptNBookNo, "12");
                    }
                    TransactionServiceImpl.log.info((Object)("Start Updating Receipt Number Into Configuration Table For UG :: Status::" + updateReciptNumberStatus));
                }
            }
            else {
                updateReciptNumberStatus = "SUCCESS";
            }
            if (updateReciptNumberStatus == null || updateReciptNumberStatus.equalsIgnoreCase("")) {
                TransactionServiceImpl.log.error((Object)"Error While Updating ReciptNumber In Configuration Table");
                throw new Exception("Error While Updating ReciptNumber In Configuration Table");
            }
            if (!updateReciptNumberStatus.equalsIgnoreCase("SUCCESS")) {
                TransactionServiceImpl.log.error((Object)"ERROR While Updating ReciptNumber In Configuration Table");
                throw new Exception("ERROR While Updating ReciptNumber In Configuration Table");
            }
            TransactionServiceImpl.log.info((Object)("Payment Has Been Done..Receipt Number::" + updateReciptNumberStatus));
            if (("1st".equalsIgnoreCase(student.getYearBatch()) && !transactionType.equalsIgnoreCase("A")) || "2nd".equalsIgnoreCase(student.getYearBatch()) || "3rd".equalsIgnoreCase(student.getYearBatch())) {
                return "Payment Has Been Done.. Receipt Number :" + tempId + "/" + bookid;
            }
            return "Payment Has Been Done.....";
        }
        catch (Exception e) {
            TransactionServiceImpl.log.error((Object)"Exception While Saving The Transaction ::", (Throwable)e);
            throw new Exception(e.getMessage());
        }
    }
    
/*    private String insertCalulationDetails(final String tranId, final String categoriesId, final String session, final String rollNo, final String Amount, final String additionalfee, final String roundUpAmt, final String headNm) {
        final List<String> defaultvalueToList = this.addDefaultvalueToList(tranId);
        final List<Map<String, Object>> headNameList = (List<Map<String, Object>>)this.transactionDao.getHeadNameById(categoriesId, session);
        if (headNameList == null) {
            return "ERROR";
        }
        Long tempAmount;
        final Long finalAmount = tempAmount = Long.parseLong(Amount);
    Label_4519:
        for (final Map<String, Object> map : headNameList) {
            final String s;
            final String headName = s = (String) map.get("HEADNAME");
            switch (s) {
                case "Tuition Fee": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(1);
                        defaultvalueToList.add(1, String.valueOf(amnt));
                    }
                    if (headNm != null && !headNm.equalsIgnoreCase("") && headNm.equalsIgnoreCase(headName)) {
                        break Label_4519;
                    }
                    continue;
                }
                case "Adm/Re-Adm": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(2);
                        defaultvalueToList.add(2, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Library": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(3);
                        defaultvalueToList.add(3, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Sports And Games": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(4);
                        defaultvalueToList.add(4, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Medical": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(5);
                        defaultvalueToList.add(5, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Meical Aid(Student and Fund)": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(6);
                        defaultvalueToList.add(6, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Labrotary": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(7);
                        defaultvalueToList.add(7, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Extra Caricular": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(8);
                        defaultvalueToList.add(8, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Poor Boys Fund": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(9);
                        defaultvalueToList.add(9, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Council": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(10);
                        defaultvalueToList.add(10, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Magazine": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(11);
                        defaultvalueToList.add(11, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "College Exam": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(12);
                        defaultvalueToList.add(12, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Puja": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(13);
                        defaultvalueToList.add(13, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "College Day": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(14);
                        defaultvalueToList.add(14, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Proctorial": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(15);
                        defaultvalueToList.add(15, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Development One": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(16);
                        defaultvalueToList.add(16, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Science/Arts Society": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(17);
                        defaultvalueToList.add(17, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Red Cross": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(18);
                        defaultvalueToList.add(18, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Calendar": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(19);
                        defaultvalueToList.add(19, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Syllabus": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(20);
                        defaultvalueToList.add(20, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Other Fees": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(21);
                        defaultvalueToList.add(21, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Building": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(22);
                        defaultvalueToList.add(22, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Academic": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(23);
                        defaultvalueToList.add(23, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Reading Room": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(24);
                        defaultvalueToList.add(24, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Registration": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(25);
                        defaultvalueToList.add(25, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Labrotary Caution Money": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(26);
                        defaultvalueToList.add(26, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Library Caution Money": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(27);
                        defaultvalueToList.add(27, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Identity Card": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(28);
                        defaultvalueToList.add(28, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Recognition": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(29);
                        defaultvalueToList.add(29, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Scout And Guide": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(30);
                        defaultvalueToList.add(30, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Tution Fees For 10 Month": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(31);
                        defaultvalueToList.add(31, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Development For 10 Month": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(32);
                        defaultvalueToList.add(32, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Social Service Guide": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(33);
                        defaultvalueToList.add(33, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Flag Day": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(34);
                        defaultvalueToList.add(34, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "EMS Fee": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(35);
                        defaultvalueToList.add(35, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "CAF": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(36);
                        defaultvalueToList.add(36, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Miscellanious": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(37);
                        defaultvalueToList.add(37, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Additional Fee": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(38);
                        defaultvalueToList.add(38, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Fine": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(39);
                        defaultvalueToList.add(39, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "CLC": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(40);
                        defaultvalueToList.add(40, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Migration/Original Certificate": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(41);
                        defaultvalueToList.add(41, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "CHSE Exam Fee": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(42);
                        defaultvalueToList.add(42, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
                case "Development Two": {
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    final long amnt = bd.longValue();
                    if (tempAmount != 0L) {
                        tempAmount -= amnt;
                        defaultvalueToList.remove(43);
                        defaultvalueToList.add(43, String.valueOf(amnt));
                        continue;
                    }
                    continue;
                }
            }
        }
        if (additionalfee != null && !additionalfee.equalsIgnoreCase("")) {
            final String defaultValue = defaultvalueToList.get(38);
            if (defaultValue.equalsIgnoreCase("0")) {
                defaultvalueToList.remove(38);
                defaultvalueToList.add(38, additionalfee);
            }
            else {
                defaultvalueToList.remove(38);
                defaultvalueToList.add(38, String.valueOf(Long.parseLong(additionalfee) + Long.parseLong(defaultValue)));
            }
        }
        if (tempAmount == Integer.parseInt(roundUpAmt)) {
            defaultvalueToList.remove(44);
            defaultvalueToList.add(44, roundUpAmt);
        }
        final Object[] transactionParam = defaultvalueToList.toArray();
        final String transactionDetailsStatus = this.transactionDao.saveTransactionDetailsByHead(transactionParam);
        return transactionDetailsStatus;
    }*/
    
   /* private String insertCalulationDetailsUG(final String tranId, final String categoriesId, final String session, final String rollNo, final String Amount, final String additionalfee, final String roundUpAmt, final String headNm) {
        final List<String> defaultvalueToList = this.addDefaultvalueToListUG(tranId);
        final List<Map<String, Object>> headNameList = (List<Map<String, Object>>)this.transactionDao.getHeadNameByIdUG(categoriesId, session);
        if (headNameList == null) {
            return "ERROR";
        }
        Long tempAmount;
        final Long finalAmount = tempAmount = Long.parseLong(Amount);
    Label_7176:
        for (final Map<String, Object> map : headNameList) {
            final String s;
            final String headName = s = (String) map.get("HEADNAME");
            switch (s) {
                case "Tuition Fee": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(1);
                            defaultvalueToList.add(1, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(1);
                            defaultvalueToList.add(1, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    //break;
                }
                case "Adm/Re-Adm": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(2);
                            defaultvalueToList.add(2, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(2);
                            defaultvalueToList.add(2, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                   // break;
                }
                case "Library": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(3);
                            defaultvalueToList.add(3, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(3);
                            defaultvalueToList.add(3, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    //break;
                }
                case "Sports And Games": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(4);
                            defaultvalueToList.add(4, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(4);
                            defaultvalueToList.add(4, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                   // break;
                }
                case "Medical": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(5);
                            defaultvalueToList.add(5, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(5);
                            defaultvalueToList.add(5, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    //break;
                }
                case "Labrotary": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(6);
                            defaultvalueToList.add(6, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(6);
                            defaultvalueToList.add(6, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                   // break;
                }
                case "Extra Caricular": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(7);
                            defaultvalueToList.add(7, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(7);
                            defaultvalueToList.add(7, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    //break;
                }
                case "Poor Boys Fund": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(8);
                            defaultvalueToList.add(8, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(8);
                            defaultvalueToList.add(8, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                   // break;
                }
                case "B.U.FEE": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(9);
                            defaultvalueToList.add(9, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(9);
                            defaultvalueToList.add(9, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                   // break;
                }
                case "Magazine": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(10);
                            defaultvalueToList.add(10, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(10);
                            defaultvalueToList.add(10, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                   // break;
                }
                case "College Exam": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(11);
                            defaultvalueToList.add(11, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(11);
                            defaultvalueToList.add(11, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                   // break;
                }
                case "Puja": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(12);
                            defaultvalueToList.add(12, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(12);
                            defaultvalueToList.add(12, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                  //  break;
                }
                case "College Day": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(13);
                            defaultvalueToList.add(13, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(13);
                            defaultvalueToList.add(13, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    //break;
                }
                case "Proctorial": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(14);
                            defaultvalueToList.add(14, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(14);
                            defaultvalueToList.add(14, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    //break;
                }
                case "Development One": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(15);
                            defaultvalueToList.add(15, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(15);
                            defaultvalueToList.add(15, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                   // break;
                }
                case "Science/Arts Society": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(16);
                            defaultvalueToList.add(16, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(16);
                            defaultvalueToList.add(16, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Red Cross": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(17);
                            defaultvalueToList.add(17, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(17);
                            defaultvalueToList.add(17, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Calendar": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(18);
                            defaultvalueToList.add(18, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(18);
                            defaultvalueToList.add(18, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Other Fees": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(19);
                            defaultvalueToList.add(19, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(19);
                            defaultvalueToList.add(19, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Building": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(20);
                            defaultvalueToList.add(20, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(20);
                            defaultvalueToList.add(20, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Reading Room": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(21);
                            defaultvalueToList.add(21, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(21);
                            defaultvalueToList.add(21, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Registration": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(22);
                            defaultvalueToList.add(22, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(22);
                            defaultvalueToList.add(22, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Labrotary Caution Money": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(23);
                            defaultvalueToList.add(23, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(23);
                            defaultvalueToList.add(23, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Library Caution Money": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(24);
                            defaultvalueToList.add(24, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(24);
                            defaultvalueToList.add(24, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Identity Card": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(25);
                            defaultvalueToList.add(25, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(25);
                            defaultvalueToList.add(25, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Cultural": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(26);
                            defaultvalueToList.add(26, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(26);
                            defaultvalueToList.add(26, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Scout And Guide": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(27);
                            defaultvalueToList.add(27, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(27);
                            defaultvalueToList.add(27, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Tution Fees For 10 Month": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(29);
                            defaultvalueToList.add(29, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(29);
                            defaultvalueToList.add(29, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Development For 10 Month": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(28);
                            defaultvalueToList.add(28, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(28);
                            defaultvalueToList.add(28, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Social Service Guide": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(30);
                            defaultvalueToList.add(30, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(30);
                            defaultvalueToList.add(30, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Flag Day": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(31);
                            defaultvalueToList.add(31, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(31);
                            defaultvalueToList.add(31, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "BU Examination": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(32);
                            defaultvalueToList.add(32, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(32);
                            defaultvalueToList.add(32, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Hons Fee": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(33);
                            defaultvalueToList.add(33, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(33);
                            defaultvalueToList.add(33, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Seminar Fee": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(34);
                            defaultvalueToList.add(34, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(34);
                            defaultvalueToList.add(34, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "CLC": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(35);
                            defaultvalueToList.add(35, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(35);
                            defaultvalueToList.add(35, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Development Two": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(36);
                            defaultvalueToList.add(36, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(36);
                            defaultvalueToList.add(36, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "CAF": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(37);
                            defaultvalueToList.add(37, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(37);
                            defaultvalueToList.add(37, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Additional Fee": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm == null || headNm.equalsIgnoreCase("")) {
                        continue;
                    }
                    if (headNm.equalsIgnoreCase(headName)) {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(38);
                            defaultvalueToList.add(38, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(38);
                            defaultvalueToList.add(38, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Fine": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(39);
                            defaultvalueToList.add(39, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(39);
                            defaultvalueToList.add(39, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
                case "Miscellanious": {
                    final String bd = (String) map.get("AMOUNT");
                    final long amnt = Long.parseLong(bd);
                    if (headNm != null && !headNm.equalsIgnoreCase("")) {
                        if (!headNm.equalsIgnoreCase(headName)) {
                            continue;
                        }
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(40);
                            defaultvalueToList.add(40, String.valueOf(amnt));
                            break Label_7176;
                        }
                        break Label_7176;
                    }
                    else {
                        if (tempAmount != 0L) {
                            tempAmount -= amnt;
                            defaultvalueToList.remove(40);
                            defaultvalueToList.add(40, String.valueOf(amnt));
                            continue;
                        }
                        continue;
                    }
                    // break;
                }
            }
        }
        if (additionalfee != null && !additionalfee.equalsIgnoreCase("")) {
            final String defaultValue = defaultvalueToList.get(32);
            if (defaultValue.equalsIgnoreCase("0")) {
                defaultvalueToList.remove(32);
                defaultvalueToList.add(32, additionalfee);
            }
            else {
                defaultvalueToList.remove(32);
                defaultvalueToList.add(32, String.valueOf(Long.parseLong(additionalfee) + Long.parseLong(defaultValue)));
            }
        }
        if (tempAmount != null && tempAmount != 0L && tempAmount == Integer.parseInt(roundUpAmt)) {
            defaultvalueToList.remove(41);
            defaultvalueToList.add(41, roundUpAmt);
        }
        final Object[] transactionParam = defaultvalueToList.toArray();
        final String transactionDetailsStatus = this.transactionDao.saveTransactionDetailsByHeadUG(transactionParam);
        return transactionDetailsStatus;
    }*/
    
    private List<String> addDefaultvalueToList(final String tranId) {
        final ArrayList<String> list = new ArrayList<String>(44);
        list.add(0, tranId);
        for (int i = 1; i <= 44; ++i) {
            list.add(i, "0");
        }
        return list;
    }
    
    private List<String> addDefaultvalueToListUG(final String tranId) {
        final ArrayList<String> list = new ArrayList<String>(41);
        list.add(0, tranId);
        for (int i = 1; i <= 41; ++i) {
            list.add(i, "0");
        }
        return list;
    }
    
    @Override
    public String updateverifyStatus(final String verifyStatus, final String rollNo, final String userName, final String scienceType) throws Exception {
        final String updateStatus = this.transactionDao.updateVerifyStatus(verifyStatus, userName, rollNo, scienceType);
        return updateStatus;
    }
    
    @Override
    public String updateverifyStatusAD(final String verifyStatus, final String rollNo, final String userName, final String scienceType) throws Exception {
        final String updateStatus = this.transactionDao.updateVerifyStatusAD(verifyStatus, userName, rollNo, scienceType);
        return updateStatus;
    }
    
    @Override
    public String updateverifyStatusRF(final String verifyStatus, final String rollNo, final String userName, final String scienceType) throws Exception {
        final String updateStatus = this.transactionDao.updateVerifyStatusRF(verifyStatus, userName, rollNo, scienceType);
        return updateStatus;
    }
    
    @Override
    public String checkMrnExist(final String collgeName, final String mrinNo, final String currentSession) {
        final String mrnStatus = this.transactionDao.checkMrinExist(collgeName, mrinNo, currentSession);
        if (mrnStatus.equalsIgnoreCase("ERROR")) {
            return "MRN_ERROR";
        }
        return mrnStatus;
    }
    
    @Override
    public String getAvailableAmount(final String id) {
        return this.transactionDao.getAvailbleAmount(id);
    }
    
    @Override
    public String updateAvailableAmount(final Integer availableAmount, final String totalAMount, final String configId) {
        final String updatedAMount = String.valueOf(availableAmount + Integer.parseInt(totalAMount));
        return this.transactionDao.updateAvailableAmount(updatedAMount, configId);
    }
    
    @Override
    public String getRecepitNumber(final String tranType, final String rollNo, final String scienceType) {
        final String currsentSession = this.transactionDao.getAvailbleAmount("6");
        final List<Map<String, Object>> recepitNumber = (List<Map<String, Object>>)this.transactionDao.getRecepitNumber(tranType, rollNo, currsentSession, scienceType);
        final String receiptNo = (String) recepitNumber.get(0).get("RECEIPT_NO");
        final String bookNo = (String) recepitNumber.get(0).get("BOOK_NO");
        if (bookNo == null) {
            return receiptNo;
        }
        return receiptNo + "/" + bookNo;
    }
    
    @Override
    public String getTransactionDate(final String tranType, final String rollNo, final String scienceType) {
        final String currsentSession = this.transactionDao.getAvailbleAmount("6");
        final Date tranDate = this.transactionDao.getTransactionDate(tranType, rollNo, currsentSession, scienceType);
        final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(tranDate);
    }
    
    @Override
    public long getHeadDetailsForprint(final String rollNo, final String transactionType, final HttpServletRequest req, final String scienceType) {
    	 final Object[] params = { rollNo, transactionType, scienceType };
         long amount = 0L;
         List<HeadDeatils> headDetailsForprintUG = this.transactionDao.getHeadDetailsForprint(params);
         List<HeadDeatils> addReason = this.transactionDao.getAddReason(params);
        /* List<HeadDeatils> collectHeadDetails = addReason.stream()
         .filter(p -> p!=null)
 	     .map(obj -> new HeadDeatils(obj.getHeadname(), obj.getHeadAmount()))
 	     .collect(Collectors.toList());
         collectHeadDetails.addAll(headDetailsForprintUG);*/
         if(addReason.get(0).getHeadname()!=null)
         headDetailsForprintUG.add(addReason.get(0));
         amount= headDetailsForprintUG.stream().filter(p-> p.getHeadAmount()>0).mapToLong(o->o.getHeadAmount()).sum();
         req.getSession().setAttribute("printMap", (Object)headDetailsForprintUG);
         return amount;
    	
    	
    	/* final Object[] params = { rollNo, transactionType, scienceType };
        long amount = 0L;
        final List<Map<String, Object>> headDetailsForprint = (List<Map<String, Object>>)this.transactionDao.getHeadDetailsForprint(params);
        if (headDetailsForprint != null && !headDetailsForprint.isEmpty()) {
            final Map<String, Object> headDetailsMap = new LinkedHashMap<String, Object>();
            for (final Map<String, Object> map : headDetailsForprint) {
                headDetailsMap.put("Tuition Fee", map.get("TUITION_FEES"));
                headDetailsMap.put("Adm/Re-Adm", map.get("Adm_ReAdm"));
                headDetailsMap.put("CLC", map.get("CLC"));
                headDetailsMap.put("Migration & Original Certificate", map.get("MIGRATION_ORIGINAL_CERTIFICATE"));
                headDetailsMap.put("Library", map.get("LIBRARY"));
                headDetailsMap.put("Sports & Games", map.get("SPORTS_GAMES"));
                headDetailsMap.put("Medical", map.get("MEDICAL"));
                headDetailsMap.put("Medical aid Fund", map.get("MEDICAL_AID"));
                headDetailsMap.put("Laboratory", map.get("LABORTARY"));
                headDetailsMap.put("Extra Curricular", map.get("EXTRA_CARICULAR"));
                headDetailsMap.put("Poor Boys Fund", map.get("POOR_BOYS_FUND"));
                headDetailsMap.put("Council", map.get("COUNCIL"));
                headDetailsMap.put("Magazine", map.get("MAGAZINE"));
                headDetailsMap.put("College Examination", map.get("COLLEGE_EXAM"));
                headDetailsMap.put("Puja", map.get("PUJA"));
                headDetailsMap.put("College Day", map.get("COLLEGE_DAY"));
                headDetailsMap.put("Proctorial", map.get("PROCTORIAL"));
                headDetailsMap.put("Development Fee (I)", map.get("DEVELOPMENT"));
                headDetailsMap.put("Science & Arts Society", map.get("SC_ARTS_SOCIETY"));
                headDetailsMap.put("Red Cross", map.get("RED_CROSS"));
                headDetailsMap.put("Calendar", map.get("CALENDER"));
                headDetailsMap.put("Syllabus", map.get("SYLLABUS"));
                headDetailsMap.put("Other Fees", map.get("OTHER_FEES"));
                headDetailsMap.put("Building", map.get("BUILDING"));
                headDetailsMap.put("Academic", map.get("ACADEMIC"));
                headDetailsMap.put("Reading Room", map.get("READING_ROOM"));
                headDetailsMap.put("Registration", map.get("REGISTRATION"));
                headDetailsMap.put("Laboratory Caution Money", map.get("LAB_CAUTION_MONEY"));
                headDetailsMap.put("Library Caution Money", map.get("LIB_CAUTION_MONEY"));
                headDetailsMap.put("Identity Card", map.get("IDENTITY_CARD"));
                headDetailsMap.put("Recognition", map.get("RECOGNITION"));
                headDetailsMap.put("Scout and Guide", map.get("SCOUT_AND_GUIDE"));
                headDetailsMap.put("Tuition Fee for 10 Months", map.get("TUITIONFEES_FOR_MONTH"));
                headDetailsMap.put("Development for 10 Months", map.get("DEVELOPMENT_FOR_10_MONTH"));
                headDetailsMap.put("Social Service Guild", map.get("SOCIAL_SCIENCE"));
                headDetailsMap.put("Flag Day", map.get("FLAG_DAY"));
                headDetailsMap.put("EMS Fee", map.get("EMS_FEE"));
                headDetailsMap.put("Development Fee (II)", map.get("DEVELOPMENT_TWO"));
                headDetailsMap.put("Roundup", map.get("ROUNDUP"));
                final BigDecimal bd = (BigDecimal) map.get("TOTAL_AMOUNT");
                amount = bd.longValue();
            }
            req.getSession().setAttribute("printMap", (Object)headDetailsMap);
        }
        return amount;*/
    }
    
    @Override
    public long getHeadDetailsForprintUG(final String rollNo, final String transactionType, final HttpServletRequest req, final String yearBatch, final String scienceType) {
        final Object[] params = { rollNo, transactionType, scienceType };
        long amount = 0L;
        List<HeadDeatils> headDetailsForprintUG = this.transactionDao.getHeadDetailsForprintUG(params);
        long amt=this.transactionDao.getPracticalAmount(params);
        headDetailsForprintUG.stream().filter(p-> p.getHeadname().equalsIgnoreCase("BU Examination")).forEach(d -> d.setHeadAmount(d.getHeadAmount()+(int)amt));
        amount= headDetailsForprintUG.stream().filter(p-> p.getHeadAmount()>0).mapToLong(o->o.getHeadAmount()).sum();
        if(yearBatch.equalsIgnoreCase("1st")) {
        	HeadDeatils hd=new HeadDeatils();
        	hd.setHeadname("Round Up");
        	double totalRoundUpAmt=(Math.rint((double) amount / 10) * 10);
            if(amount-totalRoundUpAmt >0) {
            	totalRoundUpAmt +=10;
            }
        	int roundUpAmt=(int) (totalRoundUpAmt-amount);
        	hd.setHeadAmount(roundUpAmt);
        	headDetailsForprintUG.add(hd);	
        	amount =(long) totalRoundUpAmt;
        }
        req.getSession().setAttribute("printMap", (Object)headDetailsForprintUG);
        return amount;
        /*if (headDetailsForprint != null && !headDetailsForprint.isEmpty()) {
            final Map<String, Object> headDetailsMap = new LinkedHashMap<String, Object>();
            for (final Map<String, Object> map : headDetailsForprint) {
                headDetailsMap.put("Tuition Fee", map.get("TUTION_FEES"));
                headDetailsMap.put("Adm/Re-Adm", map.get("ADM_READM"));
                headDetailsMap.put("Library", map.get("LIBRARY"));
                headDetailsMap.put("Sports & Games", map.get("GAMES_AND_SPORTS"));
                headDetailsMap.put("Medical", map.get("MEDICAL"));
                headDetailsMap.put("Laboratory", map.get("LABORTARY"));
                headDetailsMap.put("Extra Curricular", map.get("EXTRA_CARICULAR"));
                headDetailsMap.put("Poor Boys Fund", map.get("POOR_BOYS_FUND"));
                headDetailsMap.put("B.U.FEE", map.get("BU_FEE"));
                headDetailsMap.put("Magazine", map.get("MAGAZINE"));
                headDetailsMap.put("College Examination", map.get("COLLEGE_EXAM"));
                headDetailsMap.put("Puja", map.get("PUJA"));
                headDetailsMap.put("College Day", map.get("COLLEGE_DAY"));
                headDetailsMap.put("Proctorial", map.get("PROCTORIAL"));
                headDetailsMap.put("Development Fee (I)", map.get("DEVELOPMENT_ONE"));
                headDetailsMap.put("Science & Arts Society", map.get("SCINCE_ARTS_SOCIETY"));
                headDetailsMap.put("Red Cross", map.get("RED_CROSS"));
                headDetailsMap.put("Calendar", map.get("CALENDAR"));
                headDetailsMap.put("Other Fees", map.get("OTHER_FEES"));
                headDetailsMap.put("Building", map.get("BUILDING"));
                headDetailsMap.put("Reading Room", map.get("READING_ROOM"));
                headDetailsMap.put("Registration", map.get("REGISTRATION"));
                headDetailsMap.put("Laboratory Caution Money", map.get("LABROTARY_CAUTION_MONEY"));
                headDetailsMap.put("Library Caution Money", map.get("LIBRARY_CAUTION_MONEY"));
                headDetailsMap.put("Identity Card", map.get("IDENTITY_CARD"));
                headDetailsMap.put("Cultural", map.get("CULTURAL"));
                headDetailsMap.put("Scout And Guide", map.get("SCOUT_AND_GUIDE"));
                headDetailsMap.put("Tuition Fee for 10 Months", map.get("TUTION_FEE_FOR_10_MONTH"));
                headDetailsMap.put("Development For 10 Month", map.get("DEVELOPMENT_FOR_10_MONTH"));
                headDetailsMap.put("Social Service Guild", map.get("SOCIAL_SERVICE_GUIDE"));
                headDetailsMap.put("Flag Day", map.get("FLAG_DAY"));
                headDetailsMap.put("BU Examination Fees", map.get("BU_EXAMINATION"));
                headDetailsMap.put("Hons Fee", map.get("HONS_FEE"));
                headDetailsMap.put("Seminar Fee", map.get("SEMINAR_FEE"));
                headDetailsMap.put("CLC", map.get("CLC"));
                headDetailsMap.put("Development Fee (II)", map.get("DEVELOPMENT_TWO"));
                if (yearBatch.equalsIgnoreCase("1st")) {
                    headDetailsMap.put("Roundup", map.get("ROUNDUP"));
                }
                final BigDecimal bd = (BigDecimal) map.get("TOTAL_AMOUNT");
                amount = bd.longValue();
            }*/
           
        //}
       
    }
    
    @Override
    public List<HeadDeatils> getHeadNameAndAmountUG(final Student student, final String currentSession) {
        try {
            List<HeadDeatils> headDetailsList = null;
            if (student.getCategory() != null && (student.getCategory().equalsIgnoreCase("General") || student.getCategory().equalsIgnoreCase("OBC"))) {
                student.setCategory("Gen/OBC");
            }
            else {
                student.setCategory("SC/ST/Girls");
            }
            final Object[] params = { student.getCourse(), student.getStream(), student.getCategory(), student.getYearBatch(), currentSession };
            final List<Map<String, Object>> headNameAndAmount = (List<Map<String, Object>>)this.transactionDao.getHeadNameAndAmountUG(params);
            HeadDeatils head = null;
            if (headNameAndAmount != null) {
                headDetailsList = new ArrayList<HeadDeatils>();
                for (final Map<String, Object> map : headNameAndAmount) {
                    head = new HeadDeatils();
                    head.setHeadname((String)map.get("HEADNAME"));
                    head.setHeadAmount(Integer.parseInt((String) map.get("AMOUNT")));
                    headDetailsList.add(head);
                }
                return headDetailsList;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public String updateStudent(final Student student, final String userName) throws Exception {
        final Calendar cal = Calendar.getInstance();
        final Date today = cal.getTime();
        final Object[] studentObjectParam = { today, userName, student.getFundType(), "10", student.getRollNo(), student.getScienceSelect() };
        TransactionServiceImpl.log.info((Object)"Start Updating Student Information");
        TransactionServiceImpl.log.info((Object)("Inputs ::" + Arrays.toString(studentObjectParam)));
        final String status = this.transactionDao.updateStudent(studentObjectParam);
        TransactionServiceImpl.log.info((Object)"End Saving Student Information");
        TransactionServiceImpl.log.info((Object)("Count ::" + status));
        if (status.equalsIgnoreCase("1")) {
            return status;
        }
        return null;
    }
    
   /* @Override
    public Long getStudentAmountUG(final String rollNo, final String scienceType) {
        final Object[] studentObjectParam = { rollNo, scienceType };
        final String amount = this.transactionDao.getStudentAmountUG(studentObjectParam);
        if (amount != "0") {
            return Long.parseLong(amount);
        }
        return 0L;
    }*/
    
    @Transactional(rollbackFor = { Exception.class })
    @Override
    public String updateTransacton(final Transaction transaction, final Student student, final String userName, final String currentSession) throws Exception {
        try {
            if (transaction.getTransactiontype().equalsIgnoreCase("RF")) {
                final Integer count = this.transactionDao.isChequeNoExist(transaction.getBankName(), transaction.getChequeNumber());
                if (count == null || count >= 1) {
                    return "Cheque Number With Selected Bank Is Already Exist In Database.. Please Try With Other";
                }
            }
            final DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
            final SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
            final Date FormatDate = inputFormat.parse(transaction.getTransactionDate());
            final String outputText = outputFormat.format(FormatDate);
            final Date refundDate = outputFormat.parse(outputText);
            final Date transactionDate = this.transactionDao.getTransactionDate("A", student.getRollNo(), student.getSession(), student.getScienceSelect());
            final String tranDate = outputFormat.format(transactionDate);
            final Date trandat = outputFormat.parse(tranDate);
            if (trandat.compareTo(refundDate) > 0) {
                return "Please Choose Same or After Date Of Admission Date.. Admission Date Was :: " + trandat;
            }
            final String collegename = this.transactionDao.getCollegeName("1");
            final StringBuffer sb = new StringBuffer();
            if (collegename == null || collegename.equalsIgnoreCase("")) {
                throw new Exception("Exception WHile Getting College Name");
            }
            sb.append(collegename);
            sb.append("-");
            Integer transactionTypeCount = 0;
            try {
                if (transaction.getTransactiontype().equalsIgnoreCase("RF")) {
                    sb.append("REF");
                    transactionTypeCount = this.transactionDao.getRefundTransactionTypeCount("RF", currentSession);
                }
                else {
                    sb.append("REJ");
                    transactionTypeCount = this.transactionDao.getRefundTransactionTypeCount("RE", currentSession);
                }
            }
            catch (Exception e) {
                throw new Exception("Exception While Getting Transaction Count");
            }
            sb.append("-");
            final String tempId = String.format("%04d", transactionTypeCount + 1);
            sb.append(tempId);
            final String tran_Id = this.transactionDao.getTransactionId(student.getRollNo(), student.getScienceSelect());
            if (tran_Id == null || tran_Id.equalsIgnoreCase("")) {
                throw new Exception("Exception WHile Getting Transaction Id");
            }
            String studentTrackStatus = "";
            if (transaction.getTransactiontype().equalsIgnoreCase("RE")) {
                studentTrackStatus = this.transactionDao.updateStudentTrack("Rejection", student.getRollNo());
            }
            else {
                studentTrackStatus = "SUCCESS";
            }
            if (studentTrackStatus == null || studentTrackStatus.equalsIgnoreCase("")) {
                throw new Exception("Update Student Track Status Is Null/Empty");
            }
            if (!studentTrackStatus.equalsIgnoreCase("SUCCESS")) {
                throw new Exception("Update Student Track Status is ERROR");
            }
            final String studentUpdateStatus = this.transactionDao.updateStudentRefStatus("30", student.getRollNo(), student.getScienceSelect());
            if (studentUpdateStatus == null || studentUpdateStatus.equalsIgnoreCase("")) {
                throw new Exception("Update Student Status is NULL/Empty");
            }
            if (!studentUpdateStatus.equalsIgnoreCase("SUCCESS")) {
                throw new Exception("Update Student Status is ERROR");
            }
            final String head_Category_Id = this.transactionDao.getheadCategoryId(student.getRollNo(), student.getScienceSelect());
            final Object[] transactionStatusParam = { tran_Id, student.getRollNo(), transaction.getActualAmount(), transaction.getTotalAmount(), refundDate, userName, sb.toString(), "Cash", transaction.getChequeNumber(), transaction.getBankName(), currentSession, head_Category_Id, student.getScienceSelect(), transaction.getTransactiontype() };
            final String updateTransactionTrackStatus = this.transactionDao.updateTransactionTrack(transactionStatusParam);
            if (updateTransactionTrackStatus == null || updateTransactionTrackStatus.equalsIgnoreCase("")) {
                throw new Exception("Update Transaction Track Status is NULL/EMPTY");
            }
            if (updateTransactionTrackStatus.equalsIgnoreCase("SUCCESS")) {
                return "Payment has been done..Receipt No is: " + sb.toString();
            }
            throw new Exception("Update Transaction Track Status is ERROR");
        }
        catch (Exception e2) {
            throw new Exception("Exception WHile Updating Trancaction For Refund");
        }
    }
    
    /*private String updateCalulationDetailsUG(final String tranId) {
        final Object[] params = { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "100", "0", "0", "0", "0", "0", tranId };
        final String status = this.transactionDao.updateTransactionDetailsUG(params);
        return status;
    }*/
    
    @Override
    public String getBankName(final String rollNo, final String scienceType) {
        return this.transactionDao.getBankName(rollNo, scienceType, "RF");
    }
    
    @Override
    public String getChequeDDNo(final String rollNo, final String scienceType) {
        return this.transactionDao.getChequeNumber(rollNo, scienceType, "RF");
    }
    
    @Override
    public long getTotalAmount(final String rollNo, final String transactionType, final String scienceType) {
        return this.transactionDao.getTotalAmount(rollNo, scienceType, transactionType);
    }
    
    @Override
    public String getRecepitNumberRF(final String tranType, final String rollNo, final String scienceType) {
        final String currsentSession = this.transactionDao.getAvailbleAmount("6");
        final String receiptNumber = this.transactionDao.getRecepitNumberRF(tranType, rollNo, currsentSession, scienceType);
        return receiptNumber;
    }
    
    @Override
    public String getTransactionDateRF(final String tranType, final String rollNo, final String scienceType) {
        final String currsentSession = this.transactionDao.getAvailbleAmount("6");
        final Date tranDate = this.transactionDao.getTransactionDateRF(tranType, rollNo, currsentSession, scienceType);
        final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(tranDate);
    }
    
    @Override
    public String getRecepitNumberAD(final String tranType, final String rollNo, final String scienceType) {
        final String currsentSession = this.transactionDao.getAvailbleAmount("6");
        final String receiptNumber = this.transactionDao.getRecepitNumberAD(tranType, rollNo, currsentSession, scienceType);
        return receiptNumber;
    }
    
    @Override
    public String getBookIdAD(final String tranType, final String rollNo, final String scienceType) {
        final String currsentSession = this.transactionDao.getAvailbleAmount("6");
        final String receiptNumber = this.transactionDao.getBookAD(tranType, rollNo, currsentSession, scienceType);
        return receiptNumber;
    }
    
    @Override
    public String getTransactionDateAD(final String tranType, final String rollNo, final String scienceType) {
        final String currsentSession = this.transactionDao.getAvailbleAmount("6");
        final Date tranDate = this.transactionDao.getTransactionDateAD(tranType, rollNo, currsentSession, scienceType);
        final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(tranDate);
    }
    
    @Override
    public long getTotalAmountAD(final String rollNo, final String scienceType, final String transactionType) {
        return this.transactionDao.getTotalAmountAD(rollNo, scienceType, transactionType);
    }
    
    @Override
    public String getHeadName(final String rollNo, final String scienceType) {
        return this.transactionDao.getHeadName(rollNo, scienceType, "AD");
    }
    
   /* @Override
    public String getCLCAmountUG(final String rollNo, final String scienceType) {
        return this.transactionDao.getCLCAmountUG(rollNo, scienceType);
    }*/
    
    @Override
    public List<HeadDeatils> getHeadNameAndAmount(final Student student, final String currentSession) {
        try {
            List<HeadDeatils> headDetailsList = null;
            if (student.getCategory() != null && (student.getCategory().equalsIgnoreCase("General") || student.getCategory().equalsIgnoreCase("OBC"))) {
                student.setCategory("Gen/OBC");
            }
            else {
                student.setCategory("SC/ST/Girls");
            }
            final Object[] params = { student.getCourse(), student.getStream(), student.getCategory(), student.getYearBatch(), currentSession };
            final List<Map<String, Object>> headNameAndAmount = (List<Map<String, Object>>)this.transactionDao.getHeadNameAndAmount(params);
            HeadDeatils head = null;
            if (headNameAndAmount != null) {
                headDetailsList = new ArrayList<HeadDeatils>();
                for (final Map<String, Object> map : headNameAndAmount) {
                    head = new HeadDeatils();
                    head.setHeadname((String)map.get("HEADNAME"));
                    final BigDecimal bd = (BigDecimal) map.get("AMOUNT");
                    head.setHeadAmount(bd.intValue());
                    headDetailsList.add(head);
                }
                return headDetailsList;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /*@Override
    public Long getStudentAmount(final String rollNo, final String scienceType) {
        final Object[] studentObjectParam = { rollNo, scienceType };
        final String amount = this.transactionDao.getStudentAmount(studentObjectParam);
        if (amount != "0") {
            return Long.parseLong(amount);
        }
        return 0L;
    }*/
    
   /* @Override
    public String getCLCAmount(final String rollNo, final String scienceType) {
        return this.transactionDao.getCLCAmount(rollNo, scienceType);
    }*/
    
    @Override
    public List<AdditionalTransaction> getPrintDetails(final String rollNo, final String tranType) {
        try {
            final List<Map<String, Object>> headNameAndAmount = (List<Map<String, Object>>)this.transactionDao.getPrintDetails(rollNo, tranType);
            if (headNameAndAmount != null) {
                final List<AdditionalTransaction> printDetails = new ArrayList<AdditionalTransaction>();
                AdditionalTransaction addTransaction = null;
                for (final Map<String, Object> map : headNameAndAmount) {
                    addTransaction = new AdditionalTransaction();
                    addTransaction.setACADEMIC_SESSION((String)map.get("ACADEMIC_SESSION"));
                    addTransaction.setBOOK_NO((String)map.get("BOOK_NO"));
                    addTransaction.setHEAD_AMOUNT((String)map.get("HEAD_AMOUNT"));
                    addTransaction.setHEAD_CATEGORIES_ID((String)map.get("HEAD_CATEGORIES_ID"));
                    addTransaction.setHEAD_NAME((String)map.get("HEAD_NAME"));
                    addTransaction.setRECEIPT_NUMBER((String)map.get("RECEIPT_NUMBER"));
                    addTransaction.setROLL_NO((String)map.get("ROLL_NO"));
                    addTransaction.setSCIENCE_OPTION((String)map.get("SCIENCE_OPTION"));
                    addTransaction.setTRAN_BY((String)map.get("TRAN_BY"));
                    addTransaction.setTRAN_DATE((Date)map.get("TRAN_DATE"));
                    addTransaction.setTRAN_ID((String)map.get("TRAN_ID"));
                    addTransaction.setTRANSACTION_MODE((String)map.get("TRANSACTION_MODE"));
                    addTransaction.setTRANSACTION_TYPE((String)map.get("TRANSACTION_TYPE"));
                    printDetails.add(addTransaction);
                }
                return printDetails;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public List<Map<String, Object>> getRecepitNumberCLC(final String tranType, final String rollNo, final String scienceType) {
        final String currsentSession = this.transactionDao.getAvailbleAmount("6");
        final List<Map<String, Object>> recepitNumber = (List<Map<String, Object>>)this.transactionDao.getRecepitNumberCLC(tranType, rollNo, currsentSession, scienceType);
        return recepitNumber;
    }
    
   /* @Override
    public long getTotalAmountCLC(final String rollNo, final String scienceType, final String transactionType) {
        return Long.parseLong(this.transactionDao.getCLCAmount(rollNo, scienceType));
    }*/
    
    @Override
    public String getTransactionDateCLC(final String tranType, final String rollNo, final String scienceType) {
        final String currsentSession = this.transactionDao.getAvailbleAmount("6");
        final Date tranDate = this.transactionDao.getTransactionDateAD(tranType, rollNo, currsentSession, scienceType);
        final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(tranDate);
    }
    
    static {
        log = Logger.getLogger((Class)TransactionServiceImpl.class);
    }
}
