package com.daily.collection.web.reports.service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.daily.collection.web.reports.bean.DCRBook;
import com.daily.collection.web.reports.bean.Reports;

public interface IReportService {

	public void getAllreport(Reports reports,HttpServletRequest request) throws ParseException; 
	
	public String getheadDetails(Reports reports,HttpServletRequest request);
	
	public String getstudentDetails(Reports reports,HttpServletRequest request);
	
	public List<Map<String, Object>> getDCRBook(DCRBook dcrBook,String config,List<String> stream);
	
}
