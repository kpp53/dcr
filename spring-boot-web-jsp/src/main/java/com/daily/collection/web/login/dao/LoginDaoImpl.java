package com.daily.collection.web.login.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.daily.collection.web.transaction.repository.ITransactionDao;

@Repository
public class LoginDaoImpl implements ILoginDao {

	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	
	@Override
	public String getCurrentSession(String id) {
		try{
			String value=jdbcTemplate.queryForObject(ITransactionDao.GET_AVAILABLE_AMOUNT,new Object[]{id},String.class);
			return value;
		}catch(Exception e){
			e.printStackTrace();
			return "";
		}
	}
	
	
	@Override
	public List<String> getConfigurationDetails(String configId) {
		try{
			List<String> configurationList=jdbcTemplate.queryForList(ILoginDao.CONF_DETAILS, String.class, new Object[]{configId,"Y"});
			return configurationList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
