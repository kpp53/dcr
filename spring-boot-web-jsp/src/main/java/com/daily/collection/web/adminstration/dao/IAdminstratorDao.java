package com.daily.collection.web.adminstration.dao;

import java.util.Date;
import java.util.List;

public interface IAdminstratorDao {
	

	public static final String SAVE_CONFIGURATION="INSERT INTO CONFIGURATION (VALUE,STATUS,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,CONFIG_ID)"
			+ "VALUES(?,?,?,sysdate,?,?,?)";
	
	public static final String UPDATE_CONFIGURATION="UPDATE CONFIGURATION SET STATUS=?,MODIFIED_BY=?,MODIFIED_DATE=? WHERE VALUE=?";
	
	public static final String GET_CONFIGURATION_BY_ID="SELECT VALUE FROM CONFIGURATION WHERE CONFIG_ID=?";
	
	
	public List<String> getCnfigurationDetailsByConfigId(String id);
	
	public String saveConfiguration(Object[] params);
	
	public String updateConfiguration(String status,String value,String userName,Date createdDate);
	
	
	
	
}
