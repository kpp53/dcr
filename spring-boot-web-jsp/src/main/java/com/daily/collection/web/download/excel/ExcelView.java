package com.daily.collection.web.download.excel;

import org.springframework.web.servlet.view.document.*;
import org.apache.poi.hssf.usermodel.*;
import javax.servlet.http.*;
import com.daily.collection.web.reports.bean.*;
import org.apache.poi.ss.usermodel.*;
import java.util.*;
import com.daily.collection.web.tresaury.bean.*;
import com.daily.collection.web.analytic.bean.*;
import com.daily.collection.web.creation.bean.*;
import java.math.*;

public class ExcelView extends AbstractExcelView
{
    protected void buildExcelDocument(final Map<String, Object> model, final HSSFWorkbook workbook, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        final String from = (String) model.get("from");
        if (from != null && !"".equalsIgnoreCase(from)) {
            if (from.equalsIgnoreCase("ALL")) {
                this.downloadAllReport(model, (Workbook)workbook, request, response);
            }
            else if (from.equalsIgnoreCase("Student")) {
                this.downloadStudentReport(model, (Workbook)workbook, request, response);
            }
            else if (from.equalsIgnoreCase("Fund")) {
                this.downloadHeadReport(model, workbook, request, response);
            }
            else if (from.equalsIgnoreCase("CollectionALL")) {
                this.downloadCollectionAllReport(model, workbook, request, response);
            }
            else if (from.equalsIgnoreCase("DCRBook")) {
                this.downloadDCRBookReport(model, workbook, request, response);
            }
        }
    }
    
    private void downloadDCRBookReport(final Map<String, Object> model, final HSSFWorkbook workbook, final HttpServletRequest request, final HttpServletResponse response) {
        response.setHeader("Content-Disposition", "attachment; filename=\"DCRBook.xls\"");
        final Sheet sheet = (Sheet)workbook.createSheet("DCR BooK ");
        sheet.setDefaultColumnWidth(15);
        final Reports reports = (Reports)request.getSession().getAttribute("reports");
        final CellStyle cellStyle = createBorderedStyle((Workbook)workbook);
        final DataFormat format = (DataFormat)workbook.createDataFormat();
        cellStyle.setDataFormat(format.getFormat("0"));
        final CellStyle createCellStyle = (CellStyle)workbook.createCellStyle();
        final Font font = (Font)workbook.createFont();
        font.setBoldweight((short)700);
        createCellStyle.setFont(font);
        createCellStyle.setBorderRight((short)1);
        createCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        createCellStyle.setBorderBottom((short)1);
        createCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        createCellStyle.setBorderLeft((short)1);
        createCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        createCellStyle.setBorderTop((short)1);
        createCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
        final String config = (String)request.getSession().getAttribute("config");
        createHeadCell(sheet, workbook, createCellStyle, config);
        final List<Map<String, String>> dcrBookResponse = (List<Map<String, String>>)request.getSession().getAttribute("dcrBookResponse");
        final Row dummyRow = sheet.createRow(1);
        final Row row2 = sheet.createRow(2);
        final Row row3 = sheet.createRow(3);
        final Row row4 = sheet.createRow(4);
        final Row row5 = sheet.createRow(5);
        final Row row6 = sheet.createRow(6);
        final Row row7 = sheet.createRow(7);
        final Row row8 = sheet.createRow(8);
        final Row row9 = sheet.createRow(9);
        final Map<String, String> map = dcrBookResponse.get(0);
        final ArrayList<String> keyList = new ArrayList<String>(map.keySet());
        keyList.add(0, "");
        keyList.add("");
        Integer totalAmt = 0;
        int j = 0;
        int o1 = 0;
        final Row row10 = sheet.createRow(10);
        for (final String key : keyList) {
            final Cell cell = row10.createCell(o1);
            if (o1 == 0) {
                cell.setCellStyle(createCellStyle);
                cell.setCellValue("SL No.");
            }
            else if (o1 == keyList.size() - 1) {
                cell.setCellStyle(createCellStyle);
                cell.setCellValue("Total");
            }
            else {
                cell.setCellStyle(createCellStyle);
                cell.setCellValue(key);
            }
            ++o1;
        }
        for (final Map<String, String> m : dcrBookResponse) {
            final Row row11 = sheet.createRow(j + 11);
            final Integer in = j;
            Integer total = 0;
            int o2 = 0;
            for (final String key2 : keyList) {
                final Cell cell2 = row11.createCell(o2);
                if (o2 == 0) {
                    cell2.setCellStyle(cellStyle);
                    cell2.setCellValue((double)(in + 1));
                }
                else if (o2 > 10 && o2 < keyList.size() - 1) {
                    cell2.setCellStyle(cellStyle);
                    final String value = m.get(keyList.get(o2));
                    if (value == null || value.equalsIgnoreCase(" ")) {
                        cell2.setCellValue(0.0);
                    }
                    else {
                        if (value.equalsIgnoreCase("0")) {
                            cell2.setCellValue(0.0);
                        }
                        else {
                            cell2.setCellValue((double)Integer.parseInt(value));
                        }
                        total += Integer.parseInt(value);
                    }
                }
                else if (o2 == keyList.size() - 1) {
                    cell2.setCellStyle(cellStyle);
                    cell2.setCellValue((double)total);
                }
                else {
                    cell2.setCellStyle(cellStyle);
                    cell2.setCellValue((String)m.get(keyList.get(o2)));
                }
                ++o2;
            }
            totalAmt += total;
            ++j;
        }
        for (int i = 0; i < keyList.size(); ++i) {
            final Cell cell3 = dummyRow.createCell(i);
            cell3.setCellStyle(cellStyle);
            final Cell cell = row2.createCell(i);
            cell.setCellStyle(createCellStyle);
            final Cell cell4 = row3.createCell(i);
            cell4.setCellStyle(createCellStyle);
            final Cell cell5 = row4.createCell(i);
            cell5.setCellStyle(createCellStyle);
            final Cell cell6 = row5.createCell(i);
            cell6.setCellStyle(createCellStyle);
            final Cell cell7 = row6.createCell(i);
            cell7.setCellStyle(createCellStyle);
            final Cell cell8 = row7.createCell(i);
            cell8.setCellStyle(createCellStyle);
            final Cell cell9 = row8.createCell(i);
            cell9.setCellStyle(createCellStyle);
            final Cell cell10 = row9.createCell(i);
            cell10.setCellStyle(createCellStyle);
            if (i != 0) {
                if (i == 1) {
                    cell.setCellValue("SESSION");
                    cell4.setCellValue("FROM");
                    cell5.setCellValue("TO");
                    cell6.setCellValue("STREAM");
                    cell7.setCellValue("CLASS");
                    cell8.setCellValue("CATEGORY");
                    cell9.setCellValue("FUND TYPE");
                }
                else if (i == 2) {
                    cell.setCellValue(reports.getSession());
                    cell4.setCellValue(reports.getDateFrom());
                    cell5.setCellValue(reports.getDateTo());
                    cell6.setCellValue((reports.getStream() == null) ? "ALL" : reports.getStream().toUpperCase());
                    cell7.setCellValue((reports.getClassType() == null) ? "ALL" : reports.getClassType().toUpperCase());
                    cell8.setCellValue((reports.getCategory() == null) ? "ALL" : reports.getCategory().toUpperCase());
                    cell9.setCellValue((reports.getFundType() == null) ? "ALL" : reports.getFundType().toUpperCase());
                }
                else if (i != 3) {
                    if (i == 4) {
                        cell.setCellValue("Collection Amount");
                        cell4.setCellValue("Miscellanious");
                        cell5.setCellValue("Total Amount");
                    }
                    else if (i == 5) {
                        cell.setCellValue((double)totalAmt);
                        cell4.setCellValue(0.0);
                        cell5.setCellValue((double)totalAmt);
                    }
                }
            }
        }
    }
    
    public static Map<String, String> ascOrder(final Map<String, String> map) {
        final TreeMap<String, String> mapSorted = new TreeMap<String, String>(map);
        final Map<String, String> finalMap = new HashMap<String, String>();
        mapSorted.forEach((key, value) -> finalMap.put(key, value));
        return finalMap;
    }
    
    private void downloadCollectionAllReport(final Map<String, Object> model, final HSSFWorkbook workbook, final HttpServletRequest request, final HttpServletResponse response) {
        response.setHeader("Content-Disposition", "attachment; filename=\"CollectionDetails.xls\"");
        final Sheet sheet = (Sheet)workbook.createSheet("Collection Details Report");
        sheet.setDefaultColumnWidth(15);
        final CellStyle cellStyle = createBorderedStyle((Workbook)workbook);
        final CollectionBean collectionBean = (CollectionBean)request.getSession().getAttribute("collectionBean");
        final CellStyle createCellStyle = (CellStyle)workbook.createCellStyle();
        final Font font = (Font)workbook.createFont();
        font.setBoldweight((short)700);
        createCellStyle.setFont(font);
        createCellStyle.setBorderRight((short)1);
        createCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        createCellStyle.setBorderBottom((short)1);
        createCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        createCellStyle.setBorderLeft((short)1);
        createCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        createCellStyle.setBorderTop((short)1);
        createCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
        final String config = (String)request.getSession().getAttribute("config");
        createHeadCell(sheet, workbook, createCellStyle, config);
        final Row dummyRow = sheet.createRow(1);
        final Row row2 = sheet.createRow(2);
        final Row row3 = sheet.createRow(3);
        final Row row4 = sheet.createRow(4);
        final Row row5 = sheet.createRow(5);
        final Row row6 = sheet.createRow(6);
        final Row row7 = sheet.createRow(7);
        final Row row8 = sheet.createRow(8);
        final Row row9 = sheet.createRow(9);
        for (int i = 0; i < 6; ++i) {
            final Cell cell0 = dummyRow.createCell(i);
            cell0.setCellStyle(cellStyle);
            final Cell cell2 = row2.createCell(i);
            cell2.setCellStyle(createCellStyle);
            final Cell cell3 = row3.createCell(i);
            cell3.setCellStyle(cellStyle);
            final Cell cell4 = row4.createCell(i);
            cell4.setCellStyle(cellStyle);
            final Cell cell5 = row5.createCell(i);
            cell5.setCellStyle(cellStyle);
            final Cell cell6 = row6.createCell(i);
            cell6.setCellStyle(cellStyle);
            final Cell cell7 = row7.createCell(i);
            cell7.setCellStyle(cellStyle);
            final Cell cell8 = row8.createCell(i);
            cell8.setCellStyle(cellStyle);
            final Cell cell9 = row9.createCell(i);
            cell9.setCellStyle(createCellStyle);
            if (i == 0) {
                cell2.setCellValue("SESSION");
                cell3.setCellValue(collectionBean.getSession());
                cell9.setCellValue("SL NO.");
            }
            else if (i == 1) {
                cell2.setCellValue("FROM");
                cell3.setCellValue(collectionBean.getDateFrom());
                cell4.setCellValue("STREAM");
                cell5.setCellValue("CLASS");
                cell6.setCellValue("CATEGORY");
                cell7.setCellValue("FUND TYPE");
                cell9.setCellValue("DATE");
            }
            else if (i == 2) {
                cell2.setCellValue("TO");
                cell3.setCellValue(collectionBean.getDateTo());
                cell4.setCellValue((collectionBean.getStream() == null) ? "ALL" : collectionBean.getStream().toUpperCase());
                cell5.setCellValue((collectionBean.getYearBatch() == null) ? "ALL" : collectionBean.getYearBatch().toUpperCase());
                cell6.setCellValue((collectionBean.getCtegory() == null) ? "ALL" : collectionBean.getCtegory().toUpperCase());
                cell7.setCellValue((collectionBean.getFundType() == null) ? "ALL" : collectionBean.getFundType().toUpperCase());
                cell9.setCellValue("FUND");
            }
            else if (i == 3) {
                cell9.setCellValue("CAF FEE");
            }
            else if (i == 4) {
                cell9.setCellValue("MISCELLANEOUS");
            }
            else if (i == 5) {
                cell9.setCellValue("TOTAL");
            }
        }
        int i = 10;
        int count = 1;
        long totalAmount = 0L;
        long totalFundAmount = 0L;
        long totalCAFAmount = 0L;
        long totalMiscAmt = 0L;
        long total = 0L;
        for (final CollectionList collectionList : collectionBean.getCollectionList()) {
            final Row row10 = sheet.createRow(i);
            final Cell cell10 = row10.createCell(0);
            cell10.setCellStyle(cellStyle);
            cell10.setCellValue((double)(count++));
            final Cell cell11 = row10.createCell(1);
            cell11.setCellStyle(cellStyle);
            cell11.setCellValue(collectionList.getDate());
            final Cell cell12 = row10.createCell(2);
            cell12.setCellStyle(cellStyle);
            cell12.setCellValue(collectionList.getTotalFund());
            final Cell cell13 = row10.createCell(3);
            cell13.setCellStyle(cellStyle);
            cell13.setCellValue(collectionList.getCafAmount());
            final Cell cell14 = row10.createCell(4);
            cell14.setCellStyle(cellStyle);
            cell14.setCellValue(collectionList.getMiscellaniousAmount());
            totalAmount = Integer.parseInt(collectionList.getTotalFund()) + Integer.parseInt(collectionList.getCafAmount()) + Integer.parseInt(collectionList.getMiscellaniousAmount());
            final Cell cell15 = row10.createCell(5);
            cell15.setCellStyle(cellStyle);
            cell15.setCellValue((double)totalAmount);
            totalFundAmount += Integer.parseInt(collectionList.getTotalFund());
            totalCAFAmount += Integer.parseInt(collectionList.getCafAmount());
            totalMiscAmt += Integer.parseInt(collectionList.getMiscellaniousAmount());
            total += totalAmount;
            ++i;
        }
        final Row row11 = sheet.createRow(i);
        final Cell cell16 = row11.createCell(1);
        cell16.setCellStyle(createCellStyle);
        cell16.setCellValue("Total");
        final Cell cell17 = row11.createCell(2);
        cell17.setCellStyle(createCellStyle);
        cell17.setCellValue((double)totalFundAmount);
        final Cell cell18 = row11.createCell(3);
        cell18.setCellStyle(createCellStyle);
        cell18.setCellValue((double)totalCAFAmount);
        final Cell cell19 = row11.createCell(4);
        cell19.setCellStyle(createCellStyle);
        cell19.setCellValue((double)totalMiscAmt);
        final Cell cell20 = row11.createCell(5);
        cell20.setCellStyle(createCellStyle);
        cell20.setCellValue((double)total);
    }
    
    private void downloadHeadReport(final Map<String, Object> model, final HSSFWorkbook workbook, final HttpServletRequest request, final HttpServletResponse response) {
        response.setHeader("Content-Disposition", "attachment; filename=\"HeadDetails.xls\"");
        final Reports reports = (Reports)request.getSession().getAttribute("reports");
        final Sheet sheet = (Sheet)workbook.createSheet("Head Details Report");
        sheet.setDefaultColumnWidth(15);
        final CellStyle cellStyle = createBorderedStyle((Workbook)workbook);
        final List<HeadDeatils> headDetailsList = (List<HeadDeatils>)request.getSession().getAttribute("headDetailsMap");
        final CellStyle createCellStyle = (CellStyle)workbook.createCellStyle();
        final Font font = (Font)workbook.createFont();
        font.setBoldweight((short)700);
        createCellStyle.setFont(font);
        final String config = (String)request.getSession().getAttribute("config");
        createHeadCell(sheet, workbook, createCellStyle, config);
        final Row dummyRow = sheet.createRow(1);
        final Row row2 = sheet.createRow(2);
        final Row row3 = sheet.createRow(3);
        final Row row4 = sheet.createRow(4);
        final Row row5 = sheet.createRow(5);
        final Row row6 = sheet.createRow(6);
        final Row row7 = sheet.createRow(7);
        final Row row8 = sheet.createRow(8);
        final Row row9 = sheet.createRow(9);
        for (int i = 0; i < 4; ++i) {
            final Cell cell0 = dummyRow.createCell(i);
            cell0.setCellStyle(cellStyle);
            final Cell cell2 = row2.createCell(i);
            cell2.setCellStyle(createCellStyle);
            final Cell cell3 = row3.createCell(i);
            cell3.setCellStyle(cellStyle);
            final Cell cell4 = row4.createCell(i);
            cell4.setCellStyle(cellStyle);
            final Cell cell5 = row5.createCell(i);
            cell5.setCellStyle(cellStyle);
            final Cell cell6 = row6.createCell(i);
            cell6.setCellStyle(cellStyle);
            final Cell cell7 = row7.createCell(i);
            cell7.setCellStyle(cellStyle);
            final Cell cell8 = row8.createCell(i);
            cell8.setCellStyle(cellStyle);
            final Cell cell9 = row9.createCell(i);
            cell9.setCellStyle(createCellStyle);
            if (i == 0) {
                cell2.setCellValue("SESSION");
                cell3.setCellValue(reports.getSession());
                cell9.setCellValue("SL NO.");
            }
            else if (i == 1) {
                cell2.setCellValue("FROM");
                cell3.setCellValue(reports.getDateFrom());
                cell4.setCellValue("STREAM");
                cell5.setCellValue("CLASS");
                cell6.setCellValue("CATEGORY");
                cell7.setCellValue("FUND TYPE");
                cell9.setCellValue("TYPE OF HEAD");
            }
            else if (i == 2) {
                cell2.setCellValue("TO");
                cell3.setCellValue(reports.getDateTo());
                cell4.setCellValue(reports.getStream().toUpperCase());
                cell5.setCellValue(reports.getClassType().toUpperCase());
                cell6.setCellValue(reports.getCategory().toUpperCase());
                cell7.setCellValue(reports.getFundType().toUpperCase());
                cell9.setCellValue("AMOUNT");
            }
            else if (i == 3) {
                cell9.setCellValue("NO OF STUDENT");
            }
        }
        int i = 10;
        int count = 1;
        long totalAmount = 0L;
        long noOfStudent = 0L;
        for (final HeadDeatils head : headDetailsList) {
            final Row row10 = sheet.createRow(i);
            final Cell cell10 = row10.createCell(0);
            cell10.setCellStyle(cellStyle);
            cell10.setCellValue((double)(count++));
            final Cell cell11 = row10.createCell(1);
            cell11.setCellStyle(cellStyle);
            cell11.setCellValue(head.getHeadname());
            final Cell cell12 = row10.createCell(2);
            cell12.setCellStyle(cellStyle);
            cell12.setCellValue((double)head.getHeadAmount());
            final Cell cell13 = row10.createCell(3);
            cell13.setCellStyle(cellStyle);
            cell13.setCellValue((double)head.getStudentCount().intValue());
            totalAmount += head.getHeadAmount();
            noOfStudent += head.getStudentCount().intValue();
            ++i;
        }
        final Row row11 = sheet.createRow(i);
        final Cell cell14 = row11.createCell(1);
        cell14.setCellStyle(createCellStyle);
        cell14.setCellValue("Total Count");
        final Cell cell15 = row11.createCell(2);
        cell15.setCellStyle(createCellStyle);
        cell15.setCellValue((double)totalAmount);
    }
    
    private void downloadStudentReport(final Map<String, Object> model, final Workbook workbook, final HttpServletRequest request, final HttpServletResponse response) {
        response.setHeader("Content-Disposition", "attachment; filename=\"StudentList.xls\"");
        final Reports reports = (Reports)request.getSession().getAttribute("reports");
        final Sheet sheet = workbook.createSheet("Student Report");
        sheet.setDefaultColumnWidth(15);
        final CellStyle cellStyle = createBorderedStyle(workbook);
        final List<Student> listStudent = (List<Student>)request.getSession().getAttribute("studentListreport");
        final CellStyle createCellStyle = workbook.createCellStyle();
        final Font font = workbook.createFont();
        font.setBoldweight((short)700);
        createCellStyle.setFont(font);
        final String config = (String)request.getSession().getAttribute("config");
        createHeadCell(sheet, (HSSFWorkbook)workbook, createCellStyle, config);
        final Row dummyRow = sheet.createRow(1);
        final Row row2 = sheet.createRow(2);
        final Row row3 = sheet.createRow(3);
        final Row row4 = sheet.createRow(4);
        final Row row5 = sheet.createRow(5);
        final Row row6 = sheet.createRow(6);
        final Row row7 = sheet.createRow(7);
        final Row row8 = sheet.createRow(8);
        final Row row9 = sheet.createRow(9);
        final Row row10 = sheet.createRow(10);
        for (int i = 0; i < 11; ++i) {
            final Cell cell0 = dummyRow.createCell(i);
            cell0.setCellStyle(cellStyle);
            final Cell cell2 = row2.createCell(i);
            cell2.setCellStyle(cellStyle);
            final Cell cell3 = row3.createCell(i);
            cell3.setCellStyle(cellStyle);
            final Cell cell4 = row4.createCell(i);
            cell4.setCellStyle(cellStyle);
            final Cell cell5 = row5.createCell(i);
            cell5.setCellStyle(cellStyle);
            final Cell cell6 = row6.createCell(i);
            cell6.setCellStyle(cellStyle);
            final Cell cell7 = row7.createCell(i);
            cell7.setCellStyle(cellStyle);
            final Cell cell8 = row8.createCell(i);
            cell8.setCellStyle(cellStyle);
            final Cell cell9 = row9.createCell(i);
            cell9.setCellStyle(cellStyle);
            final Cell cell10 = row10.createCell(i);
            cell10.setCellStyle(createCellStyle);
            if (i == 0) {
                cell10.setCellValue("SL NO.");
            }
            else if (i == 1) {
                cell2.setCellValue("SESSION");
                cell3.setCellValue(reports.getSession());
                cell4.setCellValue("STREAM");
                cell5.setCellValue("CATEGORY");
                cell6.setCellValue("CLASS");
                cell7.setCellValue("FUND TYPE");
                cell10.setCellValue("SESSION");
            }
            else if (i == 2) {
                cell2.setCellValue("FROM");
                cell3.setCellValue(reports.getDateFrom());
                cell4.setCellValue(reports.getStream().toUpperCase());
                cell5.setCellValue(reports.getCategory().toUpperCase());
                cell6.setCellValue(reports.getClassType().toUpperCase());
                cell7.setCellValue(reports.getFundType().toUpperCase());
                cell10.setCellValue("DOA");
            }
            else if (i == 3) {
                cell2.setCellValue("TO");
                cell3.setCellValue(reports.getDateTo());
                cell10.setCellValue("RECEIPT No");
            }
            else if (i == 4) {
                cell10.setCellValue("STUDENT NAME");
            }
            else if (i == 5) {
                cell10.setCellValue("ROLL NO");
            }
            else if (i == 6) {
                cell10.setCellValue("DOB");
            }
            else if (i == 7) {
                cell10.setCellValue("CATEGORY");
            }
            else if (i == 8) {
                cell10.setCellValue("AMOUNT");
            }
            else if (i == 9) {
                cell10.setCellValue("FUND TYPE");
            }
            else if (i == 10) {
                cell10.setCellValue("STATUS");
            }
        }
        int i = 11;
        int count = 1;
        long totalAmount = 0L;
        final long noOfStudent = 0L;
        for (final Student student : listStudent) {
            final Row row11 = sheet.createRow(i);
            final Cell cell11 = row11.createCell(0);
            cell11.setCellStyle(cellStyle);
            cell11.setCellValue((double)(count++));
            final Cell cell12 = row11.createCell(1);
            cell12.setCellStyle(cellStyle);
            cell12.setCellValue(student.getSession());
            final Cell cell13 = row11.createCell(2);
            cell13.setCellStyle(cellStyle);
            cell13.setCellValue(student.getCreated_Date());
            final Cell cell14 = row11.createCell(3);
            cell14.setCellStyle(cellStyle);
            cell14.setCellValue(student.getReceiptNo());
            final Cell cell15 = row11.createCell(4);
            cell15.setCellStyle(cellStyle);
            cell15.setCellValue(student.getName());
            final Cell cell16 = row11.createCell(5);
            cell16.setCellStyle(cellStyle);
            cell16.setCellValue(student.getRollNo());
            final Cell cell17 = row11.createCell(6);
            cell17.setCellStyle(cellStyle);
            cell17.setCellValue(student.getDob());
            final Cell cell18 = row11.createCell(7);
            cell18.setCellStyle(cellStyle);
            cell18.setCellValue(student.getCategory());
            final Cell cell19 = row11.createCell(8);
            cell19.setCellStyle(cellStyle);
            cell19.setCellValue((double)student.getTotalAmount());
            final Cell cell20 = row11.createCell(9);
            cell20.setCellStyle(cellStyle);
            cell20.setCellValue(student.getFundType());
            final Cell cell21 = row11.createCell(10);
            cell21.setCellStyle(cellStyle);
            cell21.setCellValue(student.getStatus());
            totalAmount += student.getTotalAmount();
            ++i;
        }
        final Row row12 = sheet.createRow(i);
        final Cell cell22 = row12.createCell(7);
        cell22.setCellStyle(createCellStyle);
        cell22.setCellValue("Total Amount");
        final Cell cell23 = row12.createCell(8);
        cell23.setCellStyle(createCellStyle);
        cell23.setCellValue((double)totalAmount);
    }
    
    private void downloadAllReport(final Map<String, Object> model, final Workbook workbook, final HttpServletRequest request, final HttpServletResponse response) {
        response.setHeader("Content-Disposition", "attachment; filename=\"AllReport.xls\"");
        final Reports reports = (Reports)request.getSession().getAttribute("reports");
        final Sheet sheet = workbook.createSheet("All Report");
        sheet.setDefaultColumnWidth(30);
        final CellStyle style = createBorderedStyle(workbook);
        final CellStyle style2 = createThickBorderedStyle(workbook);
        final CellStyle createCellStyle = workbook.createCellStyle();
        final Font font = workbook.createFont();
        font.setBoldweight((short)700);
        createCellStyle.setFont(font);
        final Map<String, Object> allAdmissionreport = (Map<String, Object>)request.getSession().getAttribute("admissionData");
        final Map<String, Object> allreAdmissionreport = (Map<String, Object>)request.getSession().getAttribute("readmissionData");
        final Map<String, Object> allCafreport = (Map<String, Object>)request.getSession().getAttribute("allcafData");
        final Map<String, Object> allMisreport = (Map<String, Object>)request.getSession().getAttribute("misData");
        final Map<String, Object> allAdditionalreport = (Map<String, Object>)request.getSession().getAttribute("allAdditionalFeeReport");
        final Map<String, Object> allFinereport = (Map<String, Object>)request.getSession().getAttribute("allFinereport");
        final Map<String, Object> allRefreport = (Map<String, Object>)request.getSession().getAttribute("allRefundreport");
        final Map<String, Object> alloldStudentAddreport = (Map<String, Object>)request.getSession().getAttribute("allAdditionalFeeReportForOldStudent");
        final String config = (String)request.getSession().getAttribute("config");
        createHeadCell(sheet, (HSSFWorkbook)workbook, createCellStyle, config);
        final Row dummyRow = sheet.createRow(1);
        final Row header = sheet.createRow(2);
        final Cell cell0 = header.createCell(0);
        cell0.setCellStyle(style2);
        cell0.setCellValue("Report Type");
        final Cell cell2 = header.createCell(1);
        cell2.setCellStyle(style);
        cell2.setCellValue("ALL");
        final Row header2 = sheet.createRow(3);
        final Cell cell3 = header2.createCell(0);
        cell3.setCellStyle(style2);
        cell3.setCellValue("Session");
        final Cell cell4 = header2.createCell(1);
        cell4.setCellStyle(style);
        cell4.setCellValue(reports.getSession());
        final Row header3 = sheet.createRow(4);
        final Cell cell5 = header3.createCell(0);
        cell5.setCellStyle(style2);
        cell5.setCellValue("From");
        final Cell cell6 = header3.createCell(1);
        cell6.setCellStyle(style);
        cell6.setCellValue(reports.getDateFrom());
        final Row header4 = sheet.createRow(5);
        final Cell cell7 = header4.createCell(0);
        cell7.setCellStyle(style2);
        cell7.setCellValue("To");
        final Cell cell8 = header4.createCell(1);
        cell8.setCellStyle(style);
        cell8.setCellValue(reports.getDateTo());
        final Row dummyRow2 = sheet.createRow(6);
        final Row header5 = sheet.createRow(7);
        final Cell cell9 = header5.createCell(0);
        cell9.setCellStyle(style2);
        cell9.setCellValue("Sl No.");
        final Cell cell10 = header5.createCell(1);
        cell10.setCellStyle(style2);
        cell10.setCellValue("Type Of Fund");
        final Cell cell11 = header5.createCell(2);
        cell11.setCellStyle(style2);
        cell11.setCellValue("No Of Student");
        final Cell cell12 = header5.createCell(3);
        cell12.setCellStyle(style2);
        cell12.setCellValue("Amount");
        Long totalNoOfStudent = 0L;
        Long totalAmount = 0L;
        final Row header6 = sheet.createRow(8);
        final Cell cell13 = header6.createCell(0);
        cell13.setCellStyle(style);
        cell13.setCellValue("1");
        final Cell cell14 = header6.createCell(1);
        cell14.setCellStyle(style);
        cell14.setCellValue("Admission");
        final BigDecimal studentCount = (BigDecimal) allAdmissionreport.get("SCOUNT");
        if (studentCount == null) {
            totalNoOfStudent += 0L;
        }
        else {
            totalNoOfStudent += studentCount.longValue();
        }
        final Cell cell15 = header6.createCell(2);
        cell15.setCellStyle(style);
        cell15.setCellValue((studentCount == null) ? "0" : studentCount.toString());
        final BigDecimal transactionCount = (BigDecimal) allAdmissionreport.get("TCOUNT");
        if (transactionCount == null) {
            totalAmount += 0L;
        }
        else {
            totalAmount += transactionCount.longValue();
        }
        final Cell cell16 = header6.createCell(3);
        cell16.setCellStyle(style);
        cell16.setCellValue((transactionCount == null) ? "0" : transactionCount.toString());
        final Row header7 = sheet.createRow(9);
        final Cell cell17 = header7.createCell(0);
        cell17.setCellStyle(style);
        cell17.setCellValue("2");
        final Cell cell18 = header7.createCell(1);
        cell18.setCellStyle(style);
        cell18.setCellValue("Re-Admission");
        final BigDecimal studentCountR = (BigDecimal) allreAdmissionreport.get("SCOUNT");
        if (studentCountR == null) {
            totalNoOfStudent += 0L;
        }
        else {
            totalNoOfStudent += studentCountR.longValue();
        }
        final Cell cell19 = header7.createCell(2);
        cell19.setCellStyle(style);
        cell19.setCellValue((studentCountR == null) ? "0" : studentCountR.toString());
        final BigDecimal transactionCountR = (BigDecimal) allreAdmissionreport.get("TCOUNT");
        if (transactionCountR == null) {
            totalAmount += 0L;
        }
        else {
            totalAmount += transactionCountR.longValue();
        }
        final Cell cell20 = header7.createCell(3);
        cell20.setCellStyle(style);
        cell20.setCellValue((transactionCountR == null) ? "0" : transactionCountR.toString());
        final Row header8 = sheet.createRow(10);
        final Cell cell21 = header8.createCell(0);
        cell21.setCellStyle(style);
        cell21.setCellValue("3");
        final Cell cell22 = header8.createCell(1);
        cell22.setCellStyle(style);
        cell22.setCellValue("CAF");
        final BigDecimal studentCountC = (BigDecimal) allCafreport.get("SCOUNT");
        if (studentCountC == null) {
            totalNoOfStudent += 0L;
        }
        else {
            totalNoOfStudent += studentCountC.longValue();
        }
        final Cell cell23 = header8.createCell(2);
        cell23.setCellStyle(style);
        cell23.setCellValue((studentCountC == null) ? "0" : studentCountC.toString());
        final BigDecimal transactionCountC = (BigDecimal) allCafreport.get("TCOUNT");
        if (transactionCountC == null) {
            totalAmount += 0L;
        }
        else {
            totalAmount += transactionCountC.longValue();
        }
        final Cell cell24 = header8.createCell(3);
        cell24.setCellStyle(style);
        cell24.setCellValue((transactionCountC == null) ? "0" : transactionCountC.toString());
        final Row header9 = sheet.createRow(11);
        final Cell cell25 = header9.createCell(0);
        cell25.setCellStyle(style);
        cell25.setCellValue("4");
        final Cell cell26 = header9.createCell(1);
        cell26.setCellStyle(style);
        cell26.setCellValue("Additional Fee");
        final BigDecimal studentCountAdd = (BigDecimal) allAdditionalreport.get("SCOUNT");
        if (studentCountAdd == null) {
            totalNoOfStudent += 0L;
        }
        else {
            totalNoOfStudent += studentCountAdd.longValue();
        }
        final Cell cell27 = header9.createCell(2);
        cell27.setCellStyle(style);
        cell27.setCellValue((studentCountAdd == null) ? "0" : studentCountAdd.toString());
        final BigDecimal transactionCountAdd = (BigDecimal) allAdditionalreport.get("TCOUNT");
        if (transactionCountAdd == null) {
            totalAmount += 0L;
        }
        else {
            totalAmount += transactionCountAdd.longValue();
        }
        final Cell cell28 = header9.createCell(3);
        cell28.setCellStyle(style);
        cell28.setCellValue((transactionCountAdd == null) ? "0" : transactionCountAdd.toString());
        final Row header10 = sheet.createRow(12);
        final Cell cell29 = header10.createCell(0);
        cell29.setCellStyle(style);
        cell29.setCellValue("5");
        final Cell cell30 = header10.createCell(1);
        cell30.setCellStyle(style);
        cell30.setCellValue("Fine");
        final BigDecimal studentCountF = (BigDecimal) allFinereport.get("SCOUNT");
        if (studentCountF == null) {
            totalNoOfStudent += 0L;
        }
        else {
            totalNoOfStudent += studentCountF.longValue();
        }
        final Cell cell31 = header10.createCell(2);
        cell31.setCellStyle(style);
        cell31.setCellValue((studentCountF == null) ? "0" : studentCountF.toString());
        final BigDecimal transactionCountF = (BigDecimal) allFinereport.get("TCOUNT");
        if (transactionCountF == null) {
            totalAmount += 0L;
        }
        else {
            totalAmount += transactionCountF.longValue();
        }
        final Cell cell32 = header10.createCell(3);
        cell32.setCellStyle(style);
        cell32.setCellValue((transactionCountF == null) ? "0" : transactionCountF.toString());
        final Row header11 = sheet.createRow(13);
        final Cell cell33 = header11.createCell(0);
        cell33.setCellStyle(style);
        cell33.setCellValue("5");
        final Cell cell34 = header11.createCell(1);
        cell34.setCellStyle(style);
        cell34.setCellValue("Old Student");
        final BigDecimal studentCountOldForAdd = (BigDecimal) alloldStudentAddreport.get("SCOUNT");
        if (studentCountOldForAdd == null) {
            totalNoOfStudent += 0L;
        }
        else {
            totalNoOfStudent += studentCountOldForAdd.longValue();
        }
        final Cell cell35 = header11.createCell(2);
        cell35.setCellStyle(style);
        cell35.setCellValue((studentCountOldForAdd == null) ? "0" : studentCountOldForAdd.toString());
        final BigDecimal transactionCountForOldStudent = (BigDecimal) alloldStudentAddreport.get("TCOUNT");
        if (transactionCountForOldStudent == null) {
            totalAmount += 0L;
        }
        else {
            totalAmount += transactionCountForOldStudent.longValue();
        }
        final Cell cell36 = header11.createCell(3);
        cell36.setCellStyle(style);
        cell36.setCellValue((transactionCountForOldStudent == null) ? "0" : transactionCountForOldStudent.toString());
        final Row header12 = sheet.createRow(14);
        final Cell cell37 = header12.createCell(0);
        cell37.setCellStyle(style);
        cell37.setCellValue("");
        final Cell cell38 = header12.createCell(1);
        cell38.setCellStyle(style);
        cell38.setCellValue("Total");
        final Cell cell39 = header12.createCell(2);
        cell39.setCellStyle(style);
        cell39.setCellValue(totalNoOfStudent.toString());
        final Cell cell40 = header12.createCell(3);
        cell40.setCellStyle(style);
        cell40.setCellValue(totalAmount.toString());
        final Row dummyRow3 = sheet.createRow(15);
        final Row header13 = sheet.createRow(16);
        final Cell cell41 = header13.createCell(0);
        cell41.setCellStyle(style2);
        cell41.setCellValue("Sl No.");
        final Cell cell42 = header13.createCell(1);
        cell42.setCellStyle(style2);
        cell42.setCellValue("Type Of Fund");
        final Cell cell43 = header13.createCell(2);
        cell43.setCellStyle(style2);
        cell43.setCellValue("No Of Particular");
        final Cell cell44 = header13.createCell(3);
        cell44.setCellStyle(style2);
        cell44.setCellValue("Amount");
        Long misTotalAMount = 0L;
        final Row header14 = sheet.createRow(17);
        final Cell cell45 = header14.createCell(0);
        cell45.setCellStyle(style);
        cell45.setCellValue("1");
        final Cell cell46 = header14.createCell(1);
        cell46.setCellStyle(style);
        cell46.setCellValue("Miscellaneous");
        final BigDecimal noOfParticluar = (BigDecimal) allMisreport.get("SCOUNT");
        final Cell celll1 = header14.createCell(2);
        celll1.setCellStyle(style);
        celll1.setCellValue((noOfParticluar == null) ? "0" : noOfParticluar.toString());
        final BigDecimal noOfParticluarAmount = (BigDecimal) allMisreport.get("TCOUNT");
        if (noOfParticluarAmount == null) {
            misTotalAMount = totalAmount + misTotalAMount + 0L;
        }
        else {
            misTotalAMount = totalAmount + misTotalAMount + noOfParticluarAmount.longValue();
        }
        final Cell cell47 = header14.createCell(3);
        cell47.setCellStyle(style);
        cell47.setCellValue((noOfParticluarAmount == null) ? "0" : noOfParticluarAmount.toString());
        final Row header15 = sheet.createRow(18);
        final Cell cell48 = header15.createCell(0);
        cell48.setCellStyle(style);
        cell48.setCellValue("");
        final Cell cell49 = header15.createCell(1);
        cell49.setCellStyle(style);
        cell49.setCellValue("GrandTotal");
        final Cell cell50 = header15.createCell(2);
        cell50.setCellStyle(style);
        cell50.setCellValue("");
        final Cell cell51 = header15.createCell(3);
        cell51.setCellStyle(style);
        cell51.setCellValue(misTotalAMount.toString());
        final Row header16 = sheet.createRow(19);
        final Row header17 = sheet.createRow(20);
        final Cell cell52 = header17.createCell(0);
        cell52.setCellStyle(style2);
        cell52.setCellValue("Sl No.");
        final Cell cell53 = header17.createCell(1);
        cell53.setCellStyle(style2);
        cell53.setCellValue("Type Of Fund");
        final Cell cell54 = header17.createCell(2);
        cell54.setCellStyle(style2);
        cell54.setCellValue("No Of Particular");
        final Cell cell55 = header17.createCell(3);
        cell55.setCellStyle(style2);
        cell55.setCellValue("Amount");
        Long refundTotalStudent = 0L;
        Long refundTotalAmount = 0L;
        final Row header18 = sheet.createRow(21);
        final Cell cell56 = header18.createCell(0);
        cell56.setCellStyle(style);
        cell56.setCellValue("1");
        final Cell cell57 = header18.createCell(1);
        cell57.setCellStyle(style);
        cell57.setCellValue("Refund");
        final BigDecimal noOfStudentRefund = (BigDecimal) allRefreport.get("SCOUNT");
        if (noOfStudentRefund == null) {
            refundTotalStudent += 0L;
        }
        else {
            refundTotalStudent += noOfStudentRefund.longValue();
        }
        final Cell cell58 = header18.createCell(2);
        cell58.setCellStyle(style);
        cell58.setCellValue((noOfStudentRefund == null) ? "0" : noOfStudentRefund.toString());
        final BigDecimal noRefundAmount = (BigDecimal) allRefreport.get("TCOUNT");
        if (noRefundAmount == null) {
            refundTotalAmount += 0L;
        }
        else {
            refundTotalAmount += noRefundAmount.longValue();
        }
        final Cell cell59 = header18.createCell(3);
        cell59.setCellStyle(style);
        cell59.setCellValue((noRefundAmount == null) ? "0" : noRefundAmount.toString());
        final Row header19 = sheet.createRow(22);
        final Cell cell60 = header19.createCell(0);
        cell60.setCellStyle(style);
        cell60.setCellValue("");
        final Cell cell61 = header19.createCell(1);
        cell61.setCellStyle(style);
        cell61.setCellValue("GRAND Total");
        final Cell cell62 = header19.createCell(2);
        cell62.setCellStyle(style);
        cell62.setCellValue(refundTotalStudent.toString());
        final Cell cell63 = header19.createCell(3);
        cell63.setCellStyle(style);
        cell63.setCellValue(refundTotalAmount.toString());
    }
    
    private static CellStyle createBorderedStyle(final Workbook wb) {
        final CellStyle style = wb.createCellStyle();
        style.setBorderRight((short)1);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom((short)1);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft((short)1);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop((short)1);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }
    
    private static CellStyle createThickBorderedStyle(final Workbook wb) {
        final CellStyle style = wb.createCellStyle();
        style.setBorderRight((short)2);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom((short)2);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft((short)2);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop((short)2);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        final Font font = wb.createFont();
        font.setBoldweight((short)700);
        style.setFont(font);
        return style;
    }
    
    private static void createHeadCell(final Sheet sheet, final HSSFWorkbook workbook, final CellStyle createCellStyle, final String config) {
        final Row headerForCollege = sheet.createRow(0);
        final Cell headCell = headerForCollege.createCell(0);
        headCell.setCellStyle(createCellStyle);
        if (config.equalsIgnoreCase("+2")) {
            headCell.setCellValue("KSHETRAMOHAN SCIENCE JUNIOR COLLEGE");
        }
        else {
            headCell.setCellValue("KSHETRAMOHAN SCIENCE DEGREE COLLEGE");
        }
    }
}
