package com.daily.collection.web.tresaury.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daily.collection.web.tresaury.bean.CollectionBean;
import com.daily.collection.web.tresaury.bean.CollectionList;
import com.daily.collection.web.tresaury.dao.ITreasuryDao;


@Service
public class TreasuryServiceImpl implements ITreasuryService {

	
	@Autowired
	ITreasuryDao treasuryDao;
	
	
	@Override
	public CollectionBean getCollectionList(CollectionBean collectionBean,HttpServletRequest request) {
		try{
			String config=(String) request.getSession().getAttribute("config");
			Object[] reportAdmParams={config,"2017-18","AD","CLC",config,"2017-18",collectionBean.getDateFrom(),collectionBean.getDateTo(),collectionBean.getDateFrom(),collectionBean.getDateTo()};
			List<CollectionList> collectionReport=null;
			List<CollectionList> collectionList=null;
			if(config.equalsIgnoreCase("UG")){
			collectionReport = treasuryDao.getCollectionReportUG(reportAdmParams);
			}
			else{
			collectionReport = treasuryDao.getCollectionReport(reportAdmParams);
			}
			
			collectionList=treasuryDao.getCollectionReportForOldStudent(reportAdmParams);
			collectionReport.addAll(collectionList);
			
			
			
			Collections.sort(collectionReport, new Comparator<CollectionList>() {
				public int compare(CollectionList o1, CollectionList o2) {
					 if (o1.getDate() == null || o2.getDate() == null)
					        return 0;
					      return o1.getDate().compareTo(o2.getDate());
				 }
				});
			collectionReport.removeIf(p  -> p.getCafAmount().equals("0") && p.getMiscellaniousAmount().equals("0") && p.getNoOfStudent().equals("0") && p.getTotalFund().equals("0"));
			collectionBean.setCollectionList(collectionReport);
			
			return collectionBean;	
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

}
