package com.daily.collection.web.transaction.service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.daily.collection.web.analytic.bean.HeadDeatils;
import com.daily.collection.web.creation.bean.Student;
import com.daily.collection.web.transaction.bean.AdditionalTransaction;
import com.daily.collection.web.transaction.bean.Transaction;

public interface ITransactionService {
	
	public String saveStudent(Student student,String userName,String from) throws Exception;
	
	public String updateStudent(Student student,String userName) throws Exception;
	
	public List<Student> getStudentDetails(String courseName,String type) throws ParseException;
	
	public Long getFinalAmount(String courseName,String streamName,String category,String yearbatch);

	public String savetransaction(Transaction transaction,String userName,Student student,String currentSession,String dashBoardConfig) throws Exception;
	
	public String updateverifyStatus(String verifyStatus,String rollNo,String userName,String scienceType) throws Exception;
	
	public String checkMrnExist(String collgeName,String mrinNo,String currentSession);
	
	public long getHeadDetailsForprintUG(String rollNo,String transactionType,HttpServletRequest req,String yearBatch,String scienceType);
	
	public String getAvailableAmount(String id);
	
	public String updateAvailableAmount(Integer availableAmount,String totalAMount,String configId);
	
	public String getRecepitNumber(String tranType,String rollNo,String scienceType);
	
	public String getRecepitNumberRF(String tranType,String rollNo,String scienceType);
	
	public List<Map<String, Object>> getRecepitNumberCLC(String tranType,String rollNo,String scienceType);

	public String getRecepitNumberAD(String tranType,String rollNo,String scienceType);
	
	public long getHeadDetailsForprint(String rollNo,String transactionType,HttpServletRequest req,String scienceType);
	
	public long getTotalAmount(String rollNo,String scienceType,String transactionType);
	
	//public long getTotalAmountCLC(String rollNo,String scienceType,String transactionType);
	
	public long getTotalAmountAD(String rollNo,String scienceType,String transactionType);
	
	public String getTransactionDate(String tranType, String rollNo,String scienceType);
	
	public String getTransactionDateRF(String tranType, String rollNo,String scienceType);
	
	public String getTransactionDateCLC(String tranType, String rollNo,String scienceType);
	
	public String getTransactionDateAD(String tranType, String rollNo,String scienceType);
	
	public List<HeadDeatils> getHeadNameAndAmountUG(Student student,String currentSession);
	
	public List<HeadDeatils> getHeadNameAndAmount(Student student,String currentSession);
	
	public Long getFinalAmountUG(String courseName, String streamName, String category, String yearbatch) ;
	
	/*public Long getFinalAmount(String courseName, String streamName, String category, String yearbatch) ;*/
	
	//public Long getStudentAmountUG(String rollNo, String scienceType) ;
	
	//public Long getStudentAmount(String rollNo, String scienceType) ;
	
	//public String getCLCAmountUG(String rollNo, String scienceType) ;
	
	//public String getCLCAmount(String rollNo, String scienceType) ;
	
	public String updateverifyStatusAD(String verifyStatus, String rollNo,String userName,String scienceType) throws Exception;
	
	public String updateverifyStatusRF(String verifyStatus, String rollNo,String userName,String scienceType) throws Exception;
	
	public String updateTransacton(Transaction transaction,Student student,String userName,String currentSession) throws Exception;
	
	public String getBankName(String rollNo,String scienceType);
	
	public String getHeadName(String rollNo,String scienceType);
	
	public String getChequeDDNo(String rollNo,String scienceType);
	
	public String getBookIdAD(String tranType, String rollNo, String scienceType) ;
	
	public List<AdditionalTransaction> getPrintDetails(String rollNO,String tranType);
}
