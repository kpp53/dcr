package com.daily.collection.web.analytic.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.daily.collection.web.analytic.bean.HeadDeatils;

public interface IAnalyticService {
	
	public void getHeadAmount(HttpServletRequest req,String session,List<String> applicationConfiguration) throws Exception;
	
	public void getHeadDropDown(HttpServletRequest req);
	
	public void getHeadAmountUG(HttpServletRequest req,String session,List<String> applicationConfiguration) throws Exception;
	
	public List<HeadDeatils>  addHead(HeadDeatils head,String userName,List<String> applicationConifsuration) throws Exception;
	
	public List<HeadDeatils>  addHeadUG(HeadDeatils head,String userName,List<String> applicationConifsuration) throws Exception;
	
	public String editHeadByCategory(String headName,int headAmount,String userName,String headnm,String yearSession) throws Exception;
	
	public String editHeadByCategoryUG(String headName,int headAmount, String userName,String headnm,String yearSession) throws Exception;
	
	public String deleteHeadByCategoryUG(HeadDeatils head, String userName,String headnm,String yearSession) throws Exception; 
	
	public String deleteHeadByCategory(HeadDeatils head,String userName,String headnm,String yearSession) throws Exception;
	
	
	
	

}
