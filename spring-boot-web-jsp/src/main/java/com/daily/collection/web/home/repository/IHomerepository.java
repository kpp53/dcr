package com.daily.collection.web.home.repository;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.daily.collection.web.home.bean.CollectionSummary;

public interface IHomerepository {
	
	public static final StringBuilder GETCOLLECTIONSUMMARY=new StringBuilder("select STREAM_NAME AS STREAM_NAME , SUM(STUDENTCOUNT) AS STUDENTCOUNT, SUM(TOTALAMOUNT) AS TOTALAMOUNT from(") 
.append("SELECT HC.STREAM_NAME AS STREAM_NAME,COUNT(T.ID) AS STUDENTCOUNT,SUM(T.TOTAL_AMOUNT) AS TOTALAMOUNT FROM TRANSACTION T, HEAD_CATEGORIES HC ") 
.append("WHERE T.HEAD_CATEGORY_ID=HC.HEAD_CATEGORIES_ID AND ") 
.append("HC.COURSE_NAME=? AND TRANSACTION_TYPE IN ('A','R') GROUP BY HC.STREAM_NAME ")
.append("union all ")
.append("SELECT HC.STREAM_NAME AS STREAM_NAME,0,SUM(T.HEAD_AMOUNT) AS TOTALAMOUNT FROM ADDITIONAL_FEE_TRACK T, HEAD_CATEGORIES HC ")
.append("WHERE T.HEAD_CATEGORIES_ID=HC.HEAD_CATEGORIES_ID AND " )  
.append("HC.COURSE_NAME=? AND TRANSACTION_TYPE IN ('AD','OAD') GROUP BY HC.STREAM_NAME ) GROUP BY STREAM_NAME ");
	
	public static final StringBuilder GETADMISSIONCOUNT=new StringBuilder("SELECT COUNT(*) ADMISSIONCOUNT FROM TRANSACTION T, HEAD_CATEGORIES HC WHERE T.HEAD_CATEGORY_ID=HC.HEAD_CATEGORIES_ID AND TRANSACTION_TYPE='A' AND HC.COURSE_NAME=?");
	
	public static final StringBuilder GETREADMISSIONCOUNT=new StringBuilder("SELECT COUNT(*) ADMISSIONCOUNT FROM TRANSACTION T, HEAD_CATEGORIES HC WHERE T.HEAD_CATEGORY_ID=HC.HEAD_CATEGORIES_ID AND TRANSACTION_TYPE='R' AND HC.COURSE_NAME=?");
	
	public static final StringBuilder GETCANCELLEDADMISSIONCOUNT=new StringBuilder("SELECT COUNT(*) CANCELLEDCOUNT FROM REFUND_TRACK RF,HEAD_CATEGORIES HC WHERE RF.HEAD_CATEGORIES_ID=HC.HEAD_CATEGORIES_ID  AND HC.COURSE_NAME=?");

	public String getNewAdmissionCount(String configId) throws ParseException;
	
	public String getReAdmissionCount(String configId) throws ParseException;
	
	public String getCancelledAdmissionCount(String configId) throws ParseException;
	
	public List<CollectionSummary> getCollectionDetails(Date from,Date To,String configId) throws ParseException;
	
}
