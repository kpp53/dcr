package com.daily.collection.web.adminstration.service;

import com.daily.collection.web.adminstration.bean.Configuration;

public interface IAdminstratorService {

	public String saveConfiguration(Configuration configuration,String userName) throws Exception;
	
	
}
