package com.daily.collection.web.transaction.bean;

import java.util.Date;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class AdditionalTransaction {

	private String TRAN_ID;
	
	private Date TRAN_DATE;
	
	private String TRAN_BY;
	
	private String RECEIPT_NUMBER;
	
	private String BOOK_NO;
	
	private String TRANSACTION_MODE;
	
	private String ACADEMIC_SESSION;
	
	private String HEAD_CATEGORIES_ID;
	
	private String SCIENCE_OPTION;
	
	private String TRANSACTION_TYPE;
	
	private String HEAD_NAME;
	
	private String HEAD_AMOUNT;
	
	private String ROLL_NO;
	
	
}
