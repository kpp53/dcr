package com.daily.collection.web.reports.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.daily.collection.web.transaction.repository.ITransactionDao;

@Repository
public class ReportsDao implements IReportsDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	@Autowired
	NamedParameterJdbcTemplate template;
	public void setTemplate(NamedParameterJdbcTemplate template) {
		this.template = template;
	}


	@Override
	public Map<String, Object> getAllreport(Object[] reportParams) {
		try{
			List<Map<String, Object>> allReportlist = jdbcTemplate.queryForList(IReportsDao.GET_ALL_REPORT, reportParams);
			return allReportlist.get(0);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Map<String, Object> getAllAddreport(Object[] reportParams) {
		try{
			List<Map<String, Object>> allReportlist = jdbcTemplate.queryForList(IReportsDao.GET_ALL_ADD_REPORT, reportParams);
			return allReportlist.get(0);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	@Override
	public List<Map<String, Object>> getHeadDeTails(MapSqlParameterSource parameters) {
		try{
			List<Map<String, Object>> queryForList = template.queryForList(IReportsDao.GET_HEAD_DETAILS, parameters);
			return queryForList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		/*try{
			Map<String, String> i=jdbcTemplate.queryForLi(ITransactionDao.GET_HEAD_DETAILS, parameters, new HeadRoWMapper());
			return i;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}*/
	}
	
	
	public final class HeadRoWMapper implements RowMapper<Map<String,String>>{

		@Override
		public Map<String, String> mapRow(ResultSet rs, int arg1) throws SQLException {
			Map<String, String> map=new LinkedHashMap<>();
			map.put("Tution Fees", rs.getString(1));
			return map;
		}
	}


	@Override
	public List<Map<String, Object>> getStudentCount(MapSqlParameterSource parameters) {
		try{
			List<Map<String, Object>> queryForList = template.queryForList(IReportsDao.GET_TUDENT_COUNT, parameters);
			return queryForList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<Map<String, Object>>  getstudentDetails(MapSqlParameterSource parameters) {
		try{
			List<Map<String, Object>> queryForList = template.queryForList(IReportsDao.GET_STUDENT_DETAILS, parameters);
			return queryForList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<Map<String, Object>> getHeadDeTailsUG(MapSqlParameterSource parameters) {
		try{
			List<Map<String, Object>> queryForList = template.queryForList(IReportsDao.GET_HEAD_DETAILS_UG, parameters);
			return queryForList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<Map<String, Object>> getStudentCountUG(MapSqlParameterSource parameters) {
		try{
			List<Map<String, Object>> queryForList = template.queryForList(IReportsDao.GET_TUDENT_COUNT_UG, parameters);
			return queryForList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public Map<String, Object> getAllRefundReport(Object[] reportParams) {
		try{
			List<Map<String, Object>> allReportlist = jdbcTemplate.queryForList(IReportsDao.GET_ALL_REFUND_REPORT, reportParams);
			return allReportlist.get(0);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<Map<String, Object>> getstudentDetailsRF(MapSqlParameterSource parameters) {
		try{
			List<Map<String, Object>> queryForList = template.queryForList(IReportsDao.GET_STUDENT_DETAILS_RF, parameters);
			return queryForList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<Map<String, Object>> getHeadDeTailsUGCLC(MapSqlParameterSource parameters) {
		try{
			List<Map<String, Object>> queryForList = template.queryForList(IReportsDao.GET_HEAD_DETAILS_UG_CLC, parameters);
			return queryForList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<Map<String, Object>> getstudentDetailsAD(MapSqlParameterSource parameters) {
		try{
			List<Map<String, Object>> queryForList = template.queryForList(IReportsDao.GET_STUDENT_DETAILS_AD, parameters);
			return queryForList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	//{NAME=LIJI PADHI, ROLLNO=BS15-077, DOB=15/01/1997, RECEIPT_NO=379, BOOK_NO=01, CREATED_DATE=2017-06-15 00:00:00.0, 
	//STREAM=Science, SCIENCE_OPTION=Bio Science, YEAR_BATCH=3rd, CATEGORY=Girls, TUITION_FEES=0, ADM_READM=9, LIBRARY=150, 
	//SPORTS_GAMES=150, MEDICAL=10, LABORTARY=300, EXTRA_CARICULAR=10, POOR_BOYS_FUND=10, BU_FEE=15, MAGAZINE=100,
	//COLLEGE_EXAM=150, PUJA=20, COLLEGE_DAY=100, PROCTORIAL=10, DEVELOPMENT=150, SC_ARTS_SOCIETY=10, RED_CROSS=10, 
	//CALENDER=50, OTHER_FEES=150, BUILDING=100, READING_ROOM=20, REGISTRATION=0, LAB_CAUTION_MONEY=0, LIB_CAUTION_MONEY=0,
	//IDENTITY_CARD=0, CULTURAL=50, SCOUT_AND_GUIDE=10, TUITIONFEES_FOR_MONTH=0, DEVELOPMENT_FOR_10_MONTH=500, SOCIAL_SCIENCE=10, 
	//FLAG_DAY=5, BU_EXAMINATION=1525, CAF=0, MISCELLANIOUS=0, HONS_FEE=0, SEMINAR_FEE=100, FINE=0}


	@Override
	public List<Map<String, Object>> getDCRBookUG(SqlParameterSource parameters) {
		try{
			List<Map<String, Object>> queryForMap = template.queryForList(IReportsDao.GET_DCR_BOOK_UG.toString(), parameters);
			return queryForMap;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	@Override
	public List<Map<String, Object>> getDCRBook(SqlParameterSource parameters) {
		try{
			List<Map<String, Object>> queryForMap = template.queryForList(IReportsDao.GET_DCR_BOOK.toString(), parameters);
			return queryForMap;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<Map<String, Object>> getAddDCRBook(SqlParameterSource reportParams) {
		try{
			List<Map<String, Object>> queryForMap = template.queryForList(IReportsDao.GET_DCR_BOOK_ADD.toString(), reportParams);
			return queryForMap;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
