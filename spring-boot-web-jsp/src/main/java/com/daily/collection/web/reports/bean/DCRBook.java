package com.daily.collection.web.reports.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class DCRBook implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private Reports reports;
   	
   	private List<DCRBookList> dcrBookList=new ArrayList<DCRBookList>();
	
	
}
