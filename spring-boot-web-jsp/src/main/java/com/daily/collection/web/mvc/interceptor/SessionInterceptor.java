package com.daily.collection.web.mvc.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.daily.collection.web.login.controller.LoginController;

import lombok.extern.log4j.Log4j;
@Component
//@Log4j
public class SessionInterceptor extends  HandlerInterceptorAdapter {

	private final static Logger log = Logger.getLogger(SessionInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
	//	     log.info("Inside preHandle Method");
		        HttpSession session = request.getSession(false);
		        if (null == session) {
		        	if(request.getRequestURI().equalsIgnoreCase("/springbootapp/welcome")){
		        		log.info("Forwarding To /welcome");
		        		return true;
		        	}else if(request.getRequestURI().equalsIgnoreCase("/springbootapp/login")){
		        		log.info("Forwarding To Login");
		        		return true;
		        	}
		        	request.getSession(true).setAttribute("sessiontimeout", "Session Got Timeout.. Your Previous Data Will Not Save");
		        	response.sendRedirect(request.getContextPath()+"/"+"showLogin");
		        	log.info("Forwarding To showLogin");
		        	return false;
		        } 
		        return true;
	}
}
