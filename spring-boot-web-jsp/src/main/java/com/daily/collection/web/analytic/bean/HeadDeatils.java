package com.daily.collection.web.analytic.bean;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class HeadDeatils {
	
	public String Id;
	
	public String headname;
	
	public int headAmount;
	
	public String[] categories;
	
	public String cheboxSelect;
	
	public String otherHeadName;
	
	public String session;
	
	public BigDecimal studentCount;

	
	public HeadDeatils() {
		
	}
	
	public HeadDeatils(String headname, int headAmount) {
		super();
		this.headname = headname;
		this.headAmount = headAmount;
	}
	
	
	
	
	
	
	
	
	
	
	

	

		
	

}
