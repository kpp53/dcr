<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daily Collection Register | Upload</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/font-awesome.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/ionicons.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/datepicker3.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/dataTables.bootstrap.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/select2.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/custom.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/skin-blue.min.css"/>" />
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <jsp:include page="/WEB-INF/jsp/views/common/header.jsp"></jsp:include>
  <jsp:include page="/WEB-INF/jsp/views/common/menu.jsp"></jsp:include>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Upload Student Data
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Upload Student Data</a></li>
        <li class="active">Home</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	<!-- Alert message -->
	 <div class="alert alert-success" style="display:none;">
  <strong>Success!</strong> Indicates a successful or positive action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-info" style="display:none;">
  <strong>Info!</strong> Indicates a neutral informative change or action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-warning" style="display:none;">
  <strong>Warning!</strong> Indicates a warning that might need attention.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-danger" style="display:none;">
  <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>
        <!-- Alert message end-->  
		<div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Upload Student Data</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        <form action="" id="fileUpload">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <span id="fileselector">
              <label class="btn btn-default" for="upload-file-selector" name="file" id="uploadSpan">
                 <input id="upload-file-selector" type="file"  class="fileid" name="file">
                 <!-- <i class="fa fa-upload margin-correction"></i>Upload File -->
                 </input>
             </label>
             <label id="uploadMsg"></label>
             </span>
              </div>
              
            </div>
            <!-- /.col -->
            
            <!-- /.col -->
          </div>
          </form>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
      <div class="box">
            <div class="box-header">
              <h3 class="box-title pull-left">All Session 2017-18 Reports</h3>
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			<table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sl no</th>
                  <th>Row no</th>
                  <th>Error</th>
                  <th>Suggestion</th>
                  
				  
				  </tr>
				  </thead>
				  <tbody>
				  <tr>
				  <td>1</td>
				  <td>25</td>
				  <td>Student name</td>
				  <td>No number or special character</td>
				  
				  </tr>
				  <tr>
				  <td>2</td>
				  <td>29</td>
				  <td>Student name</td>
				  <td>No number or special character</td>
				  
				  </tr>
				  <tr>
				  <td>3</td>
				  <td>125</td>
				  <td>Student name</td>
				  <td>No number or special character</td>
				  
				  </tr>
				  <tr>
				  <td>4</td>
				  <td>200</td>
				  <td>Student name</td>
				  <td>No number or special character</td>
				  
				  </tr>
				  </tbody>
				   
				  </table>
              <div class="box-footer">
                
              </div>
            </div>
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
	<!-- Main Footer -->
      <jsp:include page="/WEB-INF/jsp/views/common/footer.jsp"></jsp:include>
  </div>
  <!-- /.content-wrapper -->

  

  
  
</div>


<div class="modal fade" id="myhelpModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Help</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- ./wrapper -->
<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
 <spring:url value="/js/jquery-2.2.3.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script>
<!-- Bootstrap 3.3.6 -->
    <spring:url value="/js/bootstrap.min.js" var="bootstrapminjs"></spring:url>
    <script src="${bootstrapminjs}"></script>
<!-- date-range-picker -->
     <spring:url value="/js/moment.min.js" var="momentminjs"></spring:url>
    <script src="${momentminjs}"></script>
  <spring:url value="/js/daterangepicker.js" var="daterangepickerjs"></spring:url>
    <script src="${daterangepickerjs}"></script>
<!-- DataTables -->
    <spring:url value="/js/jquery.slimscroll.min.js" var="jqueryslimscrollminjs"></spring:url>
    <script src="${jqueryslimscrollminjs}"></script>
<!-- bootstrap datepicker -->
   <spring:url value="/js/bootstrap-datepicker.js" var="bootstrapdatepickerjs"></spring:url>
    <script src="${bootstrapdatepickerjs}"></script>
      <spring:url value="/js/app.min.js" var="appminjs"></spring:url>
    <script src="${appminjs}"></script>

<script type="text/javascript">
$(document).ready(function() {
	$(':file').on('fileselect', function(event, numFiles, label) {
        console.log(numFiles);
        console.log(label);
        alert('aa');
    });
	
	/* $(document).on('click', '#upload-file-selector', function(){
		  var file = $(this).parent().parent().parent().find('.file');
		  file.trigger('click');
		  alert("aa")
		}); */
	
		$(document).on('change', '#upload-file-selector', function(){
		  /* $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, '')); */
		  var filename =$('#upload-file-selector').prop("files")[0]['name'];
		    $.ajax({
		    	url: 'uploadFile',
	         type: "POST",
	         data: new FormData(document.getElementById("fileUpload")),
	         enctype: 'multipart/form-data',
	         processData: false,
	         contentType: false,
		        success : function(data) {
		        	alert(data);
		        	/* $('#uploadSpan').removeClass('disabled');
		        	if(data=='SUCCESS'){
	    	  	        $('#slider_content_3 .bluiee_butt').prop('disabled', false);
	 		        $('#slider_content_3 .greeniee_butt').prop('disabled', false);
	    	        	$("#uploadMsg").html('Photo Uploaded Successfully').removeClass('photoUploadfailure').addClass('photoUploadsuccess');
	    	        	fileUpload='true';
		        	}else{
	    	  	        $('#slider_content_3 .bluiee_butt').prop('disabled', true);
	    	  	        $('#slider_content_3 .greeniee_butt').prop('disabled', false);
	    	        	$("#uploadMsg").html('Error While Uploading The Photo').addClass('photoUploadfailure');
		        	} */
		        },
		        error : function(err) {
		        	/* $("#uploadMsg").html(err); */
		        }
		    }); 
		  
		  
		  
		  
		  
		  
		  
		});
	
	
	
$("#upload-file-selector").on('select',function(){
	alert('aa');
	    fileUpload='false';
	   $("#uploadMsg").html('');
	   var filename =$('#upload-file-selector').prop("files")[0]['name']
	   if (!(isJpg(filename) || isPng(filename))) {
			$("#uploadMsg").html('Please browse a .JPG/.PNG file to upload ...').addClass('photoUploadfailure');
			return;
		} 
	   $('#uploadSpan').addClass('disabled');
	   $('#slider_content_3 .bluiee_butt').prop('disabled', true);
	   $('#slider_content_3 .greeniee_butt').prop('disabled', true);
	    $.ajax({
	    	url: 'saveStudentImage',
         type: "POST",
         data: new FormData(document.getElementById("fileUpload")),
         enctype: 'multipart/form-data',
         processData: false,
         contentType: false,
	        success : function(data) {
	        	$('#uploadSpan').removeClass('disabled');
	        	if(data=='SUCCESS'){
    	  	        $('#slider_content_3 .bluiee_butt').prop('disabled', false);
 		        $('#slider_content_3 .greeniee_butt').prop('disabled', false);
    	        	$("#uploadMsg").html('Photo Uploaded Successfully').removeClass('photoUploadfailure').addClass('photoUploadsuccess');
    	        	fileUpload='true';
	        	}else{
    	  	        $('#slider_content_3 .bluiee_butt').prop('disabled', true);
    	  	        $('#slider_content_3 .greeniee_butt').prop('disabled', false);
    	        	$("#uploadMsg").html('Error While Uploading The Photo').addClass('photoUploadfailure');
	        	}
	        },
	        error : function(err) {
	        	$("#uploadMsg").html(err);
	        }
	    }); 
	});
});
</script>





<script>
  $(function () {
  //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
	  format: 'dd/mm/yyyy'
    });
  //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'dd/mm/yyyy h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('D MMMM, YYYY') + ' - ' + end.format('D MMMM, YYYY'));
        }
    );
  });
</script>

</body>
</html>
