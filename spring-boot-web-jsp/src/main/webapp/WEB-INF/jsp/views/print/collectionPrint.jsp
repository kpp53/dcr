<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.text.SimpleDateFormat"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ page import = "java.io.*,java.util.*, javax.servlet.*" %>

<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<!-- <title>E Receipt Khetramohan Science Junior College</title> -->
	<title>E Receipt  Ramanarayan Junior College</title>
	
	 <link rel="stylesheet" href="<c:url value="/css/common/style.css"/>" />
      <link rel="stylesheet" href="<c:url value="/css/common/print.css"/>" media="print"/>
	  <spring:url value="/img" var="images" />
	<style type="">
	  @media print {
         #printPageButton {
          display: none;
       }
     }
     
     .button {
   /* background-color: #4CAF50; */
    border: none;
    color: white;
    padding: 6px 44px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: -10px 1px;
    cursor: pointer;
    background-color: #00c0ef;
    border-color: #00acd6;
}
	  
	  
	  </style>

</head>

<body>

	<div id="page-wrap">

		
		
		<div id="identity">
		
            

            <div id="logo">
          

              
               <img id="image" src="${images}/logo.jpg" alt="logo" />
            </div>
		
		</div>
		
				<div id="customer">


            <div>
<c:if test="${config eq '+2'}">
Ramanarayan Junior College<br></c:if>
<c:if test="${config eq 'UG'}">
Ramanarayan Degree College<br></c:if>
<!-- Accredited with "B" Grade by NAAC<br> -->
Dura,Ganjam,Odisha<br>
Phone No: 0680 2490229<br>
www.ramanarayanjuniorcollege.org

			</div>
		
		</div>
		
		<table id="items">
		<tr>
		      <th class="receptno">Session ${collectionBeanObj.session}</th>
		      <th class="sname" style="text-align:center;">All Report</th>
		      <th>${config}/All/All</th>
		      
		  </tr>
		  <tr>
		      <th>Date -
		      <%
		 SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");     
         Date date = new Date();
         out.print( format.format(date));
          %>
		      </th>
		      <th></th>
		      <th>Category - All</th>
		      
		  </tr>
		  <tr>
		  <td colspan="3" class="nopadding">
		  <table class="inner">
		  <thead>
		  <tr>
		  <th class="slno">SL No.</th>
          <th>Date</th>
		      <th>Fund</th>
		      <th>No Of Student</th>
              <th>CAF Fee</th>
			  <th class="times">Miscellaneous</th>
		      <th>Amount</th>
		  </tr>
		  </thead>
		 <c:set var="tranCoount" value="${0}"></c:set>
		  <c:set var="fundTotal" value="${0}"></c:set>
		  <c:set var="cafTotal" value="${0}"></c:set>
		  <c:set var="MisTotal" value="${0}"></c:set>
		  <c:set var="Total" value="${0}"></c:set>
		  <c:set var="NoOfStudent" value="${0}"></c:set>
		  <tbody>
		   <c:forEach var="reqcollectionBean" items="${reqcollectionBean.collectionList}" varStatus="loop">
		   <c:set var="tranCoount" value="${tranCoount+ 1}"></c:set>
		   <c:set var="fundTotal" value="${fundTotal + reqcollectionBean.totalFund}"></c:set>
		   <c:set var="cafTotal" value="${cafTotal + reqcollectionBean.cafAmount}"></c:set>
		   <c:set var="MisTotal" value="${MisTotal + reqcollectionBean.miscellaniousAmount}"></c:set>
		   <c:set var="Total" value="${Total + reqcollectionBean.cafAmount+ reqcollectionBean.totalFund + reqcollectionBean.miscellaniousAmount}"></c:set>
		   <c:set var="NoOfStudent" value="${NoOfStudent + reqcollectionBean.noOfStudent}"></c:set> 
		  <tr>
		  <td>${loop.index+1}</td>
          <td>${reqcollectionBean.date}</td>
		      <td>${reqcollectionBean.totalFund}</td>
		      <td>${reqcollectionBean.noOfStudent}</td>
			  <td>${reqcollectionBean.cafAmount}</td>
		      <td>${reqcollectionBean.miscellaniousAmount}</td>
              <td>${reqcollectionBean.totalFund + reqcollectionBean.cafAmount + reqcollectionBean.miscellaniousAmount}</td>
		  </tr>
          </c:forEach>
   		  </tbody>
		  <tfoot>
           <tr>
		  <th class="slno">Grand Total</th>
          <th>${tranCoount} Transaction</th>
		      <th class="">${fundTotal}</th>
		      <th>${NoOfStudent}</th>
              <th>${cafTotal}</th>
			  <th class="times">${MisTotal}</th>
		      <th>${Total}</th>
		  </tr>
		  <tr>
		  <th class="slno"></th>
         
		      <th colspan="5" class="headname">In Word -${amt} Rupess Only.</th>
			  
		      <th></th>
		  </tr>
		  </tfoot>
		  </table>
		  </td>
		      
		      
		  </tr>
		
		
		</table>
		
		<div id="terms">
		  
		  <textarea>This Is Computer Generated Bill And Valid With Seal & Signature Of Authorization Person.</textarea>
		</div>
	<div class="sign">Accountant/DAO</div>
	<button onclick="window.print()" id="printPageButton" class="button">PRINT</button>
	<button onclick="window.close()" id="printPageButton" class="button">CANCELL</button>
	</div>
	<spring:url value="/js/jquery-1.3.2.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script>
    <spring:url value="/js/example.js" var="examplejs"></spring:url>
    <script src="${examplejs}"></script>
</body>

</html>