<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>E Receipt Khetramohan Science College</title>
	
	  <link rel="stylesheet" href="<c:url value="/css/common/style.css"/>" />
      <link rel="stylesheet" href="<c:url value="/css/common/print.css"/>" media="print"/>
	  <spring:url value="/img" var="images" />
	  
	  <style type="">
	  @media print {
         #printPageButton {
          display: none;
       }
     }
     
     .button {
   /* background-color: #4CAF50; */
    border: none;
    color: white;
    padding: 6px 44px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: -10px 1px;
    cursor: pointer;
    background-color: #00c0ef;
    border-color: #00acd6;
}
	  
	  
	  </style>

	

</head>

<body>

	<div id="page-wrap">

		
		
		<div id="identity">
		
            

            <div id="logo">
          

              
               <img id="image" src="${images}/logo.jpg" alt="logo" /> 
              <%-- <img id="image" src="${images}/RNJC.jpg" alt="logo" /> --%>
            </div>
		
		</div>
		
				<div id="customer">


            <div>
            
<%-- 			E Receipt<br>
<c:if test="${config eq '+2'}">
Ramanarayan Junior College<br></c:if>
<c:if test="${config eq 'UG'}">
Ramanarayan Degree College<br></c:if>
<!-- Accredited with "B" Grade by NAAC<br> -->
Dura,Ganjam,Odisha<br>
Phone No: 0680 2490229<br>
www.ramanarayanjuniorcollege.org --%>

			E Receipt<br>
			<c:if test="${config eq '+2'}">
Khetramohan Science Junior College<br></c:if>
<c:if test="${config eq 'UG'}">
Khetramohan Science Degree College<br></c:if>
Accredited with "B" Grade by NAAC<br>
Narendrapur,Ganjam,Odisha<br>
Phone No: 06811 259524/259054<br>
www.kmsc.org.in
			</div>
		
		</div>
		
		<table id="items">
		<tr>
		     <th class="">Receipt No -${printstudent.receiptNo} </th>
		      <th class="sname">Name - ${printstudent.name}</th>
		      <th>${printstudent.course}/ ${printstudent.stream}/ ${printstudent.yearBatch}</th>
		      
		  </tr>
		  <tr>
		      <th>Date - ${printstudent.created_Date}</th>
		      <th>Roll No - ${printstudent.rollNo}</th>
		      <th>${printstudent.category}/${printstudent.session}</th>
		      
		  </tr>
		  <tr>
		  <td colspan="3" class="nopadding">
		  <table class="inner">
		  <thead>
		  <tr>
		  <th class="slno">SL No.</th>
		      <th class="headname">Description</th>
		      <th>Amount</th>
		  </tr>
		  </thead>
		  <tbody>
		            <tr style="line-height:100%">
		               <td>1</td>
				       <td><br>
				         Admission Cancelled-<b>${Status}</b></br></br>
				         <div width="100px" style="margin-left: 200px">${tranType}</div></br>
				         <c:if test="${Status eq 'TC Taken'}">
				           Bank Name- <b>${bankName}</b><br>
				           Cheque No- <b>${chequeNo}</b></br></br>
				         </c:if>
				       </td>
				       <td>${amount}</td>
		            </tr>
		  </tbody>
		  <tfoot>
		 <tr>
		  <th class="slno"></th>
		      <th class="headname">In Word - ${amntinwrds} Rupees Only.</th>
		      <th>Total :Rs ${amount}</th>
		  </tr>
		  </tfoot>
		  </table>
		  </td>
		  </tr>
		</table>
		
		<div id="terms">
		  
		  <textarea>This Is Computer Generated Bill And Valid With Seal & Signature Of Authorization Person.</textarea>
		</div>
	<div class="sign">Accountant/DA</div>
	<button onclick="window.print()" id="printPageButton" class="button">PRINT</button>
	<button onclick="window.close()" id="printPageButton" class="button">CANCELL</button>
	
	</div>
	<spring:url value="/js/jquery-1.3.2.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script>
    <spring:url value="/js/example.js" var="examplejs"></spring:url>
    <script src="${examplejs}"></script>
</body>

</html>