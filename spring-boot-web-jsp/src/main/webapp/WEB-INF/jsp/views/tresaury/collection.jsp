<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daily Collection Register | Collection</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/font-awesome.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/ionicons.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/datepicker3.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/dataTables.bootstrap.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/custom.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/skin-blue.min.css"/>" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
 <jsp:include page="/WEB-INF/jsp/views/common/header.jsp"></jsp:include>
  <jsp:include page="/WEB-INF/jsp/views/common/menu.jsp"></jsp:include>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Collection
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Collection</a></li>
        <li class="active">Treasury</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	<!-- Alert message -->
	 <div class="alert alert-success" style="display:none;">
  <strong>Success!</strong> Indicates a successful or positive action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-info" style="display:none;">
  <strong>Info!</strong> Indicates a neutral informative change or action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-warning" style="display:none;">
  <strong>Warning!</strong> Indicates a warning that might need attention.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-danger" style="display:none;">
  <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>
        <!-- Alert message end-->  
		<div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Search</h3>

          <div class="box-tools pull-right">
           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
           
              <div class="form-group">
                <label>Search by</label>
                <select class="form-control select2" style="width: 100%;" id="searchBy">
                  <option selected="selected">All</option>
                  <option>Student</option>
                  <option>Fund</option>
                  <option>Heads</option>
                  <option>CAF</option>
                  <option>Miscellaneous</option>
                                   
                </select>
              </div>
              
              <div class="form-group">
                <label>Fund Type </label>
                <select class="form-control select2" style="width: 100%;" id="fundTypeDiv"  disabled="disabled">
                  <option selected="selected">All</option>
                     <c:forEach var="fundType" items="${FundTypeMap}">
				             <option value="${fundType}">${fundType}</option>
				     </c:forEach>
				     <option value="Additional Fee">Additional Fee</option>
				     <option value="Fine">Fine</option>
                    
                </select>
              </div>
             
              <div class="form-group">
                <label>Category </label>
                <select class="form-control select2" style="width: 100%;"  id="categoryDiv" disabled="disabled">
                  <option selected="selected">All</option>
                   <c:forEach var="category" items="${categoryMap}">
				             <option value="${category}">${category}</option>
				   </c:forEach>
                                
                </select>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <!-- Date range -->
              
                <label>Date range:</label>

               
                <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
    <span></span> 
</div>
              <div class="clearfix"></div>
              </div>
              <!-- /.form-group -->
               <div class="form-group">
                <label>Class </label>
                <select class="form-control select2" style="width: 100%;" id="classDiv" disabled="disabled">
                <option value="all">All</option>
                <c:forEach var="yearBatch" items="${yearBacthMap}">
                 <c:if test="${config eq '+2'}">
                   <c:if test="${yearBatch ne '3rd'}">
                      <option value="${yearBatch}">${yearBatch}</option>
                   </c:if>
                  </c:if>
                <c:if test="${config ne '+2'}">
                      <option value="${yearBatch}">${yearBatch}</option>
				 </c:if>           
				</c:forEach>
                                     
                </select>
              </div>
              <div class="form-group" >
                <label>Stream </label>
                <select class="form-control select2" style="width: 100%;" id="streamDiv" disabled="disabled">
                  <c:forEach var="stream" items="${streamMap}">
				             <option value="${stream}">${stream}</option>
				  </c:forEach>
                </select>
              </div>
              
              <!-- /.form-group -->
            </div>
            
            
             <div class="col-md-12">
             <div class="form-group">
                <label>Session</label>
                <select class="form-control select2" style="width: 100%;" id="searchbySession">
                  <option selected="selected">2017-18</option>
                  <option>2016-17</option>
                  <option>2015-16</option>
                </select>
              </div>
            
            
            </div>
            
            
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Select all the above field to get accurate result.
          <button type="submit" class="btn btn-info pull-right" id="searchIdInReport">Submit</button>
        </div>
      </div>
      <!-- /.box -->
      <div class="box" >
            <div class="box-header">
              <h3 class="box-title pull-left"><b>Collection Reports ${collectionBeanObj.searchBy}/Session ${collectionBeanObj.session}/Reports From ${collectionBeanObj.dateFrom} To ${collectionBeanObj.dateTo}</b></h3>
             
            </div>
            <!-- /.box-header -->
            <c:if test="${reqcollectionBean ne null}">
            <div class="box-body">
			<table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sl no</th>
                  <th>Date</th>
                  <th>Fund</th>
                  <th>CAF Fee</th>
                  <th>Miscellaneous</th>
                  <th>Total</th>
                  <th>More Details</th>
				  
				  </tr>
				  </thead>
				  <tbody>
				  
				  <c:set var="totalAmt" value="${0}" />
				  <c:set var="fundTotal" value="${0}"></c:set>
		          <c:set var="cafTotal" value="${0}"></c:set>
		          <c:set var="MisTotal" value="${0}"></c:set>
		  
				  <c:forEach var="reqcollectionBean" items="${reqcollectionBean.collectionList}" varStatus="loop">
				   <c:set var="fundTotal" value="${fundTotal + reqcollectionBean.totalFund}"></c:set>
		           <c:set var="cafTotal" value="${cafTotal + reqcollectionBean.cafAmount}"></c:set>
		           <c:set var="MisTotal" value="${MisTotal + reqcollectionBean.miscellaniousAmount}"></c:set>
				  <tr>
				  <td>${loop.index+1}</td>
				  <td>${reqcollectionBean.date}</td>
				  <td title="No Of Student::${reqcollectionBean.noOfStudent}">${reqcollectionBean.totalFund}</td>
				  <td>${reqcollectionBean.cafAmount}</td>
                  <td>${reqcollectionBean.miscellaniousAmount}</td>
                  <td>${reqcollectionBean.cafAmount + reqcollectionBean.miscellaniousAmount + reqcollectionBean.totalFund}</td>
				  <td><a href="#">Details</a></td>
				  </tr>
				  <c:set var="totalAmt" value="${totalAmt + reqcollectionBean.cafAmount + reqcollectionBean.miscellaniousAmount + reqcollectionBean.totalFund}" /> 
				  </c:forEach>
				  </tbody>
				   <tfoot>
                <tr>
                 <th></th>
                  <th>Grand Total</th>
                  <th>${fundTotal}</th>
                   <th>${cafTotal}</th>
                  <th>${MisTotal}</th>
                  <th>${totalAmt}</th>
                  <th></th>
				  </tr>
				  </tfoot>
				  </table>
              <div class="box-footer">
                <button type="submit" class="btn btn-default" id="downloadExcel">Download Excel</button>
				<button type="submit" class="btn btn-default">Download PDF</button>
                <button type="submit" class="btn btn-info pull-right" id="print">Print</button>
              </div>
            </div>
            </c:if>
            <c:if test="${reqcollectionBean eq null}">
				  No Record Found For This Search
		    </c:if>
				  
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
	<!-- Main Footer -->
  <jsp:include page="/WEB-INF/jsp/views/common/footer.jsp"></jsp:include>
  </div>
  <!-- /.content-wrapper -->

  

  
  
</div>


<div class="modal fade" id="myhelpModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Help</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="dataconfmodal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Student Admission Data</h4>
      </div>
      <div class="modal-body" style="height:400px; overflow-y:scroll;">
	  <h5>Please Verify the data before making admission.</h5>
        <div class="table-responsive">
  <table class="table">
  <tbody>
  <tr>
  <td>Name</td>
  <td>Sai Raj Khuntia</td>
  <td>Father Name</td>
  <td>Raghu Raj Khuntia</td>
  </tr>
  <tr>
  <td>Mother Name</td>
  <td>Sasmitha Khuntia</td>
  <td>Date Of Birth</td>
  <td>15/06/2001</td>
  </tr>
  </tbody>
   
  </table>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--Admission popup-->
<div class="modal fade" id="admissionmodal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Admission</h4>
      </div>
      <div class="modal-body" >
	  
        <div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Amount</label>

                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="inputamount" placeholder="Amount">
                  </div>
                </div>
				<div class="row form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Additional Reason
                      </label>
                    </div>
                  </div>
                </div>
				<div class="row form-group">
                  <label for="adinputreason" class="col-sm-2 control-label">Reason</label>

                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="adinputreason" placeholder="Reason">
                  </div>
				  <label for="adinputamount" class="col-sm-2 control-label">Amount</label>

                  <div class="col-sm-4">
                    <input type="number" class="form-control" id="adinputamount" placeholder="Amount">
                  </div>
                </div>
				
				<div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Payment Mode</label>

                  <div class="col-sm-4">
                    <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Cash</option>
                  <option>Cheque</option>
				  <option>DD</option>             
                  </select>
                  </div>
				  <label for="chequeddno" class="col-sm-2 control-label">Cheque/DD no</label>

                  <div class="col-sm-4">
                    <input type="number" class="form-control" id="chequeddno" placeholder="Cheque/DD no">
                  </div>
                </div>
				
      </div>
	  <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Save/Print</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ./wrapper -->
<!--Quick Admission popup-->
<div class="modal fade" id="quickadmissionmodal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Quick Admission</h4>
      </div>
      <div class="modal-body" >
	  
        <div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="inputamount" placeholder="Amount">
                  </div>
                </div>
				
				<div class="row form-group">
                  <label for="inputrollno" class="col-sm-2 control-label">Roll no</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputrollno" placeholder="Roll no">
                  </div>
				  
                </div>
				<div class="row form-group">
                  <label for="inputdob" class="col-sm-2 control-label">Date of Birth</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputdob" placeholder="Date of Birth">
                  </div>
				  
                </div>
				
				
      </div>
	  <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ./wrapper -->
<!-- REQUIRED JS SCRIPTS -->

<spring:url value="/js/jquery-2.2.3.min.js" var="jqueryminjs"></spring:url>
<script src="${jqueryminjs}"></script>
<spring:url value="/js/bootstrap.min.js" var="bootstrapminjs"></spring:url>
<script src="${bootstrapminjs}"></script>
<spring:url value="/js/moment.min.js" var="momentminjs"></spring:url>
<script src="${momentminjs}"></script>
<spring:url value="/js/daterangepicker.js" var="daterangepickerjs"></spring:url>
<script src="${daterangepickerjs}"></script>
<spring:url value="/js/jquery.dataTables.min.js" var="jquerydataTablesminjs"></spring:url>
<script src="${jquerydataTablesminjs}"></script>
<spring:url value="/js/dataTables.bootstrap.min.js" var="dataTablesbootstrapminjs"></spring:url>
<script src="${dataTablesbootstrapminjs}"></script>
<spring:url value="/js/jquery.slimscroll.min.js" var="jqueryslimscrollminjs"></spring:url>
<script src="${jqueryslimscrollminjs}"></script>
<spring:url value="/js/bootstrap-datepicker.js" var="bootstrapdatepickerjs"></spring:url>
<script src="${bootstrapdatepickerjs}"></script>
<spring:url value="/js/app.min.js" var="appminjs"></spring:url>
<script src="${appminjs}"></script>

<script type="text/javascript">


$(document).on('click' , '#downloadExcel' , function(){
	
	/* var from='${reports.searchBy}'; */
	var url='${print_url}';
	var from='CollectionALL';
	var win = window.open(url+"/downloadReport.xls?from="+from, "_self"); 
});




$(document).on('click' , '#print' , function(){
	var url='${print_url}'
    var win = window.open(url+"/collectionPrint", '_blank');
	//var win = window.open("http://host:3030/DailyCollectionRegister/print?printid="+savedId, '_blank');

});


$('#searchBy').change(function(){	
	 var searchBy=$("#searchBy").val();
	 if(searchBy=='All'){
		 $("#classDiv").prop("disabled",true);
		 $("#streamDiv").prop("disabled",true);
		 $("#categoryDiv").prop("disabled",true);
		 $("#fundTypeDiv").prop("disabled",true);
	 }else{
		 $("#classDiv").prop("disabled",false);
		 $("#streamDiv").prop("disabled",false);
		 $("#categoryDiv").prop("disabled",false);
		 $("#fundTypeDiv").prop("disabled",false);
	 }
	 
 });


$(document).on('click' , '#searchIdInReport' , function(){
	var session=$("#searchbySession").val();
	var searchBy=$("#searchBy").val();
	var dateRange=$("#reportrange span").text();
	var dateRangeArray=dateRange.split("To");
	var formDetailsJson={};
	/* if(searchBy=='Fund'){
		 var classType=$("#classDiv").val();
		 var stream=$("#streamDiv").val();
		 var category=$("#categoryDiv").val();
		 var fundType=$("#fundTypeDiv").val();
		 formDetailsJson={
					"session":session,
					"searchBy": searchBy,
					"dateFrom": dateRangeArray[0],
					"dateTo": dateRangeArray[1],
					"classType": classType,
					"stream": stream,
					"category": category,
					"fundType": fundType
			    }
		 $.ajax({
	    	    type: "POST",
	    	    url: "reportFundSearch", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
	    	    success: function(result) {
	    	    	window.location.href = "showMasterReports";
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Verify The Data At This Moment");
	    	    }
	    	     });
		
		
	}else if(searchBy=='student'){
		 var classType=$("#classDiv").val();
		 var stream=$("#streamDiv").val();
		 var category=$("#categoryDiv").val();
		 var fundType=$("#fundTypeDiv").val();
		 formDetailsJson={
					"session":session,
					"searchBy": searchBy,
					"dateFrom": dateRangeArray[0],
					"dateTo": dateRangeArray[1],
					"classType": classType,
					"stream": stream,
					"category": category,
					"fundType": fundType
			    }
		 $.ajax({
	    	    type: "POST",
	    	    url: "reportStudentSearch", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
	    	    success: function(result) {
	    	    	window.location.href = "showMasterReports";
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Verify The Data At This Moment");
	    	    }
	    	     });
	}else{ */
		    formDetailsJson={
				"session":session,
				"searchBy": searchBy,
				"dateFrom":dateRangeArray[0],
				"dateTo": dateRangeArray[1]
		    }
		    $.ajax({
	    	    type: "POST",
	    	    url: "getCollectionReport", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
	    	    success: function(result) {
	    	    	window.location.href = "showCollection";
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Verify The Data At This Moment");
	    	    }
	    	     });
/* 	}  */
 	  
});






$(function() {
       $("#example1").DataTable();
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('D-MMM-YYYY') + ' To ' + end.format('D-MMM-YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
    
});
</script>
</body>
</html>
