<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daily Collection Register | Head Details</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <!-- Bootstrap 3.3.6 -->
    <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap.min.css"/>" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<c:url value="/css/common/font-awesome.min.css"/>" />
  <!-- Ionicons -->
  <link rel="stylesheet" href="<c:url value="/css/common/ionicons.min.css"/>" />
   <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<c:url value="/css/common/datepicker3.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/dataTables.bootstrap.css"/>" />
  <!-- Select2 -->
  <link rel="stylesheet" href="<c:url value="/css/common/select2.min.css"/>" />
  <!-- Theme style -->
  <link rel="stylesheet" href="<c:url value="/css/customize/custom.min.css"/>" />
  
  <link rel="stylesheet" href="<c:url value="/css/common/skin-blue.min.css"/>" />
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <jsp:include page="/WEB-INF/jsp/views/common/header.jsp"></jsp:include>
  <jsp:include page="/WEB-INF/jsp/views/common/menu.jsp"></jsp:include>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Head Details
        <small>Analytic</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Analytic</a></li>
        <li class="active">Head Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	<!-- Alert message -->
	 <div class="alert alert-success" style="display:none;">
  <strong>Success!</strong> Indicates a successful or positive action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-info" style="display:none;">
  <strong>Info!</strong> Indicates a neutral informative change or action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-warning" style="display:none;">
  <strong>Warning!</strong> Indicates a warning that might need attention.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-danger" style="display:none;">
  <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>
        <!-- Alert message end-->  
		<!-- Search filter--> 
		<div class="row">		
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Search</h3>

              
			  </div>
			  <div class="box-body">
			  <div class="form-group">
                <label>Search By Session</label>
                <select class="form-control" data-placeholder="Select a Stream" style="width: 100%;" id="searchbySession">
                             <option value="">Select</option>
                             <option value="2017-18">2017-18</option>
                             <option value="2016-17">2016-17</option>
                             <option value="2015-16">2015-16</option>
                </select>
              </div>
			  <div class="form-group">
                <label>Search head</label>
                <select class="form-control select2" multiple="multiple" data-placeholder="Select a Stream" style="width: 100%;" id="searchCategory">
                  <c:if test="${config eq '+2'}">
                <c:forEach var="applicationConfig" items="${applicationConfig}">
                   <c:if test="${applicationConfig eq 'Science'}">
                     <option value="firstTable" id="first">+2 1st yr science</option>
                     <option value="secondTable" id="second">+2 2nd yr science</option>
                   </c:if>
                   <c:if test="${applicationConfig eq 'Arts'}">
                    <option value="thirdTable" id="third"> +2 1st yr arts</option>
                    <option value="fourthTable" id="fourth">+2 2nd yr arts</option>
                   </c:if>
                   <c:if test="${applicationConfig eq 'Commerce'}">
                     <option value="fifthTable" id="fifth"> +2 1st yr commerce</option>
                     <option value="sixthTable" id="sixth">+2 2nd yr commerce</option>
                   </c:if>
                 </c:forEach> 
                 </c:if>
                 <c:if test="${config eq 'UG'}">
                 <c:forEach var="applicationConfig" items="${applicationConfig}">
                   <c:if test="${applicationConfig eq 'Science'}">
                     <option value="firstTable" id="first">UG 1st yr science</option>
                     <option value="secondTable" id="second">UG 2nd yr science</option>
                      <option value="seventhTable" id="third">UG 3rd yr science</option>
                   </c:if>
                   <c:if test="${applicationConfig eq 'Arts'}">
                    <option value="thirdTable" id="third"> UG 1st yr arts</option>
                    <option value="fourthTable" id="fourth">UG 2nd yr arts</option>
                    <option value="eigthTable" id="third">UG 3rd yr arts</option>
                   </c:if>
                   <c:if test="${applicationConfig eq 'Commerce'}">
                     <option value="fifthTable" id="fifth"> UG 1st yr commerce</option>
                     <option value="sixthTable" id="sixth">UG 2nd yr commerce</option>
                   </c:if>
                 </c:forEach> 
                 </c:if>
                </select>
              </div>
              <div class="form-group">
                 <label>Select All Categories</label>
                  <div class="col-sm-1" style="width: 10px;">
                    <input type="checkbox"  value="selectAllsearch" id="checkboxforsearch">
                  </div>
              </div>
               <div class="form-group">
              <c:if test="${searchSession ne null}">
                 <label>Search For :: ${searchSession}</label>
               </c:if>  
              </div>
			  </div>
			  </div>
			  </div>
			  </div>
			  
        <!--Science 1st year-->
        <div class="row" id="21stscience" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">+2 Science 1st yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science1" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${head1 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head1}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head1 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Science 1st yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science2" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${head2 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head2}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head2 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
<!--Science 1st year End-->

<!--Science 2nd year-->
 <div class="row" id="22ndscience" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Science 2nd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science21" class="table table-bordered table-striped">
                 <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                 <c:if test="${head3 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head3}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head3 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Science 2nd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                 <c:if test="${head4 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head4}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head4 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
<!--Science 2nd year end-->

 <div class="row" id="33ndscience" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Science 3rd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
                  
                </tr>
                </thead>
                <tbody>
                       <c:if test="${headUG13 ne null}">
                       <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG13}" var="thirdhead" varStatus="loop">
                 <c:if test="${thirdhead ne null}">
                 <c:set var="total" value="${total + thirdhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="thirteenheadname${loop.index}">${thirdhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${thirdhead.headAmount}" disabled="disabled" id="thirteenheadamount${loop.index}"> </td>
                </tr>
                 </c:if>  
                 <c:if test="${thirdhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
				  <th><input type="number" class="form-control" id="thirteenheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG13 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Science 3rd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${headUG14 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG14}" var="fourthhead" varStatus="loop">
                 <c:if test="${fourthhead ne null}">
                 <c:set var="total" value="${total + fourthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="fourteenheadName${loop.index}">${fourthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${fourthhead.headAmount}" disabled="disabled" id="fourteenheadAmount${loop.index}"> </td>
                </tr>
                 </c:if>  
                 <c:if test="${fourthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="fourteenheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG14 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>

</div>



 <!--Arts 1st year-->
        <div class="row" id="21starts" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Arts 1st yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts1" class="table table-bordered table-striped">
                 <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                 <c:if test="${head5 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head5}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head5 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Arts 1st yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts2" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                 <c:if test="${head6 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head6}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head6 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
<!--Arts 1st year End-->

<!--Arts 2nd year-->
 <div class="row" id="22ndarts" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Arts 2nd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts21" class="table table-bordered table-striped">
                 <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${head7 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head7}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head7 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Arts 2nd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${head8 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head8}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head8 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
<div class="row" id="33ndarts" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Arts 3rd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                  
                </tr>
                </thead>
                <tbody>
                       <c:if test="${headUG15 ne null}">
                       <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG15}" var="thirdhead" varStatus="loop">
                 <c:if test="${thirdhead ne null}">
                 <c:set var="total" value="${total + thirdhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="fifteenheadName${loop.index}">${thirdhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${thirdhead.headAmount}" disabled="disabled" id="fifteenheadAmount${loop.index}"> </td>
                </tr>
                 </c:if>  
                 <c:if test="${thirdhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="fifteenheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG15 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Arts 3rd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${headUG16 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG16}" var="fourthhead" varStatus="loop">
                 <c:if test="${fourthhead ne null}">
                 <c:set var="total" value="${total + fourthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="sixteenheadName${loop.index}">${fourthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${fourthhead.headAmount}" disabled="disabled" id="sixteenheadAmount${loop.index}"> </td>
                </tr>
                 </c:if>  
                 <c:if test="${fourthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="sixteenheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG16 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
</div>

 <div class="row" id="21stcommerce" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Commerce 1st yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts21" class="table table-bordered table-striped">
                 <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${head9 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head9}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head9 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Commerce 1st yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${head10 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head10}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head10 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
 <div class="row" id="22ndcommerce" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Commerce 2nd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts21" class="table table-bordered table-striped">
                 <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${head11 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head11}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head11 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Commerce 2nd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${head12 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head12}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head12 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>



<div class="row" id="31stscience" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">UG Science 1st yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science1" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${headUG1 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG1}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG1 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Science 1st yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science2" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${headUG2 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG2}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG2 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
<!--Science 1st year End-->

<!--Science 2nd year-->
 <div class="row" id="32ndscience" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Science 2nd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science21" class="table table-bordered table-striped">
                 <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                 <c:if test="${headUG3 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG3}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG3 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Science 2nd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                 <c:if test="${headUG4 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG4}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG4 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
<!--Science 2nd year end-->

 <!--Arts 1st year-->
        <div class="row" id="31starts" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Arts 1st yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts1" class="table table-bordered table-striped">
                 <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                 <c:if test="${headUG5 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG5}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG5 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Arts 1st yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts2" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                 <c:if test="${headUG6 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG6}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG6 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
<!--Arts 1st year End-->

<!--Arts 2nd year-->
 <div class="row" id="32ndarts" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Arts 2nd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts21" class="table table-bordered table-striped">
                 <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${headUG7 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG7}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG7 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Arts 2nd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${headUG8 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG8}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG8 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
 <div class="row" id="31stcommerce" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Commerce 1st yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts21" class="table table-bordered table-striped">
                 <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${headUG9 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG9}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG9 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Commerce 1st yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${headUG10 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG10}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG10 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>
 <div class="row" id="32ndcommerce" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Commerce 2nd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts21" class="table table-bordered table-striped">
                 <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${headUG11 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG11}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG11 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Commerce 2nd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
				  <th>Head Amount</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${headUG12 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG12}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td>${firsthead.headAmount}</td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG12 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>












<!--Arts 2nd year end-->

    </section>
    <!-- /.content -->
	<!-- Main Footer -->
  <jsp:include page="/WEB-INF/jsp/views/common/footer.jsp"></jsp:include>
  </div>
  <!-- /.content-wrapper -->

  

  
  
</div>


<div class="modal fade" id="myhelpModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Help</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--Admission popup-->

<!-- ./wrapper -->
<!--Quick Admission popup-->
<div class="modal fade" id="addnewheadmodal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add new head</h4>
      </div>
      <div class="modal-body" >
	  
        <div class="row form-group">
                  <label for="inputheadname" class="col-sm-2 control-label">Head Name</label>

                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="inputheadname" placeholder="Head Name">
                  </div>
                </div>
				
				<div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Amount</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputamount" placeholder="Amount">
                  </div>
				  
                </div>
				
				
				
      </div>
	  <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ./wrapper -->
<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
    <spring:url value="/js/jquery-2.2.3.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script>
<!-- Bootstrap 3.3.6 -->
    <spring:url value="/js/bootstrap.min.js" var="bootstrapminjs"></spring:url>
    <script src="${bootstrapminjs}"></script>
<!-- Select2 -->
    <spring:url value="/js/select2.full.min.js" var="selectminjs"></spring:url>
    <script src="${selectminjs}"></script>
<!-- DataTables -->
    <spring:url value="/js/jquery.dataTables.min.js" var="jquerydataTablesminjs"></spring:url>
    <script src="${jquerydataTablesminjs}"></script>
     <spring:url value="/js/dataTables.bootstrap.min.js" var="dataTablesbootstrapminjs"></spring:url>
    <script src="${dataTablesbootstrapminjs}"></script>
<!-- SlimScroll -->
    <spring:url value="/js/jquery.slimscroll.min.js" var="jqueryslimscrollminjs"></spring:url>
    <script src="${jqueryslimscrollminjs}"></script>
<!-- bootstrap datepicker -->
    <spring:url value="/js/bootstrap-datepicker.js" var="bootstrapdatepickerjs"></spring:url>
    <script src="${bootstrapdatepickerjs}"></script>
    <spring:url value="/js/app.min.js" var="appminjs"></spring:url>
    <script src="${appminjs}"></script>
    
    <script type="text/javascript">
    
    $(document).ready(function() {
    $('#searchbySession').change(function(){	
    	var sessionDropdown=$('#searchbySession').val();
		 if(sessionDropdown!=''){
			 $.ajax({
	    	    type: "POST",
	    	    url: "searchBySessionForDetails", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(sessionDropdown), //json object or array of json objects
	    	    success: function(result) {
	    	    	 if(result=="ERROR"){
	    	    		 alert("Unable To Get Session By Head Details..Please try later");
	    	    	 }else{
	    	    		window.location.href = "showManageDetails"; 
	    	    	} 
	    	    },
	    	    error: function(result){
   	    		 alert("Unable To Get Session By Head Details..Please try later");
	    	    }
	        })
		 }else{
		    alert("Please Select a session to search the head details");	 
		 }
	});
    
    $('#checkboxforsearch').change(function(){	
         var subjectList = '${config}';	    	
    	if(subjectList=='+2'){	 
    		if($(this).is(':checked')){
    			<c:forEach items="${applicationConfig}" var="applicationConfig">
    			<c:if test="${applicationConfig eq 'Science'}">
    			     $("#21stscience").show();
    			     $("#22ndscience").show();
    			</c:if>
    			<c:if test="${applicationConfig eq 'Arts'}">
    			    $("#22ndarts").show();
    			    $("#21starts").show();
    		    </c:if>
    		    <c:if test="${applicationConfig eq 'Commerce'}">
    		        $("#21stcommerce").show();
    			    $("#22ndcommerce").show();
    		    </c:if>
    			</c:forEach>
    		}else{
    			 $("#21stscience").hide();
    			 $("#22ndscience").hide();
    			 $("#22ndarts").hide();
    			 $("#21starts").hide();
    			 $("#21stcommerce").hide();
    			 $("#22ndcommerce").hide();
    		}
    	}else if(subjectList=='UG'){
    		if($(this).is(':checked')){
    			<c:forEach items="${applicationConfig}" var="applicationConfig">
    			<c:if test="${applicationConfig eq 'Science'}">
    			     $("#31stscience").show();
    			     $("#32ndscience").show();
    			     $("#33ndscience").show();
    			</c:if>
    			<c:if test="${applicationConfig eq 'Arts'}">
    			    $("#32ndarts").show();
    			    $("#31starts").show();
    			    $("#33ndarts").show();
    		    </c:if>
    		    <c:if test="${applicationConfig eq 'Commerce'}">
    		        $("#31stcommerce").show();
    			    $("#32ndcommerce").show();
    		    </c:if>
    			</c:forEach>
    		}else{
    			 $("#31stscience").hide();
    			 $("#32ndscience").hide();
    			 $("#32ndarts").hide();
    			 $("#31starts").hide();
    			 $("#31stcommerce").hide();
    			 $("#32ndcommerce").hide();
    			 $("#33ndscience").hide();
    			 $("#33ndarts").hide();
    		}
    	}else{
    		
    	}
		$("#searchCategory").prop("disabled", $(this).is(':checked'));
	});
    
    
    
	
    $("#searchCategory").change(function(){	
   	 var muultiselect=$('#searchCategory').val();
   	 var subjectList = '${config}';
   	 $("#21stscience").hide();
   	 $("#22ndscience").hide();
   	 $("#22ndarts").hide();
   	 $("#21starts").hide();
   	 $("#21stcommerce").hide();
   	 $("#22ndcommerce").hide();
   	 $("#31stscience").hide();
   	 $("#32ndscience").hide();
   	 $("#32ndarts").hide();
   	 $("#31starts").hide();
   	 $("#31stcommerce").hide();
   	 $("#32ndcommerce").hide();
    	$("#33ndscience").hide();
    	$("#33ndarts").hide();
   	 if(muultiselect!=null){
            if(subjectList=='+2'){
           	 for (var i=0; i<muultiselect.length; i++) {
       			 switch(muultiselect[i]) {
       			    case 'firstTable':
       			    	$("#21stscience").show();
       			        break;
       			    case 'secondTable':
       			    	$("#22ndscience").show();
       			        break;
       			    case 'seventhTable':
 			    	    $("#33ndscience").show();
 			            break;       
       			    case 'thirdTable':
       			    	$("#21starts").show();
       			        break;
       			    case 'fourthTable':
       			    	$("#22ndarts").show();
       			        break;
       			     case 'eigthTable':
 			    	    $("#33ndarts").show();
 			            break;  
       			    case 'fifthTable':
       			    	 $("#21stcommerce").show();
       			        break;
       			    case 'sixthTable':
       			    	$("#22ndcommerce").show();
       			        break;    
       			}
       		 }		 
            }else if(subjectList=='UG'){
           	 for (var i=0; i<muultiselect.length; i++) {
       			 switch(muultiselect[i]) {
       			    case 'firstTable':
       			    	$("#31stscience").show();
       			        break;
       			    case 'secondTable':
       			    	$("#32ndscience").show();
       			        break;
       			    case 'thirdTable':
       			    	$("#31starts").show();
       			        break;
       			    case 'fourthTable':
       			    	$("#32ndarts").show();
       			        break;
       			    case 'fifthTable':
       			    	 $("#31stcommerce").show();
       			        break;
       			    case 'sixthTable':
       			    	$("#32ndcommerce").show();
       			        break;  
       			    case 'seventhTable':
 			    	    $("#33ndscience").show();
 			            break;    
       			    case 'eigthTable':
 			    	    $("#33ndarts").show();
 			            break;    
       			}
       		 }
            }else{
           	 
            }
   		
   	 }else{
   	 }
    	});   
    }); 
    
    
    
    </script>
    
    
    
    
<script>
//Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
</script>
<script>
  $(function () {
   //Initialize Select2 Elements
    $(".select2").select2();

    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

</body>
</html>
