<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<%--   <link rel="stylesheet" href="<c:url value="/css/common/bootstrap.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/font-awesome.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/ionicons.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/custom.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/blue.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/skin-blue.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/select2.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/morris.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/jquery-jvectormap-1.2.2.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/datepicker3.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap3-wysihtml5.min.css"/>" />
   --%>
  <spring:url value="/img" var="images" />




</head>
<body>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="active"><a href="showHome"><i class="fa fa-home"></i> <span>Home</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>On line Application</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="#"><i class="fa fa-circle-o"></i>Admission</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Pre Admission</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Cancel Admission</a></li>
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Add Student</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="showStudent"><i class="fa fa-circle-o"></i>Student</a></li>
            <li class="active"><a href="showUploadStudent"><i class="fa fa-circle-o"></i>Upload Student Data</a></li>
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i> <span>Transactions</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="showAdmission"><i class="fa fa-circle-o"></i>Admission/Re-Admission</a></li>
            <li class="active"><a href="showAdditionalFee"><i class="fa fa-circle-o"></i>Additional Fee/Fine</a></li>
			<li class="active"><a href="showRefund"><i class="fa fa-circle-o"></i>Refund/Rejection</a></li>
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i> <span>Reports</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           <li class="active"><a href="showMasterReports"><i class="fa fa-circle-o"></i>Master Reports</a></li>
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>E-Document</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="#"><i class="fa fa-circle-o"></i>Fee Structure</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>File</a></li>			
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Analytic</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <c:if test="${userName eq 'admin1' or userName eq 'admin2'}">
            <li class="active"><a href="showManageHead"><i class="fa fa-circle-o"></i>Head Manage</a></li>
            </c:if>
            <li class="active"><a href="showManageDetails"><i class="fa fa-circle-o"></i>Head Details</a></li>
            			
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Treasury</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="showCollection"><i class="fa fa-circle-o"></i>Collection</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Expense</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i>Balance</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i>Ledger Balance</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i>Denomination</a></li>
          </ul>
        </li>
        <c:if test="${userName eq 'admin1' or userName eq 'admin2'}">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Administration</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="#"><i class="fa fa-circle-o"></i>User Management</a></li>
            <li class="active"><a href="#" data-toggle="modal" data-target="#configModal"><i class="fa fa-circle-o"></i>Configuration</a></li>			
          </ul>
        </li>
        </c:if>
		
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

<div class="modal fade" id="configModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title">Configuration</h4>
      </div>
      <div class="modal-body">
        <div class="row">
		<div class="col-sm-6 form-group">
		<label>Add Course</label>
		 <select class="form-control select2" multiple="multiple" data-placeholder="Select a Stream" style="width: 100%;" id="confgCourse">
                  <option>+2</option>
                  <option>UG</option>
                  <option>PG</option>
         </select>
		</div>
		<div class="col-sm-6">
		<label>Add Stream</label>
		 <select class="form-control select2" multiple="multiple" data-placeholder="Select a Stream" style="width: 100%;" id="confgStream">
                  <option>Science</option>
                  <option>Arts</option>
                  <option>Commerce</option>
         </select>
		</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="confgOk">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

    <spring:url value="/js/jquery-2.2.3.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script>
    
<script type="text/javascript">
 
 $(document).ready(function() {
 
 $(document).on('click' , '#confgOk' , function(){
   if($("#confgCourse").val()==null){
	   alert("We will select all the course for configuration...Because you haven't selected any course from the dropdown");
   }
   if($("#confgStream").val()==null){
	   alert("We will select all the stream for configuration...Because you haven't selected any stream from the deopdown");
   }
	 
	 var formDetailsJson={};
	    formDetailsJson={
			"configCourse": $("#confgCourse").val(),
			"configStream": $("#confgStream").val(),
	    }
	             $.ajax({
     		    	    type: "POST",
     		    	    url: "saveConfigurationDetails", //BookInfoyour valid url
     		    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
     		    	    processData: false,
     		    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
     		    	    success: function(result) {
     		    	    	 if(result=='SUCCESS'){
     		    	    		alert("Configuration Done....");
     		    	    		$("#configModal").modal('hide');
     		    	    	 }else{
     		    	    		alert("Error While Configuration");
     		    	    	 }
     		    	    	 
     		    	    },
     		    	    error: function(result){
     		    	    	alert("Unable To Save Student At This Time");
     		    	    }
     		    	     });
         	
      	    return false;
 });
 
 });
 
 
 </script>
 
 
 



</body>
</html>