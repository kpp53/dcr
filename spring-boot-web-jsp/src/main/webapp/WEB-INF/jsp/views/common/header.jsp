<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<%--    <link rel="stylesheet" href="<c:url value="/css/common/bootstrap.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/font-awesome.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/ionicons.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/custom.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/blue.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/skin-blue.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/morris.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/jquery-jvectormap-1.2.2.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/datepicker3.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap3-wysihtml5.min.css"/>" /> --%>
  <spring:url value="/img" var="images" /> 
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div>
 <header class="main-header">
    <!-- Logo -->
    <a href="index.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>D</b>CR</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Daily</b>Collection <br><span>Register</span> </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
         
          <!-- /.messages-menu -->

          <!-- Notifications Menu -->
         
          <!-- Tasks Menu -->
         
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="${images}/user2-160x160.jpg" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">${userName}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="${images}/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  ${userName}
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          
         
        </ul>
      </div>
    </nav>
  </header>
  </div>
  
  
 
  
  
  
  
  
  
</body>
</html>