<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daily Collection Register | Reports</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/font-awesome.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/ionicons.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/datepicker3.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/dataTables.bootstrap.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/select2.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/custom.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/skin-blue.min.css"/>" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
 <jsp:include page="/WEB-INF/jsp/views/common/header.jsp"></jsp:include>
  <jsp:include page="/WEB-INF/jsp/views/common/menu.jsp"></jsp:include>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Reports
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Reports</a></li>
        <li class="active">Home</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	<!-- Alert message -->
	 <div class="alert alert-success" style="display:none;">
  <strong>Success!</strong> Indicates a successful or positive action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-info" style="display:none;">
  <strong>Info!</strong> Indicates a neutral informative change or action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-warning" style="display:none;">
  <strong>Warning!</strong> Indicates a warning that might need attention.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-danger" style="display:none;">
  <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>
        <!-- Alert message end-->  
		<div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Search Report</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
         <div class="form-group">
                <label>Search By Session</label>
                <select class="form-control" data-placeholder="Select a Stream" style="width: 100%;" id="searchbySession">
                             <option value="2017-18">2017-18</option>
                             <option value="2016-17">2016-17</option>
                             <option value="2015-16">2015-16</option>
                </select>
              </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Search by</label>
                <select class="form-control select2" style="width: 100%;" id="searchBy">
                  <option selected="selected" value="All">All</option>
                  <option value="student">Student</option>
                  <option value="Fund">Fund</option>
                  <option value="CAF">CAF</option>
                  <option value="Miscellaneous">Miscellaneous</option>
                  <option value="DCRBook">DCR Book</option>
                </select>
              </div>
              <div class="form-group">
               <%--  <label>Class</label>
                <select class="form-control" style="width: 100%;" disabled="disabled" id="classDiv">
                 <option value="all">All</option>
                 <c:forEach var="yearBatch" items="${yearBacthMap}">
                 <c:if test="${config eq '+2'}">
                   <c:if test="${yearBatch ne '3rd'}">
                      <option value="${yearBatch}">${yearBatch}</option>
                   </c:if>
                 </c:if>
                 <c:if test="${config ne '+2'}">
                  <option value="${yearBatch}">${yearBatch}</option>
				 </c:if>           
				     </c:forEach>
                </select> --%>
                 <label>Stream:</label> 
               <select class="form-control" style="width: 100%;" disabled="disabled" id="streamDiv">
               <option value="all">All</option>
                    <c:forEach var="stream" items="${streamMap}">
				             <option value="${stream}">${stream}</option>
				     </c:forEach>
                </select>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <!-- Date range -->
              <%--  <label>Stream:</label> 
               <select class="form-control" style="width: 100%;" disabled="disabled" id="streamDiv">
               <option value="all">All</option>
                    <c:forEach var="stream" items="${streamMap}">
				             <option value="${stream}">${stream}</option>
				     </c:forEach>
                </select> --%>
                <label>Date range:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="reservation">
                </div> 
              </div>
              <!-- /.form-group -->
              <div class="form-group">
               <%--  <label>Category</label>
                <select class="form-control select2" style="width: 100%;" disabled="disabled" id="categoryDiv">
                <option value="all">All</option>
                   <c:forEach var="category" items="${categoryMap}">
				             <option value="${category}">${category}</option>
				   </c:forEach>
                </select> --%>
                 <label>Class</label>
                <select class="form-control" style="width: 100%;" disabled="disabled" id="classDiv">
                 <option value="all">All</option>
                 <c:forEach var="yearBatch" items="${yearBacthMap}">
                 <c:if test="${config eq '+2'}">
                   <c:if test="${yearBatch ne '3rd'}">
                      <option value="${yearBatch}">${yearBatch}</option>
                   </c:if>
                 </c:if>
                 <c:if test="${config ne '+2'}">
                  <option value="${yearBatch}">${yearBatch}</option>
				 </c:if>           
				     </c:forEach>
                </select> 
              </div>
              <!-- /.form-group -->
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <!-- <label>Date range:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="reservation">
                </div>  -->
                 <label>Category</label>
                <select class="form-control select2" style="width: 100%;" disabled="disabled" id="categoryDiv">
                <option value="all">All</option>
                   <c:forEach var="category" items="${categoryMap}">
				             <option value="${category}">${category}</option>
				   </c:forEach>
                </select>
                <!-- /.input group -->
              
              </div>
            </div>
            <div class="col-md-6">
             <div class="form-group">
                <label>Fund Type</label>
                <select class="form-control select2" style="width: 100%;" disabled="disabled" id="fundTypeDiv">
                <option value="all">All</option>
                   <c:forEach var="fundType" items="${FundTypeMap}">
				             <option value="${fundType}">${fundType}</option>
				     </c:forEach>
				     <option value="Additional Fee">Additional Fee</option>
				     <option value="Fine">Fine</option>
				      <option value="Old Student Additional Fee">Old Student Additional Fee</option>
                </select>
              </div>
              <div class="form-group">
              <button type="button" class="btn btn-info pull-right" id="searchIdInReport">Search</button>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Select all the above field to get accurate result.
        </div>
      </div>
      <!-- /.box -->
     <c:if test="${allDiv ne null}"> 
      <div class="box"  id="allDiv">
            <div class="box-header">
              <h3 class="box-title pull-left">${reports.searchBy} Session ${reports.session} Reports From ${reports.dateFrom} To ${reports.dateTo}</h3>
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			<table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sl no</th>
                  <th>Type of fund</th>
                  <th>No of student</th>
                  <th>Amount</th>
                  <th>Details</th>
				  
				  </tr>
				  </thead>
				  <c:set var="totalAmt" value="${0}" />
				  <tbody>
				  <c:set var="totalstudent" value="${0}" />
				  <tr>
				  <td>1</td>
				  <td>Admission</td>
				  <td>${admissionData.SCOUNT}</td>
				  <c:set var="totalstudent" value="${totalstudent + admissionData.SCOUNT}" />
				  <c:if test="${admissionData.TCOUNT eq null}">
				   <td>0</td>
				   <c:set var="totalAmt" value="${totalAmt + 0}" />
				  </c:if>
				  <c:if test="${admissionData.TCOUNT ne null}">
				  <td>${admissionData.TCOUNT}</td>
				  <c:set var="totalAmt" value="${totalAmt + admissionData.TCOUNT}" />
				  </c:if>
				  <td><a href="#">Details</a></td>
				  </tr>
				  <tr>
				  <td>2</td>
				  <td>Re Admission</td>
				  <td>${readmissionData.SCOUNT}</td>
				  <c:set var="totalstudent" value="${totalstudent + readmissionData.SCOUNT}" />
				  <c:if test="${readmissionData.TCOUNT eq null}">
				   <td>0</td>
				   <c:set var="totalAmt" value="${totalAmt + 0}" />
				  </c:if>
				  <c:if test="${readmissionData.TCOUNT ne null}">
				  <td>${readmissionData.TCOUNT}</td>
				  <c:set var="totalAmt" value="${totalAmt + readmissionData.TCOUNT}" />
				  </c:if>
				  <td><a href="#">Details</a></td>
				  </tr>
				  <tr>
				  <td>3</td>
				  <td>CAF</td>
				  <td>${allcafData.SCOUNT}</td>
				  <c:set var="totalstudent" value="${totalstudent + allcafData.SCOUNT}" />
				  <c:if test="${allcafData.TCOUNT eq null}">
				   <td>0</td>
				   <c:set var="totalAmt" value="${totalAmt + 0}" />
				  </c:if>
				  <c:if test="${allcafData.TCOUNT ne null}">
				  <td>${allcafData.TCOUNT}</td>
				  <c:set var="totalAmt" value="${totalAmt + allcafData.TCOUNT}" />
				  </c:if>
				  <td><a href="#">Details</a></td>
				  </tr>
				  <tr>
				  <td>4</td>
				  <td>Additional Fee</td>
				  <td>${allAdditionalFeeReport.SCOUNT}</td>
				  <c:set var="totalstudent" value="${totalstudent + allAdditionalFeeReport.SCOUNT}" />
				  <c:if test="${allAdditionalFeeReport.TCOUNT eq null}">
				   <td>0</td>
				   <c:set var="totalAmt" value="${totalAmt + 0}" />
				  </c:if>
				  <c:if test="${allAdditionalFeeReport.TCOUNT ne null}">
				  <td>${allAdditionalFeeReport.TCOUNT}</td>
				  <c:set var="totalAmt" value="${totalAmt + allAdditionalFeeReport.TCOUNT}" />
				  </c:if>
				  <td><a href="#">Details</a></td>
				  </tr>
				  <tr>
				  <td>5</td>
				  <td>Fine</td>
				  <td>${allFinereport.SCOUNT}</td>
				  <c:set var="totalstudent" value="${totalstudent + allFinereport.SCOUNT}" />
				  <c:if test="${allFinereport.TCOUNT eq null}">
				   <td>0</td>
				   <c:set var="totalAmt" value="${totalAmt + 0}" />
				  </c:if>
				  <c:if test="${allFinereport.TCOUNT ne null}">
				  <td>${allFinereport.TCOUNT}</td>
				  <c:set var="totalAmt" value="${totalAmt + allFinereport.TCOUNT}" />
				  </c:if>
				  <td><a href="#">Details</a></td>
				  </tr>
				   <tr>
				  <td>6</td>
				  <td>Old Student</td>
				  <td>${allAdditionalFeeReportForOldStudent.SCOUNT}</td>
				  <c:set var="totalstudent" value="${totalstudent + allAdditionalFeeReportForOldStudent.SCOUNT}" />
				  <c:if test="${allAdditionalFeeReportForOldStudent.TCOUNT eq null}">
				   <td>0</td>
				   <c:set var="totalAmt" value="${totalAmt + 0}" />
				  </c:if>
				  <c:if test="${allAdditionalFeeReportForOldStudent.TCOUNT ne null}">
				  <td>${allAdditionalFeeReportForOldStudent.TCOUNT}</td>
				  <c:set var="totalAmt" value="${totalAmt + allAdditionalFeeReportForOldStudent.TCOUNT}" />
				  </c:if>
				  <td><a href="#">Details</a></td>
				  </tr>
				  
				  
				  
				  </tbody>
				   <tfoot>
                <tr>
                 <th></th>
                  <th>TOTAL</th>
                  <th>${totalstudent}</th>
                  <th>${totalAmt}</th>
                  <th></th>
				  </tr>
				  </tfoot>
				  </table>
				  <br><br>
	           <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sl no</th>
                  <th>Type of fund</th>
                  <th>No of Particulars</th>
                  <th>Amount</th>
                  <th>Details</th>
				  
				  </tr>
				  </thead>
				  <tbody>
				  <tr>
				  <c:set var="totalmisAmt" value="${0}" />
				  <td>1</td>
				  <td>Miscellaneous</td>
				  <td>${misData.SCOUNT}</td>
				  <c:if test="${misData.TCOUNT eq null}">
				   <td>0</td>
				   <c:set var="totalmisAmt" value="${totalmisAmt + 0}" />
				  </c:if>
				  <c:if test="${misData.TCOUNT ne null}">
				  <td>${misData.TCOUNT}</td>
				  <c:set var="totalmisAmt" value="${totalmisAmt + misData.TCOUNT}" />
				  </c:if>
				  <td><a href="#">Details</a></td>
				  </tr>
				  </tbody>
				   <tfoot>
                <tr>
                  <th></th>
                  <th>GRAND TOTAL</th>
                  <th></th>
                  <th>${totalmisAmt + totalAmt}</th>
                  <th></th>
				  </tr>
				  </tfoot>
				  </table>
				  <br><br>
				<table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sl no</th>
                  <th>Type of fund</th>
                  <th>No of Student</th>
                  <th>Amount</th>
                  <th>Details</th>
				  </tr>
				  </thead>
				  <tbody>
				   <tr>
				   <c:set var="totalrefstudent" value="${0}"></c:set>
				  <td>1</td>
				  <td>Refund</td>
				  <td>${allRefundreport.SCOUNT}</td>
				  <c:if test="${allRefundreport.TCOUNT eq null}">
				   <td>0</td>
				   <c:set var="totalrefstudent" value="${totalrefstudent + 0}" />
				  </c:if>
				  <c:if test="${allRefundreport.TCOUNT ne null}">
				  <td>${allRefundreport.TCOUNT}</td>
				  <c:set var="totalrefstudent" value="${totalrefstudent + allRefundreport.TCOUNT}" />
				  </c:if>
				  <td><a href="#">Details</a></td>
				  </tr>
				  
				  </tbody>
				   <tfoot>
                <tr>
                  <th></th>
                  <th>GRAND TOTAL</th>
                  <th>${allRefundreport.SCOUNT}</th>
                  <th>${totalrefstudent}</th>
                  <th></th>
				  </tr>
				  </tfoot>
				  </table>
				  
				  
				  
				  
				  
				  
				  
              <div class="box-footer">
                <button type="submit" class="btn btn-default" id="downloadExcel">Download Excel</button>
				<button type="submit" class="btn btn-default" id="downloadPdf">Download PDF</button>
                <button type="submit" class="btn btn-info pull-right" id="Print">Print</button>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
    </c:if>
    <c:if test="${fundtypeblock ne null}"> 
      <div class="box"  id="fund">
            <div class="box-header">
              <h3 class="box-title pull-left">${reports.searchBy} Session ${reports.session} Reports From ${reports.dateFrom} To ${reports.dateTo}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <c:if test="${headDetailsMap ne null}">
			<table class="table table-bordered table-striped">
                
                <thead>
                <tr>
                  <th>Sl no</th>
                  <th>Type of fund</th>
                  <th>Amount</th>
                  <th>No of student</th>
                  <th>Details</th>
				  </tr>
				  </thead>
				  <tbody>
				  <c:set var="totalAmt" value="${0}" />
				  
				  <%-- <c:forEach var="studentCountList" items="${studentCountList}"> --%>
				  <c:forEach var="headDetails" items="${headDetailsMap}" varStatus="loop">
				   <c:set var="totalAmt" value="${totalAmt + headDetails.headAmount}" /> 
				  <tr>
					  <td>${loop.index+1}</td>
					  <td>${headDetails.headname}</td>
				  <td>${headDetails.headAmount}</td>
				  <td><a href="#">${headDetails.studentCount}</a></td>
				  <td><a href="#">Details</a></td>
				  </tr>
				  </c:forEach>
				  <%-- </c:forEach> --%>
				  </tbody>
				   <tfoot>
                <tr>
                 <th></th>
                  <th>TOTAL</th>
                  <th>${totalAmt}</th>
                  <th></th>
                  <th></th>
				  </tr>
				  </tfoot>
				  </table>
				  </c:if>
				  <c:if test="${headDetailsMap eq null}">
				     <label>No Result Found For This Search</label>
				  </c:if>
				  <br><br>
			<c:if test="${headDetailsMap ne null}">	  
              <div class="box-footer">
                <button type="submit" class="btn btn-default" id="downloadExcel">Download Excel</button>
				<button type="submit" class="btn btn-default">Download PDF</button>
                <button type="submit" class="btn btn-info pull-right">Print</button>
              </div>
              </c:if>
            </div>
            <!-- /.box-body -->
          </div>
    </c:if>
    <c:if test="${studentblock ne null}">
     <div class="box">
            <div class="box-header">
              <h3 class="box-title pull-left">${reports.searchBy} Session ${reports.session} Reports From ${reports.dateFrom} To ${reports.dateTo}</h3>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
         <%-- url::<input type="text" value="${url}"/> --%>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>SL No.</th>
                  <th>Session</th>
                  <th>DOA</th>
                  <th>Student Name</th>
                  <th>Roll No</th>
                  <th>Receipt No.</th>
                  <th>Amount</th>
                  <th>Status</th>
				  <th>Details</th>
                </tr>
                </thead>
                <tbody>
             <c:if test="${studentListreport ne ''}">   
            <c:forEach items="${studentListreport}" var="studentListreport" varStatus="loop">   
                <tr>
                  <td id="name${loop.index}">${loop.index+1}</td>
                  <td id="name${loop.index}">${studentListreport.session}</td>
                  <td id="name${loop.index}">${studentListreport.created_Date}</td>
                  <td id="name${loop.index}">${studentListreport.name}</td>
                  <td id="rollno${loop.index}" style="color:#0000FF">${studentListreport.rollNo}</td>
                  <td id="dob${loop.index}">${studentListreport.receiptNo}</td>
                  <td id="dob${loop.index}">${studentListreport.totalAmount}</td>
                  <td id="dob${loop.index}">${studentListreport.status}</td>
                  <td><a href="javascript:void()" class="more${loop.index}" id="moreDetails">More Details</a></td>
                </tr>
                 </c:forEach>
                 </c:if>
                 </tbody>
              </table>
            </div>
         
            <!-- /.box-body -->
            
              <div class="box-footer">
                <button type="submit" class="btn btn-default" id="downloadExcel">Download Excel</button>
				<button type="submit" class="btn btn-default">Download PDF</button>
                <button type="submit" class="btn btn-info pull-right">Print</button>
              </div>
          </div>
    
    
    </c:if>
    
    <c:if test="${dcrBookBlock ne null}">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title pull-left">${reports.searchBy} Session ${reports.session} Reports From ${reports.dateFrom} To ${reports.dateTo}</h3>
              <div class="clearfix"></div>
            </div>
            <c:if test="${dcrBookResponse ne null}">
               <div class="box-footer">
                <button type="submit" class="btn btn-default" id="downloadExcel">Download Excel</button>
              </div>
            </c:if>
        </div>    
    </c:if>
    
    
    
    
    
    </section>
    <!-- /.content -->
	<!-- Main Footer -->
  <jsp:include page="/WEB-INF/jsp/views/common/footer.jsp"></jsp:include>
  </div>
  <!-- /.content-wrapper -->

  

  
  
</div>


<div class="modal fade" id="myhelpModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Help</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="dataconfmodal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Student Details Data</h4>
      </div>
      <div class="modal-body" style="height:400px; overflow-y:scroll;">
        <div class="table-responsive">
  <table class="table">
  <tbody>
  <tr>
  <td>Name ::</td>
  <td id="Pname"></td>
  </tr>
  <tr>
  <td>RollNo ::</td>
  <td id="ProllNo" style="color: #0000FF"></td>
  </tr>
  <tr>
  <td>Receipt No ::</td>
  <td id="PreceiptNo" style="color: #0000FF"></td>
  </tr>
   <tr>
  <td>CollegeCode/MRIN No ::</td>
  <td id="cCode"></td>
  </tr>
   <tr>
  <td>Date Of Admission ::</td>
  <td id="dAdm"></td>
  </tr>
  <tr>
  <td>Date Of Refund ::</td>
  <td id="dfAdm"></td>
  </tr>
  <tr>
  <td>Session ::</td>
  <td id="Psession"></td>
  </tr>
  
  <tr>
  <td>Date Of Birth ::</td>
  <td id="Pdob"></td>
  </tr>
  <tr>
  <td>Stream ::</td>
  <td id="PStream"></td>
  </tr>
  <tr>
  <td>Science Type ::</td>
  <td id="PScienceType" style="color: #0000FF"></td>
  </tr> 
  <tr>
  <td>Class ::</td>
  <td id="PClass"></td>
  </tr>
  <tr>
  <td>Category ::</td>
  <td id="PCategory"></td>
  </tr>
  <tr>
  <td>Date Of CAF Submission ::</td>
  <td id="PCafSub"></td>
  </tr>
  <tr>
  <td>Fund Type ::</td>
  <td id="PfundType"></td>
  </tr>
  <tr>
  <td>Status ::</td>
  <td id="Pstatus" style="color: #0000FF"></td>
  </tr>
  <tr>
  <td>Payment Mode ::</td>
  <td id="PpaymentMode"></td>
  </tr>
  <tr>
  <td>Admission Amount ::</td>
  <td id="PAdmAm"></td>
  </tr>
  <tr>
  <td>CAF Amount ::</td>
  <td id="PCAFAm"></td>
  </tr>
  <tr>
  <td>Total Amount ::</td>
  <td id="PtotalAm"></td>
  </tr>
  
  
  </tbody>
   
  </table>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="EditStudent">Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--Admission popup-->

<!-- ./wrapper -->
<!--Quick Admission popup-->

<!-- ./wrapper -->
<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
 <spring:url value="/js/jquery-2.2.3.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script>
<!-- Bootstrap 3.3.6 -->
    <spring:url value="/js/bootstrap.min.js" var="bootstrapminjs"></spring:url>
    <script src="${bootstrapminjs}"></script>
    <spring:url value="/js/select2.full.min.js" var="selectminjs"></spring:url>
    <script src="${selectminjs}"></script>
<!-- date-range-picker -->
     <spring:url value="/js/moment.min.js" var="momentminjs"></spring:url>
    <script src="${momentminjs}"></script>
  <spring:url value="/js/daterangepicker.js" var="daterangepickerjs"></spring:url>
    <script src="${daterangepickerjs}"></script>
<!-- DataTables -->
 <spring:url value="/js/jquery.dataTables.min.js" var="jquerydataTablesminjs"></spring:url>
    <script src="${jquerydataTablesminjs}"></script>
    <spring:url value="/js/dataTables.bootstrap.min.js" var="dataTablesbootstrapminjs"></spring:url>
    <script src="${dataTablesbootstrapminjs}"></script>
<!-- SlimScroll -->
    <spring:url value="/js/jquery.slimscroll.min.js" var="jqueryslimscrollminjs"></spring:url>
    <script src="${jqueryslimscrollminjs}"></script>
<!-- bootstrap datepicker -->
   <spring:url value="/js/bootstrap-datepicker.js" var="bootstrapdatepickerjs"></spring:url>
    <script src="${bootstrapdatepickerjs}"></script>
      <spring:url value="/js/app.min.js" var="appminjs"></spring:url>
    <script src="${appminjs}"></script>


<script>
  $(function () {
  //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
  //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'dd/mm/yyyy'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	
	$(document).on('click' , '#downloadExcel' , function(){
		
		var from='${reports.searchBy}';
		 var url='${print_url}';
		var win = window.open(url+"/downloadReport.xls?from="+from, "_self"); 
		
		  /* $.ajax({
    	    type: "POST",
    	    url: "downloadReport.xls", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(from), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result==''){
    	    		 alert("Success");
    	    		// alert("Unable To Show Student Details At This Time");
    	    	 }else{
    	    	
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To find the amount withe the combination of inputs...Please manage the head before doing transaction");
    	    }
    	     });  */
	});
	
	
	
	 $('#searchBy').change(function(){	
		 var searchBy=$("#searchBy").val();
		 if(searchBy=='All'){
			 $("#classDiv").prop("disabled",true);
			 $("#streamDiv").prop("disabled",true);
			 $("#categoryDiv").prop("disabled",true);
			 $("#fundTypeDiv").prop("disabled",true);
		 }else{
			 $("#classDiv").prop("disabled",false);
			 $("#streamDiv").prop("disabled",false);
			 $("#categoryDiv").prop("disabled",false);
			 $("#fundTypeDiv").prop("disabled",false);
		 }
		 
	  });
	 
	 $(document).on('click' , '#moreDetails' , function(){
	    	var saveId= $(this).prop('class');
	    	savedId=saveId.substring(4,saveId.length);
	    	 $.ajax({
		    	    type: "POST",
		    	    url: "getStudentDetails", //BookInfoyour valid url
		    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
		    	    processData: false,
		    	    data: JSON.stringify(savedId), //json object or array of json objects
		    	    success: function(result) {
		    	    	 if(result==''){
		    	    		 alert("Unable To Show Student Details At This Time");
		    	    	 }else{
		    	    		 $('#Pname').html(result.name);
		    	    		 $('#ProllNo').html(result.rollNo);
		    	    		 $('#cCode').html(result.collegeCode+'/'+result.mrNo);
		    	    		 $('#dAdm').html(result.created_Date);
		    	    		 $('#dfAdm').html(result.dateOfRefund);
		    	    		 //$('#PCafSub').html(result.rollNo);
		    	    		 $('#Pstatus').html(result.status);
		    	    		 $('#PAdmAm').html(result.totalAmount);
		    	    		// $('#PCAFAm').html(result.rollNo);
		    	    		 $('#PtotalAm').html(result.totalAmount);
		    	    		 $('#Pdob').html(result.dob);
		    	    		 $('#PfundType').html(result.fundType);
		    	    		 $('#Psession').html(result.session);
		    	    		 $('#PCourse').html(result.course);
		    	    		 $('#PStream').html(result.stream);
		    	    		 $('#PClass').html(result.yearBatch);
		    	    		 $('#PCategory').html(result.category);
		    	    		 $('#PreceiptNo').html(result.receiptNo);
		    	    		 $('#PpaymentMode').html(result.paymentModeDrop);
		    	    		 $('#PScienceType').html(result.scienceSelect);
		    	    		 $("#dataconfmodal").modal('show');
		    	    	} 
		    	    },
		    	    error: function(result){
		    	    	alert("Unable To find the amount withe the combination of inputs...Please manage the head before doing transaction");
		    	    }
		    	     });
	    });
	
	
	
	

	
	
	$(document).on('click' , '#searchIdInReport' , function(){
	var session=$("#searchbySession").val();
	var searchBy=$("#searchBy").val();
	var dateRange=$("#reservation").val();
	var dateRangeArray=dateRange.split("-");
	var formDetailsJson={};
	if(searchBy=='Fund'){
		 var classType=$("#classDiv").val();
		 var stream=$("#streamDiv").val();
		 var category=$("#categoryDiv").val();
		 var fundType=$("#fundTypeDiv").val();
		 formDetailsJson={
					"session":session,
					"searchBy": searchBy,
					"dateFrom": dateRangeArray[0],
					"dateTo": dateRangeArray[1],
					"classType": classType,
					"stream": stream,
					"category": category,
					"fundType": fundType
			    }
		 $.ajax({
	    	    type: "POST",
	    	    url: "reportFundSearch", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
	    	    success: function(result) {
	    	    	window.location.href = "showMasterReports";
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Get Fund Report AT This Moment");
	    	    }
	    	     });
		
		
	}else if(searchBy=='student'){
		 var classType=$("#classDiv").val();
		 var stream=$("#streamDiv").val();
		 var category=$("#categoryDiv").val();
		 var fundType=$("#fundTypeDiv").val();
		 formDetailsJson={
					"session":session,
					"searchBy": searchBy,
					"dateFrom": dateRangeArray[0],
					"dateTo": dateRangeArray[1],
					"classType": classType,
					"stream": stream,
					"category": category,
					"fundType": fundType
			    }
		 $.ajax({
	    	    type: "POST",
	    	    url: "reportStudentSearch", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
	    	    success: function(result) {
	    	    	window.location.href = "showMasterReports";
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Get Student Report At This Moment");
	    	    }
	    	     });
	}
	else if(searchBy=='DCRBook'){
		 var classType=$("#classDiv").val();
		 var stream=$("#streamDiv").val();
		 var category=$("#categoryDiv").val();
		 var fundType=$("#fundTypeDiv").val();
		 formDetailsJson={
					"session":session,
					"searchBy": searchBy,
					"dateFrom": dateRangeArray[0],
					"dateTo": dateRangeArray[1],
					"classType": classType,
					"stream": stream,
					"category": category,
					"fundType": fundType
			    }
		 $.ajax({
	    	    type: "POST",
	    	    url: "reportDCRBookSearch", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
	    	    success: function(result) {
	    	    	window.location.href = "showMasterReports";
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Get DCR Book At This Moment");
	    	    }
	    	     });
	}
	
	
	
	else{
		    formDetailsJson={
				"session":session,
				"searchBy": searchBy,
				"dateFrom":dateRangeArray[0],
				"dateTo": dateRangeArray[1]
		    }
		    $.ajax({
	    	    type: "POST",
	    	    url: "reportAllSearch", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
	    	    success: function(result) {
	    	    	window.location.href = "showMasterReports";
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Get All Report AT This Moment");
	    	    }
	    	     });
	} 
 	  
});
	function getFormattedDate(input){
	    var pattern=/\//;
	    var result = input.replace(pattern,function(match,p1,p2,p3){
	        var months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	        return (p2<10?+p2:p2)+"-"+months[(p1-1)]+"-"+p3;
	    });
	   return result;
	  } 	     
});

</script>



</body>
</html>
