<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daily Collection Register | Student</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/font-awesome.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/ionicons.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/datepicker3.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/select2.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/dataTables.bootstrap.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/custom.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/skin-blue.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  
  
  
 <%--  <%
    String url=;
  request.setAttribute("url", url); 
  %> --%>
  
  
  
  
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
    <jsp:include page="/WEB-INF/jsp/views/common/header.jsp"></jsp:include>
    <jsp:include page="/WEB-INF/jsp/views/common/menu.jsp"></jsp:include>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Refund/Rejection
        <small>Transactions</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Transactions</a></li>
        <li class="active">Refund/Rejection</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	<!-- Alert message -->
	 <div class="alert alert-success" style="display:none;">
  <strong>Success!</strong> Indicates a successful or positive action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-info" style="display:none;">
  <strong>Info!</strong> Indicates a neutral informative change or action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-warning" style="display:none;">
  <strong>Warning!</strong> Indicates a warning that might need attention.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-danger" style="display:none;">
  <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>
        <!-- Alert message end-->  
      <div class="box">
           <!--  <div class="box-header">
              <h3 class="box-title pull-left">Additional Fee/Fine</h3>
              <h3 class="box-title pull-right"><a href="#" data-toggle="modal" data-target="#quickadmissionmodal">Add Student</a></h3>
              <div class="clearfix"></div>
            </div> -->
            <!-- /.box-header -->
         <%-- url::<input type="text" value="${url}"/> --%>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Roll No</th>
                  <th>DOB</th>
                  <th>Fund Type</th>
                  <th>Session</th>
				  <th>Course</th>
				  <th>Stream</th>
                  <th>Class</th>
				  <th>Category</th>
				  <th></th>
				  <th></th>
                </tr>
                </thead>
                <tbody>
             <c:if test="${listStudent ne ''}">   
            <c:forEach items="${listStudent}" var="listStudent" varStatus="loop">   
                
                <tr>
                  <td id="name${loop.index}">${listStudent.name}</td>
                  <td id="rollno${loop.index}" style="color:#0000FF">${listStudent.rollNo}</td>
                  <td id="dob${loop.index}">${listStudent.dob}</td>
                  <%--  <c:if test="${(listStudent.adfStatus eq null or listStudent.adfStatus eq ' ')  or listStudent.adfStatus eq 'N'}">
                  <td>
				    <form:select  path="student.fundType" name="student.fundType" class="form-control" style="width: 100%;" id="fundType${loop.index}">
				    <option value="select">Select</option>
				     <c:forEach var="fundType" items="${FundTypeMap}">
				             <option value="${fundType}" ${fundType == listStudent.fundType ? 'selected="selected"' : ''} >${fundType}</option>
				     </c:forEach>
                   </form:select>
				  </td>
				 
                  <td>
				 <form:select  path="student.session" name="student.session" class="form-control" style="width: 100%;"  id="session${loop.index}">
				     <c:forEach var="session" items="${sessionMap}">
				             <option value="${session}" ${session == listStudent.session ? 'selected="selected"' : ''} >${session}</option>
				     </c:forEach>
                   </form:select>
				  </td>
				   <td>
				   <form:select  path="student.course" name="student.course" class="form-control" style="width: 100%;" id="course${loop.index}">
				     <c:if test="${listStudent.course ne null}">
				       <option value="${listStudent.course}"  selected="selected">${listStudent.course}</option>
				    </c:if>
				     <c:forEach var="course" items="${courseMap}">
				             <option value="${course}">${course}</option>
				     </c:forEach>
                   </form:select>
				  </td>
                  <td>
				   <form:select  path="student.stream" name="student.stream" class="form-control" style="width: 100%;" id="stream${loop.index}">
				      <option value="select">Select</option>
				      <c:if test="${listStudent.stream ne null}">
				       <option value="${listStudent.stream}"  selected="selected">${listStudent.stream}</option>
				    </c:if>
				   
				     <c:forEach var="stream" items="${streamMap}">
				             <option value="${stream}">${stream}</option>
				     </c:forEach>
                   </form:select>
				  </td>
                  <td>
               <form:select  path="student.yearBatch" name="student.yearBatch" class="form-control" style="width: 100%;" id="yearBatch${loop.index}">
				     <option value="select">Select</option>
				     <c:forEach var="yearBatch" items="${yearBacthMap}">
				             <c:if test="${config eq '+2'}">
                               <c:if test="${yearBatch ne '3rd'}">
                                  <option value="${yearBatch}" ${yearBatch == listStudent.yearBatch ? 'selected="selected"' : ''} >${yearBatch} Year</option>
                              </c:if>
                            </c:if> 
                             <c:if test="${config ne '+2'}">
                                  <option value="${yearBatch}" ${yearBatch == listStudent.yearBatch ? 'selected="selected"' : ''} >${yearBatch} Year</option>
				             </c:if> 
				     </c:forEach>
                   </form:select>
				  
				  </td>
				  <td>
				 <form:select  path="student.category" name="student.category" class="form-control" style="width: 100%;" id="category${loop.index}">
				    <option value="select">Select</option>
				     <c:forEach var="category" items="${categoryMap}">
				             <option value="${category}" ${category == listStudent.category ? 'selected="selected"' : ''} >${category}</option>
				     </c:forEach>
                   </form:select>
				  </td>
				  </c:if> --%>
				   <c:if test="${listStudent.refStatus ne ''}">
				   
				   <c:if test="${listStudent.refStatus eq '0'}">
				   <td id="fundType${loop.index}">
				   <select >
				   <option value="select">Select</option>
				       <c:if test="${listStudent.yearBatch eq '1st'}">
				          <option value="refund">Refund</option>
				          <option value="rejection">Rejection</option>       
				       </c:if>
				       <c:if test="${listStudent.yearBatch eq '2nd' or listStudent.yearBatch eq '3rd'}">
				          <option value="rejection">Rejection</option>       
				       </c:if>
				   </select>
				  </td>
				  </c:if>

                <c:if test="${listStudent.refStatus ne '0'}">
				   <td id="fundType${loop.index}">
				     ${listStudent.fundType}
				   </td>
				</c:if>



				  
                  <td id="session${loop.index}">
				             ${listStudent.session}
				  </td>
				   <td id="course${loop.index}">
				     <c:if test="${listStudent.course ne null}">
				       ${listStudent.course}
				    </c:if>
				  </td>
                  <td id="stream${loop.index}">
				     <c:if test="${listStudent.stream ne null or listStudent.stream ne ''}">
				       ${listStudent.stream}
				    </c:if>
				  </td>
                  <td id="yearBatch${loop.index}">
				             ${listStudent.yearBatch}
				  
				  </td>
				  <td id="category${loop.index}">
				             ${listStudent.category}
				  </td>
				  </c:if>
				    <td>
				  <c:if test="${listStudent.refStatus eq '0'}">
				  <a href="#" data-toggle="modal"  id="${loop.index}"  class="saveStudent saveAccess${loop.index}">Save</a>
				  </c:if>
				  <c:if test="${listStudent.refStatus eq '10'}">
				  <a href="#" data-toggle="modal"  id="${loop.index}" class="saveStudent saveAccess${loop.index}">Verify</a>
				  </c:if>
				  <c:if test="${listStudent.refStatus eq '20' or listStudent.refStatus eq '30'}">
				  <a href="#" data-toggle="modal"  id="${loop.index}" class="saveStudent saveAccess${loop.index}" style="pointer-events: none;opacity: 0.3;">Verified</a>
				  </c:if>
				  </td>
				  
				   <td> 
				  
				<c:if test="${listStudent.refStatus eq '20'}">
				 <a href="#" data-toggle="modal" class="procee${loop.index}" id="proceed">Proceed</a>
				  </c:if>
				  
				  <c:if test="${listStudent.refStatus eq '30'}">
				  <a href="javascript:void()" class="procee${loop.index}" id="print">Print</a>
				  </c:if>
				  
				  <c:if test="${listStudent.refStatus eq '10' or listStudent.refStatus eq '0'}">
				  <a href="#" data-toggle="modal"  class="procee${loop.index}" id="proceed" style="pointer-events: none;opacity: 0.3;">Proceed</a>
				  </c:if> 
                </td>
                </tr>
                

                 </c:forEach>
                 </c:if>
                 </tbody>
                 <%--  <c:if test="${listStudent eq ' '}">
                      <tbody>
                        <tr>
                          <td><label> No Student Found</label> </td>
                        </tr>
                         <tr>
                        </tr>
                         <tr>
                        </tr>
                         <tr>
                        </tr>
                      </tbody>
                 </c:if> --%>
                <tfoot>
                <tr>
                 <th>Name</th>
                  <th>Roll No</th>
                  <th>DOB</th>
                  <th>Fund Type</th>
                  <th>Session</th>
				  <th>Course</th>
				  <th>Stream</th>
                  <th>Class</th>
				  <th>Category</th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
         
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
	<!-- Main Footer -->
   <jsp:include page="/WEB-INF/jsp/views/common/footer.jsp"></jsp:include>
  </div>
  <!-- /.content-wrapper -->

  

  
  
</div>


<div class="modal fade" id="myhelpModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Help</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="dataconfmodal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Student Admission Data</h4>
      </div>
      <div class="modal-body" style="height:400px; overflow-y:scroll;">
	  <h5>Please Verify the data before making admission.</h5>
        <div class="table-responsive">
  <table class="table">
  <tbody>
  <tr>
  <td>Name ::</td>
  <td id="Pname"></td>
  </tr>
  <tr>
  <td>RollNo ::</td>
  <td id="ProllNo" style="color: #0000FF"></td>
  </tr>
  <tr>
  <td>DOB ::</td>
  <td id="Pdob"></td>
  </tr>
  <tr>
  <td>Fund Type ::</td>
  <td id="PfundType"></td>
  </tr>
  <tr>
  <td>Session ::</td>
  <td id="Psession"></td>
  </tr>
  <tr>
  <td>Course ::</td>
  <td id="PCourse" style="color: #0000FF"></td>
  </tr>
  <tr>
  <td>Stream ::</td>
  <td id="PStream"></td>
  </tr>
  <tr>
  <td>Science Type ::</td>
  <td id="PScienceType" style="color: #0000FF"></td>
  </tr>
  <tr>
  <td>Class ::</td>
  <td id="PClass"></td>
  </tr>
  <tr>
  <td>Category ::</td>
  <td id="PCategory"></td>
  </tr>
  </tbody>
   
  </table>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="EditStudent">Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--Admission popup-->
<div class="modal fade" id="admissionmodal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form id="saveTransaction" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Refund/Rejection</h4>
      </div>
      <div class="modal-body" >
              <div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Refund Balance</label>
                   <div class="col-sm-2">
                    <label id="avlrefAmnt"></label>
                  </div>
                  <label for="inputdob" class="col-sm-2 control-label">Date of Transaction</label>
                  <div class="col-sm-6">
                   <div class="input-group date">
                       <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                       </div>
                        <input id="datepicker1" type="text" path="dob" class="form-control pull-right" name="dob" placeholder="Date Of Transaction"/>
                   </div>
                  </div>
                </div>
				<div class="row form-group" >
                  <label for="adinputreason" class="col-sm-2 control-label">Collected Amount</label>
                  <div class="col-sm-2">
                    <label id="avlAmnt"></label>
                  </div>
				 <!--  <label for="adinputamount" class="col-sm-2 control-label">Deducted Amount</label>
                  <div class="col-sm-2">
                    <label id="dedAmnt"></label>
                  </div> -->
                   <label for="adinputamount" class="col-sm-2 control-label">Refund Amount</label>
                  <div class="col-sm-2">
                   <label id="totalAmnt"></label>
                  </div>
                </div>
                
                <div class="row form-group" id="paymentMode">
                  <label for="adinputreason" class="col-sm-2 control-label">Mode Of Payment</label>
                  <div class="col-sm-4">
                    <label id="" style="color: blue;">Cheque</label>
                  </div>
				  <label for="adinputamount" class="col-sm-2 control-label">Cheque Number</label>
                  <div class="col-sm-4">
                    <input id="draftNumber" type="text"  class="form-control pull-right" placeholder="Cheque Number" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;" maxlength="6"/>
                  </div>
                </div>
                 <div class="row form-group" id="bankName">
                 <label for="adinputamount" class="col-sm-2 control-label">Bank Name</label>
                  <div class="col-sm-10">
                  <select class="form-control" data-placeholder="Select a Stream" style="width: 100%;" id="bankName">
                    <option value="">Select</option>
                    <option value="">Indian Overseas Bank</option>
                    <option value="">State Bank Of India</option>
                  </select>
                  </div>
                 </div>
                
                
      </div>
	  <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-default" id="finalSave">Save</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ./wrapper -->
<!--Quick Admission popup-->
<div class="modal fade" id="quickadmissionmodal" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Student</h4>
      </div>
      <div class="modal-body" >
	            <div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Session</label>

                  <div class="col-sm-10">
                  <select class="form-control" data-placeholder="Select a Stream" style="width: 100%;" id="session">
                             <option value="">Select</option>
                             <option value="2017-18">2017-18</option>
                             <option value="2016-17">2016-17</option>
                             <option value="2015-16">2015-16</option>
	                              <option value="2014-15">2014-15</option>
	                               <option value="2013-14">2013-14</option>
	                                <option value="2012-13">2012-13</option>
	                                 <option value="2011-12">2011-12</option>
	                                  <option value="2010-11">2010-11</option>
	                                   <option value="2009-10">2009-10</option>
	                                    <option value="2008-09">2008-09</option>
	                                     <option value="2007-08">2007-08</option>
	                                      <option value="2006-07">2006-07</option>
	                                       <option value="2005-06">2005-06</option>
	                                        <option value="2004-05">2004-05</option>
	                                         <option value="2003-04">2003-04</option>
	                                        <option value="2002-03">2002-03</option>
	                                        <option value="2001-02">2001-02</option>
                                            <option value="2000-01">2000-01</option>
                                            <option value="1999-00">1999-00</option>
                                            <option value="1998-99">1998-99</option>
                                            <option value="1997-98">1997-98</option>
                                            <option value="1996-97">1996-97</option>
                                            <option value="1995-96">1995-96</option>
                                            <option value="1994-95">1994-95</option>
                                            <option value="1993-94">1993-94</option>
                                            <option value="1992-93">1992-93</option>
                                            <option value="1991-92">1991-92</option>
                                            <option value="1990-91">1990-91</option>
                                            
                 </select>
                  </div>
                </div>
	  
              <div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                  <input id="name" type="text" path="name" class="form-control" name="name" style="text-transform: uppercase" placeholder="Student Name" maxlength="29"/>
                  </div>
                </div>
				
				<div class="row form-group">
                  <label for="inputrollno" class="col-sm-2 control-label">RollNo</label>
                  <div class="col-sm-3">
                  <input id="rollNo" type="text" path="rollNo" class="form-control" name="rollNo" placeholder="RollNo" style="float:left; width:105%; margin-right:1%" maxlength="8"/>
                  </div>
                   <label for="inputdob" class="col-sm-2 control-label">Date of Birth</label>
                  <div class="col-sm-5">
                   <div class="input-group date">
                       <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                       </div>
                        <input id="datepicker" type="text" path="dob" class="form-control pull-right" name="dob" placeholder="Date Of Birth"/>
                   </div>
                  </div>
                </div>
                
                <div class="row form-group">
                  <label for="inputrollno" class="col-sm-2 control-label">Fund Type</label>
                  <div class="col-sm-3">
                  <select class="form-control" data-placeholder="Select a Stream" style="width: 100%;" id="fundTypeA">
                             <option value="">Select</option>
                             <option value="Additional Fee">Additional Fee</option>
                             <option value="Fine">Fine</option>
                  </select>
                  </div>
                   <label for="inputdob" class="col-sm-2 control-label">Stream</label>
                  <div class="col-sm-5">
                  <select class="form-control" data-placeholder="Select a Stream" style="width: 100%;" id="stream" >
                             <option value="">Select</option>
                             <option value="Science">Science</option>
                             <option value="Arts">Arts</option>
                  </select>
                  </div>
                </div>
                  <div class="row form-group">
                  <label for="inputrollno" class="col-sm-2 control-label">Class</label>
                  <div class="col-sm-3">
                 <select class="form-control" data-placeholder="Select a Stream" style="width: 100%;" id="class">
                             <option value="">Select</option>
                             <option value="1st">1st Year</option>
                             <option value="2nd">2nd Year</option>
                             <c:if test="${config eq 'UG'}">
                             <option value="3rd">3rd Year</option>
                             </c:if>
                  </select>
                  </div>
                   <label for="inputdob" class="col-sm-2 control-label">Category</label>
                  <div class="col-sm-5">
                    <select class="form-control" data-placeholder="Select a Stream" style="width: 100%;" id="category">
                             <option value="">Select</option>
                             <option value="General">General</option>
                             <option value="OBC">OBC</option>
                             <option value="SC">SC</option>
                             <option value="ST">ST</option>
                             <option value="Girls">Girls</option>
                  </select>
                  </div>
                </div>
                
                
                
                <div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Science Category</label>
                  <div class="col-sm-10">
                   <select class="form-control" data-placeholder="Select a Stream" style="width: 100%;" id="scienceType" disabled="disabled">
                             <option value="">Select</option>
                             <option value="Science">Science</option>
                             <option value="Phy Science">Phy Science</option>
                             <option value="Bio Science">Bio Science</option>
                  </select>
                  </div>
                </div>
                
                
                
                
                
                
                
				
			
				
      </div>
	  <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-default" id="showAdmission">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="scienceChooseModel" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Types Of Science</h4>
      </div>
      <div class="modal-body" >
	  
	  
              <div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Category</label>

                  <div class="col-sm-10">
                  <select name="scienceChoose" class="form-control" style="width: 100%;" id="scienceChoose">
				             <option value="Select">Select</option>
				             <option value="Biology">Science</option>
				             <option value="Biology">PHY Science</option>
				             <option value="Regular Science">Bio Science</option>
                   </select>
                  </div>
                </div>
      </div>
	  <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-default" id="saveScienceChoose">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->






<!-- ./wrapper -->
<!-- REQUIRED JS SCRIPTS -->



    <spring:url value="/js/jquery-2.2.3.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script>
    
    <spring:url value="/js/bootstrap.min.js" var="bootstrapminjs"></spring:url>
    <script src="${bootstrapminjs}"></script>
    
    <spring:url value="/js/select2.full.min.js" var="selectminjs"></spring:url>
    <script src="${selectminjs}"></script>
    
    <spring:url value="/js/jquery.dataTables.min.js" var="jquerydataTablesminjs"></spring:url>
    <script src="${jquerydataTablesminjs}"></script>
    
     <spring:url value="/js/dataTables.bootstrap.min.js" var="dataTablesbootstrapminjs"></spring:url>
    <script src="${dataTablesbootstrapminjs}"></script>
    
    <spring:url value="/js/bootstrap-datepicker.js" var="bootstrapdatepickerjs"></spring:url>
    <script src="${bootstrapdatepickerjs}"></script>
    
    <spring:url value="/js/jquery.slimscroll.min.js" var="jqueryslimscrollminjs"></spring:url>
    <script src="${jqueryslimscrollminjs}"></script>
    

    
    <spring:url value="/js/app.min.js" var="appminjs"></spring:url>
    <script src="${appminjs}"></script>


<script>
//Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy'
    });
    
  // $('#datepicker1').datepicker("setDate", new Date());
    var today = new Date();
    $('#datepicker1').datepicker({
    	 setDate:today,
    	 format: 'dd/mm/yyyy',
         autoclose:true,
         endDate: "today",
         maxDate: today,
         todayHighlight: true
         
    
    	/*  maxDate: '0',
    	/* 'autoclose': true,
        'format': 'dd/mm/yyyy',
        'todayHighlight': true */ 
        	
      });/* .datepicker("setDate", new Date()); */
      
      
      /* on('changeDate', function (ev) {
          $(this).datepicker('hide');
      }); */
    
    
    
    
</script>

 <script type="text/javascript">
 

 
    $(document).ready(function() {
    	var dateTime='';
    	var savedId= '';    	
    	
    	var selectedSciencevalue='';
    	
    		$(document).on('click' , '#saveScienceChoose' , function(){
    			selectedSciencevalue=$('#scienceChoose').find(':selected').text();
    			alert(selectedSciencevalue);
    			$("#scienceChooseModel").modal('hide');
    	  });
    	
    	
    	var disableStatus=true;
    	var additionalCheckBoxStatus=false;
    	$('#additionalCheckBox').change(function(){	
    		var yearBatch=$('#yearBatch'+savedId).find(':selected').text();
    		var course=$('#course'+savedId).find(':selected').text();
    		var inputAmount=$("#totalinputtext").val();
    		var totalrounupAmount=$("#totalrounup").val();
    		if($(this).is(':checked')){
    			$("#addReasonDiv").show();
    			additionalCheckBoxStatus=true;
    		}else{
    			additionalCheckBoxStatus=false;
    			$("#addReasonDiv").hide();
    			 if(course=='UG' && yearBatch=='1st Year'){
	    			 $("#roundUpAmount").show();
	    			 $("#totalrounup").val(parseInt(totalrounupAmount));	
	    		 }else{
	    			 $("#roundUpAmount").hide();
	    		 }
    			 $("#totalinputtext").val(parseInt(inputAmount));
    		}
    	});
    	
    	$("#stream").change(function(){	
    		var stream=$('#stream').find(':selected').text();
    		if(stream=='Science'){
    			$("#scienceType").prop("disabled", false);
    		}else{
    			$("#scienceType").prop("disabled", true);
    		}
    	});
    	
    	
    	$("#inputheadname").change(function(){	
    		var ind=$('option:selected',this).index();
    		ind=ind-1;
    		  $.ajax({
 	    	    type: "POST",
 	    	    url: "getHeadAmount", //BookInfoyour valid url
 	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
 	    	    processData: false,
 	    	    data: JSON.stringify(ind), //json object or array of json objects
 	    	    success: function(result) {
 	    	    	if(result=='ERROR'){
 	    	    		 alert("Unable To Get The Amount At This Moment");
 	    	    	 }
 	    	    		 else{
 	    	    			$("#adinputamounttext").val(parseInt(result));
 	    	    			$("#totalinputtext").val(parseInt(result));
 	    	    	} 
 	    	    },
 	    	    error: function(result){
 	    	    	alert("Unable To Get The Amount At This Moment");
 	    	    }
 	    	     });
    	});

    	
    	  $(document).on('click' , '#print' , function(){
    		   	var saveId= $(this).prop('class');
    	    	savedId=saveId.substring(6,saveId.length);
    	    	var url='${print_url}'
    	    	var fundType=$('#fundType'+savedId).html();
    		    var win = window.open(url+"/print?printid="+savedId+"&tran_Type="+fundType, '_blank');
    	    	//var win = window.open("http://host:3030/DailyCollectionRegister/print?printid="+savedId, '_blank');

    	  });
    $('#showAdmission').click(function(){
    	var name=$("#name").val();
    	var alpha = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/
    	var rollNo=$("#rollNo").val();
    	var fundType=$('#fundTypeA').find(':selected').text();
    	var stream=$('#stream').find(':selected').text();
    	var yearBatch=$('#class').find(':selected').text();
    	var category=$('#category').find(':selected').text();
    	var session=$('#session').find(':selected').text();
    	var scienceType=$('#scienceType').find(':selected').text();
    	if(session=='Select'){
    		alert("Please Enter Session");
    		return;
    	}
    	if(name==''){
    		alert("Please Enter A Name Before Save");
    		return;
    	}
    	 if (!alpha.test(name)) {
    	        alert('Only Characters Allowed In Name');       
    	        return false;
    	   }
    	if($("#datepicker").val()==''){
    		alert("Date Of Birth is mandatory");
    		return;
    	}
    	
    	if(rollNo==''){
    		alert("RollNo Is Mandatory");
    		return;
    	}
    	if(fundType=='Select'){
    		alert("FundType Is Mandatory");
    		return;
    	}
    	if(stream=='Select'){
    		alert("Stream Is Mandatory");
    		return;
    	}
    	if(stream=='Science'){
    		if(scienceType=='Select'){
    			alert("Science Type Is Mandatory");
    			return;
    		}
    	}
    	if(stream=='Arts'){
    		scienceType='NA';
    	}
    	
    	if(yearBatch=='Select'){
    		alert("Class Is Mandatory");
    		return;
    	}
    	if(category=='Select'){
    		alert("Category Is Mandatory");
    		return;
    	}
    	var batch='';
    	if(yearBatch=='1st Year'){
    		batch='1st';
    	}else if(yearBatch=='2nd Year'){
    		batch='2nd';
    	}else{
    		batch='3rd';
    	}
    	
    	
    	var rollNoCourse=rollNo.substr(0,1);
    	var rollNoStream=rollNo.substr(1,1);
    	var rollNoSession=rollNo.substr(2,3);
    	var rollNoseperator=rollNo.substr(3,3);
    	var rollNoCount=rollNo.substr(4,7);
    /* 	alert(rollNoCourse);
    	alert(rollNoStream);
    	alert(rollNoSession);
    	alert(rollNoseperator);
    	alert(rollNoCount);
    	return;
    	 */
    	
    	
    	
    	 var formDetailsJson={};
   	    formDetailsJson={
   			"name":name.toUpperCase(),
   			"dob": $("#datepicker").val(),
   			"session": session,
   			"rollNo": rollNo,
   			"fundType": fundType,
   			"stream": stream,
   			"yearBatch": batch,
   			"category": category,
   			"scienceSelect":scienceType
   	    }
    	 $.ajax({
	    	    type: "POST",
	    	    url: "saveAddtionalFeeStudent", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
	    	    success: function(result) {
	    	    	if(result=='ERROR'){
	    	    		 alert("Unable To Add Student Details At This Moment");
	    	    	 }
	    	    		 else{
	    	    		 window.location.href = "additionalFeeShowFromPopup";
	    	    	} 
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Add Student Details At This Moment");
	    	    }
	    	     });
  	});
    
     $(document).on('click' , '#EditStudent' , function(){
    	if($(this).text()=='Edit'){
    		 $("#fundType"+savedId).prop("disabled", false);
    		 $("#course"+savedId).prop("disabled", false);
    		 $("#stream"+savedId).prop("disabled", false);
    		 $("#yearBatch"+savedId).prop("disabled", false);
    		 $("#category"+savedId).prop("disabled", false);
    		 $("#session"+savedId).prop("disabled", false);
    		 var saveButton = $('.saveAccess'+savedId).css({'pointer-events': '', 'opacity' :'1'});
    		 saveButton.empty().append("Save");
    		 var proceed =$('.procee'+savedId).css({'pointer-events': 'none', 'opacity' :'0.3'});
    		 proceed.empty().append("Proceed");	
    	}else{
    		var id=savedId;
    		savedId=savedId+'RF';
    		 $.ajax({
 	    	    type: "POST",
 	    	    url: "saveVerifyStatus", //BookInfoyour valid url
 	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
 	    	    processData: false,
 	    	    data: JSON.stringify(savedId), //json object or array of json objects
 	    	    success: function(result) {
 	    	    	 if(result=='ERROR'){
 	    	    		 alert("Unable To Verify The Data At This Moment");
 	    	    	 }else{
 	    	    		 var saveButton = $('.saveAccess'+id).css({'pointer-events': 'none', 'opacity' :'0.3'});
 	    	    		 saveButton.empty().append("Verified");
 	    	    		 var proceed =$('.procee'+id).css({'pointer-events': '', 'opacity' :'1'});
 	    	    		 proceed.empty().append("Proceed");	
 	    	    	} 
 	    	    },
 	    	    error: function(result){
 	    	    	alert("Unable To Verify The Data At This Moment");
 	    	    }
 	    	     });
    	}
    }); 
    
   /*  var selectedItem = sessionStorage.getItem("showPopup");  
    if(selectedItem=='true'){
    	$("#avlAmnt").html('${availableAmt}');  
    	//$("#inputamount").val(sessionStorage.getItem("inputamount"));
    	//$("#totalinputtext").val(sessionStorage.getItem("totalinputtext"));
	    $("#inputamount").prop("disabled", true);
	    $("#admissionmodal").modal('show');
	    savedId=sessionStorage.getItem("index")
	    sessionStorage.setItem('showPopup', "false");
    } */
    
    $(document).on('click' , '#proceed' , function(){
     	var saveId= $(this).prop('class');
    	savedId=saveId.substring(6,saveId.length);
    	 $.ajax({
	    	    type: "POST",
	    	    url: "getStudentAmount", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(savedId), //json object or array of json objects
	    	    success: function(result) {
	    	    	 if(result=='0'){
	    	    		 alert("Unable To find the amount with the combination of inputs...Please manage the head before doing transaction");
	    	    	 }else{
	    	    		 $("#avlrefAmnt").html('${availablerefAmt}');
	    	    		 var splitCode=result.split("/");
	    	    		 var fundType=$("#fundType"+savedId).html();
	    	    		 if(fundType.includes('Refund')){
	    	    			 var debAmt=parseInt(splitCode[1])+parseInt(100);
	    	    			 $("#dedAmnt").html(debAmt);
	    	    			 $("#totalAmnt").html(parseInt(splitCode[0])-debAmt);
	    	    			 $("#paymentMode").show();
	    	    			 $("#bankName").show();
	    	    		 }else if(fundType.includes('Rejection')){
	    	    			 $("#dedAmnt").html(0);
	    	    			 $("#totalAmnt").html(0);
	    	    			 $("#paymentMode").hide();
	    	    			 $("#bankName").hide();
	    	    			 
   	    	    		 }
	    	    		  $("#avlAmnt").html(parseInt(splitCode[0]));
	    	    		 $("#admissionmodal").modal('show');
	    	    		 
	    	    		 
	    	    		 
	    	    	} 
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To find the amount withe the combination of inputs...Please manage the head before doing transaction");
	    	    }
	    	     }); 
    });
    $(document).on('click' , '#finalSave' , function(){
    	 var availableRefAmount=$("#avlrefAmnt").text();
    	 var actAmnt=$("#avlAmnt").text();
    	 var totalAmnt=$("#totalAmnt").text();
    	 var transactionDate=$("#datepicker1").val();
    	 var draftNumber=$("#draftNumber").val();
    	 var bankName=$('#bankName').find(':selected').text();
    	 var today=new Date()
    	 var fundType=$("#fundType"+savedId).html();
    	 if(transactionDate==''){
    		 alert("Pleae Enter Transaction Date");
    		 return;
    	 }	
		 if(fundType.includes('Refund')){
			 if(draftNumber==''){
	    		 alert("Pleae Enter Cheque Number");
	    		 return;
	    	 }else if(draftNumber.length<6){
	    		 alert("Cheque Number Shoud Be 6 Digit");
	    		 return;
	    	 }
	    	 
	    	 if(bankName=='Select' || bankName==''){
	    		 alert("Please Select AtLeast One Bank Name");
	    		 return;
	    	 }
	    	 
	    	 fundType='RF';
		 }else{
			 bankName=''
			 draftNumber='';
			 fundType='RE';
		 }
    	var formDetailsJson={};
  	    formDetailsJson={
  			"savedId":savedId,
  			"actualAmount":actAmnt,
  			"totalAmount": totalAmnt,
  			"transactionDate": transactionDate,
  			"chequeNumber": draftNumber,
  			"bankName":bankName,
  			"transactiontype":fundType
  	    }
  	   var saveButton = $('#finalSave').css('pointer-events', 'none');
		saveButton.empty().append("Processing..<i class='fa fa-spinner fa-spin'></i>");
    	 $.ajax({
	    	    type: "POST",
	    	    url: "updateTransaction", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
	    	    success: function(result) {
	    	    	 if(result=='ERROR'){
	    	    		 alert("Unable To Refund At This Time");
	    	    		 location.reload();
	    	    	 }else{
	    	    		 
	    	    		 if(result.includes('Please Choose')){
	    	    			 alert(result);
	    	    			 var saveButton = $('#finalSave').css('pointer-events', '');
	    	    			 saveButton.empty().append("Save");
	    	    			 return;
	    	    		 }
	    	    		 alert(result);
	    	    		 var saveButton = $('#finalSave').css('pointer-events', 'none');
	    	    		 saveButton.empty().append("Completed");
	    	    		 window.location.href = "showRefund";
	    	    	} 
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Refund At This Time");
	    	    }
	    	     });   
    });
    $( "#adinputamounttext" ).keyup(function() {
    	var inputAmount=$("#totalinputtext").val();
    	var totalRoundUp=$("#totalrounup").val();
    	var amt=$("#inputamount").val();
    	var addtionalAmount=$("#adinputamounttext").val();
    	 var yearBatch=$('#yearBatch'+savedId).find(':selected').text();
		 var course=$('#course'+savedId).find(':selected').text();
    	if(additionalCheckBoxStatus==true){
    		if(addtionalAmount!=''){
    			if(course=='UG' && yearBatch=='1st Year'){
    				 $("#roundUpAmount").show();
    				var totalamt=parseInt(addtionalAmount)+parseInt(amt);
        			var roundupAmount=Math.ceil(totalamt/ 10) * 10;
        			var roundamt=parseInt(roundupAmount)-parseInt(totalamt);
        			$("#totalrounup").val(roundamt);
            		$("#totalinputtext").val(parseInt(roundupAmount));	
    	    	}else{
    	    		$("#roundUpAmount").hide();
    	    		$("#totalinputtext").val(parseInt(amt)+parseInt(addtionalAmount));
    	    	}
        	}else{
        		if(course=='UG' && yearBatch=='1st Year'){
        			$("#roundUpAmount").show();
        			var roundupAmount=Math.ceil(amt/ 10) * 10;
            		$("#totalrounup").val(parseInt(roundupAmount)-parseInt(amt));
            		$("#totalinputtext").val(parseInt(inputAmount));	
        		}else{
        			$("#roundUpAmount").hide();
        			$("#totalinputtext").val(parseInt(amt));	
        		}
        	}
    	}else{
    		if(course=='UG' && yearBatch=='1st Year'){
    			$("#roundUpAmount").show();
    			var roundupAmount=Math.ceil(amt/ 10) * 10;
        		$("#totalrounup").val(parseInt(roundupAmount)-parseInt(amt));
        		$("#totalinputtext").val(parseInt(inputAmount));	
    		}else{
    			$("#roundUpAmount").hide();
    			$("#totalinputtext").val(parseInt(amt));	
    		}
    	}
    	
    	
    });
   /*  $( "#admissionmodal" ).keyup(function() {
    	var inputAmount=$("#inputamount").val();
    	var addtionalAmount=$("#adinputamounttext").val();
    	var roundUpAmt=$("#totalrounup").val();
    	if(additionalCheckBoxStatus==true){
    		if(addtionalAmount!=''){
        		$("#totalinputtext").val(parseInt(addtionalAmount)+parseInt(inputAmount)+parseInt(roundUpAmt));	
        	}else{
        		$("#totalinputtext").val(parseInt(inputAmount)+parseInt(roundUpAmt));	
        	}
    	}else{
    		$("#totalinputtext").val(parseInt(inputAmount)+parseInt(roundUpAmt));	
    	}
    }); */
    
    
    
    
    
    
    
    
    function showVerifyPopup(saveId){
    	savedId=saveId;
    	 $.ajax({
	    	    type: "POST",
	    	    url: "getStudentByStudentId", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(saveId), //json object or array of json objects
	    	    success: function(result) {
	    	    	 if(result==null){
	    	    		 alert("Unable To Show Student Verify Popup At This Time");
	    	    	 }else{
	    	    		 $('#Pname').html(result.name);
	    	    		 $('#ProllNo').html(result.rollNo);
	    	    		 $('#Pdob').html(result.dob);
	    	    		 $('#PfundType').html(result.fundType);
	    	    		 $('#Psession').html(result.session);
	    	    		 $('#PCourse').html(result.course);
	    	    		 $('#PStream').html(result.stream);
	    	    		 $('#PClass').html(result.yearBatch);
	    	    		 $('#PCategory').html(result.category);
	    	    		 $('#PScienceType').html(result.scienceSelect);
	    	    		 $("#dataconfmodal").modal('show');
	    	    	} 
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Show Student Verify Popup At This Time");
	    	    }
	    	     });
    }
    
    
    
    $(document).on('click' , '.saveStudent' , function(){
    	if($(this).text()=='Verify' || $(this).text()=='Verified'){
    		var saveId= $(this).prop('id');
    		showVerifyPopup(saveId);
    	}else{
    		var saveId= $(this).prop('id');
        	var fundType=$('#fundType'+saveId).find(':selected').text();
        	 if(fundType=='Select'){
             	alert("Please Select Proper FundType");
             	return;
             }
        	 var formDetailsJson={};
      	     formDetailsJson={
      			"savedid":saveId,
      			"fundType":$('#fundType'+saveId).find(':selected').text(), 
      	     }	
        	    var saveButton = $('.saveAccess'+saveId).css('pointer-events', 'none');
    			saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    			 $.ajax({
    		    	    type: "POST",
    		    	    url: "updateStudent", //BookInfoyour valid url
    		    	    contentType: 'application/json;charset=UTF-8', 
    		    	    processData: false,
    		    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    		    	    success: function(result) {
    		    	    	 if(result==null){
    		    	    		 alert("Unable To Update Student At This Time");
    		    	    	 }else{
    		    	    		 window.location.href = "showRefund";
    		    	    	} 
    		    	    },
    		    	    error: function(result){
    		    	    	alert("Unable To Save Student At This Time");
    		    	    }
    		    	     });
    	}
    });
  });
    
    </script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<script>
 $(window).load(function(){  
   $(".select2").select2(); 
   $('#courseModal').modal('show');
   
    }); 
     
 </script>

</body>
</html>
