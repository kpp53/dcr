<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daily Collection Register | Student</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/font-awesome.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/ionicons.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/datepicker3.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/select2.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/dataTables.bootstrap.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/custom.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/skin-blue.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  
  
  
 <%--  <%
    String url=;
  request.setAttribute("url", url); 
  %> --%>
  
  
  
  
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
    <jsp:include page="/WEB-INF/jsp/views/common/header.jsp"></jsp:include>
    <jsp:include page="/WEB-INF/jsp/views/common/menu.jsp"></jsp:include>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admission
        <small>Transactions</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Transactions</a></li>
        <li class="active">Admission</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	<!-- Alert message -->
	 <div class="alert alert-success" style="display:none;">
  <strong>Success!</strong> Indicates a successful or positive action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-info" style="display:none;">
  <strong>Info!</strong> Indicates a neutral informative change or action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-warning" style="display:none;">
  <strong>Warning!</strong> Indicates a warning that might need attention.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-danger" style="display:none;">
  <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>
        <!-- Alert message end-->  
      <div class="box">
            <div class="box-header">
              <h3 class="box-title pull-left">Admission/Re-Admission</h3>
              <h3 class="box-title pull-right"><a href="#" data-toggle="modal" data-target="#quickadmissionmodal" data-backdrop="static" data-keyboard="false"><!-- <a href="#" data-toggle="modal" data-target="#quickadmissionmodal"> -->Quick Admission</a></h3>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
         <%-- url::<input type="text" value="${url}"/> --%>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Roll No</th>
                  <th>DOB</th>
                  <th>Fund Type</th>
                  <th>Session</th>
				  <th>Course.</th>
				  <th>Stream</th>
                  <th>Class</th>
				  <th>Category</th>
				  <th></th>
				  <th></th>
                </tr>
                </thead>
                <tbody>
             <c:if test="${listStudent ne ''}">   
            <c:forEach items="${listStudent}" var="listStudent" varStatus="loop">   
                <tr>
                  <td id="name${loop.index}">${listStudent.name}</td>
                  <td id="rollno${loop.index}" style="color:#0000FF">${listStudent.rollNo}</td>
                  <td id="dob${loop.index}">${listStudent.dob}</td>
                   <c:if test="${listStudent.admStatus eq ''}">
                  <td>
				    <form:select  path="student.fundType" name="student.fundType" class="form-control" style="width: 100%;" id="fundType${loop.index}">
				    <option value="select">Select</option>
				     <c:forEach var="fundType" items="${FundTypeMap}">
				             <option value="${fundType}" ${fundType == listStudent.fundType ? 'selected="selected"' : ''} >${fundType}</option>
				     </c:forEach>
                   </form:select>
				  </td>
				 
                  <td>
				 <form:select  path="student.session" name="student.session" class="form-control" style="width: 100%;"  id="session${loop.index}">
				     <c:forEach var="session" items="${sessionMap}">
				             <option value="${session}" ${session == listStudent.session ? 'selected="selected"' : ''} >${session}</option>
				     </c:forEach>
                   </form:select>
				  </td>
				   <td>
				   <form:select  path="student.course" name="student.course" class="form-control" style="width: 100%;" id="course${loop.index}">
				     <c:if test="${listStudent.course ne null}">
				       <option value="${listStudent.course}"  selected="selected">${listStudent.course}</option>
				    </c:if>
				     <c:forEach var="course" items="${courseMap}">
				             <option value="${course}">${course}</option>
				     </c:forEach>
                   </form:select>
				  </td> -
				  <%-- <td>
				    <form:select  path="student.receiptNo" name="student.receiptNo" class="form-control" style="width: 100%;" id="receiptNo${loop.index}">
				    
				    </form:select>
				  
				  </td> --%>
                  <td>
				   <form:select  path="student.stream" name="student.stream" class="form-control" style="width: 100%;" id="stream${loop.index}">
				      <option value="select">Select</option>
				      <c:if test="${listStudent.stream ne null}">
				       <option value="${listStudent.stream}"  selected="selected">${listStudent.stream}</option>
				    </c:if>
				   
				     <c:forEach var="stream" items="${streamMap}">
				             <option value="${stream}">${stream}</option>
				     </c:forEach>
                   </form:select>
				  </td>
                  <td>
               <form:select  path="student.yearBatch" name="student.yearBatch" class="form-control" style="width: 100%;" id="yearBatch${loop.index}">
				     <option value="select">Select</option>
				     <c:forEach var="yearBatch" items="${yearBacthMap}">
				             <c:if test="${config eq '+2'}">
                               <c:if test="${yearBatch ne '3rd'}">
                                  <option value="${yearBatch}" ${yearBatch == listStudent.yearBatch ? 'selected="selected"' : ''} >${yearBatch} Year</option>
                              </c:if>
                            </c:if> 
                             <c:if test="${config ne '+2'}">
                                  <option value="${yearBatch}" ${yearBatch == listStudent.yearBatch ? 'selected="selected"' : ''} >${yearBatch} Year</option>
				             </c:if> 
				     </c:forEach>
                   </form:select>
				  
				  </td>
				  <td>
				 <form:select  path="student.category" name="student.category" class="form-control" style="width: 100%;" id="category${loop.index}">
				    <option value="select">Select</option>
				     <c:forEach var="category" items="${categoryMap}">
				             <option value="${category}" ${category == listStudent.category ? 'selected="selected"' : ''} >${category}</option>
				     </c:forEach>
                   </form:select>
				  </td>
				  </c:if>
				   <c:if test="${listStudent.admStatus eq '10' or listStudent.admStatus eq '20' or listStudent.admStatus eq '30' or listStudent.admStatus eq '40'}">
				   <td>
				    <form:select  path="student.fundType" name="student.fundType" class="form-control" style="width: 100%;" id="fundType${loop.index}" disabled="true">
				     <c:forEach var="fundType" items="${FundTypeMap}">
				             <option value="${fundType}" ${fundType == listStudent.fundType ? 'selected="selected"' : ''} >${fundType}</option>
				     </c:forEach>
                   </form:select>
				  </td>
                  <td>
				 <form:select  path="student.session" name="student.session" class="form-control" style="width: 100%;"  id="session${loop.index}" disabled="true">
				     <c:forEach var="session" items="${sessionMap}">
				             <option value="${session}" ${session == listStudent.session ? 'selected="selected"' : ''} >${session}</option>
				     </c:forEach>
                   </form:select>
				  </td>
				   <td>
				   <form:select  path="student.course" name="student.course" class="form-control courseChange" style="width: 100%;" id="course${loop.index}" disabled="true">
				     <c:if test="${listStudent.course ne null}">
				       <option value="${listStudent.course}"  selected="selected">${listStudent.course}</option>
				    </c:if>
				     <c:forEach var="course" items="${courseMap}">
				             <option value="${course}" ${course == listStudent.course ? 'selected="selected"' : ''} >${course}</option>
				     </c:forEach>
                   </form:select>
				  </td>
                  <td>
				  <form:select  path="student.stream" name="student.stream" class="form-control" style="width: 100%;" id="stream${loop.index}" disabled="true">
				     <c:if test="${listStudent.stream ne null or listStudent.stream ne ''}">
				       <option value="${listStudent.stream}"  selected="selected">${listStudent.stream}</option>
				    </c:if>
				     <c:forEach var="stream" items="${streamMap}">
				             <option value="${stream}">${stream}</option>
				     </c:forEach>
                   </form:select>
				  </td>
                  <td>
               <form:select  path="student.yearBatch" name="student.yearBatch" class="form-control" style="width: 100%;" id="yearBatch${loop.index}" disabled="true">
				     <c:forEach var="yearBatch" items="${yearBacthMap}">
				             <option value="${yearBatch}" ${yearBatch == listStudent.yearBatch ? 'selected="selected"' : ''} >${yearBatch} Year</option>
				     </c:forEach>
                   </form:select>
				  
				  </td>
				  <td>
				 <form:select  path="student.category" name="student.category" class="form-control" style="width: 100%;" id="category${loop.index}" disabled="true">
				     <c:forEach var="category" items="${categoryMap}">
				             <option value="${category}" ${category == listStudent.category ? 'selected="selected"' : ''} >${category}</option>
				     </c:forEach>
                   </form:select>
				  </td>
				  </c:if>
				  <td>
				  <c:if test="${listStudent.admStatus eq ''}">
				  <a href="#" data-toggle="modal"  id="${loop.index}"  class="saveStudent saveAccess${loop.index}">Save</a>
				  </c:if>
				  <c:if test="${listStudent.admStatus eq '10'}">
				  <a href="#" data-toggle="modal"  id="${loop.index}" class="saveStudent saveAccess${loop.index}">Verify</a>
				  </c:if>
				  <c:if test="${listStudent.admStatus eq '20' or listStudent.admStatus eq '30'}">
				  <a href="#" data-toggle="modal"  id="${loop.index}" class="saveStudent saveAccess${loop.index}" style="pointer-events: none;opacity: 0.3;">Verified</a>
				  </c:if>
				  
				  </td>
				  
				  <td> 
				  
				  <%-- <c:if test="${(listStudent.isTxnDone eq null or listStudent.isTxnDone eq ' ' or listStudent.isTxnDone eq 'N')  and  (listStudent.isSaved eq null or listStudent.isSaved eq ' ' or listStudent.isSaved eq 'N') and listStudent.isVerified eq 'N'}"> --%>
				<c:if test="${listStudent.admStatus eq '20'}">
				 <a href="#" data-toggle="modal" class="procee${loop.index}" id="proceed">Proceed</a>
				  </c:if>
				  
				  <%-- <c:if test="${(listStudent.isTxnDone eq null or listStudent.isTxnDone eq ' '  or listStudent.isTxnDone eq 'N')  and  (listStudent.isSaved eq 'Y') and (listStudent.isVerified eq null or listStudent.isVerified eq ' ' or listStudent.isVerified eq 'N')}"> --%>
				  <c:if test="${listStudent.admStatus eq '30'}">
				  <a href="javascript:void()" class="procee${loop.index}" id="print">Print</a>
				  </c:if>
				  
				  <c:if test="${listStudent.admStatus eq '10' or listStudent.admStatus eq ''}">
				  <a href="#" data-toggle="modal"  class="procee${loop.index}" id="proceed" style="pointer-events: none;opacity: 0.3;">Proceed</a>
				  </c:if> 
				  
				 <%-- <c:if test="${(listStudent.isTxnDone eq null or listStudent.isTxnDone eq ' '  or listStudent.isTxnDone eq 'N')  and  (listStudent.isSaved eq null or listStudent.isSaved eq ' ' or listStudent.isSaved eq 'N') and (listStudent.isVerified eq null or listStudent.isVerified eq ' ' or listStudent.isVerified eq 'N')}">
				  <a href="#" data-toggle="modal"  class="procee${loop.index}" id="proceed" style="pointer-events: none;opacity: 0.3;">Proceed</a>
				  </c:if> 
				  
				   <c:if test="${listStudent.isTxnDone eq 'N' and listStudent.isSaved eq 'Y' and listStudent.isVerified eq 'Y'}">
				  <a href="#" data-toggle="modal" class="procee${loop.index}" id="proceed">Proceed</a>
				  </c:if>
				  
				   <c:if test="${listStudent.isTxnDone eq 'Y' and listStudent.isSaved eq 'Y' and listStudent.isVerified eq 'Y'}">
				  <a href="javascript:void()" class="procee${loop.index}" id="print">Print</a>
				  </c:if> --%>
				  
                </td>
                </tr>
                

                 </c:forEach>
                 </c:if>
                 </tbody>
                 <%--  <c:if test="${listStudent eq ' '}">
                      <tbody>
                        <tr>
                          <td><label> No Student Found</label> </td>
                        </tr>
                         <tr>
                        </tr>
                         <tr>
                        </tr>
                         <tr>
                        </tr>
                      </tbody>
                 </c:if> --%>
                <tfoot>
                <tr>
                 <th>Name</th>
                  <th>Roll No</th>
                  <th>DOB</th>
                  <th>Fund Type</th>
                  <th>Session</th>
				  <th>Course</th>
				  <th>Stream</th>
                  <th>Class</th>
				  <th>Category</th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
         
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
	<!-- Main Footer -->
   <jsp:include page="/WEB-INF/jsp/views/common/footer.jsp"></jsp:include>
  </div>
  <!-- /.content-wrapper -->

  

  
  
</div>


<div class="modal fade" id="myhelpModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Help</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="dataconfmodal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Student Admission Data</h4>
      </div>
      <div class="modal-body" style="height:400px; overflow-y:scroll;">
	  <h5>Please Verify the data before making admission.</h5>
        <div class="table-responsive">
  <table class="table">
  <tbody>
  <tr>
  <td>Name ::</td>
  <td id="Pname"></td>
  </tr>
  <tr>
  <td>RollNo ::</td>
  <td id="ProllNo" style="color: #0000FF"></td>
  </tr>
  <tr>
  <td>DOB ::</td>
  <td id="Pdob"></td>
  </tr>
  <tr>
  <td>Fund Type ::</td>
  <td id="PfundType"></td>
  </tr>
  <tr>
  <td>Session ::</td>
  <td id="Psession"></td>
  </tr>
  <tr>
  <td>Course ::</td>
  <td id="PCourse" style="color: #0000FF"></td>
  </tr>
  <tr>
  <td>Stream ::</td>
  <td id="PStream"></td>
  </tr>
  <tr>
  <td>Science Type ::</td>
  <td id="PScienceType" style="color: #0000FF"></td>
  </tr>  <tr>
  <td>Class ::</td>
  <td id="PClass"></td>
  </tr>
  <tr>
  <td>Category ::</td>
  <td id="PCategory"></td>
  </tr>
  </tbody>
   
  </table>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="EditStudent">Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--Admission popup-->
<div class="modal fade" id="admissionmodal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form id="saveTransaction" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Admission/Re-Admission</h4>
      </div>
      <div class="modal-body" >
              <div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Available Balance</label>
                   <div class="col-sm-2">
                    <label id="avlAmnt"></label>
                  </div>
                  <label for="inputdob" class="col-sm-2 control-label">Date of Transaction</label>
                  <div class="col-sm-6">
                   <div class="input-group date">
                       <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                       </div>
                        <input id="datepicker1" type="text" path="dob" class="form-control pull-right" name="dob" placeholder="Date Of Transaction"/>
                   </div>
                  </div>
                  
                  
                  
                  
                </div>
              <div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Amount</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputamount" placeholder="Amount" name="inputamount" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;" maxlength="10">
                  </div>
                </div>
                <div class="row form-group" id="receiptNumberCheckBoxDiv">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="receiptNumberCheckBox" value="Y" name="receiptNumber" >Manually Enter Receipt Number 
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row form-group" style="display: none;" id="receiptNumberDiv">
                  <label for="adinputreason" class="col-sm-2 control-label">Receipt Number</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="receiptNumberText" placeholder="Receipt Number" name="receiptNumberText" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;" maxlength="5">
                  </div>
                  <label for="adinputreason" class="col-sm-4 control-label">Last Receipt Number</label>
                  <div class="col-sm-2">
                     <label id="lastReceiptNumber">${lastReciptNumber}</label>
                  </div>
                </div>
                
                
                
                
				<div class="row form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="additionalCheckBox" value="Y" name="additionalCheckBox" > Additional Reason
                      </label>
                    </div>
                  </div>
                </div>
				<div class="row form-group" style="display: none;" id="addReasonDiv">
                  <label for="adinputreason" class="col-sm-2 control-label">Reason</label>
                  <c:if test="${config eq 'UG'}">       
                  <div class="col-sm-4">
                  <select class="form-control" style="width: 100%;" id="adinputreasontext" name="adinputreasontext">
		                  <option selected="selected">Select</option>
		                  <option>Practical</option>
		                  <option>Others</option>
                  </select>
                  </div>
                  </c:if>
                  <c:if test="${config eq '+2'}">       
                  <div class="col-sm-4">
                      <input type="text" class="form-control" id="addReasonText" placeholder="Additional Reason" name="addReadon" maxlength="15">
                  </div>
                  </c:if>
				  <label for="adinputamount" class="col-sm-2 control-label">Amount</label>

                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="adinputamounttext" placeholder="Amount" name="addAmount" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;" maxlength="10">
                  </div>
                </div>
                  <div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Payment Mode</label>

                  <div class="col-sm-4">
                    <select class="form-control" style="width: 100%;" id="paymentModeDrop" name="paymentModeDrop">
                  <option selected="selected">Cash</option>
                  <option>Cheque</option>
				  <option>DD</option>             
                  </select>
                  </div>
				  <label for="chequeddno" class="col-sm-2 control-label">Cheque/DD no</label>

                  <div class="col-sm-4">
                    <input class="form-control" id="chequeddnotext" placeholder="Cheque/DD no" disabled="disabled" name="chequeddnotext" maxlength="15">
                  </div>
                </div>
                               
                <div class="row form-group" id="roundUpAmount">
				  <label for="adinputamount" class="col-sm-2 control-label">RoundUp Amount</label>

                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="totalrounup" placeholder="RoundUp" disabled="disabled" name="roundup">
                  </div>
                </div>
                
                <div class="row form-group">
				  <label for="adinputamount" class="col-sm-2 control-label">Total Amount</label>

                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="totalinputtext" placeholder="TotalAmount" disabled="disabled" name="totalAmount" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;" maxlength="10">
                  </div>
                </div>
                
      </div>
	  <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-default" id="finalSave">Save</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ./wrapper -->
<!--Quick Admission popup-->
<div class="modal fade" id="quickadmissionmodal" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Quick Admission</h4>
      </div>
      <div class="modal-body" >
	  
	  
              <div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                  <input id="name" type="text" path="name" class="form-control" name="name" style="text-transform: uppercase" placeholder="Student Name" maxlength="29"/>
                  </div>
                </div>
				
				<div class="row form-group">
                  <label for="inputrollno" class="col-sm-2 control-label">MRIN</label>

                  <div class="col-sm-10">
                  <input id="collegeCode" type="text" path="rollNo" class="form-control" name="rollNo" placeholder="College Code" style="float:left; width:49%; margin-right:1%" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;" maxlength="8"/>
                  <input id="mrno" type="text" path="rollNo" class="form-control" name="rollNo" placeholder="MRIN NO" style="float:left; width:49%; margin-right:1%" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;" maxlength="4"/>
                   <!--  <input type="text" class="form-control" id="inputrollno" placeholder="Roll no"> -->
                  </div>
				  
                </div>
				<div class="row form-group">
                  <label for="inputdob" class="col-sm-2 control-label">Date of Birth</label>
                  <div class="col-sm-10">
                   <div class="input-group date">
                       <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                       </div>
                        <input id="datepicker" type="text" path="dob" class="form-control pull-right" name="dob" placeholder="Date Of Birth"/>
                   </div>
                  
                  
                   <!--  <input type="text" class="form-control" id="inputdob" placeholder="Date of Birth"> -->
                  </div>
				  
                </div>
			
				
      </div>
	  <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-default" id="showAdmission">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="scienceChooseModel" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Types Of Science</h4>
      </div>
      <div class="modal-body" >
	  
	  
              <div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Category</label>

                  <div class="col-sm-10">
                  <select name="scienceChoose" class="form-control" style="width: 100%;" id="scienceChoose">
				             <option value="Select">Select</option>
				             <option value="Biology">Phy Science</option>
				             <option value="Regular Science">Bio Science</option>
                   </select>
                  </div>
                </div>
      </div>
	  <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-default" id="saveScienceChoose">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->






<!-- ./wrapper -->
<!-- REQUIRED JS SCRIPTS -->



    <spring:url value="/js/jquery-2.2.3.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script>
    
    <spring:url value="/js/bootstrap.min.js" var="bootstrapminjs"></spring:url>
    <script src="${bootstrapminjs}"></script>
    
    <spring:url value="/js/select2.full.min.js" var="selectminjs"></spring:url>
    <script src="${selectminjs}"></script>
    
    <spring:url value="/js/jquery.dataTables.min.js" var="jquerydataTablesminjs"></spring:url>
    <script src="${jquerydataTablesminjs}"></script>
    
     <spring:url value="/js/dataTables.bootstrap.min.js" var="dataTablesbootstrapminjs"></spring:url>
    <script src="${dataTablesbootstrapminjs}"></script>
    
    <spring:url value="/js/bootstrap-datepicker.js" var="bootstrapdatepickerjs"></spring:url>
    <script src="${bootstrapdatepickerjs}"></script>
    
    <spring:url value="/js/jquery.slimscroll.min.js" var="jqueryslimscrollminjs"></spring:url>
    <script src="${jqueryslimscrollminjs}"></script>
    

    
    <spring:url value="/js/app.min.js" var="appminjs"></spring:url>
    <script src="${appminjs}"></script>


<script>
//Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy'
    });
    
    //$('#datepicker1').datepicker("setDate", new Date());
     var today = new Date();
    $('#datepicker1').datepicker({
    	 setDate:today,
    	 format: 'dd/mm/yyyy',
         autoclose:true,
         endDate: "today",
         maxDate: today,
         todayHighlight: true
        	/* onSelect: function(datetext){
                var d = new Date(); // for now
                var h = d.getHours();
                h = (h < 10) ? ("0" + h) : h ;
                var m = d.getMinutes();
                m = (m < 10) ? ("0" + m) : m ;
                var s = d.getSeconds();
                s = (s < 10) ? ("0" + s) : s ;
                datetext = datetext + " " + h + ":" + m + ":" + s;
                alert(datetext);
                //dateTime=datetext;
            } */
      });
    
    
    
</script>

 <script type="text/javascript">
 

 
    $(document).ready(function() {
    	 /* $('#admissionmodal').modal({
 	        backdrop: 'static',
 	        keyboard: false,
 	        
 	        
 	    })
    	$("#admissionmodal").modal('hide'); */
    	
    	var dateTime='';
    	var savedId= '';    	
    	
    	var selectedSciencevalue='';
    	
    		$(document).on('click' , '#saveScienceChoose' , function(){
    			selectedSciencevalue=$('#scienceChoose').find(':selected').text();
    			$("#scienceChooseModel").modal('hide');
    	  });
    	
    		var receiptNumberCheckBoxStatus=false;
    		
    		$('#receiptNumberCheckBox').change(function(){	
    			if($(this).is(':checked')){
        			$("#receiptNumberDiv").show();
        			receiptNumberCheckBoxStatus=true;
        		}else{
        			receiptNumberCheckBoxStatus=false;
        			$("#receiptNumberDiv").hide();
        		}	
    			
    		 });
    		
    		
    	var disableStatus=true;
    	var additionalCheckBoxStatus=false;
    	$('#additionalCheckBox').change(function(){	
    		
    		var yearBatch=$('#yearBatch'+savedId).find(':selected').text();
    		var course=$('#course'+savedId).find(':selected').text();
    		var inputAmount=$("#totalinputtext").val();
    		var totalrounupAmount=$("#totalrounup").val();
    		var config='${config}';
    		if($(this).is(':checked')){
    			$("#addReasonDiv").show();
    			additionalCheckBoxStatus=true;
    			//$("#totalinputtext").val(parseInt(inputAmount));	
    		}else{
    			additionalCheckBoxStatus=false;
    			$("#addReasonDiv").hide();
    			 if(course=='UG' && yearBatch=='1st Year'){
	    			 $("#roundUpAmount").show();
	    			 $("#totalrounup").val(parseInt(totalrounupAmount));	
	    		 }else{
	    			 $("#roundUpAmount").hide();
	    		 }
    			 if(config=='UG'){
    				 $("#totalinputtext").val($("#inputamount").val());	 
    			 }else{
    				 $("#totalinputtext").val($("#inputamount").val());
    			 }
    			 
    		}
    	});
    	
    	$("#paymentModeDrop").change(function(){	
    	   if($("#paymentModeDrop").val()=='Cash'){
    		   disableStatus=true;
    		   $("#chequeddnotext").prop("disabled", true);
    	   }else{
    		   disableStatus=false;
    		   $("#chequeddnotext").prop("disabled", false);
    	   }
    	});
    	
    	$("#adinputreasontext").change(function(){	
    		var actalAmt=$("#inputamount").val();
    		var totalrounupAmount=$("#totalrounup").val();
     	   if($("#adinputreasontext").val()=='Practical'){
 			  //var roundupAmount=Math.ceil(actalAmt / 10) * 10;
 			  //$("#totalrounup").val(parseInt(roundupAmount)-parseInt(actalAmt));
 			  $("#totalinputtext").val(actalAmt);
     		   $("#adinputamounttext").prop("disabled", true);
     		   $("#adinputamounttext").val('100');
     		   var inputAmount=$("#totalinputtext").val();
         	   $("#totalinputtext").val(parseInt(inputAmount)+100);
     	   }else if ($("#adinputreasontext").val()=='Others'){
     		  $("#adinputamounttext").prop("disabled", false);
    		  $("#adinputamounttext").val('');
    		  //$("#totalinputtext").val(parseInt($("#totalrounup").val())+parseInt(actalAmt));
    		  //alert(totalrounupAmount);
    		  $("#totalinputtext").val(parseInt(actalAmt));
     	   }else{
     		  $("#adinputamounttext").prop("disabled", true);
     		  $("#adinputamounttext").val('');
     		 $("#totalinputtext").val(parseInt(actalAmt));
     	   }
     	});
    	
    	
    	

    	
    	  $(document).on('click' , '#print' , function(){
    		   	var saveId= $(this).prop('class');
    	    	savedId=saveId.substring(6,saveId.length);
    	    	var url='${print_url}'
    	    	var fundType=$('#fundType'+savedId).find(':selected').text();
    		    var win = window.open(url+"/print?printid="+savedId+"&tran_Type="+fundType, '_blank');
    	    	//var win = window.open("http://host:3030/DailyCollectionRegister/print?printid="+savedId, '_blank');

    	  });
    $('#showAdmission').click(function(){
    	var name=$("#name").val();
    	var alpha = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/
    	var collegeCode=$("#collegeCode").val();
    	var mrno=$("#mrno").val();
    	if(name==''){
    		alert("Please Enter A Name Before Save");
    		return;
    	}
    	 if (!alpha.test(name)) {
    	        alert('Only Characters Allowed In Name');       
    	        return false;
    	   }
    	
    	if(collegeCode==''){
    		alert("College Code Is Mandatory");
    		return;
    	}
    	if(collegeCode.length<8){
    		alert("College Code Should Be 8 Digit");
    		return;
    	}
    	if(mrno==''){
    		alert("MRI No Is Mandatory");
    		return;
    	}
    	if(mrno.length<4){
    		alert("MRI No Should Be 4 Digit");
    		return;
    	} 
    	if($("#datepicker").val()==''){
    		alert("Dae Of Birth is mandatory");
    		return;
    	}
    	
    	 var formDetailsJson={};
   	    formDetailsJson={
   			"name":name.toUpperCase(),
   			"collegeCode": $("#collegeCode").val(),
   			"mrNo": $("#mrno").val(),
   			"dob": $("#datepicker").val()
   	    }
    	 $.ajax({
	    	    type: "POST",
	    	    url: "showAdmissionFromPopup", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
	    	    success: function(result) {
	    	    	if(result=='ERROR'){
	    	    		 alert("Unable To Add Student Details At This Moment");
	    	    	 }else if(result=='MRN_ERROR'){
	    	    		 alert("College ID/Mrin No is Available in Database...Try With Other");
	    	    	 }
	    	    		 else{
	    	    		 window.location.href = "admisssionShowFromPopup";
	    	    	} 
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Verify The Data At This Moment");
	    	    }
	    	     });
  	});
    
    $(document).on('click' , '#EditStudent' , function(){
    	if($(this).text()=='Edit'){
    		 $("#fundType"+savedId).prop("disabled", false);
    		 $("#course"+savedId).prop("disabled", false);
    		 $("#stream"+savedId).prop("disabled", false);
    		 $("#yearBatch"+savedId).prop("disabled", false);
    		 $("#category"+savedId).prop("disabled", false);
    		 $("#session"+savedId).prop("disabled", false);
    		 var saveButton = $('.saveAccess'+savedId).css({'pointer-events': '', 'opacity' :'1'});
    		 saveButton.empty().append("Save");
    		 var proceed =$('.procee'+savedId).css({'pointer-events': 'none', 'opacity' :'0.3'});
    		 proceed.empty().append("Proceed");	
    	}else{
    		var id=savedId;
    		savedId=savedId+'AD';
    		 $.ajax({
 	    	    type: "POST",
 	    	    url: "saveVerifyStatus", //BookInfoyour valid url
 	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
 	    	    processData: false,
 	    	    data: JSON.stringify(savedId), //json object or array of json objects
 	    	    success: function(result) {
 	    	    	 if(result=='ERROR'){
 	    	    		 alert("Unable To Verify The Data At This Moment");
 	    	    	 }else{
 	    	    		 var saveButton = $('.saveAccess'+id).css({'pointer-events': 'none', 'opacity' :'0.3'});
 	    	    		 saveButton.empty().append("Verified");
 	    	    		 var proceed =$('.procee'+id).css({'pointer-events': '', 'opacity' :'1'});
 	    	    		 proceed.empty().append("Proceed");	
 	    	    	} 
 	    	    },
 	    	    error: function(result){
 	    	    	alert("Unable To Verify The Data At This Moment");
 	    	    }
 	    	     });
    		
    		
    		
    	}
    });
    
    $(document).on('click' , '#proceed' , function(){
    	var saveId= $(this).prop('class');
    	savedId=saveId.substring(6,saveId.length);
    	 $.ajax({
	    	    type: "POST",
	    	    url: "getFinalAmount", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(savedId), //json object or array of json objects
	    	    success: function(result) {
	    	    	 if(result==''){
	    	    		 alert("Unable To find the amount with the combination of inputs...Please manage the head before doing transaction");
	    	    		/*  $("#admissionmodal").modal('show');
	 	    	    	$("#inputamount").prop("disabled", false); */
	    	    	 }else{
	    	    		 $("#avlAmnt").html('${availableAmt}');
	    	    		 $("#inputamount").val(result);
	    	    		 var yearBatch=$('#yearBatch'+savedId).find(':selected').text();
	    	    		 var course=$('#course'+savedId).find(':selected').text();
	    	    		 if(course=='UG' && yearBatch=='1st Year'){
	    	    			 $("#roundUpAmount").show();
	    	    			 var roundupAmount=Math.ceil($("#inputamount").val() / 10) * 10;
	    	    			 $("#totalrounup").val(parseInt(roundupAmount)-parseInt(result));
	    	    			 $("#totalinputtext").val(roundupAmount);
	    	    			 $("#receiptNumberCheckBoxDiv").hide();
	    	    		 }else if(course=='UG' && (yearBatch=='2nd Year' || yearBatch=='3rd Year')){
	    	    			 $("#receiptNumberCheckBoxDiv").show();
	    	    			 $("#roundUpAmount").hide();
	    	    			 $("#totalinputtext").val(parseInt(result));
	    	    			 
	    	    		 }
	    	    		 else{
	    	    			 $("#roundUpAmount").hide();
	    	    			 $("#totalinputtext").val(parseInt(result));
	    	    			 $("#receiptNumberCheckBoxDiv").hide();
	    	    		 }
	    	    		 $("#inputamount").prop("disabled", true);
	    	    		 $("#admissionmodal").modal('show');
	    	    	} 
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To find the amount withe the combination of inputs...Please manage the head before doing transaction");
	    	    }
	    	     });
    });
    $(document).on('click' , '#finalSave' , function(){
    	 var amount=$("#inputamount").val();
    	 var adinputamount=$("#adinputamount").val();
    	 var transactionDate=$("#datepicker1").val();;
    	 var alpha = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/
    	 var additionalCheckBoxval='';
    	 var config='${config}';
    	 var addReasonInputText=$("#adinputreasontext").val();
    	 if(amount==''){
    		 alert("Pleae Enter Amount");
    		 return;
    	 }
    	 if(transactionDate==''){
    		 alert("Pleae Enter Transaction Date");
    		 return;
    	 }
    	 var receiptNumber='';
    	 if(receiptNumberCheckBoxStatus==true){
    		 var receiptNumberText=$("#receiptNumberText").val();
    		 if(receiptNumberText==''){
    			 alert("Pleae Enter Receipt Number");
        		 return;
    		 }
    		 else if(receiptNumberText==0 || receiptNumberText>440){
    			 alert("Please Choose Receipt Number Between 1 To 440");
    			 return;
    		 }
    		 receiptNumber=receiptNumberText;
    	 }
    	 
    	 
    	 
    	 
    	 if(additionalCheckBoxStatus==true){
    		 if(config=='UG'){
    			 var inputreason=$("#adinputreasontext").val();
    	    		if(inputreason=='Select'){
    	    			alert("Pleae Select Additional Reason");
    	    			return;
    	    		}
    	    		else{
    	    			additionalCheckBoxval=$("#adinputreasontext").val();
    	    		}	 
    		 }else{
    			 var inputreasonText=$("#addReasonText").val();
    			 if(inputreasonText=='' || inputreasonText==null){
    				 alert("Pleae Enter Additional Reason");
 	    			 return;
    			 }else{
    				 additionalCheckBoxval=inputreason;	 
    				 addReasonInputText=inputreason;
    			 }
    			 
    		 }
    		
    		 var inputAmount=$("#adinputamounttext").val();
    		 if(inputAmount==''){
      			alert("Pleae Enter Additional Amount");
      			return;
      		}    	
    		/* if(inputreason=='Others'){
         		if(inputAmount==''){
         			alert("Pleae Enter Additional Amount");
         			return;
         		}    	
         	} */
    		
    		
    		
    	 }
    	 if(disableStatus==false){
         	var chequeNo=$("#chequeddnotext").val();
         	if(chequeNo==''){
         		alert("Pleae Enter DD/Cheque No.");
         		return;
         	}
         }      
    
    	  var formDetailsJson={};
  	    formDetailsJson={
  			"savedId":savedId,
  			"inputamount": $("#inputamount").val(),
  			"paymentModeDrop": $("#paymentModeDrop").val(),
  			"chequeddnotext":  $("#chequeddnotext").val(),
  			"additionalCheckBox": additionalCheckBoxval,
  			"addReason":addReasonInputText, 
  			"addAmount":$("#adinputamounttext").val(),
  			"totalAmount": $("#totalinputtext").val(),
  			"rounupAmount": $("#totalrounup").val(),
  			"transactionDate": transactionDate,
  			"receiptNumber":receiptNumber
  	    }
  	   var saveButton = $('#finalSave').css('pointer-events', 'none');
		saveButton.empty().append("Processing..<i class='fa fa-spinner fa-spin'></i>");
    	 
    	 
    	 
    	 $.ajax({
	    	    type: "POST",
	    	    url: "saveTransaction", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
	    	    success: function(result) {
	    	    	 if(result=='ERROR'){
	    	    		 alert("Unable To Do Payment At This Time");
	    	    		 location.reload();
	    	    	 }else{
	    	    		 alert(result);
	    	    		 var saveButton = $('#finalSave').css('pointer-events', '');
	    	    		 saveButton.empty().append("Save");
	    	    		if(result.includes('Another')){
	    	    			return;
	    	    		} 
	    	    		 saveButton.empty().append("Completed");
	    	    		 window.location.href = "showAdmission";
	    	    	} 
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Save Student At This Time");
	    	    }
	    	     });  
    });
    $( "#adinputamounttext" ).keyup(function() {
    	var inputAmount=$("#totalinputtext").val();
    	var totalRoundUp=$("#totalrounup").val();
    	var amt=$("#inputamount").val();
    	var addtionalAmount=$("#adinputamounttext").val();
    	 var yearBatch=$('#yearBatch'+savedId).find(':selected').text();
		 var course=$('#course'+savedId).find(':selected').text();
    	if(additionalCheckBoxStatus==true){
    		if(addtionalAmount!=''){
    			if(course=='UG' && yearBatch=='1st Year'){
    				 $("#roundUpAmount").show();
    				var totalamt=parseInt(addtionalAmount)+parseInt(amt);
        			var roundupAmount=Math.ceil(totalamt/ 10) * 10;
        			var roundamt=parseInt(roundupAmount)-parseInt(totalamt);
        			$("#totalrounup").val(roundamt);
            		$("#totalinputtext").val(parseInt(roundupAmount));	
    	    	}else{
    	    		$("#roundUpAmount").hide();
    	    		$("#totalinputtext").val(parseInt(amt)+parseInt(addtionalAmount));
    	    	}
        	}else{
        		if(course=='UG' && yearBatch=='1st Year'){
        			$("#roundUpAmount").show();
        			var roundupAmount=Math.ceil(amt/ 10) * 10;
            		$("#totalrounup").val(parseInt(roundupAmount)-parseInt(amt));
            		$("#totalinputtext").val(parseInt(inputAmount));	
        		}else{
        			$("#roundUpAmount").hide();
        			$("#totalinputtext").val(parseInt(amt));	
        		}
        	}
    	}else{
    		if(course=='UG' && yearBatch=='1st Year'){
    			$("#roundUpAmount").show();
    			var roundupAmount=Math.ceil(amt/ 10) * 10;
        		$("#totalrounup").val(parseInt(roundupAmount)-parseInt(amt));
        		$("#totalinputtext").val(parseInt(inputAmount));	
    		}else{
    			$("#roundUpAmount").hide();
    			$("#totalinputtext").val(parseInt(amt));	
    		}
    	}
    	
    	
    });
   /*  $( "#admissionmodal" ).keyup(function() {
    	var inputAmount=$("#inputamount").val();
    	var addtionalAmount=$("#adinputamounttext").val();
    	var roundUpAmt=$("#totalrounup").val();
    	if(additionalCheckBoxStatus==true){
    		if(addtionalAmount!=''){
        		$("#totalinputtext").val(parseInt(addtionalAmount)+parseInt(inputAmount)+parseInt(roundUpAmt));	
        	}else{
        		$("#totalinputtext").val(parseInt(inputAmount)+parseInt(roundUpAmt));	
        	}
    	}else{
    		$("#totalinputtext").val(parseInt(inputAmount)+parseInt(roundUpAmt));	
    	}
    }); */
    
    
    
    
    
    
    
    
    function showVerifyPopup(saveId){
    	savedId=saveId;
    	 $.ajax({
	    	    type: "POST",
	    	    url: "getStudentByStudentId", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(saveId), //json object or array of json objects
	    	    success: function(result) {
	    	    	 if(result==null){
	    	    		 alert("Unable To Show Student Verify Popup At This Time");
	    	    	 }else{
	    	    		 $('#Pname').html(result.name);
	    	    		 $('#ProllNo').html(result.rollNo);
	    	    		 $('#Pdob').html(result.dob);
	    	    		 $('#PfundType').html(result.fundType);
	    	    		 $('#Psession').html(result.session);
	    	    		 $('#PCourse').html(result.course);
	    	    		 $('#PStream').html(result.stream);
	    	    		 $('#PClass').html(result.yearBatch);
	    	    		 $('#PCategory').html(result.category);
	    	    		 $('#PScienceType').html(result.scienceSelect);
	    	    		 $("#dataconfmodal").modal('show');
	    	    	} 
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Show Student Verify Popup At This Time");
	    	    }
	    	     });
    }
    
    
    
    $(document).on('click' , '.saveStudent' , function(){
    	if($(this).text()=='Verify' || $(this).text()=='Verified'){
    		var saveId= $(this).prop('id');
    		//showVerifyPopup(saveId);
    		savedId=saveId;
       	 $.ajax({
       	          type: "POST",
  	              url: "getStudentByStudentId.json", //BookInfoyour valid url
  	              contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
  	              processData: false,
  	              data: JSON.stringify(saveId),   
  	              dataType: 'json',
   	    	      success: function(result) {
   	    	    	if(result==null){
   	    	    		 alert("Unable To Show Student Verify Popup At This Time");
   	    	    	 }else{
   	    	    		 $('#Pname').html(result.name);
   	    	    		 $('#ProllNo').html(result.rollNo);
   	    	    		 $('#Pdob').html(result.dob);
   	    	    		 $('#PfundType').html(result.fundType);
   	    	    		 $('#Psession').html(result.session);
   	    	    		 $('#PCourse').html(result.course);
   	    	    		 $('#PStream').html(result.stream);
   	    	    		 $('#PClass').html(result.yearBatch);
   	    	    		 $('#PCategory').html(result.category);
   	    	    		 $('#PScienceType').html(result.scienceSelect);
   	    	    		 $("#dataconfmodal").modal('show');
   	    	    	}  
   	    	    },
   	    	    error: function(result){
   	    	    	alert("Unable To Show Student Verify Popup At This Time");
   	    	    }
   	    	     });
    	}else{
    		var saveId= $(this).prop('id');
            var sessionId=$('#session'+saveId).find(':selected').text();
            var currenetSession='${currentSession}';
            var fundType=$('#fundType'+saveId).find(':selected').text();
            var rollNo=$('#rollno'+saveId).html();
            var yearBatch=$('#yearBatch'+saveId).find(':selected').text();
            var course=$('#course'+saveId).find(':selected').text();
            var stream=$('#stream'+saveId).find(':selected').text();
            var category=$('#category'+saveId).find(':selected').text();
           /*  if(sessionId=='Select'){
            	alert("Please Select Proper Session");
            	return;
            } */
            if(fundType=='Select'){
            	alert("Please Select Proper FundType");
            	return;
            }
            if(yearBatch=='Select'){
            	alert("Please Select Proper Class");
            	return;
            }
           /*  if(course=='Select'){
            	alert("Please Select Proper Course");
            	return;
            } */
            if(stream=='Select'){
            	alert("Please Select Proper Stream");
            	return;
            }
            if(category=='Select'){
            	alert("Please Select Proper Category");
            	return;
            }
            
            if(stream=='Science' && course=='UG'){
            	var chooseScience=$('#scienceChoose').find(':selected').text();
            	if(chooseScience=='' || chooseScience=='Select'){
            		$("#scienceChooseModel").modal('show');
            		return;
            	}
            }
            
            
            if(fundType=='Admission' && rollNo==''){
            	if(sessionId!=currenetSession){
            		alert("Please Choose Current Session For Admission");
            		return;
            	}
            	if(yearBatch=='1st Year'){
                	var formDetailsJson={};
             	    formDetailsJson={
             			"savedid":saveId,
             			"name": $('#name'+saveId).html(),
             			"rollNo": $('#rollno'+saveId).html(),
             			"dob": $('#dob'+saveId).html(),
             			"session": sessionId,
             			"fundType":$('#fundType'+saveId).find(':selected').text(), 
             			"course":$('#course'+saveId).find(':selected').text(),
             			"stream": $('#stream'+saveId).find(':selected').text(),
             			"yearBatch": '1st',
             			"category": $('#category'+saveId).find(':selected').text(),
             			"scienceSelect":$('#scienceChoose').find(':selected').text()
             	    }	
             	    var saveButton = $('.saveAccess'+saveId).css('pointer-events', 'none');
        			saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
        			 $.ajax({
        		    	    type: "POST",
        		    	    url: "saveStudent", //BookInfoyour valid url
        		    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
        		    	    processData: false,
        		    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
        		    	    success: function(result) {
        		    	    	 if(result==null){
        		    	    		 alert("Unable To Save Student At This Time");
        		    	    	 }else{
        		    	    		 var saveButton = $('.saveAccess'+saveId).css({'pointer-events': '', 'opacity' :'1'});
        		    	    		 saveButton.empty().append("Verify");
        		    	    		 $("#fundType"+saveId).prop("disabled", true);
        		    	    		 $("#course"+saveId).prop("disabled", true);
        		    	    		 $("#stream"+saveId).prop("disabled", true);
        		    	    		 $("#yearBatch"+saveId).prop("disabled", true);
        		    	    		 $("#category"+saveId).prop("disabled", true);
        		    	    		 $("#session"+saveId).prop("disabled", true);
        		    	    		// $('#rollno'+saveId).html(result);
        		    	    		 window.location.href = "showAdmission";
        		    	    		 
        		    	    	} 
        		    	    },
        		    	    error: function(result){
        		    	    	alert("Unable To Save Student At This Time");
        		    	    }
        		    	     });
            	}else{
            		alert("Admission Should Be Start From 1st Year");
            	}

            }else if(fundType=='Re-Admission' && rollNo==''){
            	alert("Please Perform Admission First....Then Do Re-admission");
            }else{
            	alert("Please Perform Admission First....Then Do Re-admission");
            }
         	    return false;
    	}
    });
  });
    
    </script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<script>
 $(window).load(function(){  
   $(".select2").select2(); 
   $('#courseModal').modal('show');
   
    }); 
     
 </script>

</body>
</html>
