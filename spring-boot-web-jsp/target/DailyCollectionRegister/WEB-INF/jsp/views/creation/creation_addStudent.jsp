<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daily Collection Register | Student</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/font-awesome.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/ionicons.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/custom.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/blue.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/skin-blue.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/morris.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/jquery-jvectormap-1.2.2.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/datepicker3.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap3-wysihtml5.min.css"/>" />
  
   <spring:url value="/img" var="images" />
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <jsp:include page="/WEB-INF/jsp/views/common/header.jsp"></jsp:include>
  <jsp:include page="/WEB-INF/jsp/views/common/menu.jsp"></jsp:include>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Student
        <small>Creation</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Creation</a></li>
        <li class="active">Student</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	<!-- Alert message -->
	<!--  <div class="alert alert-success">
  <strong>Success!</strong> Indicates a successful or positive action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
</div>

<div class="alert alert-info">
  <strong>Info!</strong> Indicates a neutral informative change or action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
</div> -->

<!-- <div class="alert alert-warning">
  <strong>Warning!</strong> Indicates a warning that might need attention.
  
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">X�</a>
</div> -->
        <!-- Alert message end-->  
      <div class="box box-default">
	 
	  <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Personal Info</a></li>
              <li><a href="#tab_2" data-toggle="tab">Educational Info</a></li>
              <li><a href="#tab_3" data-toggle="tab">Additional Info</a></li>
              <li><a href="#tab_4" data-toggle="tab">Communication Info</a></li>
			  
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
              <div class="box-body">
			  <div class="row">
            <div class="col-md-3">
			<div class="form-group">
                <label>Prefix</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Mr.</option>
                  <option>Mrs.</option>
                  <option>Miss</option>
                  <option>Transgender</option>
                  
                </select>
              </div>
			</div>
			<div class="col-md-3">
			<div class="form-group">
                  <label for="firstname">First Name</label>
                  <input type="text" id="firstname" class="form-control" placeholder="First Name">
                </div>
			</div>
			<div class="col-md-3">
			<div class="form-group">
                  <label for="middlename">Middle Name</label>
                  <input type="text" id="middlename" class="form-control" placeholder="Middle Name">
                </div>
			</div>
			<div class="col-md-3">
			<div class="form-group">
                  <label for="lastname">Last Name</label>
                  <input type="text" id="lastname" class="form-control" placeholder="Last Name">
                </div>
			</div>
			</div>
			 <div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="fathername">Father Name</label>
                  <input type="text" id="fathername" class="form-control" placeholder="Father Name">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="mothername">Mother Name</label>
                  <input type="text" id="mothername" class="form-control" placeholder="Mother Name">
                </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                <label>Date of Birth</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
                <!-- /.input group -->
              </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                <label>Religion</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Hindu</option>
                  <option>Muslim</option>
                  <option>Christian</option>
                  <option>Skih</option>
                  
                </select>
              </div>
			</div>
			</div>
          <div class="row">
            <div class="col-md-6">
			<div class="form-group">
                <label>Caste</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">General</option>
                  <option>OBC</option>
                  <option>BC</option>
                  <option>SC</option>
                  
                </select>
              </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                <label>Community</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Hindu</option>
                  <option>Muslim</option>
                  <option>Christian</option>
                  <option>Skih</option>
                  
                </select>
              </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                <label>Gender</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Male</option>
                  <option>Female</option>
                  
                  
                </select>
              </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                <label>Marital Status</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Married</option>
                  <option>Un Married</option>
                  
                  
                </select>
              </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                <label>Mother Tongue</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Odia</option>
                  <option>Hindi</option>
				  <option>Telugu</option>
                  
                  
                </select>
              </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                <label>Nationality</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Indian</option>
                  <option>NRI</option>
                  
                  
                </select>
              </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="aadharnumber">Aadhar Number</label>
                  <input type="text" id="aadharnumber" class="form-control" placeholder="Aadhar Number">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                <label>Blood Group</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">o+</option>
                  <option>O</option>
				  <option>A+</option>
				  <option>A</option>
				  <option>B+</option>
				  <option>B</option>
                  
                  
                </select>
              </div>
			</div>
			</div>
			
          <!-- /.row -->
        </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <div class="row">
            <div class="col-md-6">
			<div class="form-group">
                <label>Academic Session</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Session</option>
                  
                  
                </select>
              </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                <label>Course Name</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Name</option>
                 
                  
                </select>
              </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                <label>Stream</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Science</option>
                  <option>Commerce</option>
				  <option>Arts</option>
                  
                  
                </select>
              </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                <label>Class</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">1st year</option>
                  <option>2nd year</option>
                  
                  
                </select>
              </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                <label>Board</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">CBSC</option>
                  <option>SSC</option>
				  
                  
                  
                </select>
              </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="lastinstname">Last Institute Name</label>
                  <input type="text" id="lastinstname" class="form-control" placeholder="Last Institute Name">
                </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="passingyear">Passing Year</label>
                  <input type="text" id="passingyear" class="form-control" placeholder="Passing Year">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                <label>Exam Type</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Exam Type</option>
                 
                  
                  
                </select>
              </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="cafno">CAF number</label>
                  <input type="text" id="cafno" class="form-control" placeholder="CAF number">
                </div>
			</div>
			 <div class="col-md-6">
			 
			</div>
			</div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <div class="row">
            <div class="col-md-6">
			<div class="form-group">
                <label>Physically Challenged</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Session</option>
                  
                  
                </select>
              </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                <label>NCC Particular</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Name</option>
                 
                  
                </select>
              </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                <label>Sports Player</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Science</option>
                  <option>Commerce</option>
				  <option>Arts</option>
                  
                  
                </select>
              </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                <label>Awarded By</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">1st year</option>
                  <option>2nd year</option>
                  
                  
                </select>
              </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                <label>Athletic Level</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">CBSC</option>
                  <option>SSC</option>
				  
                  
                  
                </select>
              </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                <label>Extra Degree</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">CBSC</option>
                  <option>SSC</option>
				  
                  
                  
                </select>
              </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="fatheroccup">Father’s Occupation</label>
                  <input type="text" id="fatheroccup" class="form-control" placeholder="Father’s Occupation">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="motheroccup">Mother’s Occupation</label>
                  <input type="text" id="motheroccup" class="form-control" placeholder="Mother’s Occupation">
                </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="annualincome">Annual Income</label>
                  <input type="text" id="annualincome" class="form-control" placeholder="Annual Income">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="uploadphoto">Upload Photo</label>
			 <input type="file" id="uploadphoto">
			 </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="uploadsign">Upload Signature</label>
			 <input type="file" id="uploadsign">
			 </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="uploadthumb">Upload Thumb</label>
			 <input type="file" id="uploadthumb">
			 </div>
			</div>
			</div>
              </div>
			  <div class="tab-pane" id="tab_4">
			  <h3 class="box-title">Permanent Address </h3>
			  <div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="blockname">Block/ULB</label>
                  <input type="text" id="blockname" class="form-control" placeholder="Block/ULB">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="cityname">City</label>
                  <input type="text" id="cityname" class="form-control" placeholder="City">
                </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="blockname">District</label>
                  <input type="text" id="blockname" class="form-control" placeholder="District">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="cityname">Pin Code</label>
                  <input type="text" id="cityname" class="form-control" placeholder="Pin Code">
                </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="blockname">State</label>
                  <input type="text" id="blockname" class="form-control" placeholder="State">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="cityname">Country</label>
                  <input type="text" id="cityname" class="form-control" placeholder="Country">
                </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="blockname">Contact No</label>
                  <input type="text" id="blockname" class="form-control" placeholder="Contact No">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="cityname">Mail-id</label>
                  <input type="text" id="cityname" class="form-control" placeholder="Mail-id">
                </div>
			</div>
			</div>
			<!-- permanent address end -->
			<h3 class="box-title pull-left">Mailing Address </h3>
			<div class="checkbox pull-right">
                      <label>
                        <input type="checkbox"> Same as permanent address.
                      </label>
                    </div>
					<div class="clearfix"></div>
			  <div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="blockname">Block/ULB</label>
                  <input type="text" id="blockname" class="form-control" placeholder="Block/ULB">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="cityname">City</label>
                  <input type="text" id="cityname" class="form-control" placeholder="City">
                </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="blockname">District</label>
                  <input type="text" id="blockname" class="form-control" placeholder="District">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="cityname">Pin Code</label>
                  <input type="text" id="cityname" class="form-control" placeholder="Pin Code">
                </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="blockname">State</label>
                  <input type="text" id="blockname" class="form-control" placeholder="State">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="cityname">Country</label>
                  <input type="text" id="cityname" class="form-control" placeholder="Country">
                </div>
			</div>
			</div>
			<div class="row">
            <div class="col-md-6">
			<div class="form-group">
                  <label for="blockname">Contact No</label>
                  <input type="text" id="blockname" class="form-control" placeholder="Contact No">
                </div>
			</div>
			 <div class="col-md-6">
			 <div class="form-group">
                  <label for="cityname">Mail-id</label>
                  <input type="text" id="cityname" class="form-control" placeholder="Mail-id">
                </div>
			</div>
			</div>
			  </div>
			 
              <!-- /.tab-pane -->
            </div>
            
			<!-- /.tab-content -->
          </div>
        <div class="box-footer">
                <button type="submit" class="btn btn-default">Go for Admission</button>
                <button type="submit" class="btn btn-info pull-right" data-toggle="modal" data-target="#datamodal">Save</button>
        </div>
        <!-- /.box-header -->
        
       
      </div>

    </section>
    <!-- /.content -->
	<!-- Main Footer -->
 <jsp:include page="/WEB-INF/jsp/views/common/footer.jsp"></jsp:include>
  </div>
  <!-- /.content-wrapper -->

  

  
  
</div>


<div class="modal fade" id="myhelpModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Help</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="datamodal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Student Data</h4>
      </div>
      <div class="modal-body">
	  <h5>Please Verify the data before making final save.</h5>
        <div class="table-responsive">
  <table class="table">
  <tbody>
  <tr>
  <td>Name</td>
  <td>Sai Raj Khuntia</td>
  <td>Father Name</td>
  <td>Raghu Raj Khuntia</td>
  </tr>
  <tr>
  <td>Mother Name</td>
  <td>Sasmitha Khuntia</td>
  <td>Date Of Birth</td>
  <td>15/06/2001</td>
  </tr>
  </tbody>
   
  </table>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
 <spring:url value="/js/jquery-2.2.3.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script>
    
    <spring:url value="/js/bootstrap.min.js" var="bootstrapminjs"></spring:url>
    <script src="${bootstrapminjs}"></script>
    
 <spring:url value="/js/bootstrap-datepicker.js" var="bootstrapdatepickerjs"></spring:url>
    <script src="${bootstrapdatepickerjs}"></script>
    
     <spring:url value="/js/app.min.js" var="appminjs"></spring:url>
    <script src="${appminjs}"></script>


<script>
//Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
</script>

</body>
</html>
