<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daily Collection Register | Head Manage</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap.min.css"/>" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<c:url value="/css/common/font-awesome.min.css"/>" />
  <!-- Ionicons -->
  <link rel="stylesheet" href="<c:url value="/css/common/ionicons.min.css"/>" />
   <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<c:url value="/css/common/datepicker3.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/dataTables.bootstrap.css"/>" />
  <!-- Select2 -->
  <link rel="stylesheet" href="<c:url value="/css/common/select2.min.css"/>" />
  <!-- Theme style -->
  <link rel="stylesheet" href="<c:url value="/css/customize/custom.min.css"/>" />
  
  <link rel="stylesheet" href="<c:url value="/css/common/skin-blue.min.css"/>" />
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

  <!-- Main Header -->
  <jsp:include page="/WEB-INF/jsp/views/common/header.jsp"></jsp:include>
   <jsp:include page="/WEB-INF/jsp/views/common/menu.jsp"></jsp:include>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Head Manage
        <small>Analytic</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Analytic</a></li>
        <li class="active">Head Manage</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	<!-- Alert message -->
	 <div class="alert alert-success" style="display:none;">
  <strong>Success!</strong> Indicates a successful or positive action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-info" style="display:none;">
  <strong>Info!</strong> Indicates a neutral informative change or action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-warning" style="display:none;">
  <strong>Warning!</strong> Indicates a warning that might need attention.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>

<div class="alert alert-danger" style="display:none;">
  <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
</div>
        <!-- Alert message end-->  
		<!-- Search filter--> 
		<div class="row">		
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Search</h3>

              <div class="box-tools pull-right">
			  <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#addnewheadmodal"><i class="fa fa-plus"></i>  Add New Head</button>
			  </div>
			  </div>
			  <div class="box-body">
			  <div class="form-group">
                <label>Search By Session</label>
                <select class="form-control" data-placeholder="Select a Stream" style="width: 100%;" id="searchbySession">
                             <option value="">Select</option>
                             <option value="2017-18">2017-18</option>
                             <option value="2016-17">2016-17</option>
                             <option value="2015-16">2015-16</option>
                </select>
              </div>
			  <div class="form-group">
                <label>Search head to manage</label>
                <select class="form-control select2" multiple="multiple" data-placeholder="Select a Stream" style="width: 100%;" id="searchCategory">
                 <c:if test="${config eq '+2'}">
                 <c:forEach var="applicationConfig" items="${applicationConfig}">
                   <c:if test="${applicationConfig eq 'Science'}">
                     <option value="firstTable" id="first">+2 1st yr science</option>
                     <option value="secondTable" id="second">+2 2nd yr science</option>
                   </c:if>
                   <c:if test="${applicationConfig eq 'Arts'}">
                    <option value="thirdTable" id="third"> +2 1st yr arts</option>
                    <option value="fourthTable" id="fourth">+2 2nd yr arts</option>
                   </c:if>
                   <c:if test="${applicationConfig eq 'Commerce'}">
                     <option value="fifthTable" id="fifth"> +2 1st yr commerce</option>
                     <option value="sixthTable" id="sixth">+2 2nd yr commerce</option>
                   </c:if>
                 </c:forEach> 
                 </c:if>
                 <c:if test="${config eq 'UG'}">
                 <c:forEach var="applicationConfig" items="${applicationConfig}">
                   <c:if test="${applicationConfig eq 'Science'}">
                     <option value="firstTable" id="first">UG 1st yr science</option>
                     <option value="secondTable" id="second">UG 2nd yr science</option>
                     <option value="seventhTable" id="third">UG 3rd yr science</option>
                   </c:if>
                   <c:if test="${applicationConfig eq 'Arts'}">
                    <option value="thirdTable" id="third"> UG 1st yr arts</option>
                    <option value="fourthTable" id="fourth">UG 2nd yr arts</option>
                    <option value="eigthTable" id="third">UG 3rd yr arts</option>
                   </c:if>
                   <c:if test="${applicationConfig eq 'Commerce'}">
                     <option value="fifthTable" id="fifth"> UG 1st yr commerce</option>
                     <option value="sixthTable" id="sixth">UG 2nd yr commerce</option>
                     <option value="ninthTable" id="third">UG 3rd yr commerce</option>
                   </c:if>
                 </c:forEach> 
                 </c:if>
                </select>
              </div>
              <div class="form-group">
                 <label>Select All Categories</label>
                  <div class="col-sm-1" style="width: 10px;">
                    <input type="checkbox"  value="selectAllsearch" id="checkboxforsearch">
                  </div>
              </div>
              <div class="form-group">
              <c:if test="${searchSession ne null}">
                 <label>Search For :: ${searchSession}</label>
               </c:if>  
              </div>
			  </div>
			  </div>
			  </div>
			  </div>
			  
        <!--Science 1st year-->
        <div class="row" id="21stscience" style="display: none;">
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Science 1st yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <div class="box-body">
              <table id="science1" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
               
               <tbody>
               <c:if test="${head1 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head1}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td><input type="text" class="form-control" value="${firsthead.headAmount}" disabled="disabled" id="firstheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editfirsthead${loop.index}" class="EditFirstHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletefirsthead${loop.index}" class="DeleteFirstHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head1 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
           
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Science 1st yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <div class="box-body">
              <table id="science2" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
               <c:if test="${head2 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${head2}" var="secondHead" varStatus="loop">
                 <c:if test="${secondHead ne null}">
                 <c:set var="total" value="${total + secondHead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="secondheadName${loop.index}">${secondHead.headname}</td>
   				    <td><input type="text" class="form-control" value="${secondHead.headAmount}" disabled="disabled" id="secondheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editsecondhead${loop.index}" class="EditSecondHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletesecondhead${loop.index}" class="DeleteSecondHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${secondHead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="secondheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head2 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
           
          </div>
</div>
</div> 
<!--Science 1st year End-->

<!--Science 2nd year-->
 <div class="row" id="22ndscience" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Science 2nd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                       <c:if test="${head3 ne null}">
                       <c:set var="total" value="${0}" />
                 <c:forEach items="${head3}" var="thirdhead" varStatus="loop">
                 <c:if test="${thirdhead ne null}">
                 <c:set var="total" value="${total + thirdhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="thirdheadName${loop.index}">${thirdhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${thirdhead.headAmount}" disabled="disabled" id="thirdheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editthirdhead${loop.index}" class="EditthirdHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletethirdhead${loop.index}" class="DeletethirdHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${thirdhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="thirdheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head3 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Science 2nd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${head4 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${head4}" var="fourthhead" varStatus="loop">
                 <c:if test="${fourthhead ne null}">
                 <c:set var="total" value="${total + fourthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="fourthheadName${loop.index}">${fourthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${fourthhead.headAmount}" disabled="disabled" id="fourthheadAmount${loop.index}"> </td>
				   <td><a href="javascript:void()" id="editfourthhead${loop.index}" class="EditfourthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletefourthhead${loop.index}" class="DeletefourthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${fourthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="fourthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head4 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
</div>
<!--Science 2nd year end-->

 <!--Arts 1st year-->
        <div class="row" id="21starts" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Arts 1st yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts1" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                             <c:if test="${head5 ne null}">
                             <c:set var="total" value="${0}" />
                 <c:forEach items="${head5}" var="fifthhead" varStatus="loop">
                 <c:if test="${fifthhead ne null}">
                 <c:set var="total" value="${total + fifthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="fifthheadName${loop.index}">${fifthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${fifthhead.headAmount}" disabled="disabled" id="fifthheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editfifthhead${loop.index}" class="EditfifthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletefifthhead${loop.index}" class="DeletefifthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${fifthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="fifthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head5 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
 <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Arts 1st yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <div class="box-body">
              <table id="arts2" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                             <c:if test="${head6 ne null}">
                             <c:set var="total" value="${0}" />
                 <c:forEach items="${head6}" var="sixthhead" varStatus="loop">
                 <c:if test="${sixthhead ne null}">
                 <c:set var="total" value="${total + sixthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="sixthheadName${loop.index}">${sixthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${sixthhead.headAmount}" disabled="disabled" id="sixthheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editsixthhead${loop.index}" class="EditsixthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletesixthhead${loop.index}" class="DeletesixthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${sixthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="sixthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head6 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
           
          </div>
</div> -->
</div>
<!--Arts 1st year End-->

<!--Arts 2nd year-->
 <div class="row" id="22ndarts" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Arts 2nd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${head7 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${head7}" var="seventhhead" varStatus="loop">
                 <c:if test="${seventhhead ne null}">
                 <c:set var="total" value="${total + seventhhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="seventhheadName${loop.index}">${seventhhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${seventhhead.headAmount}" disabled="disabled" id="seventhheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editseventhhead${loop.index}" class="EditseventhHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deleteseventhhead${loop.index}" class="DeleteseventhHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${seventhhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="seventhheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head7 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Arts 2nd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                             <c:if test="${head8 ne null}">
                             <c:set var="total" value="${0}" />
                 <c:forEach items="${head8}" var="eighthhead" varStatus="loop">
                 <c:if test="${eighthhead ne null}">
                 <c:set var="total" value="${total + eighthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="eighthheadName${loop.index}">${eighthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${eighthhead.headAmount}" disabled="disabled" id="eighthheadAmount${loop.index}"> </td>
				   <td><a href="javascript:void()" id="editeighthhead${loop.index}" class="EditeighthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deleteeighthhead${loop.index}" class="DeleteeighthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${eighthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="eighthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head8 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
               
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
</div>
<!--Arts 2nd year end-->


<div class="row" id="21stcommerce" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 commerce 1st yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${head9 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${head9}" var="ninthhead" varStatus="loop">
                 <c:if test="${ninthhead ne null}">
                 <c:set var="total" value="${total + ninthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="ninthheadName${loop.index}">${ninthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${ninthhead.headAmount}" disabled="disabled" id="ninthheadAmount${loop.index}"> </td>
				   <td><a href="javascript:void()" id="editninthhead${loop.index}" class="EditninthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deleteninthhead${loop.index}" class="DeleteninthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${ninthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="ningthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head9 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 commerce 1st yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${head10 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${head10}" var="tengthhead" varStatus="loop">
                 <c:if test="${tengthhead ne null}">
                 <c:set var="total" value="${total + tengthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="tengthheadName${loop.index}">${tengthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${tengthhead.headAmount}" disabled="disabled" id="tengthheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="edittengthhead${loop.index}" class="EdittengthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletetengthhead${loop.index}" class="DeletetengthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${tengthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="tengthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head10 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
</div>

<div class="row" id="22ndcommerce" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 commerce 2nd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                             <c:if test="${head11 ne null}">
                             <c:set var="total" value="${0}" />
                 <c:forEach items="${head11}" var="eleventhhead" varStatus="loop">
                 <c:if test="${eleventhhead ne null}">
                 <c:set var="total" value="${total + eleventhhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="eleventhheadName${loop.index}">${eleventhhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${eleventhhead.headAmount}" disabled="disabled" id="eleventhheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editelevengthhead${loop.index}" class="EditEleventhHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deleteelevengthhead${loop.index}" class="DeleteEleventhHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${eleventhhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="eleventhheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head11 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage +2 Commerce 2nd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${head12 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${head12}" var="twelthhead" varStatus="loop">
                 <c:if test="${twelthhead ne null}">
                 <c:set var="total" value="${total + twelthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="twelthheadName${loop.index}">${twelthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${twelthhead.headAmount}" disabled="disabled" id="twelthheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="edittwelthhead${loop.index}" class="EditTwelthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletetwelthhead${loop.index}" class="DeleteTwelthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${twelthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="twelthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${head12 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
               
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
</div>




  <div class="row" id="31stscience" style="display: none;">
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Science 1st yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <div class="box-body">
              <table id="science1" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
               
               <tbody>
               <c:if test="${headUG1 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG1}" var="firsthead" varStatus="loop">
                 <c:if test="${firsthead ne null}">
                 <c:set var="total" value="${total + firsthead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="firstheadName${loop.index}">${firsthead.headname}</td>
   				    <td><input type="text" class="form-control" value="${firsthead.headAmount}" disabled="disabled" id="firstheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editfirsthead${loop.index}" class="EditFirstHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletefirsthead${loop.index}" class="DeleteFirstHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${firsthead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="firstheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG1 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
                </c:if>
                </tbody>
              </table>
            </div>
           
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Science 1st yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <div class="box-body">
              <table id="science2" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
               <c:if test="${headUG2 ne null}">
               <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG2}" var="secondHead" varStatus="loop">
                 <c:if test="${secondHead ne null}">
                 <c:set var="total" value="${total + secondHead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="secondheadName${loop.index}">${secondHead.headname}</td>
   				    <td><input type="text" class="form-control" value="${secondHead.headAmount}" disabled="disabled" id="secondheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editsecondhead${loop.index}" class="EditSecondHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletesecondhead${loop.index}" class="DeleteSecondHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${secondHead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="secondheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG2 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
           
          </div>
</div>
</div> 
<!--Science 1st year End-->

<!--Science 2nd year-->
 <div class="row" id="32ndscience" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Science 2nd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                       <c:if test="${headUG3 ne null}">
                       <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG3}" var="thirdhead" varStatus="loop">
                 <c:if test="${thirdhead ne null}">
                 <c:set var="total" value="${total + thirdhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="thirdheadName${loop.index}">${thirdhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${thirdhead.headAmount}" disabled="disabled" id="thirdheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editthirdhead${loop.index}" class="EditthirdHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletethirdhead${loop.index}" class="DeletethirdHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${thirdhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="thirdheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG3 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Science 2nd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${headUG4 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG4}" var="fourthhead" varStatus="loop">
                 <c:if test="${fourthhead ne null}">
                 <c:set var="total" value="${total + fourthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="fourthheadName${loop.index}">${fourthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${fourthhead.headAmount}" disabled="disabled" id="fourthheadAmount${loop.index}"> </td>
				   <td><a href="javascript:void()" id="editfourthhead${loop.index}" class="EditfourthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletefourthhead${loop.index}" class="DeletefourthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${fourthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="fourthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG4 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
</div>
<!--Science 2nd year end-->

 <div class="row" id="33ndscience" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Science 3rd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                       <c:if test="${headUG13 ne null}">
                       <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG13}" var="thirdhead" varStatus="loop">
                 <c:if test="${thirdhead ne null}">
                 <c:set var="total" value="${total + thirdhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="thirteenheadname${loop.index}">${thirdhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${thirdhead.headAmount}" disabled="disabled" id="thirteenheadamount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editthiteenhead${loop.index}" class="Editthirdthirteen">Edit</a></td>
				    <td><a href="javascript:void()" id="deletethirteenhead${loop.index}" class="Deletethirdthirteen">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${thirdhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
				  <th><input type="number" class="form-control" id="thirteenheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG13 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Science 3rd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${headUG14 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG14}" var="fourthhead" varStatus="loop">
                 <c:if test="${fourthhead ne null}">
                 <c:set var="total" value="${total + fourthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="fourteenheadName${loop.index}">${fourthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${fourthhead.headAmount}" disabled="disabled" id="fourteenheadAmount${loop.index}"> </td>
				   <td><a href="javascript:void()" id="editfourteenhead${loop.index}" class="Editthirdfourteen">Edit</a></td>
				    <td><a href="javascript:void()" id="deletefourteenhead${loop.index}" class="Deletethirdfourteen">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${fourthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="fourteenheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG14 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
</div>




 <!--Arts 1st year-->
        <div class="row" id="31starts" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Arts 1st yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts1" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                             <c:if test="${headUG5 ne null}">
                             <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG5}" var="fifthhead" varStatus="loop">
                 <c:if test="${fifthhead ne null}">
                 <c:set var="total" value="${total + fifthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="fifthheadName${loop.index}">${fifthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${fifthhead.headAmount}" disabled="disabled" id="fifthheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editfifthhead${loop.index}" class="EditfifthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletefifthhead${loop.index}" class="DeletefifthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${fifthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="fifthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG5 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
 <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Arts 1st yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <div class="box-body">
              <table id="arts2" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                             <c:if test="${headUG6 ne null}">
                             <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG6}" var="sixthhead" varStatus="loop">
                 <c:if test="${sixthhead ne null}">
                 <c:set var="total" value="${total + sixthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="sixthheadName${loop.index}">${sixthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${sixthhead.headAmount}" disabled="disabled" id="sixthheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editsixthhead${loop.index}" class="EditsixthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletesixthhead${loop.index}" class="DeletesixthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${sixthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="sixthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG6 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
           
          </div>
</div> -->
</div>
<!--Arts 1st year End-->


<!--Arts 2nd year-->
 <div class="row" id="32ndarts" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Arts 2nd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${headUG7 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG7}" var="seventhhead" varStatus="loop">
                 <c:if test="${seventhhead ne null}">
                 <c:set var="total" value="${total + seventhhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="seventhheadName${loop.index}">${seventhhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${seventhhead.headAmount}" disabled="disabled" id="seventhheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editseventhhead${loop.index}" class="EditseventhHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deleteseventhhead${loop.index}" class="DeleteseventhHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${seventhhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="seventhheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG7 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Arts 2nd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                             <c:if test="${headUG8 ne null}">
                             <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG8}" var="eighthhead" varStatus="loop">
                 <c:if test="${eighthhead ne null}">
                 <c:set var="total" value="${total + eighthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="eighthheadName${loop.index}">${eighthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${eighthhead.headAmount}" disabled="disabled" id="eighthheadAmount${loop.index}"> </td>
				   <td><a href="javascript:void()" id="editeighthhead${loop.index}" class="EditeighthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deleteeighthhead${loop.index}" class="DeleteeighthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${eighthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="eighthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG8 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
               
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
</div>
<!--Arts 2nd year end-->

 <div class="row" id="33ndarts" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Arts 3rd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                       <c:if test="${headUG15 ne null}">
                       <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG15}" var="thirdhead" varStatus="loop">
                 <c:if test="${thirdhead ne null}">
                 <c:set var="total" value="${total + thirdhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="fifteenheadName${loop.index}">${thirdhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${thirdhead.headAmount}" disabled="disabled" id="fifteenheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editfifteenhead${loop.index}" class="Editthirdfifteen">Edit</a></td>
				    <td><a href="javascript:void()" id="deletefifteenhead${loop.index}" class="Deletethirdfifteen">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${thirdhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="fifteenheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG15 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Arts 3rd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${headUG16 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG16}" var="fourthhead" varStatus="loop">
                 <c:if test="${fourthhead ne null}">
                 <c:set var="total" value="${total + fourthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="sixteenheadName${loop.index}">${fourthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${fourthhead.headAmount}" disabled="disabled" id="sixteenheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editsixteenhead${loop.index}" class="Editthirdsixteen">Edit</a></td>
				    <td><a href="javascript:void()" id="deletesixteenhead${loop.index}" class="Deletethirdsixteen">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${fourthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="sixteenheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG16 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
</div>


<div class="row" id="31stcommerce" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG commerce 1st yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${headUG9 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG9}" var="ninthhead" varStatus="loop">
                 <c:if test="${ninthhead ne null}">
                 <c:set var="total" value="${total + ninthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="ninthheadName${loop.index}">${ninthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${ninthhead.headAmount}" disabled="disabled" id="ninthheadAmount${loop.index}"> </td>
				   <td><a href="javascript:void()" id="editninthhead${loop.index}" class="EditninthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deleteninthhead${loop.index}" class="DeleteninthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${ninthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="ningthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG9 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG commerce 1st yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${headUG10 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG10}" var="tengthhead" varStatus="loop">
                 <c:if test="${tengthhead ne null}">
                 <c:set var="total" value="${total + tengthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="tengthheadName${loop.index}">${tengthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${tengthhead.headAmount}" disabled="disabled" id="tengthheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="edittengthhead${loop.index}" class="EdittengthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletetengthhead${loop.index}" class="DeletetengthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${tengthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="tengthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG10 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
</div>

<div class="row" id="32ndcommerce" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG commerce 2nd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                             <c:if test="${headUG11 ne null}">
                             <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG11}" var="eleventhhead" varStatus="loop">
                 <c:if test="${eleventhhead ne null}">
                 <c:set var="total" value="${total + eleventhhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="eleventhheadName${loop.index}">${eleventhhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${eleventhhead.headAmount}" disabled="disabled" id="eleventhheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="editelevengthhead${loop.index}" class="EditEleventhHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deleteelevengthhead${loop.index}" class="DeleteEleventhHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${eleventhhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="eleventhheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG11 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Commerce 2nd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="arts22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${headUG12 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG12}" var="twelthhead" varStatus="loop">
                 <c:if test="${twelthhead ne null}">
                 <c:set var="total" value="${total + twelthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="twelthheadName${loop.index}">${twelthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${twelthhead.headAmount}" disabled="disabled" id="twelthheadAmount${loop.index}"> </td>
				    <td><a href="javascript:void()" id="edittwelthhead${loop.index}" class="EditTwelthHead">Edit</a></td>
				    <td><a href="javascript:void()" id="deletetwelthhead${loop.index}" class="DeleteTwelthHead">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${twelthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="twelthheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG12 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
               
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
</div>


 <div class="row" id="33ndcommerce" style="display: none;">
        <!-- left column -->
        <div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Commerce 3rd yr Gen/OBC Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science21" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                       <c:if test="${headUG17 ne null}">
                       <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG17}" var="thirdhead" varStatus="loop">
                 <c:if test="${thirdhead ne null}">
                 <c:set var="total" value="${total + thirdhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="sevnteenheadName${loop.index}">${thirdhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${thirdhead.headAmount}" disabled="disabled" id="sevnteenheadAmount${loop.index}"> </td>
				   <td><a href="javascript:void()" id="editseventeenhead${loop.index}" class="Editthirdseventeen">Edit</a></td>
				    <td><a href="javascript:void()" id="deleteseventeenhead${loop.index}" class="Deletethirdseventeen">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${thirdhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="sevnteenheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG17 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
<div class="col-md-6">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage UG Commerce 3rd yr SC/ST/Girls Heads</h3>
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                  <i class="fa fa-minus"></i></button>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="science22" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th>SL No</th>
                  <th>Head Name</th>
                  
				  <th>Head Amount</th>
				  <th></th>
				  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                            <c:if test="${headUG18 ne null}">
                            <c:set var="total" value="${0}" />
                 <c:forEach items="${headUG18}" var="fourthhead" varStatus="loop">
                 <c:if test="${fourthhead ne null}">
                 <c:set var="total" value="${total + fourthhead.headAmount}" />
                 <tr>
				    <td>${loop.index+1}</td>
                    <td id="eighteenheadName${loop.index}">${fourthhead.headname}</td>
   				    <td><input type="text" class="form-control" value="${fourthhead.headAmount}" disabled="disabled" id="eighteenheadAmount${loop.index}"> </td>
				   <td><a href="javascript:void()" id="editeighteenhead${loop.index}" class="Editthirdeighteen">Edit</a></td>
				    <td><a href="javascript:void()" id="deleteeighteenhead${loop.index}" class="Deletethirdeighteen">Delete</a></td>
                </tr>
                 </c:if>  
                 <c:if test="${fourthhead eq null}">
                 <tr>
                 <td>No Head Yet Added</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 </tr>
                 </c:if> 
                </c:forEach> 
                <tfoot>
                <tr>
				<th></th>
                <th>Total</th>
                
				  <th><input type="number" class="form-control" id="eighteenheadtotal" value="${total}" disabled="disabled"></th>
				  <th></th>
				  <th></th>
                </tr>
                </tfoot>
                </c:if>
                <c:if test="${headUG18 eq null}">
				<tr>
				<td>No Head Yet Added</td>
				</tr>
				<c:out value="null"></c:out>
                </c:if>
                </tbody>
              </table>
            </div>
            <!-- -->
          </div>
</div>
</div>















    </section>
    <!-- /.content -->
	<!-- Main Footer -->
  <jsp:include page="/WEB-INF/jsp/views/common/footer.jsp"></jsp:include>
  </div>
  <!-- /.content-wrapper -->

  

  
  
</div>


<div class="modal fade" id="myhelpModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Help</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--Admission popup-->

<!-- ./wrapper -->
<!--Quick Admission popup-->
<div class="modal fade" id="addnewheadmodal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add new head</h4>
      </div>
      <form action="" id="addNewhead">
      <div class="modal-body" >
              <div class="row form-group">
                  <label for="inputheadname" class="col-sm-2 control-label">Session</label>
                   <div class="col-sm-10">
                  <select class="form-control" id="sessiondropDown"  name="session" data-placeholder="Select a Session" style="width: 470px;">
                             <option value="">Select</option>
                             <option value="2017-18">2017-18</option>
                             <option value="2016-17">2016-17</option>
                             <option value="2015-16">2015-16</option>
                   </select>
                  </div>
                  
                </div>
      
      
      
              <div class="row form-group">
                  <label for="inputheadname" class="col-sm-2 control-label">Head Name</label>
                   <div class="col-sm-10">
                  <select class="form-control" id="inputheadname"  name="headname" data-placeholder="Select a Head" style="width: 470px;">
                             <option value="">Select</option>
				     <c:forEach var="headDetailsList" items="${dropdownList}" varStatus="loop">
				         <c:if test="${headDetailsList.headname ne 'CAF' and headDetailsList.headname ne 'Additional Fee' and headDetailsList.headname ne 'Fine' and headDetailsList.headname ne 'Miscellanious'}">
				             <option value="${headDetailsList.headname}" id="${loop.index}">${headDetailsList.headname}</option>
				         </c:if>
				     </c:forEach>
                   </select>
                  </div>
                  
                </div>
				
				<!-- <div class="row form-group" style="display: none" id="otherHeadDiv">
                  <label for="inputamount" class="col-sm-2 control-label">Other Head name</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="otherHeadName" placeholder="Other head Name" name="otherHeadName">
                  </div>
				  
                </div> -->
				
				
				<div class="row form-group">
                  <label for="inputamount" class="col-sm-2 control-label">Amount</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputamount" placeholder="Amount" name="headAmount" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;" maxlength="9">
                  </div>
				  
                </div>
			  <div class="row form-group">
                <label for="inputamount" class="col-sm-2 control-label">Categories</label>
                 <div class="col-sm-10">
                <select class="form-control select2" id="addHeadCategory" multiple="multiple" name="categories" data-placeholder="Select a Categoy" style="width: 470px;" disabled="disabled">
                <c:if test="${config eq '+2'}">
                 <c:forEach var="applicationConfig" items="${applicationConfig}">
                   <c:if test="${applicationConfig eq 'Science'}">
                     <option value="1">+2 1st yr science Gen/OBC</option>
                     <option value="2">+2 1st yr science SC/ST/Girls</option>
                     <option value="3">+2 2nd yr science Gen/OBC</option>
                     <option value="4">+2 2nd yr science SC/ST/Girls</option>
                   </c:if>
                   <c:if test="${applicationConfig eq 'Arts'}">
                     <option value="5">+2 1st yr arts Gen/OBC</option>
                     <option value="6">+2 1st yr arts SC/ST/Girls</option>
                     <option value="7">+2 2nd yr arts Gen/OBC</option>
                     <option value="8">+2 2nd yr arts SC/ST/Girls</option>
                   </c:if>
                   <c:if test="${applicationConfig eq 'Commerce'}">
                     <option value="9">+2 1st yr commerce Gen/OBC</option>
                     <option value="10">+2 1st yr commerce SC/ST/Girls</option>
                     <option value="11">+2 2nd yr commerce Gen/OBC</option>
                     <option value="12">+2 2nd yr commerce SC/ST/Girls</option>
                   </c:if>
                 </c:forEach> 
                 </c:if>
                 <c:if test="${config eq 'UG'}">
                 <c:forEach var="applicationConfig" items="${applicationConfig}">
                   <c:if test="${applicationConfig eq 'Science'}">
                     <option value="1">UG 1st yr science Gen/OBC</option>
                     <option value="2">UG 1st yr science SC/ST/Girls</option>
                     <option value="3">UG 2nd yr science Gen/OBC</option>
                     <option value="4">UG 2nd yr science SC/ST/Girls</option>
                     <option value="13">UG 3rd yr science Gen/OBC</option>
                     <option value="14">UG 3rd yr science SC/ST/Girls</option>
                   </c:if>
                   <c:if test="${applicationConfig eq 'Arts'}">
                     <option value="5">UG 1st yr arts Gen/OBC</option>
                     <option value="6">UG 1st yr arts SC/ST/Girls</option>
                     <option value="7">UG 2nd yr arts Gen/OBC</option>
                     <option value="8">UG 2nd yr arts SC/ST/Girls</option>
                     <option value="15">UG 3rd yr arts Gen/OBC</option>
                     <option value="16">UG 3rd yr arts SC/ST/Girls</option>
                   </c:if>
                   <c:if test="${applicationConfig eq 'Commerce'}">
                     <option value="9">UG 1st yr commerce Gen/OBC</option>
                     <option value="10">UG 1st yr commerce SC/ST/Girls</option>
                     <option value="11">UG 2nd yr commerce Gen/OBC</option>
                     <option value="12">UG 2nd yr commerce SC/ST/Girls</option>
                     <option value="17">UG 3rd yr commerce Gen/OBC</option>
                     <option value="18">UG 3rd yr commerce SC/ST/Girls</option>
                   </c:if>
                 </c:forEach> 
                 </c:if>
                </select>
                </div>
              </div>
              <div class="row form-group">
              <label for="inputamount" class="col-sm-2 control-label">Select All Categories</label>
                  <div class="col-sm-10">
                    <input type="checkbox"  id="selectAllCategory" checked value="selectAllCategory" name="cheboxSelect">
                  </div>
				  
                </div>
              
              
      </div>
      
	  <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-default" id="saveHead">Save</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ./wrapper -->
<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
    <spring:url value="/js/jquery-2.2.3.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script>
<!-- Bootstrap 3.3.6 -->
    <spring:url value="/js/bootstrap.min.js" var="bootstrapminjs"></spring:url>
    <script src="${bootstrapminjs}"></script>
<!-- Select2 -->
    <spring:url value="/js/select2.full.min.js" var="selectminjs"></spring:url>
    <script src="${selectminjs}"></script>
<!-- DataTables -->
    <spring:url value="/js/jquery.dataTables.min.js" var="jquerydataTablesminjs"></spring:url>
    <script src="${jquerydataTablesminjs}"></script>
     <spring:url value="/js/dataTables.bootstrap.min.js" var="dataTablesbootstrapminjs"></spring:url>
    <script src="${dataTablesbootstrapminjs}"></script>
<!-- SlimScroll -->
    <spring:url value="/js/jquery.slimscroll.min.js" var="jqueryslimscrollminjs"></spring:url>
    <script src="${jqueryslimscrollminjs}"></script>
<!-- bootstrap datepicker -->
    <spring:url value="/js/bootstrap-datepicker.js" var="bootstrapdatepickerjs"></spring:url>
    <script src="${bootstrapdatepickerjs}"></script>
    <spring:url value="/js/app.min.js" var="appminjs"></spring:url>
    <script src="${appminjs}"></script>
<script>

$(document).ready(function() {
	
   	
	$(document).on('click' , '.DeleteFirstHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(15,length);
		var saveButton = $('#deletefirsthead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteFirstHead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deletefirsthead'+ediTId).css('pointer-events', '');
    	    		 saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	 var saveButton = $('#deletefirsthead'+ediTId).css('pointer-events', '');
	    		 saveButton.empty().append("Delete");
    	    }
   })
	});
	
	
	$(document).on('click' , '.DeleteSecondHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(16,length);
		var saveButton = $('#deletesecondhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteSecondHead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deletesecondhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deletesecondhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});
	
	$(document).on('click' , '.DeletethirdHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(15,length);
		var saveButton = $('#deletethirdhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "DeletethirdHead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deletethirdhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deletethirdhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});
	
	$(document).on('click' , '.DeletefourthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(16,length);
		var saveButton = $('#deletefourthhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteFourthHead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deletefourthhead'+ediTId).css('pointer-events', 'none');
    	    		 saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deletefourthhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});
	
	$(document).on('click' , '.DeletefifthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(15,length);
		var saveButton = $('#deletefifthhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteFifthHead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deletefifthhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deletefifthhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});
	
	$(document).on('click' , '.DeletesixthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(15,length);
		var saveButton = $('#deletesixthhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteSixthHead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deletesixthhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deletesixthhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});
	
	$(document).on('click' , '.DeleteseventhHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(17,length);
		var saveButton = $('#deleteseventhhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteSeventhHead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deleteseventhhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deleteseventhhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});
	
	$(document).on('click' , '.DeleteeighthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(16,length);
		var saveButton = $('#deleteeighthhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteEighthHead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deleteeighthhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deleteeighthhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});
	
	$(document).on('click' , '.DeleteninthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(15,length);
		var saveButton = $('#deleteninthhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteNinthHead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deleteninthhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deleteninthhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});
	
	$(document).on('click' , '.DeletetengthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(16,length);
		var saveButton = $('#deletetengthhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteTenthHead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deletetengthhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deletetengthhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    	    }
   })
	});
	
	$(document).on('click' , '.DeleteEleventhHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(19,length);
		var saveButton = $('#deleteelevengthhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteEleventhHead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deleteelevengthhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deleteelevengthhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});
	
	$(document).on('click' , '.DeleteTwelthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(16,length);
		var saveButton = $('#deletetwelthhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteTwelthHead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deletetwelthhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deletetwelthhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});
	

	
	$(document).on('click' , '.Deletethirdthirteen' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(18,length);
		var saveButton = $('#deletethirteenhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deletethirteenhead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deletethirteenhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deletethirteenhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});

	
	
	$(document).on('click' , '.Deletethirdfourteen' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(18,length);
		var saveButton = $('#deletefourteenhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deletefourteenhead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deletefourteenhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deletefourteenhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});

	
	$(document).on('click' , '.Deletethirdfifteen' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(17,length);
		var saveButton = $('#deletefifteenhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deletefifteenhead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deletefifteenhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deletefifteenhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});
	
	
	$(document).on('click' , '.Deletethirdsixteen' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(17,length);
		var saveButton = $('#deletesixteenhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deletesixteenhead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deletesixteenhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deletesixteenhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});
	

	$(document).on('click' , '.Deletethirdseventeen' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(19,length);
		var saveButton = $('#deleteseventeenhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteseventeenhead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deleteseventeenhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deleteseventeenhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});

	
	$(document).on('click' , '.Deletethirdeighteen' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(18,length);
		var saveButton = $('#deleteeighteenhead'+ediTId).css('pointer-events', 'none');
		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
		$.ajax({
    	    type: "POST",
    	    url: "deleteeighteenhead", //BookInfoyour valid url
    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    processData: false,
    	    data: JSON.stringify(ediTId), //json object or array of json objects
    	    success: function(result) {
    	    	 if(result=="ERROR"){
    	    		 alert("Unable To Delete Head Details At This Time");
    	    		 var saveButton = $('#deleteeighteenhead'+ediTId).css('pointer-events', 'none');
    	    			saveButton.empty().append("Delete");
    	    	 }else{
    	    		 alert("Successfully Delete The Head...");
           	     	 window.location.href = "showManageHead";
    	    	} 
    	    },
    	    error: function(result){
    	    	alert("Unable To Delete Head Details At This Time");
    	    	var saveButton = $('#deleteeighteenhead'+ediTId).css('pointer-events', 'none');
    			saveButton.empty().append("Delete");
    	    }
   })
	});

	
	
	
	

	$(document).on('click' , '.EditFirstHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(13, length);
		if($(this).text()=='Edit'){
			$("#firstheadAmount"+ediTId).prop("disabled",false);
		    $("#deletefirsthead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editfirsthead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#firstheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editfirsthead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#firstheadAmount'+ediTId).val(),
     			"headname": $('#firstheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "firsteditHead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==" "){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editfirsthead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deletefirsthead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#firstheadtotal").val(result);
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});
	
	
	
	$(document).on('click' , '.EditSecondHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(14,length);
		if($(this).text()=='Edit'){
			$("#secondheadAmount"+ediTId).prop("disabled",false);
		    $("#deletesecondhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editsecondhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    	    $("#secondheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editsecondhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#secondheadAmount'+ediTId).val(),
     			"headname": $('#secondheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "secondeditHead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==" "){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editsecondhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deletesecondhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#secondheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});
	
	
	
	$(document).on('click' , '.EditthirdHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(13, length);
		if($(this).text()=='Edit'){
			$("#thirdheadAmount"+ediTId).prop("disabled",false);
		    $("#deletethirdhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editthirdhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#thirdheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editthirdhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#thirdheadAmount'+ediTId).val(),
     			"headname": $('#thirdheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "thirdeditHead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==" "){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editthirdhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deletethirdhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#thirdheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});
	
	$(document).on('click' , '.EditfourthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(14, length);
		if($(this).text()=='Edit'){
			$("#fourthheadAmount"+ediTId).prop("disabled",false);
		    $("#deletefourthhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editfourthhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#fourthheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editfourthhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#fourthheadAmount'+ediTId).val(),
     			"headname": $('#fourthheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "fourtheditHead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==" "){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editfourthhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deletefourthhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#fourthheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});
	
	$(document).on('click' , '.EditfifthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(13, length);
		if($(this).text()=='Edit'){
			$("#fifthheadAmount"+ediTId).prop("disabled",false);
		    $("#deletefifthhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editfifthhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#fifthheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editfifthhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#fifthheadAmount'+ediTId).val(),
     			"headname": $('#fifthheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "fiftheditHead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==" "){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editfifthhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deletefifthhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#fifthheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});
	
	
	
	$(document).on('click' , '.EditsixthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(13, length);
		if($(this).text()=='Edit'){
			$("#sixthheadAmount"+ediTId).prop("disabled",false);
		    $("#deletesixthhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editsixthhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#sixthheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editsixthhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#sixthheadAmount'+ediTId).val(),
     			"headname": $('#sixthheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "sixtheditHead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==" "){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editsixthhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deletesixthhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#sixthheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});
	
	
	$(document).on('click' , '.EditseventhHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(15, length);
		if($(this).text()=='Edit'){
			$("#seventhheadAmount"+ediTId).prop("disabled",false);
		    $("#deleteseventhhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editseventhhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#seventhheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editseventhhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#seventhheadAmount'+ediTId).val(),
     			"headname": $('#seventhheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "seventheditHead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==" "){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editseventhhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deleteseventhhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#seventhheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});
	
	
	$(document).on('click' , '.EditeighthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(14, length);
		if($(this).text()=='Edit'){
			$("#eighthheadAmount"+ediTId).prop("disabled",false);
		    $("#deleteeighthhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editeighthhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#seventhheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editeighthhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#eighthheadAmount'+ediTId).val(),
     			"headname": $('#eighthheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "eightheditHead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==" "){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editeighthhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deleteeighthhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#eighthheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});
	
	$(document).on('click' , '.EditninthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(13, length);
		if($(this).text()=='Edit'){
			$("#ninthheadAmount"+ediTId).prop("disabled",false);
		    $("#deleteninthhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editninthhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#ninthheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editninthhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#ninthheadAmount'+ediTId).val(),
     			"headname": $('#ninthheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "nintheditHead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==" "){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editninthhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deleteninthhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#ninthheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});
	
	$(document).on('click' , '.EdittengthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(14, length);
		if($(this).text()=='Edit'){
			$("#tengthheadAmount"+ediTId).prop("disabled",false);
		    $("#deletetengthhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#edittengthhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#tengthheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#edittengthhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#tengthheadAmount'+ediTId).val(),
     			"headname": $('#tengthheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "tengtheditHead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==" "){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#edittengthhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deletetengthhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#tengthheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});
	
	
	
	$(document).on('click' , '.EditEleventhHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(17, length);
		if($(this).text()=='Edit'){
			$("#eleventhheadAmount"+ediTId).prop("disabled",false);
		    $("#deleteelevengthhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editelevengthhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#eleventhheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editelevengthhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#eleventhheadAmount'+ediTId).val(),
     			"headname": $('#eleventhheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "eleventheditHead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==""){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editelevengthhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deleteelevengthhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#eleventhheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});
	
	
	$(document).on('click' , '.EditTwelthHead' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(14, length);
		if($(this).text()=='Edit'){
			$("#twelthheadAmount"+ediTId).prop("disabled",false);
		    $("#deletetwelthhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#edittwelthhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#twelthheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#edittwelthhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#twelthheadAmount'+ediTId).val(),
     			"headname": $('#twelthheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "tweltheditHead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==""){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#edittwelthhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deletetwelthhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#twelthheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});
	

	
	$(document).on('click' , '.Editthirdthirteen' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(15, length);
		if($(this).text()=='Edit'){
			$("#thirteenheadamount"+ediTId).prop("disabled",false);
		    $("#deletethirteenhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editthiteenhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#thirteenheadamount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editthiteenhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#thirteenheadamount'+ediTId).val(),
     			"headname": $('#thirteenheadname'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "editthiteenhead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==""){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editthiteenhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deletethirteenhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#thirteenheadtotal").val(result);
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});

	
	$(document).on('click' , '.Editthirdfourteen' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(16, length);
		if($(this).text()=='Edit'){
			$("#fourteenheadAmount"+ediTId).prop("disabled",false);
		    $("#deletefourteenhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editfourteenhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#fourteenheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editfourteenhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#fourteenheadAmount'+ediTId).val(),
     			"headname": $('#fourteenheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "editfourteenhead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==""){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editfourteenhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deletefourteenhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#fourteenheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});

	
	$(document).on('click' , '.Editthirdfifteen' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(15, length);
		if($(this).text()=='Edit'){
			$("#fifteenheadAmount"+ediTId).prop("disabled",false);
		    $("#deletefifteenhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editfifteenhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#fifteenheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editfifteenhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#fifteenheadAmount'+ediTId).val(),
     			"headname": $('#fifteenheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "editfifteenhead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==""){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editfifteenhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deletefifteenhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#fifteenheadtotal").val(result);
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});

	
	$(document).on('click' , '.Editthirdsixteen' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(15, length);
		if($(this).text()=='Edit'){
			$("#sixteenheadAmount"+ediTId).prop("disabled",false);
		    $("#deletesixteenhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editsixteenhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#sixteenheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editsixteenhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#sixteenheadAmount'+ediTId).val(),
     			"headname": $('#sixteenheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "editsixteenhead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==""){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editsixteenhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deletesixteenhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#sixteenheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});

	
	$(document).on('click' , '.Editthirdseventeen' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(17, length);
		if($(this).text()=='Edit'){
			$("#sevnteenheadAmount"+ediTId).prop("disabled",false);
		    $("#deleteseventeenhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editseventeenhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#sevnteenheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editseventeenhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#sevnteenheadAmount'+ediTId).val(),
     			"headname": $('#sevnteenheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "editseventeenhead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==""){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editseventeenhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deleteseventeenhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#sevnteenheadtotal").val(result);
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});

	
	$(document).on('click' , '.Editthirdeighteen' , function(){
		var EditId= $(this).prop('id');
		var length =EditId.length;
		var ediTId= EditId.substring(16, length);
		if($(this).text()=='Edit'){
			$("#eighteenheadAmount"+ediTId).prop("disabled",false);
		    $("#deleteeighteenhead"+ediTId).css({'pointer-events': 'none', 'opacity' :'0.3'});
		    var saveButton = $('#editeighteenhead'+ediTId).css('pointer-events', '');
    		saveButton.empty().append("Save");
    	}else{
    		var length =EditId.length;
    	    $("#eighteenheadAmount"+ediTId).prop("disabled",true);
    		var saveButton = $('#editeighteenhead'+ediTId).css('pointer-events', 'none');
    		saveButton.empty().append("<i class='fa fa-spinner fa-spin'></i>");
    		var formDetailsJson={};
     	    formDetailsJson={
     			"Id":EditId,
     			"headAmount": $('#eighteenheadAmount'+ediTId).val(),
     			"headname": $('#eighteenheadName'+ediTId).html()
     	    }
    		
    		 $.ajax({
    	    	    type: "POST",
    	    	    url: "editeighteenhead", //BookInfoyour valid url
    	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
    	    	    processData: false,
    	    	    data: JSON.stringify(formDetailsJson), //json object or array of json objects
    	    	    success: function(result) {
    	    	    	 if(result==""){
    	    	    		 alert("Unable To Save Head Details At This Time");
    	    	    	 }else{
    	    	    		 var saveButton = $('#editeighteenhead'+ediTId).css('pointer-events', '');
    	    	     		 saveButton.empty().append("Edit");
    	    	     		 $("#deleteeighteenhead"+ediTId).css({'pointer-events': '', 'opacity' :''});
    	    	     		 alert("Head Amount Saved Successfully");
    	    	     		 $("#eighteenheadtotal").val(result);
    	    	     		
    	    	    	} 
    	    	    },
    	    	    error: function(result){
    	    	    	alert("Unable To Save Head Details At This Time");
    	    	    }
    	   })
    	}
	});

	
	
	
	
	
	
	
	
	
	
	
	$("#searchCategory").change(function(){	
	 var muultiselect=$('#searchCategory').val();
	 var subjectList = '${config}';
	 $("#21stscience").hide();
	 $("#22ndscience").hide();
	 $("#22ndarts").hide();
	 $("#21starts").hide();
	 $("#21stcommerce").hide();
	 $("#22ndcommerce").hide();
	 
	 $("#31stscience").hide();
	 $("#32ndscience").hide();
	 $("#33ndscience").hide();
	 $("#32ndarts").hide();
	 $("#31starts").hide();
	 $("#33ndarts").hide();
	 $("#31stcommerce").hide();
	 $("#32ndcommerce").hide();
	 $("#33ndcommerce").hide();
	 if(muultiselect!=null){
         if(subjectList=='+2'){
        	 for (var i=0; i<muultiselect.length; i++) {
    			 switch(muultiselect[i]) {
    			    case 'firstTable':
    			    	$("#21stscience").show();
    			        break;
    			    case 'secondTable':
    			    	$("#22ndscience").show();
    			        break;
    			    case 'thirdTable':
    			    	$("#21starts").show();
    			        break;
    			    case 'fourthTable':
    			    	$("#22ndarts").show();
    			        break;
    			    case 'fifthTable':
    			    	 $("#21stcommerce").show();
    			        break;
    			    case 'sixthTable':
    			    	$("#22ndcommerce").show();
    			        break;    
    			}
    		 }		 
         }else if(subjectList=='UG'){
        	 for (var i=0; i<muultiselect.length; i++) {
    			 switch(muultiselect[i]) {
    			    case 'firstTable':
    			    	$("#31stscience").show();  
    			        break;
    			    case 'secondTable':
    			    	$("#32ndscience").show();
    			        break;
    			    case 'seventhTable':
    			    	$("#33ndscience").show();
    			        break;    
    			    case 'thirdTable':
    			    	$("#31starts").show();
    			        break;
    			    case 'fourthTable':
    			    	$("#32ndarts").show();
    			        break;
    			    case 'eigthTable':
    			    	$("#33ndarts").show();
    			        break;    
    			    case 'fifthTable':
    			    	 $("#31stcommerce").show();
    			        break;
    			    case 'sixthTable':
    			    	$("#32ndcommerce").show();
    			        break;
    			    case 'ninthTable':
    			    	$("#33ndcommerce").show();
    			        break; 
    			}
    		 }
         }else{
        	 
         }
		
	 }else{
	 }
 	});
	var allCategory=true;
	$('#selectAllCategory').change(function(){	
		if($(this).is(':checked')){
			allCategory=true;
		}else{
			allCategory=false;
		}
		$("#addHeadCategory").prop("disabled", $(this).is(':checked'));
	});
	$('#checkboxforsearch').change(function(){	
		 var subjectList = '${config}';			 
		 if(subjectList!='+2' && subjectList!='UG'){
			 alert("Please Select A Appropriate Course And Search The Head List");
			 return;
		 }
	if(subjectList=='+2'){	 
		if($(this).is(':checked')){
			<c:forEach items="${applicationConfig}" var="applicationConfig">
			<c:if test="${applicationConfig eq 'Science'}">
			     $("#21stscience").show();
			     $("#22ndscience").show();
			</c:if>
			<c:if test="${applicationConfig eq 'Arts'}">
			    $("#22ndarts").show();
			    $("#21starts").show();
		    </c:if>
		    <c:if test="${applicationConfig eq 'Commerce'}">
		        $("#21stcommerce").show();
			    $("#22ndcommerce").show();
		    </c:if>
			</c:forEach>
		}else{
			 $("#21stscience").hide();
			 $("#22ndscience").hide();
			 $("#22ndarts").hide();
			 $("#21starts").hide();
			 $("#21stcommerce").hide();
			 $("#22ndcommerce").hide();
		}
	}else if(subjectList=='UG'){
		if($(this).is(':checked')){
			<c:forEach items="${applicationConfig}" var="applicationConfig">
			<c:if test="${applicationConfig eq 'Science'}">
			     $("#31stscience").show();
			     $("#32ndscience").show();
			     $("#33ndscience").show();
			</c:if>
			<c:if test="${applicationConfig eq 'Arts'}">
			    $("#32ndarts").show();
			    $("#31starts").show();
			    $("#33ndarts").show();
		    </c:if>
		    <c:if test="${applicationConfig eq 'Commerce'}">
		        $("#31stcommerce").show();
			    $("#32ndcommerce").show();
			    $("#33ndcommerce").show();
		    </c:if>
			</c:forEach>
		}else{
			 $("#31stscience").hide();
			 $("#32ndscience").hide();
			 $("#33ndscience").hide();
			 $("#32ndarts").hide();
			 $("#31starts").hide();
			 $("#33ndarts").hide();
			 $("#31stcommerce").hide();
			 $("#32ndcommerce").hide();
			 $("#33ndcommerce").hide();
		}
	}else{
		
	}
		$("#searchCategory").prop("disabled", $(this).is(':checked'));
	});
	
	
	$('#searchbySession').change(function(){	
		 var subjectList = '${config}';			 
		 if(subjectList!='+2' && subjectList!='UG'){
			 alert("Please Select A Appropriate Course And Search The Head List");
			 return;
		 }
		var sessionDropdown=$('#searchbySession').val();
		 if(sessionDropdown!=''){
			 $.ajax({
 	    	    type: "POST",
 	    	    url: "searchBySession", //BookInfoyour valid url
 	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
 	    	    processData: false,
 	    	    data: JSON.stringify(sessionDropdown), //json object or array of json objects
 	    	    success: function(result) {
 	    	    	 if(result=="ERROR"){
 	    	    		 alert("Unable To Get Session By Head Details..Please try later");
 	    	    	 }else{
 	    	    		window.location.href = "showManageHead"; 
 	    	    	} 
 	    	    },
 	    	    error: function(result){
    	    		 alert("Unable To Get Session By Head Details..Please try later");
 	    	    }
 	        })
		 }else{
		    alert("Please Select a session to search the head details");	 
		 }
	});
	
	
	
	
/* 	$('#inputheadname').change(function(){	
		var headName=$('#inputheadname').val();	
		if(headName!=''){
			$.ajax({
	    	    type: "POST",
	    	    url: "getHeadAmount", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify(headName), //json object or array of json objects
	    	    success: function(result) {
                     alert(result); 	    	    	
	    	    	 if(result==null){
	    	    		 $('#inputamount').html('0');
	    	    	 }else{
	    	    		 $('#inputamount').val(result);
	    	    	} 
	    	    },
	    	    error: function(result){
	    	    	$('#inputamount').html('0');
	    	         }
	      });
		}
	}); */

	/*  $("#saveHead").click(function(){	 */
		/* var headName=$('#inputheadname').val();
		var headAmount=$('#inputamount').val();
		var headCategory='';
		if(!allCategory){
			var headCategory=$('#addHeadCategory').val();
		 } */
		 
		 $('#saveHead').click(function(){
			 var subjectList = '${config}';	
			 if(subjectList!='+2' && subjectList!='UG' ){
				 alert("Please Select A Appropriate Course And Do Save the Head");
				 return;
			 }
			 
			 
			 var selectVal=$('#inputheadname').val();
			 var headAmount=$('#inputamount').val();
			 var sessionDropdown=$('#sessiondropDown').val();
			 if(sessionDropdown==''){
				 alert("Please Select At least One Session Before Save")
				 return;
			 }
			 if(selectVal==''){
				 alert("Please Select At least One Head Before Save")
				 return;
			 }
		     if(headAmount=='' || headAmount==null){
		    	 alert("Head Amount Must Not Be Empty")
				 return;
		     }
		 
		     if(allCategory==false){
		    	 var headCategory=$('#addHeadCategory').val();
		    	 if(headCategory==null){
		    		 alert("Please Select At least One Category Before Save");
		    		 return
		    	 }
		     }
			 
			 
		  	    var form = document.getElementById("addNewhead")
		  	    form.action = "saveHead";
		  	    form.submit();
		  	});
	






});

//Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
</script>
<script>
  $(function () {
   //Initialize Select2 Elements
    $(".select2").select2();

    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

</body>
</html>
