<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daily Collection Register | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  
  
  
  
  
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/font-awesome.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/ionicons.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/custom.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/select2.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/skin-blue.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/blue.css"/>"/>
  <link rel="stylesheet" href="<c:url value="/css/common/morris.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/jquery-jvectormap-1.2.2.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/datepicker3.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/daterangepicker.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap3-wysihtml5.min.css"/>" />
  
  
  
  
  
  <spring:url value="/img" var="images" />
  
  
  
  
  <style type="text/css">
      #loading {
   width: 100%;
   height: 100%;
   top: 0;
   left: 0;
   position: fixed;
   display: block;
   opacity: 0.7;
   background-color: #fff;
   z-index: 99;
   text-align: center;
}

#loading-image {
  position: absolute;
  top: 174px;
  left: 532px;
  z-index: 100;
}  
    
  
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <jsp:include page="/WEB-INF/jsp/views/common/header.jsp"></jsp:include>
 <jsp:include page="/WEB-INF/jsp/views/common/menu.jsp"></jsp:include>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="#" data-toggle="modal" data-target="#courseModal" id="dashBoardValue"></a> Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><span id="newAdm">00</span></h3>

              <p>New admission</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><span id="reAdm">00</span></h3>

              <p>ReAdmission</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>00</h3>

              <p>Expense this month</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><span id="canAdm">00</span></h3>

              <p>Admission Cancelled</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-left">
              
              <li class="pull-left header"><i class="fa fa-inbox"></i>Collection Summary</li>
            </ul>
            <!-- <div class="pull-right box-tools">
                <button type="button" class="btn btn-sm daterange pull-right"  title="Date range">
                  <i class="fa fa-calendar"></i>
                </button>
                
              </div> -->
            <div class="tab-content no-padding">
			
             <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sl no</th>
                  <th>Stream</th>
                  <th>No of Student</th>
                   <th></th>
                  <th></th>
                  <th>Total</th>
                  <th>Details</th>
				  
				  </tr>
				  </thead>
				  <tbody id="rowId">
				  </tbody>
				   <tfoot>
                <tr>
                 <th colspan="2">Total</th>
                  
                  <th colspan="3" id="noOfStudentCount"></th>
                 
                  <th id="TotalAmt"></th>
                  <th></th>
				  </tr>
                   <tr>
                 <th colspan="2">CAF Fee</th>
                  
                  <th colspan="3">0 (No of Student)</th>
                  
                  <th>0</th>
                  <th></th>
				  </tr>
                   <tr>
                 <th colspan="2">Miscellaneous</th>
                  
                  <th colspan="3">0 (No of Particular)</th>
                  
                  <th>0</th>
                  <th></th>
				  </tr>
                  <tr>
                 <th colspan="5">Grand Total Collection</th>
                  
                  
                  
                  <th id="grandTotal"></th>
                  <th></th>
				  </tr>
				  </tfoot>
				  </table>
              
            </div>
          </div>
          <!-- /.nav-tabs-custom -->
			<!-- TO DO List -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">To Do List</h3>

              <div class="box-tools pull-right">
                <ul class="pagination pagination-sm inline">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="todo-list">
                <li>
                  <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <!-- checkbox -->
                  <input type="checkbox" value="">
                  <!-- todo text -->
                  <span class="text">Design a nice theme</span>
                  <!-- Emphasis label -->
                  <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                  <!-- General tools such as edit or delete-->
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Make the theme responsive</span>
                  <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Let theme shine like a star</span>
                  <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Let theme shine like a star</span>
                  <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Check your messages and notifications</span>
                  <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Let theme shine like a star</span>
                  <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                
                 <li id="itemAdd" style="display: none;">
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text" id="actualtext"></span>
                  <small class="label label-default"><i class="fa fa-clock-o"></i> Few Seconds</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                
                
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#todoModal" ><i class="fa fa-plus"></i> Add item</button>
            </div>
          </div>
          <!-- /.box -->

          

          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

         

          

          <!-- Calendar -->
          <div class="box box-solid bg-green-gradient">
            <div class="box-header">
              <i class="fa fa-calendar"></i>

              <h3 class="box-title">Calendar</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- button with a dropdown -->
                
                <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div id="calendar" style="width: 100%"></div>
            </div>
			<!-- /.box-body -->
            <div class="box-footer text-black">
              <div class="row">
                <div class="col-sm-6">
                  <!-- Progress bars -->
                  <div class="clearfix">
                    <span class="pull-left">Labour Day</span>
                    <small class="pull-right">1 May</small>
                  </div>
                  

                  <div class="clearfix">
                    <span class="pull-left">Holiday</span>
                    <small class="pull-right">15 may</small>
                  </div>

                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                  <div class="clearfix">
                    <span class="pull-left">Holiday</span>
                    <small class="pull-right">18 May</small>
                  </div>


                  <div class="clearfix">
                    <span class="pull-left">Savitree Brata</span>
                    <small class="pull-right">25 May</small>
                  </div>
                  
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.box -->
		  <!-- Notification LIST -->
			<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Notifications</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                <li class="item">
                  <div class="product-img">
                    <img src="${images}/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">Collect fee
                      <small class="label label-default pull-right"><i class="fa fa-clock-o"></i> 1 month</small></a>
                        <span class="product-description">
                          Collect exam fee from 1st yr Arts Student.
                        </span>
                  </div>
                </li>
                <!-- /.item -->
                <li class="item">
                  <div class="product-img">
                    <img src="${images}/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">Declare holiday
                      <small class="label label-default pull-right"><i class="fa fa-clock-o"></i> 10 days</small></a>
                        <span class="product-description">
                          Declare holiday on the occasion of savitree puja;
                        </span>
                  </div>
                </li>
                <!-- /.item -->
                <li class="item">
                  <div class="product-img">
                    <img src="${images}/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">Collect courier <small class="label label-default pull-right"><i class="fa fa-clock-o"></i> 1 days</small></a>
                        <span class="product-description">
                          Collect courier from DTDC.
                        </span>
                  </div>
                </li>
                <!-- /.item -->
                <li class="item">
                  <div class="product-img">
                    <img src="${images}/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">Leave approved
                      <small class="label label-default pull-right"><i class="fa fa-clock-o"></i> 1 days</small></a>
                        <span class="product-description">
                          Your leave is approved
                        </span>
                  </div>
                </li>
                <!-- /.item -->
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="javascript:void(0)" class="uppercase">View All Notifications</a>
            </div>
            <!-- /.box-footer -->
          </div>
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
	<jsp:include page="/WEB-INF/jsp/views/common/footer.jsp"></jsp:include>
  </div>
  <!-- /.content-wrapper -->
  





<!-- ./wrapper -->
<div class="modal fade" id="myhelpModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Help</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--config popup-->

<!--Course popup-->
<div class="modal fade" id="courseModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title">Select Course</h4>
      </div>
      <div class="modal-body">
        <div class="row">
		<div class="col-sm-12">
		 <select class="form-control" style="width: 100%;" id="dashBoardConfgId">
                 <!--  <option selected="selected">+2</option>
                  <option>UG</option> 
				  <option>PG</option>  -->	
				  <c:forEach var="dashBoardConfig" items="${dashBoardConfig}">
				             <option value="${dashBoardConfig}">${dashBoardConfig}</option>
				  </c:forEach>			  
         </select>
		</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="configPopupId">OK</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<div class="modal fade" id="todoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">TODO</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
                  <label for="inputtodo" class="col-sm-2 control-label">TODO</label>

                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputtodo" placeholder="Todo" id="todoInput">
                  </div>
				  <div class="clearfix"></div>
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-default" id="todosubmit">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="loading" style="display: none;">
  <img id="loading-image" src="${images}/loading.gif" alt="Loading..." />
</div>


<%--     <spring:url value="/js/jquery-2.2.3.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script> --%>
    
    <spring:url value="/js/jquery-ui.min.js" var="jqueryuiminjs"></spring:url>
    <script src="${jqueryuiminjs}"></script>
    
    <script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
    
    <spring:url value="/js/bootstrap.min.js" var="bootstrapminjs"></spring:url>
    <script src="${bootstrapminjs}"></script>
    
    <spring:url value="/js/select2.full.min.js" var="selectminjs"></spring:url>
    <script src="${selectminjs}"></script>
    
  <%--   <spring:url value="/js/icheck.min.js" var="icheckminjs"></spring:url>
    <script src="${icheckminjs}"></script> --%>
    
    <spring:url value="/js/raphael-min.js" var="raphaelminjs"></spring:url>
    <script src="${raphaelminjs}"></script>
    
    <%-- <spring:url value="/js/morris.min.js" var="morrisminjs"></spring:url>
    <script src="${morrisminjs}"></script>  --%>
    
    <spring:url value="/js/jquery.sparkline.min.js" var="jquerysparklineminjs"></spring:url>
    <script src="${jquerysparklineminjs}"></script>
    
    <spring:url value="/js/jquery-jvectormap-1.2.2.min.js" var="jqueryjvectormap122minjs"></spring:url>
    <script src="${jqueryjvectormap122minjs}"></script>
    
    <spring:url value="/js/jquery-jvectormap-world-mill-en.js" var="jqueryjvectormapworldmillenjs"></spring:url>
    <script src="${jqueryjvectormapworldmillenjs}"></script>
    
    <spring:url value="/js/jquery.knob.js" var="jqueryknobjs"></spring:url>
    <script src="${jqueryknobjs}"></script>
    
     <spring:url value="/js/moment.min.js" var="momentminjs"></spring:url>
    <script src="${momentminjs}"></script>
    
     <spring:url value="/js/daterangepicker.js" var="daterangepickerjs"></spring:url>
    <script src="${daterangepickerjs}"></script>
    
    
    <spring:url value="/js/bootstrap-datepicker.js" var="bootstrapdatepickerjs"></spring:url>
    <script src="${bootstrapdatepickerjs}"></script>
    
    <spring:url value="/js/bootstrap3-wysihtml5.all.min.js" var="bootstrap3wysihtml5allminjs"></spring:url>
    <script src="${bootstrap3wysihtml5allminjs}"></script>
    
    <spring:url value="/js/jquery.slimscroll.min.js" var="jqueryslimscrollminjs"></spring:url>
    <script src="${jqueryslimscrollminjs}"></script>
    
    
     <spring:url value="/js/fastclick.js" var="fastclickjs"></spring:url>
    <script src="${fastclickjs}"></script>
    
     <spring:url value="/js/app.min.js" var="appminjs"></spring:url>
    <script src="${appminjs}"></script>
    
     <spring:url value="/js/dashboard.js" var="dashboardjs"></spring:url>
    <script src="${dashboardjs}"></script>  
    
     <spring:url value="/js/demo.js" var="demojs"></spring:url>
    <script src="${demojs}"></script>

<script>
 $(window).load(function(){ 
	 $('#loading').hide();
   $(".select2").select2(); 
   $('#courseModal').modal('show');
   
    }); 
     
 </script>


 <script type="text/javascript">
    $(document).ready(function() {
    	 var status=true;   
    	$('#dashBoardValue').html($('#dashBoardConfgId').val());
    	 $.ajax({
	    	    type: "POST",
	    	    url: "dashBoardConfig", //BookInfoyour valid url
	    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
	    	    processData: false,
	    	    data: JSON.stringify($('#dashBoardConfgId').val()), //json object or array of json objects
	    	    success: function(result) {
	    	    	 if(result=='SUCCESS'){
	    	    		 status=true;
	    	    	 }else{
	    	    		alert("Error While Configuration");
	    	    	 }
	    	    	 
	    	    },
	    	    error: function(result){
	    	    	alert("Unable To Save Student At This Time");
	    	    }
    	 });   
    	  $(document).on('click' , '#configPopupId' , function(){
    		  if(status==true){
    			  alert("Configuration Done....");
    			  $("#configModal").modal('hide');
  	    		  $('#dashBoardValue').html($('#dashBoardConfgId').val());
  	    		  $('#loading').show(); 
  	    		  var htmlId='';
  	    		  var i=0;
  	    		  var studentCount=0;
  	    		  var totalAmt=0;
  	         	$.ajax({
 		    	    type: "GET",
 		    	    url: "getDashBoardData", //BookInfoyour valid url
 		    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
 		    	    processData: false,
 		    	    success: function(result) {
 		    	    	 $("#newAdm").html(result.admissionCount);
 		    	    	 $("#reAdm").html(result.reAdmissionCount);
 		    	    	 $("#canAdm").html(result.cancelAdmCount);
 		    	    	 var obj = jQuery.parseJSON(result.collectionList);
 		    	    	$( "#rowId" ).empty();
 		    	    	$("#TotalAmt").empty();
 		    	    	$("#noOfStudentCount").empty();
 		    	    	$("#grandTotal").empty();
 		    	    	 $.each(obj, function(key,value) {
 		    	    		studentCount +=parseInt(value.studentCount);
 		    	    		totalAmt +=parseInt(value.totalAmount);
 		    	    		htmlId +='<tr><td>'+ ++i +'</td><td>'+value.streamName+'</td><td>'+value.studentCount+'</td><td></td><td></td><td>'+value.totalAmount+'</td><td><a href="#">Details</a></td></tr>'; 
 		    	    	 });
 		    	    	
 		    	    	$("#TotalAmt").append(totalAmt);
 		    	    	$("#noOfStudentCount").append(studentCount);
 		    	    	$("#rowId").append(htmlId);
 		    	    	$("#grandTotal").append(totalAmt);
 		    	    	$('#loading').hide();     	
 		    	    },
 		    	    error: function(result){
 		    	    	
 		    	    	alert("Unable To Get DashBoard Details At This Time");
 		    	    	$('#loading').hide(); 
 		    	    }
 		    	     });
  	         	
  	    		  
  	    		  
    		  }
    		  
      	  }); 
    	 
    	 $("#dashBoardConfgId").change(function(){	
    		 $.ajax({
		    	    type: "POST",
		    	    url: "dashBoardConfig", //BookInfoyour valid url
		    	    contentType: 'application/json;charset=UTF-8', //this is required for spring 3 - ajax to work (at least for me)
		    	    processData: false,
		    	    data: JSON.stringify($('#dashBoardConfgId').val()), //json object or array of json objects
		    	    success: function(result) {
		    	    	 if(result=='SUCCESS'){
		    	    		 status=true;
		    	    	 }else{
		    	    		alert("Error While Configuration");
		    	    	 }
		    	    	 
		    	    },
		    	    error: function(result){
		    	    	alert("Unable To Save Student At This Time");
		    	    }
		    	     });
      	});
    	 
  	
  	
  /* 	function popupClose(){
  		alert("aaa");
  	}
  	 */
    
    });
    
    </script>
    
    
    
    


</body>
</html>
