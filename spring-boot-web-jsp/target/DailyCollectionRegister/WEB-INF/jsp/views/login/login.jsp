<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daily Collection Register | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <link rel="stylesheet" href="<c:url value="/css/common/bootstrap.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/font-awesome.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/common/ionicons.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/custom.min.css"/>" />
  <link rel="stylesheet" href="<c:url value="/css/customize/blue.css"/>" />
  
    <spring:url value="/img" var="images" />
    
    <script type="text/javascript">
    $(document).ready(function() {
    $('#loginSubmit').click(function(){
  	    var form = document.getElementById("loginid")
  	    form.action = "login";
  	    form.submit();
  	});
    });
    
    </script>
    
    
</head>
<body class="hold-transition" background="${images}/login-bg.jpg">
<div class="login-box">
  <div class="login-logo">
    <a href="index.html"><b>Daily Collection</b>Register</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <p style="color: red">${invalid}</p>
    <p style="color: red">${sessionTimeout}</p>
    <form:form id="loginid" method="post" action="login" commandName="loginBean">
      <div class="form-group has-feedback">
        <form:input id="userName" type="text" path="userName" class="form-control" name="userName" placeholder="Email"/>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <form:input id="password" type="password" path="password" class="form-control" name="password" placeholder="Password" />
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
	 <%--  <div class="social-auth-links">
      <img src="${images}/captcha.jpg" />
    </div> --%>
      <div class="row">
        <!-- <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div> -->
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat"  id="loginSubmit">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form:form>

    
    <!-- /.social-auth-links -->

    <a href="#">I forgot my password</a><br>
    <a href="registration" class="text-center">Register a new membership</a>

  </div>
 
<div class="login-box-body">
 <div class="social-auth-links text-center">
      <img src="${images}/Clogo.jpg" /> 
      <%-- <img src="${images}/RNJC.jpg" /> --%>
	  <!-- p>Kshetramohan Science College, Narendrapur is located in district of Ganjam, Odisha. It is situated in a rural setting with Gopalpur – on – Sea, the famous tourist destination of the State and Berhampur University are very nearer to this college campus.</p -->
    </div>
</div>

 <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
 <spring:url value="/js/jquery-2.2.3.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script>
 <spring:url value="/js/bootstrap.min.js" var="bootstrapminjs"></spring:url>
    <script src="${bootstrapminjs}"></script>
 <spring:url value="/js/icheck.min.js" var="icheckminjs"></spring:url>
    <script src="${icheckminjs}"></script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
