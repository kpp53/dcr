<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>E Receipt Khetramohan Science Junior College</title>
	
	  <link rel="stylesheet" href="<c:url value="/css/common/style.css"/>" />
      <link rel="stylesheet" href="<c:url value="/css/common/print.css"/>" media="print"/>
	  <spring:url value="/img" var="images" />

</head>

<body>

	<div id="page-wrap">

		
		
		<div id="identity">
		
            

            <div id="logo">
          

              
              <img id="image" src="${images}/logo.jpg" alt="logo" />
            </div>
		
		</div>
		
				<div id="customer">


            <div>
			E Receipt<br>
Khetramohan Science Junior College<br>
Narendrapur,Ganjam,Odisha<br>
www.kmsc.org.in<br>
Phone : 06811 259524
			</div>
		
		</div>
		
		<table id="items">
		<tr>
		      <th class="">Receipt No :${printstudent.receiptNo} </th>
		      <th class="sname">Name - ${printstudent.name}</th>
		      <th>${printstudent.course}/ ${printstudent.stream}/ ${printstudent.yearBatch}</th>
		      
		  </tr>
		  <tr>
		      <th>Date - ${printstudent.created_Date}</th>
		      <th>Roll No - ${printstudent.rollNo}</th>
		      <th>Session  ${printstudent.session}</th>
		      
		  </tr>
		  <tr>
		  <td colspan="3" class="nopadding">
		  <table class="inner">
		  <thead>
		  <tr>
		      <th class="slno">SL No.</th>
		      <th class="headname">Head Description</th>
			  <th class="times">Times</th>
		      <th>Amount</th>
		  </tr>
		  </thead>
		  <tbody>
		  <c:if test="${headDetailsForprint ne null}">
		    <c:set var="total" value="${0}"/>
		    <c:forEach var="headDetailsList" items="${headDetailsForprint}" varStatus="loop">
		    <c:set var="total" value="${total + headDetailsList.headAmount}" />
		          <tr>
		               <td>${loop.index+1}</td>
				       <td>${headDetailsList.headname}</td>
					   <td>1</td>
				       <td>${headDetailsList.headAmount}</td>
		         </tr>
		        
		    
		    </c:forEach>
		  </c:if>
		  
		  </tbody>
		  <tfoot>
		  <tr>
		  <th class="slno"></th>
		      <th class="headname">In Word - ${amntinwrds} rupess only</th>
			  <th class="times">Total</th>
		      <th>Rs ${total}</th>
		  </tr>
		  </tfoot>
		  </table>
		  </td>
		      
		      
		  </tr>
		
		
		</table>
		
		<div id="terms">
		  
		  <textarea>This Is Computer Generated Bill And Valid With Seal & Signature Of Authorization Person.</textarea>
		</div>
	<div class="sign">Accountant/DAO</div>
	</div>
	<spring:url value="/js/jquery-1.3.2.min.js" var="jqueryminjs"></spring:url>
    <script src="${jqueryminjs}"></script>
    <spring:url value="/js/example.js" var="examplejs"></spring:url>
    <script src="${examplejs}"></script>
</body>

</html>